
jQuery(document).ready(function() {
	"use strict";
	jQuery('.toggle').click(function() {
	if (jQuery('.submenu').is(":hidden"))
	{
	jQuery('.submenu').slideDown("fast");
	} else {
	jQuery('.submenu').slideUp("fast");
	}
	return false;
	});

/*Phone Menu*/
jQuery(".topnav").accordion({
	accordion:false,
	speed: 300,
	closedSign: '+',
	openedSign: '-'
	});
	
	jQuery("#nav > li").hover(function() {
	var el = jQuery(this).find(".level0-wrapper");
	el.hide();
	el.css("left", "0");
	el.stop(true, true).delay(150).fadeIn(300, "easeOutCubic");
	}, function() {
	jQuery(this).find(".level0-wrapper").stop(true, true).delay(300).fadeOut(300, "easeInCubic");
	});	
	var scrolled = false;
	
jQuery("#nav li.level0.drop-menu").mouseover(function(){
	if(jQuery(window).width() >= 740){
	jQuery(this).children('ul.level1').fadeIn(100);
	}
	return false;
	}).mouseleave(function(){
	if(jQuery(window).width() >= 740){
jQuery(this).children('ul.level1').fadeOut(100);
	}
	return false;
	});
	jQuery("#nav li.level0.drop-menu li").mouseover(function(){
	if(jQuery(window).width() >= 740){
	jQuery(this).children('ul').css({top:0,left:"165px"});
	var offset = jQuery(this).offset();
	if(offset && (jQuery(window).width() < offset.left+325)){
	jQuery(this).children('ul').removeClass("right-sub");
	jQuery(this).children('ul').addClass("left-sub");
	jQuery(this).children('ul').css({top:0,left:"-167px"});
	} else {
	jQuery(this).children('ul').removeClass("left-sub");
	jQuery(this).children('ul').addClass("right-sub");
	}
	jQuery(this).children('ul').fadeIn(100);
	}
	}).mouseleave(function(){
	if(jQuery(window).width() >= 740){
	jQuery(this).children('ul').fadeOut(100);
	}
	});				
	
	jQuery("#best-seller-slider .slider-items").owlCarousel({
		items : 4, //10 items above 1000px browser width
		itemsDesktop : [1024,4], //5 items between 1024px and 901px
		itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
		itemsTablet: [600,2], //2 items between 600 and 0;
		itemsMobile : [320,1],
		navigation : true,
		navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
		slideSpeed : 500,
		pagination : false,
		rewindNav:false,
        transitionStyle: "linear"
});

/*jQuery("#featured-slider .slider-items").owlCarousel({
	items : 5, //10 items above 1000px browser width
	itemsDesktop : [1024,4], //5 items between 1024px and 901px
	itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
	itemsTablet: [600,2], //2 items between 600 and 0;
	itemsMobile : [320,1],
	navigation : false,
    pagination : false,
    autoPlay : false,
	/!*navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	slideSpeed : 1000,
	pagination : false,
	rewindNav: true,
    //Autoplay
    autoPlay : false,
    stopOnHover : true*!/
	});*/

    jQuery("#featured-slider .slider-items").liMarquee({
        // left | right | up | down
        direction: 'left',

		// How many times to loop through the html content
		// -1 = endless loop
        loop: -1,

		// The delay in milliseconds
        scrolldelay: 0,

		// <a href="http://www.jqueryscript.net/animation/">Animation</a> speed
        scrollamount: 50,

		// carousel mode
        circular: true,

		// enable draggable functionality
        drag: true,

		// scroll short text
        runshort: true,

		// pause on hover
        hoverstop: true,

		// start on hover
        inverthover: false,

		// The path to xml file
        xml: false
    });

    jQuery("#flyoff-slider .slider-items").owlCarousel({
        items : 5, //10 items above 1000px browser width
        itemsDesktop : [1024,4], //5 items between 1024px and 901px
        itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
        itemsTablet: [600,2], //2 items between 600 and 0;
        itemsMobile : [320,1],
        navigation : true,
        navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
        slideSpeed : 1000,
        pagination : false,
        rewindNav: true,
        //Autoplay
        autoPlay : 10000,
        stopOnHover : true,
        transitionStyle: "linear"
    });

    jQuery("#whatnew-slider .slider-items").owlCarousel({
        items : 5, //10 items above 1000px browser width
        itemsDesktop : [1024,4], //5 items between 1024px and 901px
        itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
        itemsTablet: [600,2], //2 items between 600 and 0;
        itemsMobile : [320,1],
        navigation : true,
        navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
        slideSpeed : 1000,
        pagination : false,
        rewindNav: true,
        transitionStyle: "linear"
        //Autoplay
       /* autoPlay : 1000,
        stopOnHover : true*/
    });

    jQuery("#wakeup-slider .slider-items").owlCarousel({
        items : 5, //10 items above 1000px browser width
        itemsDesktop : [1024,4], //5 items between 1024px and 901px
        itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
        itemsTablet: [600,2], //2 items between 600 and 0;
        itemsMobile : [320,1],
        navigation : true,
        navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
        slideSpeed : 1000,
        pagination : false,
        rewindNav: true,
        //Autoplay
        autoPlay : 10000,
        stopOnHover : true,
		transitionStyle: "linear"
    });

jQuery("#bag-slider .slider-items").owlCarousel({
	items : 6, //10 items above 1000px browser width
	itemsDesktop : [1124,4], //5 items between 1024px and 901px
	itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
	itemsTablet: [600,2], //2 items between 600 and 0;
	itemsMobile : [320,1],
	navigation : true,
	navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	slideSpeed : 500,
	pagination : false,
    rewindNav: true,
    //Autoplay
    autoPlay : 3000,
    stopOnHover : true,
    transitionStyle: "linear"
	});
jQuery("#shoes-slider .slider-items").owlCarousel({
	items : 3, //10 items above 1000px browser width
	itemsDesktop : [1024,4], //5 items between 1024px and 901px
	itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
	itemsTablet: [600,2], //2 items between 600 and 0;
	itemsMobile : [320,1],
	navigation : true,
	navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	slideSpeed : 500,
    pagination : false,
    rewindNav:false,
    transitionStyle: "linear"
	});
jQuery("#recommend-slider .slider-items").owlCarousel({
	items : 6, //10 items above 1000px browser width
	itemsDesktop : [1024,4], //5 items between 1024px and 901px
	itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
	itemsTablet: [600,2], //2 items between 600 and 0;
	itemsMobile : [320,1],
	navigation : true,
	navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	slideSpeed : 500,
    pagination : false,
    rewindNav:false,
    transitionStyle: "linear"
	});
jQuery("#brand-logo-slider .slider-items").owlCarousel({
	autoplay : true,
	items : 6, //10 items above 1000px browser width
	itemsDesktop : [1024,4], //5 items between 1024px and 901px
	itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
	itemsTablet: [600,2], //2 items between 600 and 0;
	itemsMobile : [320,1],
	navigation : true,
	navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	slideSpeed : 500,
    pagination : false,
    rewindNav:false,
    transitionStyle: "linear"
	});
jQuery("#category-desc-slider .slider-items").owlCarousel({
	autoplay : true,
	items : 1, //10 items above 1000px browser width
	itemsDesktop : [1024,1], //5 items between 1024px and 901px
	itemsDesktopSmall : [900,1], // 3 items betweem 900px and 601px
	itemsTablet: [600,1], //2 items between 600 and 0;
	itemsMobile : [320,1],
	navigation : true,
	navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	slideSpeed : 500,
    pagination : false,
    rewindNav:false,
    transitionStyle: "linear"
	});
jQuery("#more-views-slider .slider-items").owlCarousel({
	autoplay : true,
	items : 4, //10 items above 1000px browser width
	itemsDesktop : [1024,4], //5 items between 1024px and 901px
	itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
	itemsTablet: [600,2], //2 items between 600 and 0;
	itemsMobile : [320,1],
	navigation : true,
	navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	slideSpeed : 500,
    pagination : false,
    rewindNav:false,
    transitionStyle: "linear"
	});
        
jQuery("#product-images").owlCarousel({
	items : 1, //10 items above 1000px browser width
	itemsDesktop : [1024,1], //5 items between 1024px and 901px
	itemsDesktopSmall : [900,1], // 3 items betweem 900px and 601px
	itemsTablet: [600,1], //2 items between 600 and 0;
	itemsMobile : [320,1],
	navigation : true,
	navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	slideSpeed : 500,
    pagination : true,
    rewindNav: false,
    transitionStyle: "linear"
	});
jQuery(document).ready(function() {        
        var activeBorder = jQuery("#activeBorder");
        var prec = activeBorder.children().children().text();
        if (prec > 100)
            prec = 100;
        var deg = prec*3.6;
        if (deg <= 180){
            activeBorder.css('background-image','linear-gradient(' + (90+deg) + 'deg, transparent 50%, #d4dae4 50%),linear-gradient(90deg, #d4dae4 50%, transparent 50%)');
        }
        else{
            activeBorder.css('background-image','linear-gradient(' + (deg-90) + 'deg, transparent 50%, #00acf3 50%),linear-gradient(90deg, #d4dae4 50%, transparent 50%)');
        }

        var startDeg = jQuery("#startDeg").attr("class");
        activeBorder.css('transform','rotate(' + startDeg + 'deg)');
        jQuery("#circle").css('transform','rotate(' + (-startDeg) + 'deg)');
});	
jQuery(document).ready(function() {
    jQuery("#os-phrases > h2").lettering('words').children("span").lettering().children("span").lettering(); 
})
			
jQuery("#related-products-slider .slider-items").owlCarousel({
	items : 4, //10 items above 1000px browser width
	itemsDesktop : [1024,4], //5 items between 1024px and 901px
	itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
	itemsTablet: [600,2], //2 items between 600 and 0;
	itemsMobile : [320,1],
	navigation : true,
	navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	slideSpeed : 500,
	pagination : false,
    rewindNav:false,
    transitionStyle: "linear"
	});
jQuery("#upsell-products-slider .slider-items").owlCarousel({
	items : 4, //10 items above 1000px browser width
	itemsDesktop : [1024,4], //5 items between 1024px and 901px
	itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
	itemsTablet: [600,2], //2 items between 600 and 0;
	itemsMobile : [320,1],
	navigation : true,
	navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	slideSpeed : 500,
	pagination : false,
    rewindNav: false,
    transitionStyle: "linear"
	});
	jQuery("#more-views-slider .slider-items").owlCarousel({
			autoplay : true,
			items : 3, //10 items above 1000px browser width
	    	itemsDesktop : [1024,4], //5 items between 1024px and 901px
	      	itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
	      	itemsTablet: [600,2], //2 items between 600 and 0;
	      	itemsMobile : [320,1],
	      	navigation : true,
	      	navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
	      	slideSpeed : 500,
	      	pagination : false,
        	rewindNav:false,
        	transitionStyle: "linear"
			
    	});
	jQuery("#more-views-slider1 .slider-items").owlCarousel({
        autoplay : true,
        items : 3, //10 items above 1000px browser width
        itemsDesktop : [1024,4], //5 items between 1024px and 901px
        itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
        itemsTablet: [600,2], //2 items between 600 and 0;
        itemsMobile : [320,1],
        navigation : true,
        navigationText : ["<a class=\"flex-prev\"></a>","<a class=\"flex-next\"></a>"],
        slideSpeed : 500,
        pagination : false,
        rewindNav:false,
        transitionStyle: "linear"
    });

    jQuery("#header-features .slider-items").owlCarousel({
        autoplay : true,
        items : 5, //10 items above 1000px browser width
        itemsDesktop : [1024,5], //5 items between 1024px and 901px
        itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
        itemsTablet: [600,1], //2 items between 600 and 0;
        itemsMobile : [320,1],
        navigation : false,
        slideSpeed : 500,
        pagination : false,
        rewindNav: true,
        //Autoplay
        autoPlay : 3000,
        transitionStyle: "linear"
    });
});

	var isTouchDevice = ('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0);
	jQuery(window).on("load", function() {
	
	if (isTouchDevice)
	{
	jQuery('#nav a.level-top').click(function(e) {
	jQueryt = jQuery(this);
	jQueryparent = jQueryt.parent();
	if (jQueryparent.hasClass('parent'))
	{
	if ( !jQueryt.hasClass('menu-ready'))
	{                    
		jQuery('#nav a.level-top').removeClass('menu-ready');
		jQueryt.addClass('menu-ready');
		return false;
	}
	else
	{
		jQueryt.removeClass('menu-ready');
	}
	}
	});
	}
	//on load
	jQuery().UItoTop();


}); //end: on load

//]]>

jQuery(window).scroll(function() {
if (jQuery(this).scrollTop() > 1){  
jQuery('nav').addClass("sticky");
}
else{
jQuery('nav').removeClass("sticky");
}
});

/*--------| UItoTop jQuery Plugin 1.1-------------------*/
(function(jQuery){
	jQuery.fn.UItoTop = function(options) {
	
	var defaults = {
	text: '',
	min: 200,
	inDelay:600,
	outDelay:400,
	containerID: 'toTop',
	containerHoverID: 'toTopHover',
	scrollSpeed: 1200,
	easingType: 'linear'
	};
	
	var settings = jQuery.extend(defaults, options);
	var containerIDhash = '#' + settings.containerID;
	var containerHoverIDHash = '#'+settings.containerHoverID;
	
	jQuery('body').append('<a href="#" id="'+settings.containerID+'">'+settings.text+'</a>');
	jQuery(containerIDhash).hide().click(function(){
	jQuery('html, body').animate({scrollTop:0}, settings.scrollSpeed, settings.easingType);
	jQuery('#'+settings.containerHoverID, this).stop().animate({'opacity': 0 }, settings.inDelay, settings.easingType);
	return false;
	})
	.prepend('<span id="'+settings.containerHoverID+'"></span>')
	.hover(function() {
	jQuery(containerHoverIDHash, this).stop().animate({
	'opacity': 1
	}, 600, 'linear');
	}, function() { 
	jQuery(containerHoverIDHash, this).stop().animate({
	'opacity': 0
	}, 700, 'linear');
	});
	
	jQuery(window).scroll(function() {
	var sd = jQuery(window).scrollTop();
	if(typeof document.body.style.maxHeight === "undefined") {
	jQuery(containerIDhash).css({
	'position': 'absolute',
	'top': jQuery(window).scrollTop() + jQuery(window).height() - 50
	});
	}
	if ( sd > settings.min ) 
	jQuery(containerIDhash).fadeIn(settings.inDelay);
	else 
	jQuery(containerIDhash).fadeOut(settings.Outdelay);
	});
	
	};
})(jQuery);


/*--------| End UItoTop -------------------*/

function deleteCartInCheckoutPage(){ 	
	jQuery(".checkout-cart-index a.btn-remove2,.checkout-cart-index a.btn-remove").click(function(event) {
	event.preventDefault();
	if(!confirm(confirm_content)){
	return false;
	}	
	});	
	return false;
	}
function slideEffectAjax() {
	jQuery('.top-cart-contain').mouseenter(function() {
	jQuery(this).find(".top-cart-content").stop(true, true).slideDown();
	});
	
	jQuery('.top-cart-contain').mouseleave(function() {
	jQuery(this).find(".top-cart-content").stop(true, true).slideUp();
	});
	}
function deleteCartInSidebar() {
	if(is_checkout_page>0) return false;
	jQuery('#cart-sidebar a.btn-remove, #mini_cart_block a.btn-remove').each(function(){});
	}  
	
	jQuery(document).ready(function(){
	slideEffectAjax();
	});


/*-------- End Cart js -------------------*/


jQuery.extend( jQuery.easing,
	{	
	easeInCubic: function (x, t, b, c, d) {
	return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
	return c*((t=t/d-1)*t*t + 1) + b;
	},	
	});

(function(jQuery){
	jQuery.fn.extend({
	accordion: function() {       
	return this.each(function() {

function activate(el,effect){	
	jQuery(el).siblings( panelSelector )[(effect || activationEffect)](((effect == "show")?activationEffectSpeed:false),function(){
	jQuery(el).parents().show();	
	});	
	}	
	});
	}
	}); 
})(jQuery);

jQuery(function(jQuery) {
	jQuery('.accordion').accordion();	
	jQuery('.accordion').each(function(index){
	var activeItems = jQuery(this).find('li.active');
	activeItems.each(function(i){
	jQuery(this).children('ul').css('display', 'block');
	if (i == activeItems.length - 1)
	{
	jQuery(this).addClass("current");
	}
	});
	});

});



/*-------- End Nav js -------------------*/	
/*============= Responsive Nav =============*/
(function(jQuery){
	jQuery.fn.extend({	
	accordion: function(options) {	
	var defaults = {
	accordion: 'true',
	speed: 300,
	closedSign: '[+]',
	openedSign: '[-]'
	};	
	var opts = jQuery.extend(defaults, options);	
	var jQuerythis = jQuery(this);	
	jQuerythis.find("li").each(function() {
	if(jQuery(this).find("ul").size() != 0){
	jQuery(this).find("a:first").after("<em>"+ opts.closedSign +"</em>");	
	if(jQuery(this).find("a:first").attr('href') == "#"){
	jQuery(this).find("a:first").click(function(){return false;});
	}
	}
	});	
	jQuerythis.find("li em").click(function() {
	if(jQuery(this).parent().find("ul").size() != 0){
	if(opts.accordion){
	//Do nothing when the list is open
	if(!jQuery(this).parent().find("ul").is(':visible')){
	parents = jQuery(this).parent().parents("ul");
	visible = jQuerythis.find("ul:visible");
	visible.each(function(visibleIndex){
	var close = true;
	parents.each(function(parentIndex){
	if(parents[parentIndex] == visible[visibleIndex]){
		close = false;
		return false;
	}
	});
	if(close){
	if(jQuery(this).parent().find("ul") != visible[visibleIndex]){
		jQuery(visible[visibleIndex]).slideUp(opts.speed, function(){
			jQuery(this).parent("li").find("em:first").html(opts.closedSign);
		});		
	}
	}
	});
	}
	}
	if(jQuery(this).parent().find("ul:first").is(":visible")){
	jQuery(this).parent().find("ul:first").slideUp(opts.speed, function(){
	jQuery(this).parent("li").find("em:first").delay(opts.speed).html(opts.closedSign);
	});	
	}else{
	jQuery(this).parent().find("ul:first").slideDown(opts.speed, function(){
	jQuery(this).parent("li").find("em:first").delay(opts.speed).html(opts.openedSign);
	});
	}
	}
	});
	}
	});
})(jQuery);

/*============= End Responsive Nav =============*/

(function(jQuery){
	jQuery.fn.extend({
	accordionNew: function() {       
	return this.each(function() {	
	var jQueryul			= jQuery(this),
	elementDataKey			= 'accordiated',
	activeClassName			= 'active',
	activationEffect 		= 'slideToggle',
	panelSelector			= 'ul, div',
	activationEffectSpeed 	= 'fast',
	itemSelector			= 'li';	
	if(jQueryul.data(elementDataKey))
	return false;							
	jQuery.each(jQueryul.find('ul, li>div'), function(){
	jQuery(this).data(elementDataKey, true);
	jQuery(this).hide();
	});	
	jQuery.each(jQueryul.find('em.open-close'), function(){
	jQuery(this).click(function(e){
	activate(this, activationEffect);
	return void(0);
	});	
	jQuery(this).bind('activate-node', function(){
	jQueryul.find( panelSelector ).not(jQuery(this).parents()).not(jQuery(this).siblings()).slideUp( activationEffectSpeed );
	activate(this,'slideDown');
	});
	});	
	var active = (location.hash)?jQueryul.find('a[href=' + location.hash + ']')[0]:jQueryul.find('li.current a')[0];	
	if(active){
	activate(active, false);
	}	
	function activate(el,effect){	
	jQuery(el).parent( itemSelector ).siblings().removeClass(activeClassName).children( panelSelector ).slideUp( activationEffectSpeed );	
	jQuery(el).siblings( panelSelector )[(effect || activationEffect)](((effect == "show")?activationEffectSpeed:false),function(){	
	if(jQuery(el).siblings( panelSelector ).is(':visible')){
	jQuery(el).parents( itemSelector ).not(jQueryul.parents()).addClass(activeClassName);
	} else {
	jQuery(el).parent( itemSelector ).removeClass(activeClassName);
	}	
	if(effect == 'show'){
	jQuery(el).parents( itemSelector ).not(jQueryul.parents()).addClass(activeClassName);
	}	
	jQuery(el).parents().show();	
	});	
	}	
	});
	}
	}); 
})(jQuery);


/*============= End Left Nav =============*/
/*global jQuery */
/*!	
* Lettering.JS 0.6.1
*
* Copyright 2010, Dave Rupert http://daverupert.com
* Released under the WTFPL license 
* http://sam.zoy.org/wtfpl/
*
* Thanks to Paul Irish - http://paulirish.com - for the feedback.
*
* Date: Mon Sep 20 17:14:00 2010 -0600
*/
(function(jQuery){
	function injector(t, splitter, klass, after) {
		var a = t.text().split(splitter), inject = '';
		if (a.length) {
			jQuery(a).each(function(i, item) {
				inject += '<span class="'+klass+(i+1)+'">'+item+'</span>'+after;
			});	
			t.empty().append(inject);
		}
	}
	
	var methods = {
		init : function() {

			return this.each(function() {
				injector(jQuery(this), '', 'char', '');
			});

		},

		words : function() {

			return this.each(function() {
				injector(jQuery(this), ' ', 'word', ' ');
			});

		},
		
		lines : function() {

			return this.each(function() {
				var r = "eefec303079ad17405c889e092e105b0";
				// Because it's hard to split a <br/> tag consistently across browsers,
				// (*ahem* IE *ahem*), we replaces all <br/> instances with an md5 hash 
				// (of the word "split").  If you're trying to use this plugin on that 
				// md5 hash string, it will fail because you're being ridiculous.
				injector(jQuery(this).children("br").replaceWith(r).end(), r, 'line', '');
			});

		}
	};

	jQuery.fn.lettering = function( method ) {
		// Method calling logic
		if ( method && methods[method] ) {
			return methods[ method ].apply( this, [].slice.call( arguments, 1 ));
		} else if ( method === 'letters' || ! method ) {
			return methods.init.apply( this, [].slice.call( arguments, 0 ) ); // always pass an array
		}
		jQuery.error( 'Method ' +  method + ' does not exist on jQuery.lettering' );
		return this;
	};

})(jQuery);

/*========== Left Nav ===========*/
 
jQuery(document).ready(function(){      
                   
        //increase/ decrease product qunatity buttons +/- in cart.html table
        if(jQuery('.subDropdown')[0]){
                jQuery('.subDropdown').click(function(){
                        jQuery(this).toggleClass('plus');
                        jQuery(this).toggleClass('minus');
                        jQuery(this).parent().find('ul').slideToggle();
                });
        }
        
});

/*=============End Left Nav=============*/
/*-- Menu on mobile --*/
jQuery(document).ready(function(){
    jQuery('.menu-button').click(function(){
        //jQuery('#menu-wrapper').toggleClass('open');
        jQuery('body').toggleClass('menu-closed');
    });

    /* Collapse Detail Product mobile */
    jQuery(".product-view .product-collateral .new_title2").click(function(){
        jQuery(this).next(".desc-extra").toggle();
        if(jQuery(this).attr("data-open") && jQuery(this).attr("data-open") == "true"){
            jQuery(this).attr("data-open", "false");
		}else{
            jQuery(this).attr("data-open", "true");
		}
    });
});

jQuery(document).ready(function () {
        jQuery('.bar-type-free').each(function () {
            var _this = jQuery(this),
                size = _this.data('size');
            _this.animate({
                width: size + '%'
            }, {
                duration: 2500,
                step: function (progress) {
                    _this.css({
                        backgroundColor: progressColor(progress)
                    });
                }
            });
        });
		jQuery('.bar').each(function () {
            var _this = jQuery(this),
                size = _this.data('size');
            _this.animate({
                width: size + '%'
            }, {
                duration: 2500,
                step: function (progress) {
                    _this.css({
                        backgroundColor: progressColor(progress)
                    });
                }
            });
        });
		jQuery('.bar-both').each(function () {
            var _this = jQuery(this),
                size = _this.data('size');
            _this.animate({
                width: size + '%'
            }, {
                duration: 2500,
                step: function (progress) {
                    _this.css({
                        backgroundColor: progressColor(progress)
                    });
                }
            });
        });
		function progressColor(progress) {
			if (progress >= 0 && progress <= 99) return '#00acf3';
			if (progress >= 0 && progress <= 79) return '#00acf3';
			if (progress >= 0 && progress <= 300) return '#00acf3';
		}
});