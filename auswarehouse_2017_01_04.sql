-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `adminnotification_inbox`;
CREATE TABLE `adminnotification_inbox` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Notification id',
  `severity` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Problem type',
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Create date',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `description` text COMMENT 'Description',
  `url` varchar(255) DEFAULT NULL COMMENT 'Url',
  `is_read` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if notification read',
  `is_remove` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag if notification might be removed',
  PRIMARY KEY (`notification_id`),
  KEY `IDX_ADMINNOTIFICATION_INBOX_SEVERITY` (`severity`),
  KEY `IDX_ADMINNOTIFICATION_INBOX_IS_READ` (`is_read`),
  KEY `IDX_ADMINNOTIFICATION_INBOX_IS_REMOVE` (`is_remove`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Adminnotification Inbox';

INSERT INTO `adminnotification_inbox` (`notification_id`, `severity`, `date_added`, `title`, `description`, `url`, `is_read`, `is_remove`) VALUES
(1,	3,	'2008-09-16 02:09:54',	'Magento version 1.1.5 Now Available',	'Magento version 1.1.5 Now Available.\n\nThis release includes many bug fixes, a new category manager and a new skin for the default Magento theme.',	'http://www.magentocommerce.com/blog/comments/magento-version-115-now-available/',	0,	0),
(2,	3,	'2008-09-18 00:18:35',	'Magento version 1.1.6 Now Available',	'Magento version 1.1.6 Now Available.\n\nThis version includes bug fixes for Magento 1.1.x that are listed in the release notes section.',	'http://www.magentocommerce.com/blog/comments/magento-version-116-now-available/',	0,	0),
(3,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(4,	3,	'2008-11-20 06:31:12',	'Magento version 1.1.7 Now Available',	'Magento version 1.1.7 Now Available.\n\nThis version includes over 350 issue resolutions for Magento 1.1.x that are listed in the release notes section, and new functionality that includes:\n\n-Google Website Optimizer integration\n-Google Base integration\n-Scheduled DB logs cleaning option',	'http://www.magentocommerce.com/blog/comments/magento-version-117-now-available/',	0,	0),
(5,	3,	'2008-11-27 02:24:50',	'Magento Version 1.1.8 Now Available',	'Magento version 1.1.8 now available.\n\nThis version includes some issue resolutions for Magento 1.1.x that are listed in the release notes section.',	'http://www.magentocommerce.com/blog/comments/magento-version-118-now-available/',	0,	0),
(6,	3,	'2008-12-30 12:45:59',	'Magento version 1.2.0 is now available for download and upgrade',	'We are extremely happy to announce the availability of Magento version 1.2.0 for download and upgrade.\n\nThis version includes numerous issue resolutions for Magento version 1.1.x and some highly requested new features such as:\n\n    * Support for Downloadable/Digital Products. \n    * Added Layered Navigation to site search result page.\n    * Improved site search to utilize MySQL fulltext search\n    * Added support for fixed-taxes on product level.\n    * Upgraded Zend Framework to the latest stable version 1.7.2',	'http://www.magentocommerce.com/blog/comments/magento-version-120-is-now-available/',	0,	0),
(7,	2,	'2008-12-31 02:59:22',	'Magento version 1.2.0.1 now available',	'Magento version 1.2.0.1 now available.This version includes some issue resolutions for Magento 1.2.x that are listed in the release notes section.',	'http://www.magentocommerce.com/blog/comments/magento-version-1201-available/',	0,	0),
(8,	2,	'2009-01-13 01:41:49',	'Magento version 1.2.0.2 now available',	'Magento version 1.2.0.2 is now available for download and upgrade. This version includes an issue resolutions for Magento version 1.2.0.x as listed in the release notes.',	'http://www.magentocommerce.com/blog/comments/magento-version-1202-now-available/',	0,	0),
(9,	3,	'2009-01-24 05:25:56',	'Magento version 1.2.0.3 now available',	'Magento version 1.2.0.3 is now available for download and upgrade. This version includes issue resolutions for Magento version 1.2.0.x as listed in the release notes.',	'http://www.magentocommerce.com/blog/comments/magento-version-1203-now-available/',	0,	0),
(10,	3,	'2009-02-03 02:57:00',	'Magento version 1.2.1 is now available for download and upgrade',	'We are happy to announce the availability of Magento version 1.2.1 for download and upgrade.\n\nThis version includes some issue resolutions for Magento version 1.2.x. A full list of items included in this release can be found on the release notes page.',	'http://www.magentocommerce.com/blog/comments/magento-version-121-now-available/',	0,	0),
(11,	3,	'2009-02-24 05:45:47',	'Magento version 1.2.1.1 now available',	'Magento version 1.2.1.1 now available.This version includes some issue resolutions for Magento 1.2.x that are listed in the release notes section.',	'http://www.magentocommerce.com/blog/comments/magento-version-1211-now-available/',	0,	0),
(12,	3,	'2009-02-27 06:39:24',	'CSRF Attack Prevention',	'We have just posted a blog entry about a hypothetical CSRF attack on a Magento admin panel. Please read the post to find out if your Magento installation is at risk at http://www.magentocommerce.com/blog/comments/csrf-vulnerabilities-in-web-application-and-how-to-avoid-them-in-magento/',	'http://www.magentocommerce.com/blog/comments/csrf-vulnerabilities-in-web-application-and-how-to-avoid-them-in-magento/',	0,	0),
(13,	2,	'2009-03-04 04:03:58',	'Magento version 1.2.1.2 now available',	'Magento version 1.2.1.2 is now available for download and upgrade.\nThis version includes some updates to improve admin security as described in the release notes page.',	'http://www.magentocommerce.com/blog/comments/magento-version-1212-now-available/',	0,	0),
(14,	3,	'2009-03-31 06:22:40',	'Magento version 1.3.0 now available',	'Magento version 1.3.0 is now available for download and upgrade. This version includes numerous issue resolutions for Magento version 1.2.x and new features as described on the release notes page.',	'http://www.magentocommerce.com/blog/comments/magento-version-130-is-now-available/',	0,	0),
(15,	3,	'2009-04-18 08:06:02',	'Magento version 1.3.1 now available',	'Magento version 1.3.1 is now available for download and upgrade. This version includes some issue resolutions for Magento version 1.3.x and new features such as Checkout By Amazon and Amazon Flexible Payment. To see a full list of updates please check the release notes page.',	'http://www.magentocommerce.com/blog/comments/magento-version-131-now-available/',	0,	0),
(16,	3,	'2009-05-20 02:31:21',	'Magento version 1.3.1.1 now available',	'Magento version 1.3.1.1 is now available for download and upgrade. This version includes some issue resolutions for Magento version 1.3.x and a security update for Magento installations that run on multiple domains or sub-domains. If you are running Magento with multiple domains or sub-domains we highly recommend upgrading to this version.',	'http://www.magentocommerce.com/blog/comments/magento-version-1311-now-available/',	0,	0),
(17,	3,	'2009-05-30 02:54:06',	'Magento version 1.3.2 now available',	'This version includes some improvements and issue resolutions for version 1.3.x that are listed on the release notes page. also included is a Beta version of the Compile module.',	'http://www.magentocommerce.com/blog/comments/magento-version-132-now-available/',	0,	0),
(18,	3,	'2009-06-01 23:32:52',	'Magento version 1.3.2.1 now available',	'Magento version 1.3.2.1 now available for download and upgrade.\n\nThis release solves an issue for users running Magento with PHP 5.2.0, and changes to index.php to support the new Compiler Module.',	'http://www.magentocommerce.com/blog/comments/magento-version-1321-now-available/',	0,	0),
(19,	3,	'2009-07-02 05:21:44',	'Magento version 1.3.2.2 now available',	'Magento version 1.3.2.2 is now available for download and upgrade.\n\nThis release includes issue resolution for Magento version 1.3.x. To see a full list of changes please visit the release notes page http://www.magentocommerce.com/download/release_notes.',	'http://www.magentocommerce.com/blog/comments/magento-version-1322-now-available/',	0,	0),
(20,	3,	'2009-07-23 10:48:54',	'Magento version 1.3.2.3 now available',	'Magento version 1.3.2.3 is now available for download and upgrade.\n\nThis release includes issue resolution for Magento version 1.3.x. We recommend to upgrade to this version if PayPal payment modules are in use. To see a full list of changes please visit the release notes page http://www.magentocommerce.com/download/release_notes.',	'http://www.magentocommerce.com/blog/comments/magento-version-1323-now-available/',	0,	0),
(21,	4,	'2009-08-28 22:26:28',	'PayPal is updating Payflow Pro and Website Payments Pro (Payflow Edition) UK.',	'If you are using Payflow Pro and/or Website Payments Pro (Payflow Edition) UK.  payment methods, you will need to update the URLâ€˜s in your Magento Administrator Panel in order to process transactions after September 1, 2009. Full details are available here: http://www.magentocommerce.com/wiki/paypal_payflow_changes',	'http://www.magentocommerce.com/wiki/paypal_payflow_changes',	0,	0),
(22,	2,	'2009-09-24 00:16:49',	'Magento Version 1.3.2.4 Security Update',	'Magento Version 1.3.2.4 is now available. This version includes a security updates for Magento 1.3.x that solves possible XSS vulnerability issue on customer registration page and is available through SVN, Download Page and through the Magento Connect Manager.',	'http://www.magentocommerce.com/blog/comments/magento-version-1324-security-update/',	0,	0),
(23,	4,	'2009-09-25 18:57:54',	'Magento Preview Version 1.4.0.0-alpha2 is now available',	'We are happy to announce the availability of Magento Preview Version 1.4.0.0-alpha2 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-1400-alpha2-now-available/',	0,	0),
(24,	4,	'2009-10-07 04:55:40',	'Magento Preview Version 1.4.0.0-alpha3 is now available',	'We are happy to announce the availability of Magento Preview Version 1.4.0.0-alpha3 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-1400-alpha3-now-available/',	0,	0),
(25,	4,	'2009-12-09 04:30:36',	'Magento Preview Version 1.4.0.0-beta1 is now available',	'We are happy to announce the availability of Magento Preview Version 1.4.0.0-beta1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-1400-beta1-now-available/',	0,	0),
(26,	4,	'2009-12-31 14:22:12',	'Magento Preview Version 1.4.0.0-rc1 is now available',	'We are happy to announce the availability of Magento Preview Version 1.4.0.0-rc1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-1400-rc1-now-available/',	0,	0),
(27,	4,	'2010-02-13 08:39:53',	'Magento CE Version 1.4.0.0 Stable is now available',	'We are excited to announce the availability of Magento CE Version 1.4.0.0 Stable for upgrade and download.',	'http://bit.ly/c53rpK',	0,	0),
(28,	3,	'2010-02-20 07:39:36',	'Magento CE Version 1.4.0.1 Stable is now available',	'Magento CE 1.4.0.1 Stable is now available for upgrade and download.',	'http://www.magentocommerce.com/blog/comments/magento-ce-version-1401-stable-now-available/',	0,	0),
(29,	4,	'2010-04-24 00:09:03',	'Magento Version CE 1.3.3.0 Stable - Now Available With Support for 3-D Secure',	'Based on community requests, we are excited to announce the release of Magento CE 1.3.3.0-Stable with support for 3-D Secure. This release is intended for Magento merchants using version 1.3.x, who want to add support for 3-D Secure.',	'http://www.magentocommerce.com/blog/comments/magento-version-ce-1330-stable-now-available-with-support-for-3-d-secure/',	0,	0),
(30,	4,	'2010-05-31 21:20:21',	'Announcing the Launch of Magento Mobile',	'The Magento team is pleased to announce the launch of Magento mobile, a new product that will allow Magento merchants to easily create branded, native mobile storefront applications that are deeply integrated with Magentoâ€™s market-leading eCommerce platform. The product includes a new administrative manager, a native iPhone app that is fully customizable, and a service where Magento manages the submission and maintenance process for the iTunes App Store.\n\nLearn more by visiting the Magento mobile product page and sign-up to be the first to launch a native mobile commerce app, fully integrated with Magento.',	'http://www.magentocommerce.com/product/mobile',	0,	0),
(31,	4,	'2010-06-11 00:08:08',	'Magento CE Version 1.4.1.0 Stable is now available',	'We are excited to announce the availability of Magento CE Version 1.4.1.0 Stable for upgrade and download. Some of the highlights of this release include: Enhanced PayPal integration (more info to follow), Change of Database structure of the Sales module to no longer use EAV, and much more.',	'http://www.magentocommerce.com/blog/comments/magento-ce-version-1410-stable-now-available/',	0,	0),
(32,	4,	'2010-07-27 01:37:34',	'Magento CE Version 1.4.1.1 Stable is now available',	'We are excited to announce the availability of Magento CE Version 1.4.1.1 Stable for download and upgrade.',	'http://www.magentocommerce.com/blog/comments/magento-ce-version-1411-stable-now-available/',	0,	0),
(33,	4,	'2010-07-28 09:12:12',	'Magento CE Version 1.4.2.0-beta1 Preview Release Now Available',	'This release gives a preview of the new Magento Connect Manager.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-1420-beta1-now-available/',	0,	0),
(34,	4,	'2010-07-29 00:15:01',	'Magento CE Version 1.4.1.1 Patch Available',	'As some users experienced issues with upgrading to CE 1.4.1.1 through PEAR channels we provided a patch for it that is available on our blog http://www.magentocommerce.com/blog/comments/magento-ce-version-1411-stable-patch/',	'http://www.magentocommerce.com/blog/comments/magento-ce-version-1411-stable-patch/',	0,	0),
(35,	4,	'2010-10-12 04:13:25',	'Magento Mobile is now live!',	'Magento Mobile is now live! Signup today to have your own native iPhone mobile-shopping app in iTunes for the holiday season! Learn more at http://www.magentomobile.com/',	'http://www.magentomobile.com/',	0,	0),
(36,	4,	'2010-11-09 02:52:06',	'Magento CE Version 1.4.2.0-RC1 Preview Release Now Available',	'We are happy to announce the availability of Magento Preview Version 1.4.2.0-RC1 for download.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-1420-rc1-now-available/',	0,	0),
(37,	4,	'2010-12-03 01:33:00',	'Magento CE Version 1.4.2.0-RC2 Preview Release Now Available',	'We are happy to announce the availability of Magento Preview Version 1.4.2.0-RC2 for download.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-1420-rc2-now-available/',	0,	0),
(38,	4,	'2010-12-09 03:29:55',	'Magento CE Version 1.4.2.0 Stable is now available',	'We are excited to announce the availability of Magento CE Version 1.4.2.0 Stable for download and upgrade.',	'http://www.magentocommerce.com/blog/comments/magento-ce-version-1420-stable-now-available/',	0,	0),
(39,	4,	'2010-12-18 04:23:55',	'Magento Preview Version CE 1.5.0.0-alpha1 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-alpha1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-alpha1-now-available/',	0,	0),
(40,	4,	'2010-12-30 04:51:08',	'Magento Preview Version CE 1.5.0.0-alpha2 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-alpha2 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-alpha2-now-available/',	0,	0),
(41,	4,	'2011-01-14 05:35:36',	'Magento Preview Version CE 1.5.0.0-beta1 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-beta1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-beta1-now-available/',	0,	0),
(42,	4,	'2011-01-22 02:19:09',	'Magento Preview Version CE 1.5.0.0-beta2 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-beta2 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-beta2-now-available/',	0,	0),
(43,	4,	'2011-01-28 02:27:57',	'Magento Preview Version CE 1.5.0.0-rc1 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-rc1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-rc1-now-available/',	0,	0),
(44,	4,	'2011-02-04 02:56:33',	'Magento Preview Version CE 1.5.0.0-rc2 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.5.0.0-rc2 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1500-rc2-now-available/',	0,	0),
(45,	4,	'2011-02-09 00:43:23',	'Magento CE Version 1.5.0.0 Stable is now available',	'We are excited to announce the availability of Magento CE Version 1.5.0.0 Stable for download and upgrade.',	'http://www.magentocommerce.com/blog/comments/magento-community-professional-and-enterprise-editions-releases-now-availab/',	0,	0),
(46,	4,	'2011-02-10 04:42:57',	'Magento CE 1.5.0.1 stable Now Available',	'We are excited to announce the availability of Magento CE Version 1.5.0.1 Stable for download and upgrade.',	'http://www.magentocommerce.com/blog/comments/magento-ce-1501-stable-now-available/',	0,	0),
(47,	4,	'2011-03-19 00:15:45',	'Magento CE 1.5.1.0-beta1 Now Available',	'We are happy to announce the availability of Magento Preview Version CE 1.5.1.0-beta1 for download and upgrade.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1510-beta1-now-available/',	0,	0),
(48,	4,	'2011-03-31 22:43:02',	'Magento CE 1.5.1.0-rc1 Now Available',	'We are happy to announce the availability of Magento Preview Version CE 1.5.1.0-rc1 for download and upgrade.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1510-rc1-now-available/',	0,	0),
(49,	4,	'2011-04-26 23:21:07',	'Magento CE 1.5.1.0-stable Now Available',	'We are excited to announce the availability of Magento CE Version 1.5.1.0 Stable for download and upgrade.',	'http://www.magentocommerce.com/blog/comments/magento-ce-version-1510-stable-now-available/',	0,	0),
(50,	4,	'2011-05-26 23:33:23',	'Magento Preview Version CE 1.6.0.0-alpha1 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.6.0.0-alpha1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1600-alpha1-now-available/',	0,	0),
(51,	4,	'2011-06-15 22:12:08',	'Magento Preview Version CE 1.6.0.0-beta1 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.6.0.0-beta1for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1600-beta1-now-available/',	0,	0),
(52,	4,	'2011-06-30 23:03:58',	'Magento Preview Version CE 1.6.0.0-rc1 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.6.0.0-rc1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1600-rc1-now-available/',	0,	0),
(53,	4,	'2011-07-11 23:07:39',	'Magento Preview Version CE 1.6.0.0-rc2 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.6.0.0-rc2 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1600-rc2-now-available/',	0,	0),
(54,	4,	'2011-08-19 21:58:31',	'Magento CE 1.6.0.0-stable Now Available',	'We are excited to announce the availability of Magento CE Version 1.6.0.0 Stable for download and upgrade.',	'http://www.magentocommerce.com/blog/comments/magento-ce-version-1600-stable-now-available/',	0,	0),
(55,	4,	'2011-09-17 05:31:26',	'Magento Preview Version CE 1.6.1.0-beta1 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.6.1.0-beta1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1610-beta1-now-available/',	0,	0),
(56,	4,	'2011-09-29 19:44:10',	'Magento Preview Version CE 1.6.1.0-rc1 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.6.1.0-rc1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1610-rc1-now-available/',	0,	0),
(57,	4,	'2011-10-19 21:50:05',	'Magento CE 1.6.1.0-stable Now Available',	'We are excited to announce the availability of Magento CE Version 1.6.1.0 Stable for download and upgrade.',	'http://www.magentocommerce.com/blog/comments/magento-ce-version-1610-stable-now-available/',	0,	0),
(58,	4,	'2011-12-30 22:39:35',	'Magento Preview Version CE 1.7.0.0-alpha1 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.7.0.0-alpha1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1700-alpha1-now-available/',	0,	0),
(59,	4,	'2012-01-11 22:24:20',	'Magento CE 1.6.2.0-stable Now Available',	'We are excited to announce the availability of Magento CE Version 1.6.2.0 Stable for download and upgrade.',	'http://www.magentocommerce.com/blog/comments/magento-ce-version-1620-stable-now-available/',	0,	0),
(60,	4,	'2012-03-03 00:54:12',	'Magento Preview Version CE 1.7.0.0-beta1 is now available',	'We are happy to announce the availability of Magento Preview Version CE 1.7.0.0-beta1 for download.\nAs this is a preview version it is NOT recommended in any way to be used in a production environment.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1700-beta1-now-available/',	0,	0),
(61,	4,	'2012-04-23 14:02:40',	'Magento Community Preview Version CE 1.7.0.0-RC1 has been released!',	'Learn more about the exciting new features and updates in this release and how you can take it for a test drive. As this is a preview version, we need to stress that it\'s likely unstable and that we DON\'T recommend that you use it in any production environment just yet.',	'http://www.magentocommerce.com/blog/comments/magento-preview-version-ce-1700-rc1-now-available/',	0,	0),
(62,	4,	'2012-05-11 09:46:54',	'Magento Community 1.7 and Magento Enterprise 1.12 now available!',	'Learn more about the exciting new features and updates in these releases.',	'http://www.magentocommerce.com/blog/comments/magento-enterprise-112-and-community-17-now-available/',	0,	0),
(63,	4,	'2012-06-20 18:54:07',	'Magento Community Edition 1.7.0.1 now available! ',	'We have just released an updated version of Magento Community Edition, version 1.7.0.1. This update delivers new, minor functionality and fixes for some potential security vulnerabilities.',	'http://www.magentocommerce.com/blog/comments/magento-community-edition-1701-released/',	0,	0),
(64,	4,	'2012-07-05 19:21:43',	'Important Security Update - Zend Platform Vulnerability',	'We have recently learned of a serious vulnerability in the Zend platform on which Magento is built. Learn more and access a patch that addresses this issue. ',	'http://www.magentocommerce.com/blog/comments/important-security-update-zend-platform-vulnerability/',	0,	0),
(65,	4,	'2012-11-19 20:27:42',	'Wrap up more holiday sales with financing',	'Give your customers up to 6 months financing. You get paid right away with Bill Me Later, a PayPal service. It’s a great way to extend financing in time for the holidays. Learn More.',	'http://www.magentocommerce.com/paypal/billmelater?utm_source=CEMessaging&utm_medium=copy&utm_content=sixmonths&utm_campaign=BML',	0,	0),
(66,	4,	'2012-12-07 11:22:30',	'Increase Your Sales With PayPal',	'Magento merchants using PayPal Express Checkout can help increase their sales on average 18%. It is one simple thing you can do right now to help boost your sales. Learn more.',	'http://www.magentocommerce.com/add-paypal?utm_source=CEModule&utm_medium=copy&utm_content=18&utm_campaign=choosepaypal',	0,	0),
(67,	4,	'2013-01-15 22:02:07',	'Imagine 2013 Registration is Now Open!',	'Join 1500 merchants, partners, developers and enthusiasts from 35+ countries around the world for Magento’s premier global conference! Collaborate, learn, network and get inspired by the future of eCommerce. Tickets will sell out fast! April 8th – 10th in Las Vegas.',	'https://registration.imagineecommerce.com/',	0,	0),
(68,	4,	'2013-02-12 17:53:42',	'Get More eCommerce Power with Magento Enterprise',	'Limited time offer: Get a free, customized evaluation of your Community Edition site from a Magento Solution Partner. This evaluation gives you a clear look at the numerous benefits you can achieve by upgrading to Enterprise Edition. ',	'http://www.magentocommerce.com/community-to-enterprise?utm_source=CEMM&utm_medium=copy&utm_campaign=CE2EE',	0,	0),
(69,	2,	'2013-09-27 17:28:13',	'Magento Community Edition 1.8.0.0 - now available for download!',	'Get tax, security, performance, and many other improvements. \n\nLearn more at http://www.magentocommerce.com/blog/comments/magento-community-edition-one-eight-now-available/ ',	'http://www.magentocommerce.com/blog/comments/magento-community-edition-one-eight-now-available/',	0,	0),
(70,	4,	'2013-12-11 15:35:06',	'Magento Community Edition 1.8.1.0 is here!',	'This new version offers significant tax calculation, product quality, and security enhancements. Be sure to carefully review the upgrade instructions before starting. More information is available at http://www.magentocommerce.com/blog/comments/magento-community-edition-1810-is-here/',	'http://www.magentocommerce.com/blog/comments/magento-community-edition-1810-is-here/',	0,	0),
(71,	4,	'2013-12-12 23:24:39',	'Important Magento Community Edition Patch',	'A security patch is available for Magento Community Edition 1.4.0.0 through 1.7.0.2; the issue has been fixed in Magento Community Edition 1.8.0.0 and later. It resolves a vulnerability discovered through our quarterly penetration testing process and has not been reported by merchants. We encourage all merchants to apply the patch in their next regularly scheduled maintenance cycle. The patch is available at http://www.magentocommerce.com/download in the Magento Community Edition Patches section.',	'http://www.magentocommerce.com/download',	0,	0),
(72,	4,	'2014-01-21 18:34:48',	'PHP 5.4 Patch Now Available',	'Magento is advancing its platform and making development more efficient by adding support for PHP 5.4. Patches are available for download at www.magentocommerce.com/download and you can learn more about this update in our blog post at http://www.magentocommerce.com/blog/comments/magento-now-supports-php-54 .',	'http://www.magentocommerce.com/blog/comments/magento-now-supports-php-54',	0,	0),
(73,	4,	'2014-02-20 17:51:46',	'Discover Card Validation Patch Available',	'A patch that resolves an issue with validating Discover credit cards is now available for download at http://www.magentocommerce.com/download. More information on the patch is available in the Magento Knowledge Base at http://www.magentocommerce.com/knowledge-base/entry/discover-card-validation',	'http://www.magentocommerce.com/knowledge-base/entry/discover-card-validation ',	0,	0),
(74,	4,	'2014-03-04 15:12:26',	'Learn How to Quickly Increase Sales on Your Site',	'Adding the PayPal Express Checkout button to your checkout page can help increase sales 18% on average. It’s fast and easy and you can do it right now to boost sales. Express Checkout is already seamlessly integrated into Community Edition.',	'http://magento.com/paypal/CE_Express_Checkout_NA?utm_source=messagemodule&utm_medium=message&utm_campaign=Ceexpresscheckout',	0,	0),
(75,	4,	'2014-05-13 16:46:11',	'Magento Community Edition 1.9 is now available!',	'It accelerates your entry into the mobile commerce space by slashing the time and cost of getting a mobile device-friendly responsive site.  And a new Bill Me Later payment option helps boost your sales. Visit http://magento.com/blog/magento-news/magento-enables-responsive-sites-half-time to learn more.',	'http://magento.com/blog/magento-news/magento-enables-responsive-sites-half-time',	0,	0),
(76,	4,	'2014-10-08 04:55:06',	'It’s still not too late: Boost your 2014 Holiday sales today.',	'See results in as little as one week with eBay Enterprise Display and 2 business days with eBay Enterprise Affiliate. DID YOU KNOW: Get access to over 128 million highly qualified shoppers on eBay.com with eBay Enterprise Display. And the average retail client using product retargeting sees a $10 to $1 ROI on retargeting spend during Q4. With the eBay Enterprise Affiliate Network, get access to over 200,000 active network publishers across 30+ verticals, and only pay when a customer makes a purchase (Affiliate Network is only available in the U.S. & Canada).  GET STARTED TODAY by visiting www.ebayenterprise.com/turbochargesales',	'http://www.ebayenterprise.com/turbochargesales',	0,	0),
(77,	4,	'2014-11-12 19:03:26',	'Important: PayPal Users Must Discontinue Using SSL 3.0 By December 3, 2014',	'To address a vulnerability with the SSL 3.0 security protocol, PayPal and other payment gateways will be disabling SSL 3.0 support. Merchants must upgrade to Transport Layer Service (TLS) by December 3, 2014 to avoid PayPal payment operation failures. Learn more about what you need to do at https://devblog.paypal.com/poodle-ssl-3-0-vulnerability/',	'https://devblog.paypal.com/poodle-ssl-3-0-vulnerability/',	0,	0),
(78,	4,	'2014-11-24 23:25:21',	'Magento Community Edition 1.9.1 is available!',	'Magento Community Edition 1.9.1 empowers merchants to deliver compelling shopping experiences by offering enhanced responsive design capabilities, new swatches to display product variations, and improved performance through support for MySQL 5.6 and PHP 5.5. It also includes support for Google Universal Analytics and over 70 product improvements. Find out more at http://magento.com/blog/magento-news/magento-community-edition-191-now-available-download',	'http://magento.com/blog/magento-news/magento-community-edition-191-now-available-download',	0,	0),
(79,	4,	'2015-01-22 17:47:08',	'Join Us at Imagine Commerce 2015 - April 20-22 at the Wynn Las Vegas',	'Join Magento, eBay Enterprise, and over 2,000 merchants, developers, and eCommerce experts at the premier Commerce event of the year. With three days of cutting-edge keynote presentations, special technical programs, dynamic breakout sessions, and incredible networking opportunities, Imagine Commerce 2015 will educate, enrich, and inspire you to take your business to new heights. Register now at http://imagine2015.magento.com/.',	'http://imagine2015.magento.com/',	0,	0),
(80,	1,	'2015-04-16 16:17:07',	'Critical Reminder: Download and install Magento security patches.  Download now.',	'Download and implement 2 important security patches (SUPEE-5344 and SUPEE-1533) from the Magento Community Edition download page (https://www.magentocommerce.com/products/downloads/magento/).  If you have not done so already, download and install 2 previously-released patches that prevent an attacker from remotely executing code on Magento software.  These issues affect all versions of Magento Community Edition.  A press release from Check Point Software Technologies in the coming days will make one of these issues widely known, possibly alerting hackers who may try to exploit it.  Ensure the patches are in place as a preventative measure before the issue is publicized.',	'https://www.magentocommerce.com/products/downloads/magento/ ',	0,	0),
(81,	1,	'2015-04-19 22:37:00',	'Second Reminder: Download and install Magento critical security patches now.',	'If you have not done so already, download and install 2 previously-released security patches (SUPEE-5344 and SUPEE-1533) from the Magento Community Edition download page (https://www.magentocommerce.com/products/downloads/magento/).  These security issues affect all versions of Magento Community Edition and enable an attacker to remotely execute code on Magento software. A press release from Check Point Software Technologies tomorrow  will make one of these issues widely known, possibly alerting hackers who may try to exploit it.  Ensure the patches are in place as a preventative measure before the issue is publicized.',	'https://www.magentocommerce.com/products/downloads/magento/',	0,	0),
(82,	1,	'2015-04-23 19:43:31',	'Urgent: Immediately install Magento critical security patches',	'It is critical for you to download and install 2 previously-released security patches (SUPEE-5344 and SUPEE-1533) from the Magento Community Edition download page (https://www.magentocommerce.com/products/downloads/magento/).  Please do this immediately, as Check Point Software Technologies has published a technical description of how they discovered the issue, which we feel might serve as a tutorial for implementing an attack against your website. ',	'https://www.magentocommerce.com/products/downloads/magento/',	0,	0),
(83,	1,	'2015-05-15 00:34:01',	'Important: New Magento Security Patch - Install it Now',	'It is important for you to download and install a new security patch (SUPEE-5994) from the Magento Community Edition download page (https://www.magentocommerce.com/products/downloads/magento/).  Please apply this critical update immediately to help protect your site from exposure to multiple security vulnerabilities impacting all versions of the Magento Community Edition software. Please note that this patch should be installed in addition to the recent Shoplift patch (SUPEE-5344).',	'https://www.magentocommerce.com/products/downloads/magento/',	0,	0),
(84,	4,	'2015-07-07 16:28:25',	'Now available:  Enhanced site quality and security with Community Edition 1.9.2',	'Magento Community Edition 1.9.2 is now available for download and features over 105 product improvements, nearly 170 automated functional tests to help internal development teams improve implementation quality and time to market, and several security enhancements. Read our blog announcement for more information (http://magento.com/blog/magento-news/magento-community-edition-192-now-available), or go to the Community Edition download page to get the software today (https://www.magentocommerce.com/products/downloads/magento/).',	'https://www.magentocommerce.com/products/downloads/magento/',	0,	0),
(85,	1,	'2015-07-07 17:08:05',	'July 7, 2015: New Magento Security Patch (SUPEE-6285) – Install Immediately',	'Today we are providing a new security patch (SUPEE-6285) that addresses critical security vulnerabilities. The patch is available for Community Edition 1.4.1 to 1.9.1.1 and is part of the core code of our latest release, Community Edition 1.9.2, available for download today.  PLEASE NOTE:  You must first implement SUPEE-5994 to ensure SUPEE-6285 works properly. Download Community Edition 1.9.2 or the patch from the Community Edition download page: https://www.magentocommerce.com/products/downloads/magento/  ',	'https://www.magentocommerce.com/products/downloads/magento/',	0,	0),
(86,	1,	'2015-08-04 17:28:26',	'August 4, 2015: New Magento Security Patch (SUPEE-6482) – Install Immediately',	'Today we are providing a new security patch (SUPEE-6482) that addresses 4 security issues; two issues related to APIs and two cross-site scripting risks. The patch is available for Community Edition 1.4 and later releases and is part of the core code of Community Edition 1.9.2.1, which is available for download today. Before implementing this new security patch, you must first implement all previous security patches. Download Community Edition 1.9.2.1 or the patch from the Community Edition download page at https://www.magentocommerce.com/products/downloads/magento/  ',	'https://www.magentocommerce.com/products/downloads/magento/  ',	0,	0),
(87,	2,	'2015-08-05 20:12:55',	'August 5, 2015:  Security Patch (SUPEE-6482) Release Note CORRECTION',	'When we announced a new security patch (SUPEE-6482) for Community Edition yesterday we incorrectly said that it addresses 4 issues. It actually addresses 2 security issues: Autoloaded File Inclusion in Magento SOAP API and a SSRF Vulnerability in WSDL File. The patch and Community Edition 1.9.2.1 are complete, fully-tested, and ready to deploy. We strongly encourage you to deploy the patch or upgrade to the latest version of Community Edition if you haven’t done so already. You can find the patch at https://www.magentocommerce.com/products/downloads/magento/. ',	'https://www.magentocommerce.com/products/downloads/magento/',	0,	0),
(88,	4,	'2015-10-01 17:32:53',	'Episode VI: Return of Imagine Commerce | 2016 Registration is Open!',	'The force is strong with this one. Join the Magento rebellion at Imagine Commerce 2016. Enlist Now at http://imagine.magento.com.',	'http://imagine.magento.com',	0,	0),
(89,	1,	'2015-10-21 00:28:05',	'New Malware Issue. Make Sure You Have Implemented All Security Patches',	'We have received reports that some Magento sites are being targeted by Guruincsite malware (Neutrino exploit kit). We have NOT identified a new attack vector at this time. Nearly all the impacted sites checked so far were vulnerable to a previously identified and patched issue; sites not vulnerable to that issue show other unpatched issues. Visit the Magento Security Center at http://magento.com/security/news/important-security-update for more information on how to address this issue and make sure that you have implemented all recent security patches.\n',	'http://magento.com/security/news/important-security-update',	0,	0),
(90,	1,	'2015-10-27 21:48:23',	'October 27, 2015: New Magento Security Patch (SUPEE-6788) – Install Immediately',	'Today, we are releasing a new patch (SUPEE-6788) and Community Edition 1.9.2.2 to address 10+ security issues, including remote code execution and information leak vulnerabilities. This patch is unrelated to the Guruincsite malware issue. Be sure to test the patch in a development environment first, as it can affect extensions and customizations. Download the patch from the Community Edition Download page and learn more at http://magento.com/security/patches/supee-6788  ',	'http://magento.com/security/patches/supee-6788  ',	0,	0),
(91,	1,	'2015-11-18 00:25:00',	'New JavaScript Malware Issue. Make sure your site is secure.',	'Magento Commerce has received reports of a JavaScript malware exploit that forwards credit card information from checkout pages to an external site. No new attack vector has been identified. It appears most impacted sites have not implemented the February 2015 Shoplift patch, or the patch was implemented after the site was already compromised. Information on how to identify and remove the malicious code is available on the Magento Security Center at http://magento.com/security/news/new-javascript-malware-issue',	'http://magento.com/security/news/new-javascript-malware-issue ',	0,	0),
(92,	4,	'2015-11-18 21:40:40',	'Magento 2.0 Is Available!',	'Magento 2.0, our next generation open source digital commerce platform is here!  Magento 2.0 offers enhanced performance and scalability, new features to deliver better shopping experiences and conversion rates, and business agility and productivity improvements.  Learn more http://magento.com/blog/magento-news/new-era-commerce-innovation.\n\n',	'http://magento.com/blog/magento-news/new-era-commerce-innovation',	0,	0),
(93,	4,	'2016-01-13 22:51:52',	'Have questions? Magento has big answers at Imagine Commerce.',	'Big news, bigger experts, and an even bigger ecosystem. Will you be at Imagine Commerce? Join us. ',	'http://imagine.magento.com',	0,	0),
(94,	1,	'2016-01-20 22:17:19',	'Important: New Security Patch (SUPEE-7405) and Release – 1/20/2016',	'Today, we are releasing a new patch (SUPEE-7405) and Community Edition 1.9.2.3 to improve the security of Magento sites. There are no confirmed attacks related to the security issues, but certain vulnerabilities can potentially be exploited to access customer information or take over administrator sessions. You can download the patch and release from the Community Edition Download Page and learn more at https://magento.com/security/patches/supee-7405. ',	'https://magento.com/security/patches/supee-7405',	0,	0),
(95,	1,	'2016-02-24 08:06:43',	'Important: Updates to SUPEE-7405 Security Patch and Release – 2/23/2016',	'Updated versions of the recent SUPEE-7405 patch and Community Edition 1.9.2.3 release are now available. The updates add support for PHP 5.3 and address issues with upload file permissions, merging carts, and SOAP APIs experienced with the original release. They DO NOT address any new security issues, but all merchants should upgrade to the new versions to maintain compatibility with future releases. You can learn more about the updates at https://magento.com/security/patches/supee-7405',	'https://magento.com/security/patches/supee-7405',	0,	0),
(96,	1,	'2016-03-31 00:25:07',	'Protect Your Business from Brute-Force Password Guessing Attacks',	'We just posted an article on the Magento Security Center that shares best practices for protecting stores from brute-force password guessing attacks. We’ve recently been made aware of a rise in these attacks, so we strongly recommend that you review the approaches outlined in this article with your developer and hosting provider immediately and implement the ones that are best suited to your unique situation. Learn more at https://magento.com/security/best-practices/protect-your-magento-installation-password-guessing ',	'https://magento.com/security/best-practices/protect-your-magento-installation-password-guessing ',	0,	0),
(97,	4,	'2016-06-23 16:44:34',	'Increase your sales and productivity, while simplifying PCI compliance with exciting new Magento Community Edition 2.1 features.',	'Shoppers can now check out faster using credit cards stored with PayPal and a redesigned PayPal checkout. PCI compliance is easier thanks to Braintree Hosted Fields. Admin tools are redesigned to help you work more efficiently. Upgrade to Enterprise Edition for new Content Staging and Preview and Enhanced Site Search capabilities.',	'https://magento.com/blog/magento-news/magento-enterprise-edition-21-unleashes-power-marketers-and-merchandisers ',	0,	0),
(98,	1,	'2016-10-12 20:30:13',	'Community Edition 1.9.3 and SUPEE-8788 Provide Critical Security & Functional Updates – 10/12/2016',	'Community Edition 1.9.3 and SUPEE-8788 resolve multiple security issues, including critical vulnerabilities with certain payment methods and Zend Framework libraries. Community Edition 1.9.3 also includes over 120 product quality enhancements and support for PHP 5.6. Learn more about the security issues at https://magento.com/security/patches/supee-8788 and functional updates at http://devdocs.magento.com/guides/m1x/ce19-ee114/ce1.9_release-notes.html#ce19-1930 ',	'https://magento.com/security/patches/supee-8788 ',	0,	0),
(99,	1,	'2016-10-25 16:51:10',	'Dirty COW Linux OS Vulnerability – 10/25/2016',	'Dirty COW (CVE-2016-5195) is a serious vulnerability affecting most Linux Operating Systems that can be exploited to gain root access to the server. More information on how to immediately update your Linux OS is available in the Magento Security Center at https://magento.com/security/vulnerabilities/new-linux-operating-system-vulnerability. ',	'https://magento.com/security/vulnerabilities/new-linux-operating-system-vulnerability',	0,	0),
(100,	4,	'2016-11-14 21:08:04',	'Community Edition 1.9.3.1 is Now Available – 11/14/2016',	'Community Edition 1.9.3.1 addresses issues encountered by some merchants in our October 12 product releases. All merchants should either upgrade to Community Edition 1.9.3.1 or apply our latest patch, SUPEE-8788, to close several serious security vulnerabilities. More information is available in the release notes at: http://devdocs.magento.com/guides/m1x/ce19-ee114/ce1.9_release-notes.html#ce19-1931 ',	'http://devdocs.magento.com/guides/m1x/ce19-ee114/ce1.9_release-notes.html#ce19-1931 ',	0,	0),
(101,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(102,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(103,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(104,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(105,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(106,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(107,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(108,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(109,	1,	'2008-09-03 01:10:31',	'Magento Version 1.1.4 Security Update Now Available',	'Magento 1.1.4 Security Update Now Available. If you are using Magento version 1.1.x, we highly recommend upgrading to this version as soon as possible.',	'http://www.magentocommerce.com/blog/comments/magento-version-114-security-update/',	0,	0),
(110,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(111,	4,	'2016-12-30 17:53:18',	'We want your opinion! Take our Retail Trends survey for 2017.',	'Let us know what is important to you and your business for 2017. We invite you to take our retail trends survey which should take no more than a few minutes to complete. As always, any individual information you provide will remain strictly confidential. \n\nTo participate, go to:\nhttps://www.research.net/r/PWJCD2X',	'https://www.research.net/r/PWJCD2X',	0,	0),
(112,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(113,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(114,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(115,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(116,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(117,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(118,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(119,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0),
(120,	4,	'2008-11-08 04:46:42',	'Reminder: Change Magento`s default phone numbers and callouts before site launch',	'Before launching your Magento store, please remember to change Magento`s default phone numbers that appear in email templates, callouts, templates, etc.',	'',	0,	0);

DROP TABLE IF EXISTS `admin_assert`;
CREATE TABLE `admin_assert` (
  `assert_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Assert ID',
  `assert_type` varchar(20) DEFAULT NULL COMMENT 'Assert Type',
  `assert_data` text COMMENT 'Assert Data',
  PRIMARY KEY (`assert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin Assert Table';


DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Role ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Role ID',
  `tree_level` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role Tree Level',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role Sort Order',
  `role_type` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Role Type',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User ID',
  `role_name` varchar(50) DEFAULT NULL COMMENT 'Role Name',
  PRIMARY KEY (`role_id`),
  KEY `IDX_ADMIN_ROLE_PARENT_ID_SORT_ORDER` (`parent_id`,`sort_order`),
  KEY `IDX_ADMIN_ROLE_TREE_LEVEL` (`tree_level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin Role Table';

INSERT INTO `admin_role` (`role_id`, `parent_id`, `tree_level`, `sort_order`, `role_type`, `user_id`, `role_name`) VALUES
(1,	0,	1,	1,	'G',	0,	'Administrators'),
(2,	1,	2,	0,	'U',	1,	'Marcelo');

DROP TABLE IF EXISTS `admin_rule`;
CREATE TABLE `admin_rule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule ID',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Role ID',
  `resource_id` varchar(255) DEFAULT NULL COMMENT 'Resource ID',
  `privileges` varchar(20) DEFAULT NULL COMMENT 'Privileges',
  `assert_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Assert ID',
  `role_type` varchar(1) DEFAULT NULL COMMENT 'Role Type',
  `permission` varchar(10) DEFAULT NULL COMMENT 'Permission',
  PRIMARY KEY (`rule_id`),
  KEY `IDX_ADMIN_RULE_RESOURCE_ID_ROLE_ID` (`resource_id`,`role_id`),
  KEY `IDX_ADMIN_RULE_ROLE_ID_RESOURCE_ID` (`role_id`,`resource_id`),
  CONSTRAINT `FK_ADMIN_RULE_ROLE_ID_ADMIN_ROLE_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin Rule Table';

INSERT INTO `admin_rule` (`rule_id`, `role_id`, `resource_id`, `privileges`, `assert_id`, `role_type`, `permission`) VALUES
(1,	1,	'all',	NULL,	0,	'G',	'allow');

DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `firstname` varchar(32) DEFAULT NULL COMMENT 'User First Name',
  `lastname` varchar(32) DEFAULT NULL COMMENT 'User Last Name',
  `email` varchar(128) DEFAULT NULL COMMENT 'User Email',
  `username` varchar(40) DEFAULT NULL COMMENT 'User Login',
  `password` varchar(100) DEFAULT NULL COMMENT 'User Password',
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'User Created Time',
  `modified` timestamp NULL DEFAULT NULL COMMENT 'User Modified Time',
  `logdate` timestamp NULL DEFAULT NULL COMMENT 'User Last Login Time',
  `lognum` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'User Login Number',
  `reload_acl_flag` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Reload ACL',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'User Is Active',
  `extra` text COMMENT 'User Extra Data',
  `rp_token` text COMMENT 'Reset Password Link Token',
  `rp_token_created_at` timestamp NULL DEFAULT NULL COMMENT 'Reset Password Link Token Creation Date',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UNQ_ADMIN_USER_USERNAME` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Admin User Table';

INSERT INTO `admin_user` (`user_id`, `firstname`, `lastname`, `email`, `username`, `password`, `created`, `modified`, `logdate`, `lognum`, `reload_acl_flag`, `is_active`, `extra`, `rp_token`, `rp_token_created_at`) VALUES
(1,	'Marcelo',	'Alvarez',	'marcelo@mpadesign.com.au',	'marcelo',	'c4731bde836ba22e47d49eb9f8ddb473:AYhENdsQsUxILrnbTGarCZQljotoErPS',	'2016-12-14 06:05:35',	'2016-12-14 06:05:35',	'2017-01-03 02:28:51',	13,	0,	1,	'a:1:{s:11:\"configState\";a:59:{s:14:\"design_package\";s:1:\"0\";s:12:\"design_theme\";s:1:\"1\";s:11:\"design_head\";s:1:\"1\";s:13:\"design_header\";s:1:\"1\";s:13:\"design_footer\";s:1:\"1\";s:16:\"design_watermark\";s:1:\"0\";s:17:\"design_pagination\";s:1:\"0\";s:12:\"design_email\";s:1:\"1\";s:56:\"astrabootstrapsettings_astrabootstrapsettings_appearance\";s:1:\"1\";s:52:\"astrabootstrapsettings_astrabootstrapsettings_header\";s:1:\"1\";s:50:\"astrabootstrapsettings_astrabootstrapsettings_menu\";s:1:\"1\";s:52:\"astrabootstrapsettings_astrabootstrapsettings_labels\";s:1:\"1\";s:52:\"astrabootstrapsettings_astrabootstrapsettings_footer\";s:1:\"1\";s:52:\"astrabootstrapsettings_astrabootstrapsettings_social\";s:1:\"1\";s:54:\"astrabootstrapsettings_astrabootstrapsettings_products\";s:1:\"1\";s:61:\"astrabootstrapsettings_astrabootstrapsettings_newsletterpopup\";s:1:\"1\";s:58:\"astrabootstrapsettings_astrabootstrapsettings_shoppingcart\";s:1:\"1\";s:55:\"astrabootstrapsettings_astrabootstrapsettings_cloudzoom\";s:1:\"1\";s:55:\"astrabootstrapsettings_astrabootstrapsettings_quickview\";s:1:\"1\";s:7:\"web_url\";s:1:\"0\";s:7:\"web_seo\";s:1:\"0\";s:12:\"web_unsecure\";s:1:\"0\";s:10:\"web_secure\";s:1:\"0\";s:11:\"web_default\";s:1:\"1\";s:9:\"web_polls\";s:1:\"0\";s:10:\"web_cookie\";s:1:\"0\";s:11:\"web_session\";s:1:\"0\";s:24:\"web_browser_capabilities\";s:1:\"0\";s:12:\"dev_restrict\";s:1:\"0\";s:9:\"dev_debug\";s:1:\"1\";s:12:\"dev_template\";s:1:\"0\";s:20:\"dev_translate_inline\";s:1:\"0\";s:7:\"dev_log\";s:1:\"0\";s:6:\"dev_js\";s:1:\"0\";s:7:\"dev_css\";s:1:\"0\";s:16:\"checkout_options\";s:1:\"0\";s:13:\"checkout_cart\";s:1:\"1\";s:18:\"checkout_cart_link\";s:1:\"0\";s:16:\"checkout_sidebar\";s:1:\"0\";s:23:\"checkout_payment_failed\";s:1:\"0\";s:15:\"general_country\";s:1:\"1\";s:14:\"general_region\";s:1:\"1\";s:14:\"general_locale\";s:1:\"1\";s:25:\"general_store_information\";s:1:\"1\";s:16:\"catalog_frontend\";s:1:\"0\";s:15:\"catalog_sitemap\";s:1:\"0\";s:14:\"catalog_review\";s:1:\"1\";s:21:\"catalog_product_image\";s:1:\"0\";s:20:\"catalog_productalert\";s:1:\"0\";s:25:\"catalog_productalert_cron\";s:1:\"0\";s:19:\"catalog_placeholder\";s:1:\"0\";s:25:\"catalog_recently_products\";s:1:\"0\";s:13:\"catalog_price\";s:1:\"0\";s:26:\"catalog_layered_navigation\";s:1:\"0\";s:18:\"catalog_navigation\";s:1:\"0\";s:11:\"catalog_seo\";s:1:\"1\";s:14:\"catalog_search\";s:1:\"0\";s:20:\"catalog_downloadable\";s:1:\"0\";s:22:\"catalog_custom_options\";s:1:\"0\";}}',	NULL,	NULL);

DROP TABLE IF EXISTS `api2_acl_attribute`;
CREATE TABLE `api2_acl_attribute` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `user_type` varchar(20) NOT NULL COMMENT 'Type of user',
  `resource_id` varchar(255) NOT NULL COMMENT 'Resource ID',
  `operation` varchar(20) NOT NULL COMMENT 'Operation',
  `allowed_attributes` text COMMENT 'Allowed attributes',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_API2_ACL_ATTRIBUTE_USER_TYPE_RESOURCE_ID_OPERATION` (`user_type`,`resource_id`,`operation`),
  KEY `IDX_API2_ACL_ATTRIBUTE_USER_TYPE` (`user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api2 Filter ACL Attributes';


DROP TABLE IF EXISTS `api2_acl_role`;
CREATE TABLE `api2_acl_role` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `role_name` varchar(255) NOT NULL COMMENT 'Name of role',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_API2_ACL_ROLE_CREATED_AT` (`created_at`),
  KEY `IDX_API2_ACL_ROLE_UPDATED_AT` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api2 Global ACL Roles';

INSERT INTO `api2_acl_role` (`entity_id`, `created_at`, `updated_at`, `role_name`) VALUES
(1,	'2016-12-14 06:00:53',	NULL,	'Guest'),
(2,	'2016-12-14 06:00:53',	NULL,	'Customer');

DROP TABLE IF EXISTS `api2_acl_rule`;
CREATE TABLE `api2_acl_rule` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `role_id` int(10) unsigned NOT NULL COMMENT 'Role ID',
  `resource_id` varchar(255) NOT NULL COMMENT 'Resource ID',
  `privilege` varchar(20) DEFAULT NULL COMMENT 'ACL Privilege',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_API2_ACL_RULE_ROLE_ID_RESOURCE_ID_PRIVILEGE` (`role_id`,`resource_id`,`privilege`),
  CONSTRAINT `FK_API2_ACL_RULE_ROLE_ID_API2_ACL_ROLE_ENTITY_ID` FOREIGN KEY (`role_id`) REFERENCES `api2_acl_role` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api2 Global ACL Rules';


DROP TABLE IF EXISTS `api2_acl_user`;
CREATE TABLE `api2_acl_user` (
  `admin_id` int(10) unsigned NOT NULL COMMENT 'Admin ID',
  `role_id` int(10) unsigned NOT NULL COMMENT 'Role ID',
  UNIQUE KEY `UNQ_API2_ACL_USER_ADMIN_ID` (`admin_id`),
  KEY `FK_API2_ACL_USER_ROLE_ID_API2_ACL_ROLE_ENTITY_ID` (`role_id`),
  CONSTRAINT `FK_API2_ACL_USER_ADMIN_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`admin_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_API2_ACL_USER_ROLE_ID_API2_ACL_ROLE_ENTITY_ID` FOREIGN KEY (`role_id`) REFERENCES `api2_acl_role` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api2 Global ACL Users';


DROP TABLE IF EXISTS `api_assert`;
CREATE TABLE `api_assert` (
  `assert_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Assert id',
  `assert_type` varchar(20) DEFAULT NULL COMMENT 'Assert type',
  `assert_data` text COMMENT 'Assert additional data',
  PRIMARY KEY (`assert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api ACL Asserts';


DROP TABLE IF EXISTS `api_role`;
CREATE TABLE `api_role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Role id',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent role id',
  `tree_level` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Role level in tree',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order to display on admin area',
  `role_type` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Role type',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User id',
  `role_name` varchar(50) DEFAULT NULL COMMENT 'Role name',
  PRIMARY KEY (`role_id`),
  KEY `IDX_API_ROLE_PARENT_ID_SORT_ORDER` (`parent_id`,`sort_order`),
  KEY `IDX_API_ROLE_TREE_LEVEL` (`tree_level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api ACL Roles';


DROP TABLE IF EXISTS `api_rule`;
CREATE TABLE `api_rule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Api rule Id',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Api role Id',
  `resource_id` varchar(255) DEFAULT NULL COMMENT 'Module code',
  `api_privileges` varchar(20) DEFAULT NULL COMMENT 'Privileges',
  `assert_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Assert id',
  `role_type` varchar(1) DEFAULT NULL COMMENT 'Role type',
  `api_permission` varchar(10) DEFAULT NULL COMMENT 'Permission',
  PRIMARY KEY (`rule_id`),
  KEY `IDX_API_RULE_RESOURCE_ID_ROLE_ID` (`resource_id`,`role_id`),
  KEY `IDX_API_RULE_ROLE_ID_RESOURCE_ID` (`role_id`,`resource_id`),
  CONSTRAINT `FK_API_RULE_ROLE_ID_API_ROLE_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `api_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api ACL Rules';


DROP TABLE IF EXISTS `api_session`;
CREATE TABLE `api_session` (
  `user_id` int(10) unsigned NOT NULL COMMENT 'User id',
  `logdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Login date',
  `sessid` varchar(40) DEFAULT NULL COMMENT 'Sessioin id',
  KEY `IDX_API_SESSION_USER_ID` (`user_id`),
  KEY `IDX_API_SESSION_SESSID` (`sessid`),
  CONSTRAINT `FK_API_SESSION_USER_ID_API_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `api_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api Sessions';


DROP TABLE IF EXISTS `api_user`;
CREATE TABLE `api_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User id',
  `firstname` varchar(32) DEFAULT NULL COMMENT 'First name',
  `lastname` varchar(32) DEFAULT NULL COMMENT 'Last name',
  `email` varchar(128) DEFAULT NULL COMMENT 'Email',
  `username` varchar(40) DEFAULT NULL COMMENT 'Nickname',
  `api_key` varchar(100) DEFAULT NULL COMMENT 'Api key',
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'User record create date',
  `modified` timestamp NULL DEFAULT NULL COMMENT 'User record modify date',
  `lognum` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Quantity of log ins',
  `reload_acl_flag` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Refresh ACL flag',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Account status',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Api Users';


DROP TABLE IF EXISTS `captcha_log`;
CREATE TABLE `captcha_log` (
  `type` varchar(32) NOT NULL COMMENT 'Type',
  `value` varchar(32) NOT NULL COMMENT 'Value',
  `count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Count',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Update Time',
  PRIMARY KEY (`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Count Login Attempts';


DROP TABLE IF EXISTS `cataloginventory_stock`;
CREATE TABLE `cataloginventory_stock` (
  `stock_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Stock Id',
  `stock_name` varchar(255) DEFAULT NULL COMMENT 'Stock Name',
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock';

INSERT INTO `cataloginventory_stock` (`stock_id`, `stock_name`) VALUES
(1,	'Default');

DROP TABLE IF EXISTS `cataloginventory_stock_item`;
CREATE TABLE `cataloginventory_stock_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `stock_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `min_qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Min Qty',
  `use_config_min_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Min Qty',
  `is_qty_decimal` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Qty Decimal',
  `backorders` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Backorders',
  `use_config_backorders` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Backorders',
  `min_sale_qty` decimal(12,4) NOT NULL DEFAULT '1.0000' COMMENT 'Min Sale Qty',
  `use_config_min_sale_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Min Sale Qty',
  `max_sale_qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Max Sale Qty',
  `use_config_max_sale_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Max Sale Qty',
  `is_in_stock` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is In Stock',
  `low_stock_date` timestamp NULL DEFAULT NULL COMMENT 'Low Stock Date',
  `notify_stock_qty` decimal(12,4) DEFAULT NULL COMMENT 'Notify Stock Qty',
  `use_config_notify_stock_qty` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Notify Stock Qty',
  `manage_stock` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Manage Stock',
  `use_config_manage_stock` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Manage Stock',
  `stock_status_changed_auto` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Stock Status Changed Automatically',
  `use_config_qty_increments` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Qty Increments',
  `qty_increments` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Increments',
  `use_config_enable_qty_inc` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Use Config Enable Qty Increments',
  `enable_qty_increments` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Enable Qty Increments',
  `is_decimal_divided` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Divided into Multiple Boxes for Shipping',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `UNQ_CATALOGINVENTORY_STOCK_ITEM_PRODUCT_ID_STOCK_ID` (`product_id`,`stock_id`),
  KEY `IDX_CATALOGINVENTORY_STOCK_ITEM_PRODUCT_ID` (`product_id`),
  KEY `IDX_CATALOGINVENTORY_STOCK_ITEM_STOCK_ID` (`stock_id`),
  CONSTRAINT `FK_CATINV_STOCK_ITEM_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATINV_STOCK_ITEM_STOCK_ID_CATINV_STOCK_STOCK_ID` FOREIGN KEY (`stock_id`) REFERENCES `cataloginventory_stock` (`stock_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Item';

INSERT INTO `cataloginventory_stock_item` (`item_id`, `product_id`, `stock_id`, `qty`, `min_qty`, `use_config_min_qty`, `is_qty_decimal`, `backorders`, `use_config_backorders`, `min_sale_qty`, `use_config_min_sale_qty`, `max_sale_qty`, `use_config_max_sale_qty`, `is_in_stock`, `low_stock_date`, `notify_stock_qty`, `use_config_notify_stock_qty`, `manage_stock`, `use_config_manage_stock`, `stock_status_changed_auto`, `use_config_qty_increments`, `qty_increments`, `use_config_enable_qty_inc`, `enable_qty_increments`, `is_decimal_divided`) VALUES
(1,	1,	1,	100.0000,	0.0000,	1,	0,	0,	1,	1.0000,	1,	0.0000,	1,	1,	NULL,	NULL,	1,	0,	1,	0,	1,	0.0000,	1,	0,	0),
(2,	2,	1,	12.0000,	0.0000,	1,	0,	0,	1,	1.0000,	1,	0.0000,	1,	1,	NULL,	NULL,	1,	0,	1,	0,	1,	0.0000,	1,	0,	0),
(3,	3,	1,	123.0000,	0.0000,	1,	0,	0,	1,	1.0000,	1,	0.0000,	1,	1,	NULL,	NULL,	1,	0,	1,	0,	1,	0.0000,	1,	0,	0),
(4,	4,	1,	12.0000,	0.0000,	1,	0,	0,	1,	1.0000,	1,	0.0000,	1,	1,	NULL,	NULL,	1,	0,	1,	0,	1,	0.0000,	1,	0,	0),
(5,	5,	1,	123.0000,	0.0000,	1,	0,	0,	1,	1.0000,	1,	0.0000,	1,	1,	NULL,	NULL,	1,	0,	1,	0,	1,	0.0000,	1,	0,	0),
(6,	6,	1,	123.0000,	0.0000,	1,	0,	0,	1,	1.0000,	1,	0.0000,	1,	1,	NULL,	NULL,	1,	0,	1,	0,	1,	0.0000,	1,	0,	0),
(7,	7,	1,	125.0000,	0.0000,	1,	0,	0,	1,	1.0000,	1,	0.0000,	1,	1,	NULL,	NULL,	1,	0,	1,	0,	1,	0.0000,	1,	0,	0);

DROP TABLE IF EXISTS `cataloginventory_stock_status`;
CREATE TABLE `cataloginventory_stock_status` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `IDX_CATALOGINVENTORY_STOCK_STATUS_STOCK_ID` (`stock_id`),
  KEY `IDX_CATALOGINVENTORY_STOCK_STATUS_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_CATINV_STOCK_STS_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATINV_STOCK_STS_STOCK_ID_CATINV_STOCK_STOCK_ID` FOREIGN KEY (`stock_id`) REFERENCES `cataloginventory_stock` (`stock_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATINV_STOCK_STS_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status';

INSERT INTO `cataloginventory_stock_status` (`product_id`, `website_id`, `stock_id`, `qty`, `stock_status`) VALUES
(1,	1,	1,	100.0000,	1),
(2,	1,	1,	12.0000,	1),
(3,	1,	1,	123.0000,	1),
(4,	1,	1,	12.0000,	1),
(5,	1,	1,	123.0000,	1),
(6,	1,	1,	123.0000,	0),
(7,	1,	1,	125.0000,	1);

DROP TABLE IF EXISTS `cataloginventory_stock_status_idx`;
CREATE TABLE `cataloginventory_stock_status_idx` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `IDX_CATALOGINVENTORY_STOCK_STATUS_IDX_STOCK_ID` (`stock_id`),
  KEY `IDX_CATALOGINVENTORY_STOCK_STATUS_IDX_WEBSITE_ID` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status Indexer Idx';

INSERT INTO `cataloginventory_stock_status_idx` (`product_id`, `website_id`, `stock_id`, `qty`, `stock_status`) VALUES
(1,	1,	1,	100.0000,	1),
(2,	1,	1,	12.0000,	1),
(3,	1,	1,	123.0000,	1),
(4,	1,	1,	12.0000,	1),
(5,	1,	1,	123.0000,	1),
(6,	1,	1,	123.0000,	0),
(7,	1,	1,	125.0000,	1);

DROP TABLE IF EXISTS `cataloginventory_stock_status_tmp`;
CREATE TABLE `cataloginventory_stock_status_tmp` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `stock_status` smallint(5) unsigned NOT NULL COMMENT 'Stock Status',
  PRIMARY KEY (`product_id`,`website_id`,`stock_id`),
  KEY `IDX_CATALOGINVENTORY_STOCK_STATUS_TMP_STOCK_ID` (`stock_id`),
  KEY `IDX_CATALOGINVENTORY_STOCK_STATUS_TMP_WEBSITE_ID` (`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Cataloginventory Stock Status Indexer Tmp';


DROP TABLE IF EXISTS `catalogrule`;
CREATE TABLE `catalogrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From Date',
  `to_date` date DEFAULT NULL COMMENT 'To Date',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `sub_is_enable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Rule Enable For Subitems',
  `sub_simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action For Subitems',
  `sub_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount For Subitems',
  PRIMARY KEY (`rule_id`),
  KEY `IDX_CATALOGRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule';


DROP TABLE IF EXISTS `catalogrule_affected_product`;
CREATE TABLE `catalogrule_affected_product` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Affected Product';


DROP TABLE IF EXISTS `catalogrule_customer_group`;
CREATE TABLE `catalogrule_customer_group` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `IDX_CATALOGRULE_CUSTOMER_GROUP_RULE_ID` (`rule_id`),
  KEY `IDX_CATALOGRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  CONSTRAINT `FK_CATALOGRULE_CUSTOMER_GROUP_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Rules To Customer Groups Relations';


DROP TABLE IF EXISTS `catalogrule_group_website`;
CREATE TABLE `catalogrule_group_website` (
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`,`website_id`),
  KEY `IDX_CATALOGRULE_GROUP_WEBSITE_RULE_ID` (`rule_id`),
  KEY `IDX_CATALOGRULE_GROUP_WEBSITE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_CATALOGRULE_GROUP_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_CATALOGRULE_GROUP_WEBSITE_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGRULE_GROUP_WEBSITE_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATRULE_GROUP_WS_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Group Website';


DROP TABLE IF EXISTS `catalogrule_product`;
CREATE TABLE `catalogrule_product` (
  `rule_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Product Id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `from_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'From Time',
  `to_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'To time',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `action_operator` varchar(10) DEFAULT 'to_fixed' COMMENT 'Action Operator',
  `action_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Action Amount',
  `action_stop` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Action Stop',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `sub_simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action For Subitems',
  `sub_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount For Subitems',
  PRIMARY KEY (`rule_product_id`),
  UNIQUE KEY `EAA51B56FF092A0DCB795D1CEF812B7B` (`rule_id`,`from_time`,`to_time`,`website_id`,`customer_group_id`,`product_id`,`sort_order`),
  KEY `IDX_CATALOGRULE_PRODUCT_RULE_ID` (`rule_id`),
  KEY `IDX_CATALOGRULE_PRODUCT_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_CATALOGRULE_PRODUCT_WEBSITE_ID` (`website_id`),
  KEY `IDX_CATALOGRULE_PRODUCT_FROM_TIME` (`from_time`),
  KEY `IDX_CATALOGRULE_PRODUCT_TO_TIME` (`to_time`),
  KEY `IDX_CATALOGRULE_PRODUCT_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_CATALOGRULE_PRODUCT_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGRULE_PRODUCT_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATRULE_PRD_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATRULE_PRD_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Product';


DROP TABLE IF EXISTS `catalogrule_product_price`;
CREATE TABLE `catalogrule_product_price` (
  `rule_product_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Product PriceId',
  `rule_date` date NOT NULL COMMENT 'Rule Date',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `rule_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rule Price',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `latest_start_date` date DEFAULT NULL COMMENT 'Latest StartDate',
  `earliest_end_date` date DEFAULT NULL COMMENT 'Earliest EndDate',
  PRIMARY KEY (`rule_product_price_id`),
  UNIQUE KEY `UNQ_CATRULE_PRD_PRICE_RULE_DATE_WS_ID_CSTR_GROUP_ID_PRD_ID` (`rule_date`,`website_id`,`customer_group_id`,`product_id`),
  KEY `IDX_CATALOGRULE_PRODUCT_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_CATALOGRULE_PRODUCT_PRICE_WEBSITE_ID` (`website_id`),
  KEY `IDX_CATALOGRULE_PRODUCT_PRICE_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_CATALOGRULE_PRODUCT_PRICE_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATRULE_PRD_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATRULE_PRD_PRICE_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CatalogRule Product Price';


DROP TABLE IF EXISTS `catalogrule_website`;
CREATE TABLE `catalogrule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `IDX_CATALOGRULE_WEBSITE_RULE_ID` (`rule_id`),
  KEY `IDX_CATALOGRULE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_CATALOGRULE_WEBSITE_RULE_ID_CATALOGRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `catalogrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOGRULE_WEBSITE_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Rules To Websites Relations';


DROP TABLE IF EXISTS `catalogsearch_fulltext`;
CREATE TABLE `catalogsearch_fulltext` (
  `fulltext_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `data_index` longtext COMMENT 'Data index',
  PRIMARY KEY (`fulltext_id`),
  UNIQUE KEY `UNQ_CATALOGSEARCH_FULLTEXT_PRODUCT_ID_STORE_ID` (`product_id`,`store_id`),
  FULLTEXT KEY `FTI_CATALOGSEARCH_FULLTEXT_DATA_INDEX` (`data_index`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Catalog search result table';

INSERT INTO `catalogsearch_fulltext` (`fulltext_id`, `product_id`, `store_id`, `data_index`) VALUES
(13,	7,	1,	'test06|Taxable Goods|Lorem ipsum dolor 6|Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu. Tale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo. Melius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam. Ius cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no. Soleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.|Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.|123|1'),
(12,	5,	1,	'test04|Taxable Goods|Lorem ipsum dolor 4|Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu. Tale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo. Melius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam. Ius cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no. Soleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.|Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.|123|1'),
(11,	4,	1,	'test03|Taxable Goods|Lorem ipsum dolor 3|Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu. Tale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo. Melius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam. Ius cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no. Soleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.|Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.|123|1'),
(10,	3,	1,	'test2|Taxable Goods|Lorem ipsum dolor 1|Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu. Tale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo. Melius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam. Ius cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no. Soleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.|Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.|123|1'),
(8,	1,	1,	'00011|Taxable Goods|Complete Whitening + Scope|Get the stain removing power of Crest and a breath freshening blast of Scope — all in one tube. Crest Complete Multi-Benefit Whitening + Scope Striped Toothpastes are the only toothpastes that combine the whitening power of Crest toothpaste with the freshening power of Scope mouthwash. They fight cavities, prevent tartar, and provide cleaning action to help remove surface stains. • Whitens teeth by removing surface stains • Fights cavities • Fights tartar • Freshens breath|Get the stain removing power of Crest and a breath freshening blast of Scope — all in one tube.|1.3|1'),
(9,	2,	1,	'test01|Taxable Goods|Lorem ipsum dolor|Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu. Tale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo. Melius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam. Ius cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no. Soleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.|Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.|123|1');

DROP TABLE IF EXISTS `catalogsearch_query`;
CREATE TABLE `catalogsearch_query` (
  `query_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Query ID',
  `query_text` varchar(255) DEFAULT NULL COMMENT 'Query text',
  `num_results` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Num results',
  `popularity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Popularity',
  `redirect` varchar(255) DEFAULT NULL COMMENT 'Redirect',
  `synonym_for` varchar(255) DEFAULT NULL COMMENT 'Synonym for',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `display_in_terms` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Display in terms',
  `is_active` smallint(6) DEFAULT '1' COMMENT 'Active status',
  `is_processed` smallint(6) DEFAULT '0' COMMENT 'Processed status',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated at',
  PRIMARY KEY (`query_id`),
  KEY `IDX_CATALOGSEARCH_QUERY_QUERY_TEXT_STORE_ID_POPULARITY` (`query_text`,`store_id`,`popularity`),
  KEY `IDX_CATALOGSEARCH_QUERY_STORE_ID` (`store_id`),
  KEY `IDX_CATALOGSEARCH_QUERY_SYNONYM_FOR` (`synonym_for`),
  CONSTRAINT `FK_CATALOGSEARCH_QUERY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog search query table';

INSERT INTO `catalogsearch_query` (`query_id`, `query_text`, `num_results`, `popularity`, `redirect`, `synonym_for`, `store_id`, `display_in_terms`, `is_active`, `is_processed`, `updated_at`) VALUES
(1,	'complete',	1,	2,	NULL,	NULL,	1,	1,	1,	0,	'2016-12-31 16:55:44');

DROP TABLE IF EXISTS `catalogsearch_result`;
CREATE TABLE `catalogsearch_result` (
  `query_id` int(10) unsigned NOT NULL COMMENT 'Query ID',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product ID',
  `relevance` decimal(20,4) NOT NULL DEFAULT '0.0000' COMMENT 'Relevance',
  PRIMARY KEY (`query_id`,`product_id`),
  KEY `IDX_CATALOGSEARCH_RESULT_QUERY_ID` (`query_id`),
  KEY `IDX_CATALOGSEARCH_RESULT_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_CATALOGSEARCH_RESULT_QUERY_ID_CATALOGSEARCH_QUERY_QUERY_ID` FOREIGN KEY (`query_id`) REFERENCES `catalogsearch_query` (`query_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATSRCH_RESULT_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog search result table';


DROP TABLE IF EXISTS `catalog_category_anc_categs_index_idx`;
CREATE TABLE `catalog_category_anc_categs_index_idx` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `path` varchar(255) DEFAULT NULL COMMENT 'Path',
  KEY `IDX_CATALOG_CATEGORY_ANC_CATEGS_INDEX_IDX_CATEGORY_ID` (`category_id`),
  KEY `IDX_CATALOG_CATEGORY_ANC_CATEGS_INDEX_IDX_PATH_CATEGORY_ID` (`path`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Anchor Indexer Index Table';


DROP TABLE IF EXISTS `catalog_category_anc_categs_index_tmp`;
CREATE TABLE `catalog_category_anc_categs_index_tmp` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `path` varchar(255) DEFAULT NULL COMMENT 'Path',
  KEY `IDX_CATALOG_CATEGORY_ANC_CATEGS_INDEX_TMP_CATEGORY_ID` (`category_id`),
  KEY `IDX_CATALOG_CATEGORY_ANC_CATEGS_INDEX_TMP_PATH_CATEGORY_ID` (`path`,`category_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Category Anchor Indexer Temp Table';


DROP TABLE IF EXISTS `catalog_category_anc_products_index_idx`;
CREATE TABLE `catalog_category_anc_products_index_idx` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(10) unsigned DEFAULT NULL COMMENT 'Position',
  KEY `IDX_CAT_CTGR_ANC_PRDS_IDX_IDX_CTGR_ID_PRD_ID_POSITION` (`category_id`,`product_id`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Anchor Product Indexer Index Table';


DROP TABLE IF EXISTS `catalog_category_anc_products_index_tmp`;
CREATE TABLE `catalog_category_anc_products_index_tmp` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(10) unsigned DEFAULT NULL COMMENT 'Position',
  KEY `IDX_CAT_CTGR_ANC_PRDS_IDX_TMP_CTGR_ID_PRD_ID_POSITION` (`category_id`,`product_id`,`position`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Category Anchor Product Indexer Temp Table';


DROP TABLE IF EXISTS `catalog_category_entity`;
CREATE TABLE `catalog_category_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attriute Set ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Category ID',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Creation Time',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Update Time',
  `path` varchar(255) NOT NULL COMMENT 'Tree Path',
  `position` int(11) NOT NULL COMMENT 'Position',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT 'Tree Level',
  `children_count` int(11) NOT NULL COMMENT 'Child Count',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_LEVEL` (`level`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_PATH_ENTITY_ID` (`path`,`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Table';

INSERT INTO `catalog_category_entity` (`entity_id`, `entity_type_id`, `attribute_set_id`, `parent_id`, `created_at`, `updated_at`, `path`, `position`, `level`, `children_count`) VALUES
(1,	3,	0,	0,	'2016-12-14 06:01:01',	'2016-12-14 06:01:01',	'1',	0,	0,	12),
(2,	3,	3,	1,	'2016-12-14 06:01:01',	'2016-12-14 06:01:01',	'1/2',	1,	1,	11),
(3,	3,	3,	2,	'2016-12-16 21:01:25',	'2016-12-17 07:58:08',	'1/2/3',	7,	2,	0),
(4,	3,	3,	2,	'2016-12-17 07:58:26',	'2016-12-17 07:58:26',	'1/2/4',	8,	2,	0),
(5,	3,	3,	2,	'2016-12-17 07:58:40',	'2016-12-17 07:58:40',	'1/2/5',	9,	2,	0),
(6,	3,	3,	2,	'2016-12-17 07:58:51',	'2016-12-17 07:58:51',	'1/2/6',	10,	2,	0),
(7,	3,	3,	2,	'2016-12-17 07:59:06',	'2016-12-17 07:59:06',	'1/2/7',	11,	2,	0),
(8,	3,	3,	2,	'2016-12-17 07:59:44',	'2017-01-03 02:29:14',	'1/2/8',	1,	2,	0),
(9,	3,	3,	2,	'2016-12-17 08:00:00',	'2016-12-17 08:00:00',	'1/2/9',	2,	2,	0),
(10,	3,	3,	2,	'2016-12-17 08:00:24',	'2016-12-17 08:00:24',	'1/2/10',	6,	2,	0),
(11,	3,	3,	2,	'2016-12-17 08:00:39',	'2016-12-17 08:00:39',	'1/2/11',	3,	2,	0),
(12,	3,	3,	2,	'2016-12-17 08:00:51',	'2016-12-17 08:00:51',	'1/2/12',	4,	2,	0),
(13,	3,	3,	2,	'2016-12-17 08:01:06',	'2016-12-17 08:01:06',	'1/2/13',	5,	2,	0);

DROP TABLE IF EXISTS `catalog_category_entity_datetime`;
CREATE TABLE `catalog_category_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_CTGR_ENTT_DTIME_ENTT_TYPE_ID_ENTT_ID_ATTR_ID_STORE_ID` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_DATETIME_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_DATETIME_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_DATETIME_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_ENTT_DTIME_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Datetime Attribute Backend Table';

INSERT INTO `catalog_category_entity_datetime` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1,	3,	59,	0,	3,	NULL),
(2,	3,	60,	0,	3,	NULL),
(5,	3,	59,	0,	4,	NULL),
(6,	3,	60,	0,	4,	NULL),
(7,	3,	59,	0,	5,	NULL),
(8,	3,	60,	0,	5,	NULL),
(9,	3,	59,	0,	6,	NULL),
(10,	3,	60,	0,	6,	NULL),
(11,	3,	59,	0,	7,	NULL),
(12,	3,	60,	0,	7,	NULL),
(13,	3,	59,	0,	8,	NULL),
(14,	3,	60,	0,	8,	NULL),
(15,	3,	59,	0,	9,	NULL),
(16,	3,	60,	0,	9,	NULL),
(17,	3,	59,	0,	10,	NULL),
(18,	3,	60,	0,	10,	NULL),
(19,	3,	59,	0,	11,	NULL),
(20,	3,	60,	0,	11,	NULL),
(21,	3,	59,	0,	12,	NULL),
(22,	3,	60,	0,	12,	NULL),
(23,	3,	59,	0,	13,	NULL),
(24,	3,	60,	0,	13,	NULL);

DROP TABLE IF EXISTS `catalog_category_entity_decimal`;
CREATE TABLE `catalog_category_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` decimal(12,4) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_CTGR_ENTT_DEC_ENTT_TYPE_ID_ENTT_ID_ATTR_ID_STORE_ID` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_DECIMAL_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_DECIMAL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_ENTT_DEC_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Decimal Attribute Backend Table';

INSERT INTO `catalog_category_entity_decimal` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1,	3,	70,	0,	3,	NULL),
(2,	3,	70,	0,	4,	NULL),
(3,	3,	70,	0,	5,	NULL),
(4,	3,	70,	0,	6,	NULL),
(5,	3,	70,	0,	7,	NULL),
(6,	3,	70,	0,	8,	NULL),
(7,	3,	70,	0,	9,	NULL),
(8,	3,	70,	0,	10,	NULL),
(9,	3,	70,	0,	11,	NULL),
(10,	3,	70,	0,	12,	NULL),
(11,	3,	70,	0,	13,	NULL);

DROP TABLE IF EXISTS `catalog_category_entity_int`;
CREATE TABLE `catalog_category_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` int(11) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_CTGR_ENTT_INT_ENTT_TYPE_ID_ENTT_ID_ATTR_ID_STORE_ID` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_INT_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_INT_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_INT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_ENTT_INT_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Integer Attribute Backend Table';

INSERT INTO `catalog_category_entity_int` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1,	3,	67,	0,	1,	1),
(2,	3,	67,	1,	1,	1),
(3,	3,	42,	0,	2,	1),
(4,	3,	67,	0,	2,	1),
(5,	3,	42,	1,	2,	1),
(6,	3,	67,	1,	2,	1),
(7,	3,	42,	0,	3,	1),
(8,	3,	67,	0,	3,	1),
(9,	3,	50,	0,	3,	NULL),
(10,	3,	51,	0,	3,	0),
(11,	3,	68,	0,	3,	0),
(12,	3,	69,	0,	3,	0),
(14,	3,	42,	0,	4,	1),
(15,	3,	67,	0,	4,	1),
(16,	3,	50,	0,	4,	NULL),
(17,	3,	51,	0,	4,	0),
(18,	3,	68,	0,	4,	0),
(19,	3,	69,	0,	4,	0),
(20,	3,	42,	0,	5,	1),
(21,	3,	67,	0,	5,	1),
(22,	3,	50,	0,	5,	NULL),
(23,	3,	51,	0,	5,	0),
(24,	3,	68,	0,	5,	0),
(25,	3,	69,	0,	5,	0),
(26,	3,	42,	0,	6,	1),
(27,	3,	67,	0,	6,	1),
(28,	3,	50,	0,	6,	NULL),
(29,	3,	51,	0,	6,	0),
(30,	3,	68,	0,	6,	0),
(31,	3,	69,	0,	6,	0),
(32,	3,	42,	0,	7,	1),
(33,	3,	67,	0,	7,	1),
(34,	3,	50,	0,	7,	NULL),
(35,	3,	51,	0,	7,	0),
(36,	3,	68,	0,	7,	0),
(37,	3,	69,	0,	7,	0),
(38,	3,	42,	0,	8,	1),
(39,	3,	67,	0,	8,	1),
(40,	3,	50,	0,	8,	NULL),
(41,	3,	51,	0,	8,	0),
(42,	3,	68,	0,	8,	0),
(43,	3,	69,	0,	8,	0),
(44,	3,	42,	0,	9,	1),
(45,	3,	67,	0,	9,	1),
(46,	3,	50,	0,	9,	NULL),
(47,	3,	51,	0,	9,	0),
(48,	3,	68,	0,	9,	0),
(49,	3,	69,	0,	9,	0),
(50,	3,	42,	0,	10,	1),
(51,	3,	67,	0,	10,	1),
(52,	3,	50,	0,	10,	NULL),
(53,	3,	51,	0,	10,	0),
(54,	3,	68,	0,	10,	0),
(55,	3,	69,	0,	10,	0),
(56,	3,	42,	0,	11,	1),
(57,	3,	67,	0,	11,	1),
(58,	3,	50,	0,	11,	NULL),
(59,	3,	51,	0,	11,	0),
(60,	3,	68,	0,	11,	0),
(61,	3,	69,	0,	11,	0),
(62,	3,	42,	0,	12,	1),
(63,	3,	67,	0,	12,	1),
(64,	3,	50,	0,	12,	NULL),
(65,	3,	51,	0,	12,	0),
(66,	3,	68,	0,	12,	0),
(67,	3,	69,	0,	12,	0),
(68,	3,	42,	0,	13,	1),
(69,	3,	67,	0,	13,	1),
(70,	3,	50,	0,	13,	NULL),
(71,	3,	51,	0,	13,	0),
(72,	3,	68,	0,	13,	0),
(73,	3,	69,	0,	13,	0);

DROP TABLE IF EXISTS `catalog_category_entity_text`;
CREATE TABLE `catalog_category_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_CTGR_ENTT_TEXT_ENTT_TYPE_ID_ENTT_ID_ATTR_ID_STORE_ID` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_TEXT_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_TEXT_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_TEXT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_ENTT_TEXT_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Text Attribute Backend Table';

INSERT INTO `catalog_category_entity_text` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1,	3,	65,	0,	1,	NULL),
(2,	3,	65,	1,	1,	NULL),
(3,	3,	65,	0,	2,	NULL),
(4,	3,	65,	1,	2,	NULL),
(5,	3,	44,	0,	3,	NULL),
(6,	3,	47,	0,	3,	NULL),
(7,	3,	48,	0,	3,	NULL),
(8,	3,	62,	0,	3,	NULL),
(9,	3,	65,	0,	3,	NULL),
(15,	3,	44,	0,	4,	NULL),
(16,	3,	47,	0,	4,	NULL),
(17,	3,	48,	0,	4,	NULL),
(18,	3,	62,	0,	4,	NULL),
(19,	3,	65,	0,	4,	NULL),
(20,	3,	44,	0,	5,	NULL),
(21,	3,	47,	0,	5,	NULL),
(22,	3,	48,	0,	5,	NULL),
(23,	3,	62,	0,	5,	NULL),
(24,	3,	65,	0,	5,	NULL),
(25,	3,	44,	0,	6,	NULL),
(26,	3,	47,	0,	6,	NULL),
(27,	3,	48,	0,	6,	NULL),
(28,	3,	62,	0,	6,	NULL),
(29,	3,	65,	0,	6,	NULL),
(30,	3,	44,	0,	7,	NULL),
(31,	3,	47,	0,	7,	NULL),
(32,	3,	48,	0,	7,	NULL),
(33,	3,	62,	0,	7,	NULL),
(34,	3,	65,	0,	7,	NULL),
(35,	3,	44,	0,	8,	NULL),
(36,	3,	47,	0,	8,	NULL),
(37,	3,	48,	0,	8,	NULL),
(38,	3,	62,	0,	8,	NULL),
(39,	3,	65,	0,	8,	NULL),
(40,	3,	44,	0,	9,	NULL),
(41,	3,	47,	0,	9,	NULL),
(42,	3,	48,	0,	9,	NULL),
(43,	3,	62,	0,	9,	NULL),
(44,	3,	65,	0,	9,	NULL),
(45,	3,	44,	0,	10,	NULL),
(46,	3,	47,	0,	10,	NULL),
(47,	3,	48,	0,	10,	NULL),
(48,	3,	62,	0,	10,	NULL),
(49,	3,	65,	0,	10,	NULL),
(50,	3,	44,	0,	11,	NULL),
(51,	3,	47,	0,	11,	NULL),
(52,	3,	48,	0,	11,	NULL),
(53,	3,	62,	0,	11,	NULL),
(54,	3,	65,	0,	11,	NULL),
(55,	3,	44,	0,	12,	NULL),
(56,	3,	47,	0,	12,	NULL),
(57,	3,	48,	0,	12,	NULL),
(58,	3,	62,	0,	12,	NULL),
(59,	3,	65,	0,	12,	NULL),
(60,	3,	44,	0,	13,	NULL),
(61,	3,	47,	0,	13,	NULL),
(62,	3,	48,	0,	13,	NULL),
(63,	3,	62,	0,	13,	NULL),
(64,	3,	65,	0,	13,	NULL);

DROP TABLE IF EXISTS `catalog_category_entity_varchar`;
CREATE TABLE `catalog_category_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_CTGR_ENTT_VCHR_ENTT_TYPE_ID_ENTT_ID_ATTR_ID_STORE_ID` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_VARCHAR_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_CATEGORY_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CATALOG_CATEGORY_ENTITY_VARCHAR_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_ENTT_VCHR_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Varchar Attribute Backend Table';

INSERT INTO `catalog_category_entity_varchar` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1,	3,	41,	0,	1,	'Root Catalog'),
(2,	3,	41,	1,	1,	'Root Catalog'),
(3,	3,	43,	1,	1,	'root-catalog'),
(4,	3,	41,	0,	2,	'Default Category'),
(5,	3,	41,	1,	2,	'Default Category'),
(6,	3,	49,	1,	2,	'PRODUCTS'),
(7,	3,	43,	1,	2,	'default-category'),
(8,	3,	41,	0,	3,	'All Products'),
(9,	3,	43,	0,	3,	'all-products'),
(10,	3,	46,	0,	3,	NULL),
(11,	3,	49,	0,	3,	'PRODUCTS'),
(12,	3,	58,	0,	3,	NULL),
(13,	3,	61,	0,	3,	NULL),
(14,	3,	57,	1,	3,	'all-products'),
(15,	3,	57,	0,	3,	'all-products'),
(21,	3,	41,	0,	4,	'Best Sellers'),
(22,	3,	43,	0,	4,	'best-sellers'),
(23,	3,	46,	0,	4,	NULL),
(24,	3,	49,	0,	4,	'PRODUCTS'),
(25,	3,	58,	0,	4,	NULL),
(26,	3,	61,	0,	4,	NULL),
(27,	3,	57,	1,	4,	'best-sellers'),
(28,	3,	57,	0,	4,	'best-sellers'),
(29,	3,	41,	0,	5,	'Whats New'),
(30,	3,	43,	0,	5,	'whats-new'),
(31,	3,	46,	0,	5,	NULL),
(32,	3,	49,	0,	5,	'PRODUCTS'),
(33,	3,	58,	0,	5,	NULL),
(34,	3,	61,	0,	5,	NULL),
(35,	3,	57,	1,	5,	'whats-new'),
(36,	3,	57,	0,	5,	'whats-new'),
(37,	3,	41,	0,	6,	'On Sale'),
(38,	3,	43,	0,	6,	'on-sale'),
(39,	3,	46,	0,	6,	NULL),
(40,	3,	49,	0,	6,	'PRODUCTS'),
(41,	3,	58,	0,	6,	NULL),
(42,	3,	61,	0,	6,	NULL),
(43,	3,	57,	1,	6,	'on-sale'),
(44,	3,	57,	0,	6,	'on-sale'),
(45,	3,	41,	0,	7,	'All Express'),
(46,	3,	43,	0,	7,	'all-express'),
(47,	3,	46,	0,	7,	NULL),
(48,	3,	49,	0,	7,	'PRODUCTS'),
(49,	3,	58,	0,	7,	NULL),
(50,	3,	61,	0,	7,	NULL),
(51,	3,	57,	1,	7,	'all-express'),
(52,	3,	57,	0,	7,	'all-express'),
(53,	3,	41,	0,	8,	'Grocery'),
(54,	3,	43,	0,	8,	'grocery'),
(55,	3,	46,	0,	8,	NULL),
(56,	3,	49,	0,	8,	'PRODUCTS'),
(57,	3,	58,	0,	8,	NULL),
(58,	3,	61,	0,	8,	NULL),
(59,	3,	57,	1,	8,	'grocery'),
(60,	3,	57,	0,	8,	'grocery'),
(61,	3,	41,	0,	9,	'Beverage'),
(62,	3,	43,	0,	9,	'beverage'),
(63,	3,	46,	0,	9,	NULL),
(64,	3,	49,	0,	9,	'PRODUCTS'),
(65,	3,	58,	0,	9,	NULL),
(66,	3,	61,	0,	9,	NULL),
(67,	3,	57,	1,	9,	'beverage'),
(68,	3,	57,	0,	9,	'beverage'),
(69,	3,	41,	0,	10,	'Bath & Body'),
(70,	3,	43,	0,	10,	'bath-body'),
(71,	3,	46,	0,	10,	NULL),
(72,	3,	49,	0,	10,	'PRODUCTS'),
(73,	3,	58,	0,	10,	NULL),
(74,	3,	61,	0,	10,	NULL),
(75,	3,	57,	1,	10,	'bath-body'),
(76,	3,	57,	0,	10,	'bath-body'),
(77,	3,	41,	0,	11,	'Health'),
(78,	3,	43,	0,	11,	'health'),
(79,	3,	46,	0,	11,	NULL),
(80,	3,	49,	0,	11,	'PRODUCTS'),
(81,	3,	58,	0,	11,	NULL),
(82,	3,	61,	0,	11,	NULL),
(83,	3,	57,	1,	11,	'health'),
(84,	3,	57,	0,	11,	'health'),
(85,	3,	41,	0,	12,	'Cleaning Supplies'),
(86,	3,	43,	0,	12,	'cleaning-supplies'),
(87,	3,	46,	0,	12,	NULL),
(88,	3,	49,	0,	12,	'PRODUCTS'),
(89,	3,	58,	0,	12,	NULL),
(90,	3,	61,	0,	12,	NULL),
(91,	3,	57,	1,	12,	'cleaning-supplies'),
(92,	3,	57,	0,	12,	'cleaning-supplies'),
(93,	3,	41,	0,	13,	'Home Office'),
(94,	3,	43,	0,	13,	'home-office'),
(95,	3,	46,	0,	13,	NULL),
(96,	3,	49,	0,	13,	'PRODUCTS'),
(97,	3,	58,	0,	13,	NULL),
(98,	3,	61,	0,	13,	NULL),
(99,	3,	57,	1,	13,	'home-office'),
(100,	3,	57,	0,	13,	'home-office'),
(104,	3,	135,	0,	8,	'1.png');

DROP TABLE IF EXISTS `catalog_category_flat_store_1`;
CREATE TABLE `catalog_category_flat_store_1` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'entity_id',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'parent_id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'created_at',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'updated_at',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT 'path',
  `position` int(11) NOT NULL COMMENT 'position',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT 'level',
  `children_count` int(11) NOT NULL COMMENT 'children_count',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `all_children` text COMMENT 'All Children',
  `available_sort_by` text COMMENT 'Available Product Listing Sort By',
  `children` text COMMENT 'Children',
  `custom_apply_to_products` int(11) DEFAULT NULL COMMENT 'Apply To Products',
  `custom_design` varchar(255) DEFAULT NULL COMMENT 'Custom Design',
  `custom_design_from` datetime DEFAULT NULL COMMENT 'Active From',
  `custom_design_to` datetime DEFAULT NULL COMMENT 'Active To',
  `custom_layout_update` text COMMENT 'Custom Layout Update',
  `custom_use_parent_settings` int(11) DEFAULT NULL COMMENT 'Use Parent Category Settings',
  `default_sort_by` varchar(255) DEFAULT NULL COMMENT 'Default Product Listing Sort By',
  `description` text COMMENT 'Description',
  `display_mode` varchar(255) DEFAULT NULL COMMENT 'Display Mode',
  `filter_price_range` decimal(12,4) DEFAULT NULL COMMENT 'Layered Navigation Price Step',
  `image` varchar(255) DEFAULT NULL COMMENT 'Image',
  `include_in_menu` int(11) DEFAULT NULL COMMENT 'Include in Navigation Menu',
  `is_active` int(11) DEFAULT NULL COMMENT 'Is Active',
  `is_anchor` int(11) DEFAULT NULL COMMENT 'Is Anchor',
  `landing_page` int(11) DEFAULT NULL COMMENT 'CMS Block',
  `meta_description` text COMMENT 'Meta Description',
  `meta_keywords` text COMMENT 'Meta Keywords',
  `meta_title` varchar(255) DEFAULT NULL COMMENT 'Page Title',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `page_layout` varchar(255) DEFAULT NULL COMMENT 'Page Layout',
  `path_in_store` text COMMENT 'Path In Store',
  `url_key` varchar(255) DEFAULT NULL COMMENT 'URL Key',
  `url_path` varchar(255) DEFAULT NULL COMMENT 'Url Path',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_CATALOG_CATEGORY_FLAT_STORE_1_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_CATEGORY_FLAT_STORE_1_PATH` (`path`),
  KEY `IDX_CATALOG_CATEGORY_FLAT_STORE_1_LEVEL` (`level`),
  CONSTRAINT `FK_CATALOG_CATEGORY_FLAT_STORE_1_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_FLAT_STORE_1_ENTT_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Flat (Store 1)';

INSERT INTO `catalog_category_flat_store_1` (`entity_id`, `parent_id`, `created_at`, `updated_at`, `path`, `position`, `level`, `children_count`, `store_id`, `all_children`, `available_sort_by`, `children`, `custom_apply_to_products`, `custom_design`, `custom_design_from`, `custom_design_to`, `custom_layout_update`, `custom_use_parent_settings`, `default_sort_by`, `description`, `display_mode`, `filter_price_range`, `image`, `include_in_menu`, `is_active`, `is_anchor`, `landing_page`, `meta_description`, `meta_keywords`, `meta_title`, `name`, `page_layout`, `path_in_store`, `url_key`, `url_path`) VALUES
(1,	0,	'2016-12-14 06:01:01',	'2016-12-14 06:01:01',	'1',	0,	0,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'Root Catalog',	NULL,	NULL,	'root-catalog',	NULL),
(2,	1,	'2016-12-14 06:01:01',	'2016-12-14 06:01:01',	'1/2',	1,	1,	0,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'PRODUCTS',	NULL,	NULL,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	'Default Category',	NULL,	NULL,	'default-category',	NULL);

DROP TABLE IF EXISTS `catalog_category_product`;
CREATE TABLE `catalog_category_product` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`category_id`,`product_id`),
  KEY `IDX_CATALOG_CATEGORY_PRODUCT_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_CAT_CTGR_PRD_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_PRD_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Category Linkage Table';

INSERT INTO `catalog_category_product` (`category_id`, `product_id`, `position`) VALUES
(3,	1,	1),
(3,	2,	1),
(3,	3,	1),
(3,	4,	1),
(3,	5,	1),
(3,	6,	1),
(3,	7,	1),
(8,	2,	1),
(8,	3,	1),
(8,	4,	1),
(8,	5,	1),
(8,	6,	1),
(8,	7,	1);

DROP TABLE IF EXISTS `catalog_category_product_index`;
CREATE TABLE `catalog_category_product_index` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) DEFAULT NULL COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  PRIMARY KEY (`category_id`,`product_id`,`store_id`),
  KEY `IDX_CAT_CTGR_PRD_IDX_PRD_ID_STORE_ID_CTGR_ID_VISIBILITY` (`product_id`,`store_id`,`category_id`,`visibility`),
  KEY `15D3C269665C74C2219037D534F4B0DC` (`store_id`,`category_id`,`visibility`,`is_parent`,`position`),
  CONSTRAINT `FK_CATALOG_CATEGORY_PRODUCT_INDEX_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_PRD_IDX_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CTGR_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Index';

INSERT INTO `catalog_category_product_index` (`category_id`, `product_id`, `position`, `is_parent`, `store_id`, `visibility`) VALUES
(3,	1,	1,	1,	1,	4),
(3,	2,	1,	1,	1,	4),
(3,	3,	1,	1,	1,	4),
(3,	4,	1,	1,	1,	4),
(3,	5,	1,	1,	1,	4),
(3,	7,	1,	1,	1,	4),
(8,	2,	1,	1,	1,	4),
(8,	3,	1,	1,	1,	4),
(8,	4,	1,	1,	1,	4),
(8,	5,	1,	1,	1,	4),
(8,	7,	1,	1,	1,	4);

DROP TABLE IF EXISTS `catalog_category_product_index_enbl_idx`;
CREATE TABLE `catalog_category_product_index_enbl_idx` (
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `visibility` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Visibility',
  KEY `IDX_CAT_CTGR_PRD_IDX_ENBL_IDX_PRD_ID_VISIBILITY` (`product_id`,`visibility`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Enabled Indexer Index Table';


DROP TABLE IF EXISTS `catalog_category_product_index_enbl_tmp`;
CREATE TABLE `catalog_category_product_index_enbl_tmp` (
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `visibility` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Visibility',
  KEY `IDX_CAT_CTGR_PRD_IDX_ENBL_TMP_PRD_ID_VISIBILITY` (`product_id`,`visibility`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Enabled Indexer Temp Table';


DROP TABLE IF EXISTS `catalog_category_product_index_idx`;
CREATE TABLE `catalog_category_product_index_idx` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  KEY `IDX_CAT_CTGR_PRD_IDX_IDX_PRD_ID_CTGR_ID_STORE_ID` (`product_id`,`category_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Indexer Index Table';


DROP TABLE IF EXISTS `catalog_category_product_index_tmp`;
CREATE TABLE `catalog_category_product_index_tmp` (
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Category ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Parent',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL COMMENT 'Visibility',
  KEY `IDX_CAT_CTGR_PRD_IDX_TMP_PRD_ID_CTGR_ID_STORE_ID` (`product_id`,`category_id`,`store_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Category Product Indexer Temp Table';


DROP TABLE IF EXISTS `catalog_compare_item`;
CREATE TABLE `catalog_compare_item` (
  `catalog_compare_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Compare Item ID',
  `visitor_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Visitor ID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store ID',
  PRIMARY KEY (`catalog_compare_item_id`),
  KEY `IDX_CATALOG_COMPARE_ITEM_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_CATALOG_COMPARE_ITEM_PRODUCT_ID` (`product_id`),
  KEY `IDX_CATALOG_COMPARE_ITEM_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  KEY `IDX_CATALOG_COMPARE_ITEM_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `IDX_CATALOG_COMPARE_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CATALOG_COMPARE_ITEM_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CATALOG_COMPARE_ITEM_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_CMP_ITEM_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Compare Table';


DROP TABLE IF EXISTS `catalog_eav_attribute`;
CREATE TABLE `catalog_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `frontend_input_renderer` varchar(255) DEFAULT NULL COMMENT 'Frontend Input Renderer',
  `is_global` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Global',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `is_searchable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable',
  `is_filterable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable',
  `is_comparable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Comparable',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `is_html_allowed_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is HTML Allowed On Front',
  `is_used_for_price_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Price Rules',
  `is_filterable_in_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable In Search',
  `used_in_product_listing` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used In Product Listing',
  `used_for_sort_by` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Sorting',
  `is_configurable` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Configurable',
  `apply_to` varchar(255) DEFAULT NULL COMMENT 'Apply To',
  `is_visible_in_advanced_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible In Advanced Search',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_wysiwyg_enabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is WYSIWYG Enabled',
  `is_used_for_promo_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Promo Rules',
  PRIMARY KEY (`attribute_id`),
  KEY `IDX_CATALOG_EAV_ATTRIBUTE_USED_FOR_SORT_BY` (`used_for_sort_by`),
  KEY `IDX_CATALOG_EAV_ATTRIBUTE_USED_IN_PRODUCT_LISTING` (`used_in_product_listing`),
  CONSTRAINT `FK_CATALOG_EAV_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog EAV Attribute Table';

INSERT INTO `catalog_eav_attribute` (`attribute_id`, `frontend_input_renderer`, `is_global`, `is_visible`, `is_searchable`, `is_filterable`, `is_comparable`, `is_visible_on_front`, `is_html_allowed_on_front`, `is_used_for_price_rules`, `is_filterable_in_search`, `used_in_product_listing`, `used_for_sort_by`, `is_configurable`, `apply_to`, `is_visible_in_advanced_search`, `position`, `is_wysiwyg_enabled`, `is_used_for_promo_rules`) VALUES
(41,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(42,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(43,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(44,	NULL,	0,	1,	0,	0,	0,	0,	1,	0,	0,	0,	0,	1,	NULL,	0,	0,	1,	0),
(45,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(46,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(47,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(48,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(49,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(50,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(51,	NULL,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(52,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(53,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(54,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(55,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(56,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(57,	NULL,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(58,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(59,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(60,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(61,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(62,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(63,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(64,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(65,	'adminhtml/catalog_category_helper_sortby_available',	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(66,	'adminhtml/catalog_category_helper_sortby_default',	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(67,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(68,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(69,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(70,	'adminhtml/catalog_category_helper_pricestep',	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(71,	NULL,	0,	1,	1,	0,	0,	0,	0,	0,	0,	1,	1,	1,	NULL,	1,	0,	0,	0),
(72,	NULL,	0,	1,	1,	0,	1,	0,	1,	0,	0,	0,	0,	1,	NULL,	1,	0,	1,	0),
(73,	NULL,	0,	1,	1,	0,	1,	0,	1,	0,	0,	1,	0,	1,	NULL,	1,	0,	1,	0),
(74,	NULL,	1,	1,	1,	0,	1,	0,	0,	0,	0,	0,	0,	1,	NULL,	1,	0,	0,	0),
(75,	NULL,	2,	1,	1,	1,	0,	0,	0,	0,	0,	1,	1,	1,	'simple,configurable,virtual,bundle,downloadable',	1,	0,	0,	0),
(76,	NULL,	2,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	'simple,configurable,virtual,bundle,downloadable',	0,	0,	0,	0),
(77,	NULL,	2,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	'simple,configurable,virtual,bundle,downloadable',	0,	0,	0,	0),
(78,	NULL,	2,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	'simple,configurable,virtual,bundle,downloadable',	0,	0,	0,	0),
(79,	NULL,	2,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	'virtual,downloadable',	0,	0,	0,	0),
(80,	NULL,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	'simple,bundle',	0,	0,	0,	0),
(81,	NULL,	1,	1,	1,	1,	1,	0,	0,	0,	0,	0,	0,	1,	'simple',	1,	0,	0,	0),
(82,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(83,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(84,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(85,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(86,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	NULL,	0,	0,	0,	0),
(87,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	NULL,	0,	0,	0,	0),
(88,	NULL,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(89,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(90,	NULL,	2,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	'simple,configurable,virtual,bundle,downloadable',	0,	0,	0,	0),
(91,	NULL,	2,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	'simple,configurable,virtual,bundle,downloadable',	0,	0,	0,	0),
(92,	NULL,	1,	1,	1,	1,	1,	0,	0,	0,	0,	0,	0,	1,	'simple',	1,	0,	0,	0),
(93,	NULL,	2,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	NULL,	0,	0,	0,	0),
(94,	NULL,	2,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	NULL,	0,	0,	0,	0),
(95,	NULL,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(96,	NULL,	2,	1,	1,	0,	0,	0,	0,	0,	0,	1,	0,	1,	NULL,	0,	0,	0,	0),
(97,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	NULL,	0,	0,	0,	0),
(98,	NULL,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(99,	NULL,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	'simple,configurable,virtual,bundle,downloadable',	0,	0,	0,	0),
(100,	NULL,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	'simple,virtual',	0,	0,	0,	0),
(101,	NULL,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	'simple,virtual',	0,	0,	0,	0),
(102,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(103,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(104,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(105,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(106,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(107,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(108,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(109,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(110,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	NULL,	0,	0,	0,	0),
(111,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(112,	NULL,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	NULL,	0,	0,	0,	0),
(113,	NULL,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	NULL,	0,	0,	0,	0),
(114,	NULL,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	NULL,	0,	0,	0,	0),
(115,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(116,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(117,	NULL,	2,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	'simple,configurable,bundle,grouped',	0,	0,	0,	0),
(118,	'adminhtml/catalog_product_helper_form_msrp_enabled',	2,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	'simple,bundle,configurable,virtual,downloadable',	0,	0,	0,	0),
(119,	'adminhtml/catalog_product_helper_form_msrp_price',	2,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	'simple,bundle,configurable,virtual,downloadable',	0,	0,	0,	0),
(120,	NULL,	2,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	1,	'simple,bundle,configurable,virtual,downloadable',	0,	0,	0,	0),
(121,	NULL,	2,	1,	1,	0,	0,	0,	0,	0,	0,	1,	0,	1,	'simple,configurable,virtual,downloadable,bundle',	1,	0,	0,	0),
(122,	'giftmessage/adminhtml_product_helper_form_config',	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	NULL,	0,	0,	0,	0),
(123,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	'bundle',	0,	0,	0,	0),
(124,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	'bundle',	0,	0,	0,	0),
(125,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	'bundle',	0,	0,	0,	0),
(126,	NULL,	1,	1,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	'bundle',	0,	0,	0,	0),
(127,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	'bundle',	0,	0,	0,	0),
(128,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	'downloadable',	0,	0,	0,	0),
(129,	NULL,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	'downloadable',	0,	0,	0,	0),
(130,	NULL,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	'downloadable',	0,	0,	0,	0),
(131,	NULL,	1,	0,	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	'downloadable',	0,	0,	0,	0),
(132,	NULL,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(133,	NULL,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(134,	NULL,	1,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0),
(135,	NULL,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,	1,	NULL,	0,	0,	0,	0);

DROP TABLE IF EXISTS `catalog_product_bundle_option`;
CREATE TABLE `catalog_product_bundle_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `required` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Required',
  `position` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  `type` varchar(255) DEFAULT NULL COMMENT 'Type',
  PRIMARY KEY (`option_id`),
  KEY `IDX_CATALOG_PRODUCT_BUNDLE_OPTION_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_CAT_PRD_BNDL_OPT_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option';


DROP TABLE IF EXISTS `catalog_product_bundle_option_value`;
CREATE TABLE `catalog_product_bundle_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CATALOG_PRODUCT_BUNDLE_OPTION_VALUE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  CONSTRAINT `FK_CAT_PRD_BNDL_OPT_VAL_OPT_ID_CAT_PRD_BNDL_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option Value';


DROP TABLE IF EXISTS `catalog_product_bundle_price_index`;
CREATE TABLE `catalog_product_bundle_price_index` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `min_price` decimal(12,4) NOT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) NOT NULL COMMENT 'Max Price',
  PRIMARY KEY (`entity_id`,`website_id`,`customer_group_id`),
  KEY `IDX_CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_WEBSITE_ID` (`website_id`),
  KEY `IDX_CATALOG_PRODUCT_BUNDLE_PRICE_INDEX_CUSTOMER_GROUP_ID` (`customer_group_id`),
  CONSTRAINT `FK_CAT_PRD_BNDL_PRICE_IDX_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_BNDL_PRICE_IDX_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_BNDL_PRICE_IDX_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Price Index';


DROP TABLE IF EXISTS `catalog_product_bundle_selection`;
CREATE TABLE `catalog_product_bundle_selection` (
  `selection_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Selection Id',
  `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
  `parent_product_id` int(10) unsigned NOT NULL COMMENT 'Parent Product Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `position` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_default` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Default',
  `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Price Type',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Selection Price Value',
  `selection_qty` decimal(12,4) DEFAULT NULL COMMENT 'Selection Qty',
  `selection_can_change_qty` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Selection Can Change Qty',
  PRIMARY KEY (`selection_id`),
  KEY `IDX_CATALOG_PRODUCT_BUNDLE_SELECTION_OPTION_ID` (`option_id`),
  KEY `IDX_CATALOG_PRODUCT_BUNDLE_SELECTION_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_CAT_PRD_BNDL_SELECTION_OPT_ID_CAT_PRD_BNDL_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_bundle_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_BNDL_SELECTION_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection';


DROP TABLE IF EXISTS `catalog_product_bundle_selection_price`;
CREATE TABLE `catalog_product_bundle_selection_price` (
  `selection_id` int(10) unsigned NOT NULL COMMENT 'Selection Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Price Type',
  `selection_price_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Selection Price Value',
  PRIMARY KEY (`selection_id`,`website_id`),
  KEY `IDX_CATALOG_PRODUCT_BUNDLE_SELECTION_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_CAT_PRD_BNDL_SELECTION_PRICE_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DCF37523AA05D770A70AA4ED7C2616E4` FOREIGN KEY (`selection_id`) REFERENCES `catalog_product_bundle_selection` (`selection_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection Price';


DROP TABLE IF EXISTS `catalog_product_bundle_stock_index`;
CREATE TABLE `catalog_product_bundle_stock_index` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `stock_id` smallint(5) unsigned NOT NULL COMMENT 'Stock Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `stock_status` smallint(6) DEFAULT '0' COMMENT 'Stock Status',
  PRIMARY KEY (`entity_id`,`website_id`,`stock_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Stock Index';


DROP TABLE IF EXISTS `catalog_product_enabled_index`;
CREATE TABLE `catalog_product_enabled_index` (
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `visibility` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Visibility',
  PRIMARY KEY (`product_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENABLED_INDEX_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENABLED_INDEX_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENABLED_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Visibility Index Table';


DROP TABLE IF EXISTS `catalog_product_entity`;
CREATE TABLE `catalog_product_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set ID',
  `type_id` varchar(32) NOT NULL DEFAULT 'simple' COMMENT 'Type ID',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `has_options` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Has Options',
  `required_options` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Required Options',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Creation Time',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Update Time',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_ATTRIBUTE_SET_ID` (`attribute_set_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_SKU` (`sku`),
  CONSTRAINT `FK_CAT_PRD_ENTT_ATTR_SET_ID_EAV_ATTR_SET_ATTR_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Table';

INSERT INTO `catalog_product_entity` (`entity_id`, `entity_type_id`, `attribute_set_id`, `type_id`, `sku`, `has_options`, `required_options`, `created_at`, `updated_at`) VALUES
(1,	4,	4,	'simple',	'00011',	0,	0,	'2016-12-17 07:47:52',	'2016-12-17 07:47:52'),
(2,	4,	4,	'simple',	'test01',	0,	0,	'2017-01-02 22:39:53',	'2017-01-02 22:41:56'),
(3,	4,	4,	'simple',	'test2',	0,	0,	'2017-01-02 22:42:26',	'2017-01-02 22:45:54'),
(4,	4,	4,	'simple',	'test03',	0,	0,	'2017-01-02 22:44:34',	'2017-01-02 22:45:27'),
(5,	4,	4,	'simple',	'test04',	0,	0,	'2017-01-02 22:46:12',	'2017-01-02 22:46:55'),
(6,	4,	4,	'simple',	'test05',	0,	0,	'2017-01-02 22:47:01',	'2017-01-02 22:47:33'),
(7,	4,	4,	'simple',	'test06',	0,	0,	'2017-01-02 22:47:39',	'2017-01-02 22:48:08');

DROP TABLE IF EXISTS `catalog_product_entity_datetime`;
CREATE TABLE `catalog_product_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` datetime DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_PRD_ENTT_DTIME_ENTT_ID_ATTR_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_DATETIME_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_DATETIME_ENTITY_ID` (`entity_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_DATETIME_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_DTIME_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Datetime Attribute Backend Table';

INSERT INTO `catalog_product_entity_datetime` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1,	4,	93,	0,	1,	NULL),
(2,	4,	94,	0,	1,	NULL),
(3,	4,	77,	0,	1,	'2016-12-17 00:00:00'),
(4,	4,	78,	0,	1,	NULL),
(5,	4,	104,	0,	1,	NULL),
(6,	4,	105,	0,	1,	NULL),
(7,	4,	93,	0,	2,	NULL),
(8,	4,	94,	0,	2,	NULL),
(9,	4,	77,	0,	2,	NULL),
(10,	4,	78,	0,	2,	NULL),
(11,	4,	104,	0,	2,	NULL),
(12,	4,	105,	0,	2,	NULL),
(31,	4,	77,	0,	3,	NULL),
(32,	4,	78,	0,	3,	NULL),
(33,	4,	93,	0,	3,	NULL),
(34,	4,	94,	0,	3,	NULL),
(35,	4,	104,	0,	3,	NULL),
(36,	4,	105,	0,	3,	NULL),
(43,	4,	77,	0,	4,	NULL),
(44,	4,	78,	0,	4,	NULL),
(45,	4,	93,	0,	4,	NULL),
(46,	4,	94,	0,	4,	NULL),
(47,	4,	104,	0,	4,	NULL),
(48,	4,	105,	0,	4,	NULL),
(61,	4,	77,	0,	5,	NULL),
(62,	4,	78,	0,	5,	NULL),
(63,	4,	93,	0,	5,	NULL),
(64,	4,	94,	0,	5,	NULL),
(65,	4,	104,	0,	5,	NULL),
(66,	4,	105,	0,	5,	NULL),
(73,	4,	77,	0,	6,	NULL),
(74,	4,	78,	0,	6,	NULL),
(75,	4,	93,	0,	6,	NULL),
(76,	4,	94,	0,	6,	NULL),
(77,	4,	104,	0,	6,	NULL),
(78,	4,	105,	0,	6,	NULL),
(85,	4,	77,	0,	7,	NULL),
(86,	4,	78,	0,	7,	NULL),
(87,	4,	93,	0,	7,	NULL),
(88,	4,	94,	0,	7,	NULL),
(89,	4,	104,	0,	7,	NULL),
(90,	4,	105,	0,	7,	NULL);

DROP TABLE IF EXISTS `catalog_product_entity_decimal`;
CREATE TABLE `catalog_product_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` decimal(12,4) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_PRD_ENTT_DEC_ENTT_ID_ATTR_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_DECIMAL_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_DECIMAL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_DEC_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Decimal Attribute Backend Table';

INSERT INTO `catalog_product_entity_decimal` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1,	4,	80,	0,	1,	1.0000),
(2,	4,	75,	0,	1,	1.3000),
(3,	4,	76,	0,	1,	1.0000),
(4,	4,	120,	0,	1,	NULL),
(5,	4,	80,	0,	2,	1.0000),
(6,	4,	75,	0,	2,	123.0000),
(7,	4,	76,	0,	2,	NULL),
(8,	4,	120,	0,	2,	NULL),
(18,	4,	80,	0,	3,	1.0000),
(19,	4,	75,	0,	3,	123.0000),
(20,	4,	76,	0,	3,	NULL),
(21,	4,	120,	0,	3,	NULL),
(25,	4,	75,	0,	4,	123.0000),
(26,	4,	76,	0,	4,	NULL),
(27,	4,	80,	0,	4,	1.0000),
(28,	4,	120,	0,	4,	NULL),
(35,	4,	75,	0,	5,	123.0000),
(36,	4,	76,	0,	5,	NULL),
(37,	4,	80,	0,	5,	1.0000),
(38,	4,	120,	0,	5,	NULL),
(42,	4,	75,	0,	6,	123.0000),
(43,	4,	76,	0,	6,	NULL),
(44,	4,	80,	0,	6,	1.0000),
(45,	4,	120,	0,	6,	NULL),
(49,	4,	75,	0,	7,	123.0000),
(50,	4,	76,	0,	7,	NULL),
(51,	4,	80,	0,	7,	1.0000),
(52,	4,	120,	0,	7,	NULL);

DROP TABLE IF EXISTS `catalog_product_entity_gallery`;
CREATE TABLE `catalog_product_entity_gallery` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_PRD_ENTT_GLR_ENTT_TYPE_ID_ENTT_ID_ATTR_ID_STORE_ID` (`entity_type_id`,`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_GALLERY_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_GALLERY_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_GALLERY_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_GALLERY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_GLR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Gallery Attribute Backend Table';


DROP TABLE IF EXISTS `catalog_product_entity_group_price`;
CREATE TABLE `catalog_product_entity_group_price` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `all_groups` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Applicable To All Customer Groups',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group ID',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `is_percent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Percent',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `CC12C83765B562314470A24F2BDD0F36` (`entity_id`,`all_groups`,`customer_group_id`,`website_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_GROUP_PRICE_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_GROUP_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_GROUP_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_CAT_PRD_ENTT_GROUP_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_GROUP_PRICE_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DF909D22C11B60B1E5E3EE64AB220ECE` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Group Price Attribute Backend Table';


DROP TABLE IF EXISTS `catalog_product_entity_int`;
CREATE TABLE `catalog_product_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` int(11) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CATALOG_PRODUCT_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_INT_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_INT_ENTITY_ID` (`entity_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_INT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_INT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Integer Attribute Backend Table';

INSERT INTO `catalog_product_entity_int` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1,	4,	96,	0,	1,	1),
(2,	4,	102,	0,	1,	4),
(3,	4,	132,	0,	1,	0),
(4,	4,	133,	0,	1,	0),
(5,	4,	121,	0,	1,	2),
(6,	4,	100,	0,	1,	0),
(7,	4,	134,	0,	1,	0),
(8,	4,	96,	0,	2,	1),
(9,	4,	102,	0,	2,	4),
(10,	4,	132,	0,	2,	1),
(11,	4,	133,	0,	2,	1),
(12,	4,	121,	0,	2,	2),
(13,	4,	100,	0,	2,	0),
(14,	4,	134,	0,	2,	0),
(15,	4,	96,	0,	3,	1),
(16,	4,	100,	0,	3,	0),
(17,	4,	102,	0,	3,	4),
(18,	4,	121,	0,	3,	2),
(19,	4,	132,	0,	3,	1),
(20,	4,	133,	0,	3,	1),
(21,	4,	134,	0,	3,	0),
(23,	4,	96,	0,	4,	1),
(24,	4,	100,	0,	4,	0),
(25,	4,	102,	0,	4,	4),
(26,	4,	121,	0,	4,	2),
(27,	4,	132,	0,	4,	1),
(28,	4,	133,	0,	4,	1),
(29,	4,	134,	0,	4,	0),
(31,	4,	96,	0,	5,	1),
(32,	4,	100,	0,	5,	0),
(33,	4,	102,	0,	5,	4),
(34,	4,	121,	0,	5,	2),
(35,	4,	132,	0,	5,	1),
(36,	4,	133,	0,	5,	1),
(37,	4,	134,	0,	5,	0),
(39,	4,	96,	0,	6,	2),
(40,	4,	100,	0,	6,	0),
(41,	4,	102,	0,	6,	4),
(42,	4,	121,	0,	6,	2),
(43,	4,	132,	0,	6,	1),
(44,	4,	133,	0,	6,	1),
(45,	4,	134,	0,	6,	0),
(46,	4,	96,	0,	7,	1),
(47,	4,	100,	0,	7,	0),
(48,	4,	102,	0,	7,	4),
(49,	4,	121,	0,	7,	2),
(50,	4,	132,	0,	7,	1),
(51,	4,	133,	0,	7,	1),
(52,	4,	134,	0,	7,	0);

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery`;
CREATE TABLE `catalog_product_entity_media_gallery` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_ENTITY_ID` (`entity_id`),
  CONSTRAINT `FK_CAT_PRD_ENTT_MDA_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_MDA_GLR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Media Gallery Attribute Backend Table';

INSERT INTO `catalog_product_entity_media_gallery` (`value_id`, `attribute_id`, `entity_id`, `value`) VALUES
(1,	88,	1,	'/1/4/1452979393579.jpg'),
(2,	88,	1,	'/1/4/1452979394059.jpg'),
(3,	88,	1,	'/1/4/1452979395283.jpg'),
(4,	88,	1,	'/1/4/1452979397626.jpg'),
(5,	88,	1,	'/1/4/1452979400151.jpg'),
(6,	88,	1,	'/1/4/1452979402923.jpg'),
(7,	88,	1,	'/1/4/1452979407794.jpg'),
(8,	88,	2,	'/1/4/1429479594418.jpg'),
(9,	88,	2,	'/1/4/1429479597924.jpg'),
(12,	88,	3,	'/1/4/1436644265094.jpg'),
(13,	88,	3,	'/1/4/1436644266678.jpg'),
(16,	88,	4,	'/1/4/1437844866728.jpg'),
(17,	88,	4,	'/1/4/1437844868306.jpg'),
(20,	88,	5,	'/1/4/1441503569579.jpg'),
(21,	88,	5,	'/1/4/1441503569821.jpg'),
(24,	88,	6,	'/1/4/1469294152345.jpg'),
(25,	88,	6,	'/1/4/1469294152484.jpg'),
(26,	88,	7,	'/1/4/1469294152345_1.jpg'),
(27,	88,	7,	'/1/4/1469294152484_1.jpg');

DROP TABLE IF EXISTS `catalog_product_entity_media_gallery_value`;
CREATE TABLE `catalog_product_entity_media_gallery_value` (
  `value_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Value ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  `position` int(10) unsigned DEFAULT NULL COMMENT 'Position',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Disabled',
  PRIMARY KEY (`value_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CAT_PRD_ENTT_MDA_GLR_VAL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_MDA_GLR_VAL_VAL_ID_CAT_PRD_ENTT_MDA_GLR_VAL_ID` FOREIGN KEY (`value_id`) REFERENCES `catalog_product_entity_media_gallery` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Media Gallery Attribute Value Table';

INSERT INTO `catalog_product_entity_media_gallery_value` (`value_id`, `store_id`, `label`, `position`, `disabled`) VALUES
(1,	0,	NULL,	1,	0),
(2,	0,	NULL,	2,	0),
(3,	0,	NULL,	3,	0),
(4,	0,	NULL,	4,	1),
(5,	0,	NULL,	5,	0),
(6,	0,	NULL,	6,	0),
(7,	0,	NULL,	7,	0),
(8,	0,	NULL,	1,	0),
(9,	0,	NULL,	2,	0),
(12,	0,	NULL,	3,	0),
(13,	0,	NULL,	4,	0),
(16,	0,	NULL,	5,	0),
(17,	0,	NULL,	6,	0),
(20,	0,	NULL,	7,	0),
(21,	0,	NULL,	8,	0),
(24,	0,	NULL,	9,	0),
(25,	0,	NULL,	10,	0),
(26,	0,	NULL,	9,	0),
(27,	0,	NULL,	10,	0);

DROP TABLE IF EXISTS `catalog_product_entity_text`;
CREATE TABLE `catalog_product_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CATALOG_PRODUCT_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_TEXT_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_TEXT_ENTITY_ID` (`entity_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_TEXT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_TEXT_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Text Attribute Backend Table';

INSERT INTO `catalog_product_entity_text` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1,	4,	72,	0,	1,	'Get the stain removing power of Crest and a breath freshening blast of Scope — all in one tube.\r\n\r\nCrest Complete Multi-Benefit Whitening + Scope Striped Toothpastes are the only toothpastes that combine the whitening power of Crest toothpaste with the freshening power of Scope mouthwash. They fight cavities, prevent tartar, and provide cleaning action to help remove surface stains.\r\n\r\n• Whitens teeth by removing surface stains\r\n• Fights cavities\r\n• Fights tartar\r\n• Freshens breath'),
(2,	4,	73,	0,	1,	'Get the stain removing power of Crest and a breath freshening blast of Scope — all in one tube.'),
(3,	4,	83,	0,	1,	NULL),
(4,	4,	106,	0,	1,	NULL),
(5,	4,	72,	0,	2,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.\r\n\r\nTale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo.\r\n\r\nMelius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam.\r\n\r\nIus cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no.\r\n\r\nSoleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.'),
(6,	4,	73,	0,	2,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.'),
(7,	4,	83,	0,	2,	NULL),
(8,	4,	106,	0,	2,	NULL),
(15,	4,	72,	0,	3,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.\r\n\r\nTale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo.\r\n\r\nMelius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam.\r\n\r\nIus cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no.\r\n\r\nSoleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.'),
(16,	4,	73,	0,	3,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.'),
(17,	4,	83,	0,	3,	NULL),
(18,	4,	106,	0,	3,	NULL),
(21,	4,	72,	0,	4,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.\r\n\r\nTale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo.\r\n\r\nMelius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam.\r\n\r\nIus cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no.\r\n\r\nSoleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.'),
(22,	4,	73,	0,	4,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.'),
(23,	4,	83,	0,	4,	NULL),
(24,	4,	106,	0,	4,	NULL),
(29,	4,	72,	0,	5,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.\r\n\r\nTale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo.\r\n\r\nMelius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam.\r\n\r\nIus cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no.\r\n\r\nSoleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.'),
(30,	4,	73,	0,	5,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.'),
(31,	4,	83,	0,	5,	NULL),
(32,	4,	106,	0,	5,	NULL),
(35,	4,	72,	0,	6,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.\r\n\r\nTale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo.\r\n\r\nMelius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam.\r\n\r\nIus cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no.\r\n\r\nSoleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.'),
(36,	4,	73,	0,	6,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.'),
(37,	4,	83,	0,	6,	NULL),
(38,	4,	106,	0,	6,	NULL),
(41,	4,	72,	0,	7,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.\r\n\r\nTale maiorum imperdiet at eam, at omnes signiferumque eos, sea ei prima democritum. Latine consequat efficiendi his at. Mei in cibo posse omittam, id agam vivendum nec. Eu mei erat indoctum iudicabit, quod unum ei quo.\r\n\r\nMelius perfecto ius in. At has alia nulla, wisi referrentur eu sit. Illum mandamus sed ad. Eu est clita adipisci torquatos, et pro tempor maiorum facilisi. Homero recteque definiebas sed ei, ad vel suas legimus civibus, has primis essent efficiantur ad. Sed stet nemore ad, an causae aeterno eam.\r\n\r\nIus cu omnium convenire. Dolorum liberavisse mea cu, ei omnium utroque cum, atqui iriure quaerendum ad vel. Id putent epicuri vel. Populo copiosae interpretaris pro at, mei verterem indoctum percipitur no.\r\n\r\nSoleat voluptua posidonium ut mea, nec eu tota omnes soluta. Ad sed perfecto dignissim, quaeque veritus convenire quo ei. Eirmod alienum has cu, te nam mucius melius. His id omnes lobortis.'),
(42,	4,	73,	0,	7,	'Lorem ipsum dolor sit amet, harum deterruisset eos ad. Mel choro graece an, id inermis omnesque gubergren mea. Et cum nemore persius adipisci, in vix feugiat deterruisset comprehensam. Ei vis viderer maluisset. Te malis mucius mei. Ea harum nihil repudiare est, sint iracundia vim eu.'),
(43,	4,	83,	0,	7,	NULL),
(44,	4,	106,	0,	7,	NULL);

DROP TABLE IF EXISTS `catalog_product_entity_tier_price`;
CREATE TABLE `catalog_product_entity_tier_price` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `all_groups` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Applicable To All Customer Groups',
  `customer_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Group ID',
  `qty` decimal(12,4) NOT NULL DEFAULT '1.0000' COMMENT 'QTY',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `E8AB433B9ACB00343ABB312AD2FAB087` (`entity_id`,`all_groups`,`customer_group_id`,`qty`,`website_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_TIER_PRICE_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_TIER_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_TIER_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_6E08D719F0501DD1D8E6D4EFF2511C85` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_TIER_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_TIER_PRICE_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Tier Price Attribute Backend Table';


DROP TABLE IF EXISTS `catalog_product_entity_varchar`;
CREATE TABLE `catalog_product_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `entity_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_PRD_ENTT_VCHR_ENTT_ID_ATTR_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_ENTITY_VARCHAR_ENTITY_ID` (`entity_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_ENTITY_VARCHAR_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_ENTT_VCHR_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Varchar Attribute Backend Table';

INSERT INTO `catalog_product_entity_varchar` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1,	4,	71,	0,	1,	'Complete Whitening + Scope'),
(2,	4,	97,	0,	1,	'complete-whitening-scope'),
(3,	4,	117,	0,	1,	NULL),
(4,	4,	118,	0,	1,	'2'),
(5,	4,	119,	0,	1,	'4'),
(6,	4,	82,	0,	1,	NULL),
(7,	4,	84,	0,	1,	NULL),
(8,	4,	85,	0,	1,	'/1/4/1452979397626.jpg'),
(9,	4,	86,	0,	1,	'/1/4/1452979397626.jpg'),
(10,	4,	87,	0,	1,	'/1/4/1452979397626.jpg'),
(11,	4,	103,	0,	1,	NULL),
(12,	4,	107,	0,	1,	NULL),
(13,	4,	109,	0,	1,	'container1'),
(14,	4,	122,	0,	1,	NULL),
(15,	4,	112,	0,	1,	NULL),
(16,	4,	113,	0,	1,	NULL),
(17,	4,	114,	0,	1,	NULL),
(18,	4,	98,	1,	1,	'complete-whitening-scope'),
(19,	4,	98,	0,	1,	'complete-whitening-scope'),
(20,	4,	71,	0,	2,	'Lorem ipsum dolor'),
(21,	4,	97,	0,	2,	'lorem-ipsum-dolor'),
(22,	4,	117,	0,	2,	NULL),
(23,	4,	118,	0,	2,	'2'),
(24,	4,	119,	0,	2,	'4'),
(25,	4,	82,	0,	2,	NULL),
(26,	4,	84,	0,	2,	NULL),
(27,	4,	85,	0,	2,	'/1/4/1429479597924.jpg'),
(28,	4,	86,	0,	2,	'/1/4/1429479597924.jpg'),
(29,	4,	87,	0,	2,	'/1/4/1429479597924.jpg'),
(30,	4,	103,	0,	2,	NULL),
(31,	4,	107,	0,	2,	NULL),
(32,	4,	109,	0,	2,	'container1'),
(33,	4,	122,	0,	2,	NULL),
(34,	4,	112,	0,	2,	NULL),
(35,	4,	113,	0,	2,	NULL),
(36,	4,	114,	0,	2,	NULL),
(37,	4,	98,	1,	2,	'lorem-ipsum-dolor'),
(38,	4,	98,	0,	2,	'lorem-ipsum-dolor'),
(66,	4,	71,	0,	3,	'Lorem ipsum dolor 1'),
(67,	4,	82,	0,	3,	NULL),
(68,	4,	84,	0,	3,	NULL),
(69,	4,	85,	0,	3,	'/1/4/1436644266678.jpg'),
(70,	4,	86,	0,	3,	'/1/4/1436644266678.jpg'),
(71,	4,	87,	0,	3,	'/1/4/1436644266678.jpg'),
(72,	4,	97,	0,	3,	'lorem-ipsum-dolor'),
(73,	4,	98,	0,	3,	'lorem-ipsum-dolor-108'),
(74,	4,	103,	0,	3,	NULL),
(75,	4,	107,	0,	3,	NULL),
(76,	4,	109,	0,	3,	'container1'),
(77,	4,	112,	0,	3,	NULL),
(78,	4,	113,	0,	3,	NULL),
(79,	4,	114,	0,	3,	NULL),
(80,	4,	117,	0,	3,	NULL),
(81,	4,	118,	0,	3,	'2'),
(82,	4,	119,	0,	3,	'4'),
(83,	4,	122,	0,	3,	NULL),
(84,	4,	98,	1,	3,	'lorem-ipsum-dolor-108'),
(96,	4,	71,	0,	4,	'Lorem ipsum dolor 3'),
(97,	4,	82,	0,	4,	NULL),
(98,	4,	84,	0,	4,	NULL),
(99,	4,	85,	0,	4,	'/1/4/1437844868306.jpg'),
(100,	4,	86,	0,	4,	'/1/4/1437844868306.jpg'),
(101,	4,	87,	0,	4,	'/1/4/1437844868306.jpg'),
(102,	4,	97,	0,	4,	'lorem-ipsum-dolor'),
(103,	4,	98,	0,	4,	'lorem-ipsum-dolor-109'),
(104,	4,	103,	0,	4,	NULL),
(105,	4,	107,	0,	4,	NULL),
(106,	4,	109,	0,	4,	'container1'),
(107,	4,	112,	0,	4,	NULL),
(108,	4,	113,	0,	4,	NULL),
(109,	4,	114,	0,	4,	NULL),
(110,	4,	117,	0,	4,	NULL),
(111,	4,	118,	0,	4,	'2'),
(112,	4,	119,	0,	4,	'4'),
(113,	4,	122,	0,	4,	NULL),
(114,	4,	98,	1,	4,	'lorem-ipsum-dolor-109'),
(141,	4,	71,	0,	5,	'Lorem ipsum dolor 4'),
(142,	4,	82,	0,	5,	NULL),
(143,	4,	84,	0,	5,	NULL),
(144,	4,	85,	0,	5,	'/1/4/1441503569821.jpg'),
(145,	4,	86,	0,	5,	'/1/4/1441503569821.jpg'),
(146,	4,	87,	0,	5,	'/1/4/1441503569821.jpg'),
(147,	4,	97,	0,	5,	'lorem-ipsum-dolor'),
(148,	4,	98,	0,	5,	'lorem-ipsum-dolor-110'),
(149,	4,	103,	0,	5,	NULL),
(150,	4,	107,	0,	5,	NULL),
(151,	4,	109,	0,	5,	'container1'),
(152,	4,	112,	0,	5,	NULL),
(153,	4,	113,	0,	5,	NULL),
(154,	4,	114,	0,	5,	NULL),
(155,	4,	117,	0,	5,	NULL),
(156,	4,	118,	0,	5,	'2'),
(157,	4,	119,	0,	5,	'4'),
(158,	4,	122,	0,	5,	NULL),
(159,	4,	98,	1,	5,	'lorem-ipsum-dolor-110'),
(174,	4,	71,	0,	6,	'Lorem ipsum dolor 5'),
(175,	4,	82,	0,	6,	NULL),
(176,	4,	84,	0,	6,	NULL),
(177,	4,	85,	0,	6,	'/1/4/1469294152484.jpg'),
(178,	4,	86,	0,	6,	'/1/4/1469294152484.jpg'),
(179,	4,	87,	0,	6,	'/1/4/1469294152484.jpg'),
(180,	4,	97,	0,	6,	'lorem-ipsum-dolor'),
(181,	4,	98,	0,	6,	'lorem-ipsum-dolor-111'),
(182,	4,	103,	0,	6,	NULL),
(183,	4,	107,	0,	6,	NULL),
(184,	4,	109,	0,	6,	'container1'),
(185,	4,	112,	0,	6,	NULL),
(186,	4,	113,	0,	6,	NULL),
(187,	4,	114,	0,	6,	NULL),
(188,	4,	117,	0,	6,	NULL),
(189,	4,	118,	0,	6,	'2'),
(190,	4,	119,	0,	6,	'4'),
(191,	4,	122,	0,	6,	NULL),
(192,	4,	98,	1,	6,	'lorem-ipsum-dolor-111'),
(207,	4,	71,	0,	7,	'Lorem ipsum dolor 6'),
(208,	4,	82,	0,	7,	NULL),
(209,	4,	84,	0,	7,	NULL),
(210,	4,	85,	0,	7,	'/1/4/1469294152484_1.jpg'),
(211,	4,	86,	0,	7,	'/1/4/1469294152484_1.jpg'),
(212,	4,	87,	0,	7,	'/1/4/1469294152484_1.jpg'),
(213,	4,	97,	0,	7,	'lorem-ipsum-dolor'),
(214,	4,	98,	0,	7,	'lorem-ipsum-dolor-112'),
(215,	4,	103,	0,	7,	NULL),
(216,	4,	107,	0,	7,	NULL),
(217,	4,	109,	0,	7,	'container1'),
(218,	4,	112,	0,	7,	NULL),
(219,	4,	113,	0,	7,	NULL),
(220,	4,	114,	0,	7,	NULL),
(221,	4,	117,	0,	7,	NULL),
(222,	4,	118,	0,	7,	'2'),
(223,	4,	119,	0,	7,	'4'),
(224,	4,	122,	0,	7,	NULL),
(225,	4,	98,	1,	7,	'lorem-ipsum-dolor-112');

DROP TABLE IF EXISTS `catalog_product_flat_1`;
CREATE TABLE `catalog_product_flat_1` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'entity_id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'attribute_set_id',
  `type_id` varchar(32) NOT NULL DEFAULT 'simple' COMMENT 'type_id',
  `cost` decimal(12,4) DEFAULT NULL COMMENT 'cost',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'created_at',
  `gift_message_available` smallint(6) DEFAULT NULL COMMENT 'gift_message_available',
  `has_options` smallint(6) NOT NULL DEFAULT '0' COMMENT 'has_options',
  `image_label` varchar(255) DEFAULT NULL COMMENT 'image_label',
  `is_recurring` smallint(6) DEFAULT NULL COMMENT 'is_recurring',
  `links_exist` int(11) DEFAULT NULL COMMENT 'links_exist',
  `links_purchased_separately` int(11) DEFAULT NULL COMMENT 'links_purchased_separately',
  `links_title` varchar(255) DEFAULT NULL COMMENT 'links_title',
  `msrp` decimal(12,4) DEFAULT NULL COMMENT 'msrp',
  `msrp_display_actual_price_type` varchar(255) DEFAULT NULL COMMENT 'msrp_display_actual_price_type',
  `msrp_enabled` smallint(6) DEFAULT NULL COMMENT 'msrp_enabled',
  `name` varchar(255) DEFAULT NULL COMMENT 'name',
  `news_from_date` datetime DEFAULT NULL COMMENT 'news_from_date',
  `news_to_date` datetime DEFAULT NULL COMMENT 'news_to_date',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'price',
  `price_type` int(11) DEFAULT NULL COMMENT 'price_type',
  `price_view` int(11) DEFAULT NULL COMMENT 'price_view',
  `recurring_profile` text COMMENT 'recurring_profile',
  `required_options` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'required_options',
  `shipment_type` int(11) DEFAULT NULL COMMENT 'shipment_type',
  `short_description` text COMMENT 'short_description',
  `sku` varchar(64) DEFAULT NULL COMMENT 'sku',
  `sku_type` int(11) DEFAULT NULL COMMENT 'sku_type',
  `small_image` varchar(255) DEFAULT NULL COMMENT 'small_image',
  `small_image_label` varchar(255) DEFAULT NULL COMMENT 'small_image_label',
  `special_from_date` datetime DEFAULT NULL COMMENT 'special_from_date',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'special_price',
  `special_to_date` datetime DEFAULT NULL COMMENT 'special_to_date',
  `status` smallint(5) unsigned DEFAULT NULL COMMENT 'status',
  `tax_class_id` int(10) unsigned DEFAULT NULL COMMENT 'tax_class_id',
  `thumbnail` varchar(255) DEFAULT NULL COMMENT 'thumbnail',
  `thumbnail_label` varchar(255) DEFAULT NULL COMMENT 'thumbnail_label',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'updated_at',
  `url_key` varchar(255) DEFAULT NULL COMMENT 'url_key',
  `url_path` varchar(255) DEFAULT NULL COMMENT 'url_path',
  `visibility` smallint(5) unsigned DEFAULT NULL COMMENT 'visibility',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'weight',
  `weight_type` int(11) DEFAULT NULL COMMENT 'weight_type',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_FLAT_1_TYPE_ID` (`type_id`),
  KEY `IDX_CATALOG_PRODUCT_FLAT_1_ATTRIBUTE_SET_ID` (`attribute_set_id`),
  KEY `IDX_CATALOG_PRODUCT_FLAT_1_NAME` (`name`),
  KEY `IDX_CATALOG_PRODUCT_FLAT_1_PRICE` (`price`),
  KEY `IDX_CATALOG_PRODUCT_FLAT_1_STATUS` (`status`),
  CONSTRAINT `FK_CAT_PRD_FLAT_1_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Flat (Store 1)';


DROP TABLE IF EXISTS `catalog_product_index_eav`;
CREATE TABLE `catalog_product_index_eav` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_VALUE` (`value`),
  CONSTRAINT `FK_CATALOG_PRODUCT_INDEX_EAV_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_IDX_EAV_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_IDX_EAV_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Index Table';

INSERT INTO `catalog_product_index_eav` (`entity_id`, `attribute_id`, `store_id`, `value`) VALUES
(1,	121,	1,	2),
(2,	121,	1,	2),
(3,	121,	1,	2),
(4,	121,	1,	2),
(5,	121,	1,	2),
(7,	121,	1,	2);

DROP TABLE IF EXISTS `catalog_product_index_eav_decimal`;
CREATE TABLE `catalog_product_index_eav_decimal` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_VALUE` (`value`),
  CONSTRAINT `FK_CAT_PRD_IDX_EAV_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_IDX_EAV_DEC_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_IDX_EAV_DEC_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Index Table';


DROP TABLE IF EXISTS `catalog_product_index_eav_decimal_idx`;
CREATE TABLE `catalog_product_index_eav_decimal_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Indexer Index Table';


DROP TABLE IF EXISTS `catalog_product_index_eav_decimal_tmp`;
CREATE TABLE `catalog_product_index_eav_decimal_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` decimal(12,4) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_DECIMAL_TMP_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Decimal Indexer Temp Table';


DROP TABLE IF EXISTS `catalog_product_index_eav_idx`;
CREATE TABLE `catalog_product_index_eav_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_IDX_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_IDX_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_IDX_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_IDX_VALUE` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Indexer Index Table';

INSERT INTO `catalog_product_index_eav_idx` (`entity_id`, `attribute_id`, `store_id`, `value`) VALUES
(1,	121,	1,	2),
(2,	121,	1,	2),
(3,	121,	1,	2),
(4,	121,	1,	2),
(5,	121,	1,	2),
(7,	121,	1,	2);

DROP TABLE IF EXISTS `catalog_product_index_eav_tmp`;
CREATE TABLE `catalog_product_index_eav_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `value` int(10) unsigned NOT NULL COMMENT 'Value',
  PRIMARY KEY (`entity_id`,`attribute_id`,`store_id`,`value`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_TMP_ENTITY_ID` (`entity_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_TMP_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_TMP_STORE_ID` (`store_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_EAV_TMP_VALUE` (`value`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product EAV Indexer Temp Table';

INSERT INTO `catalog_product_index_eav_tmp` (`entity_id`, `attribute_id`, `store_id`, `value`) VALUES
(7,	121,	1,	2);

DROP TABLE IF EXISTS `catalog_product_index_group_price`;
CREATE TABLE `catalog_product_index_group_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_GROUP_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_GROUP_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_195DF97C81B0BDD6A2EEC50F870E16D1` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_IDX_GROUP_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_IDX_GROUP_PRICE_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Group Price Index Table';


DROP TABLE IF EXISTS `catalog_product_index_price`;
CREATE TABLE `catalog_product_index_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_PRICE_WEBSITE_ID` (`website_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_PRICE_MIN_PRICE` (`min_price`),
  KEY `IDX_CAT_PRD_IDX_PRICE_WS_ID_CSTR_GROUP_ID_MIN_PRICE` (`website_id`,`customer_group_id`,`min_price`),
  CONSTRAINT `FK_CAT_PRD_IDX_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_IDX_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_IDX_PRICE_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Index Table';

INSERT INTO `catalog_product_index_price` (`entity_id`, `customer_group_id`, `website_id`, `tax_class_id`, `price`, `final_price`, `min_price`, `max_price`, `tier_price`, `group_price`) VALUES
(1,	0,	1,	2,	1.3000,	1.0000,	1.0000,	1.0000,	NULL,	NULL),
(1,	1,	1,	2,	1.3000,	1.0000,	1.0000,	1.0000,	NULL,	NULL),
(1,	2,	1,	2,	1.3000,	1.0000,	1.0000,	1.0000,	NULL,	NULL),
(1,	3,	1,	2,	1.3000,	1.0000,	1.0000,	1.0000,	NULL,	NULL),
(2,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(2,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(2,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(2,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(3,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(3,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(3,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(3,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(4,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(4,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(4,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(4,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(5,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(5,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(5,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(5,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL);

DROP TABLE IF EXISTS `catalog_product_index_price_bundle_idx`;
CREATE TABLE `catalog_product_index_price_bundle_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class Id',
  `price_type` smallint(5) unsigned NOT NULL COMMENT 'Price Type',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'Special Price',
  `tier_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tier Percent',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Orig Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  `base_group_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Group Price',
  `group_price_percent` decimal(12,4) DEFAULT NULL COMMENT 'Group Price Percent',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Idx';


DROP TABLE IF EXISTS `catalog_product_index_price_bundle_opt_idx`;
CREATE TABLE `catalog_product_index_price_bundle_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `alt_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `alt_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  `alt_group_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Group Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Opt Idx';


DROP TABLE IF EXISTS `catalog_product_index_price_bundle_opt_tmp`;
CREATE TABLE `catalog_product_index_price_bundle_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `alt_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `alt_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  `alt_group_price` decimal(12,4) DEFAULT NULL COMMENT 'Alt Group Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Opt Tmp';


DROP TABLE IF EXISTS `catalog_product_index_price_bundle_sel_idx`;
CREATE TABLE `catalog_product_index_price_bundle_sel_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Id',
  `group_type` smallint(5) unsigned DEFAULT '0' COMMENT 'Group Type',
  `is_required` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Required',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Sel Idx';


DROP TABLE IF EXISTS `catalog_product_index_price_bundle_sel_tmp`;
CREATE TABLE `catalog_product_index_price_bundle_sel_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `selection_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Selection Id',
  `group_type` smallint(5) unsigned DEFAULT '0' COMMENT 'Group Type',
  `is_required` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Required',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`,`selection_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Sel Tmp';


DROP TABLE IF EXISTS `catalog_product_index_price_bundle_tmp`;
CREATE TABLE `catalog_product_index_price_bundle_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class Id',
  `price_type` smallint(5) unsigned NOT NULL COMMENT 'Price Type',
  `special_price` decimal(12,4) DEFAULT NULL COMMENT 'Special Price',
  `tier_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tier Percent',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Orig Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  `base_group_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Group Price',
  `group_price_percent` decimal(12,4) DEFAULT NULL COMMENT 'Group Price Percent',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Index Price Bundle Tmp';


DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_agr_idx`;
CREATE TABLE `catalog_product_index_price_cfg_opt_agr_idx` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Aggregate Index Table';


DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_agr_tmp`;
CREATE TABLE `catalog_product_index_price_cfg_opt_agr_tmp` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`parent_id`,`child_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Aggregate Temp Table';


DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_idx`;
CREATE TABLE `catalog_product_index_price_cfg_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Index Table';


DROP TABLE IF EXISTS `catalog_product_index_price_cfg_opt_tmp`;
CREATE TABLE `catalog_product_index_price_cfg_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Config Option Temp Table';


DROP TABLE IF EXISTS `catalog_product_index_price_downlod_idx`;
CREATE TABLE `catalog_product_index_price_downlod_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Minimum price',
  `max_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Maximum price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Indexer Table for price of downloadable products';


DROP TABLE IF EXISTS `catalog_product_index_price_downlod_tmp`;
CREATE TABLE `catalog_product_index_price_downlod_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Minimum price',
  `max_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Maximum price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Temporary Indexer Table for price of downloadable products';


DROP TABLE IF EXISTS `catalog_product_index_price_final_idx`;
CREATE TABLE `catalog_product_index_price_final_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  `base_group_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Group Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Final Index Table';


DROP TABLE IF EXISTS `catalog_product_index_price_final_tmp`;
CREATE TABLE `catalog_product_index_price_final_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `orig_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `base_tier` decimal(12,4) DEFAULT NULL COMMENT 'Base Tier',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  `base_group_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Group Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Final Temp Table';


DROP TABLE IF EXISTS `catalog_product_index_price_idx`;
CREATE TABLE `catalog_product_index_price_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_PRICE_IDX_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_PRICE_IDX_WEBSITE_ID` (`website_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_PRICE_IDX_MIN_PRICE` (`min_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Index Table';

INSERT INTO `catalog_product_index_price_idx` (`entity_id`, `customer_group_id`, `website_id`, `tax_class_id`, `price`, `final_price`, `min_price`, `max_price`, `tier_price`, `group_price`) VALUES
(1,	0,	1,	2,	1.3000,	1.0000,	1.0000,	1.0000,	NULL,	NULL),
(1,	1,	1,	2,	1.3000,	1.0000,	1.0000,	1.0000,	NULL,	NULL),
(1,	2,	1,	2,	1.3000,	1.0000,	1.0000,	1.0000,	NULL,	NULL),
(1,	3,	1,	2,	1.3000,	1.0000,	1.0000,	1.0000,	NULL,	NULL),
(2,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(2,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(2,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(2,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(3,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(3,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(3,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(3,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(4,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(4,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(4,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(4,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(5,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(5,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(5,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(5,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL);

DROP TABLE IF EXISTS `catalog_product_index_price_opt_agr_idx`;
CREATE TABLE `catalog_product_index_price_opt_agr_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Aggregate Index Table';


DROP TABLE IF EXISTS `catalog_product_index_price_opt_agr_tmp`;
CREATE TABLE `catalog_product_index_price_opt_agr_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`,`option_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Aggregate Temp Table';


DROP TABLE IF EXISTS `catalog_product_index_price_opt_idx`;
CREATE TABLE `catalog_product_index_price_opt_idx` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Index Table';


DROP TABLE IF EXISTS `catalog_product_index_price_opt_tmp`;
CREATE TABLE `catalog_product_index_price_opt_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Option Temp Table';


DROP TABLE IF EXISTS `catalog_product_index_price_tmp`;
CREATE TABLE `catalog_product_index_price_tmp` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `tax_class_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Tax Class ID',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `final_price` decimal(12,4) DEFAULT NULL COMMENT 'Final Price',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  `max_price` decimal(12,4) DEFAULT NULL COMMENT 'Max Price',
  `tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Tier Price',
  `group_price` decimal(12,4) DEFAULT NULL COMMENT 'Group price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_PRICE_TMP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_PRICE_TMP_WEBSITE_ID` (`website_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_PRICE_TMP_MIN_PRICE` (`min_price`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='Catalog Product Price Indexer Temp Table';

INSERT INTO `catalog_product_index_price_tmp` (`entity_id`, `customer_group_id`, `website_id`, `tax_class_id`, `price`, `final_price`, `min_price`, `max_price`, `tier_price`, `group_price`) VALUES
(7,	0,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	1,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	2,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL),
(7,	3,	1,	2,	123.0000,	123.0000,	123.0000,	123.0000,	NULL,	NULL);

DROP TABLE IF EXISTS `catalog_product_index_tier_price`;
CREATE TABLE `catalog_product_index_tier_price` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `min_price` decimal(12,4) DEFAULT NULL COMMENT 'Min Price',
  PRIMARY KEY (`entity_id`,`customer_group_id`,`website_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_TIER_PRICE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_TIER_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_CAT_PRD_IDX_TIER_PRICE_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_IDX_TIER_PRICE_ENTT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_IDX_TIER_PRICE_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Tier Price Index Table';


DROP TABLE IF EXISTS `catalog_product_index_website`;
CREATE TABLE `catalog_product_index_website` (
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  `website_date` date DEFAULT NULL COMMENT 'Website Date',
  `rate` float DEFAULT '1' COMMENT 'Rate',
  PRIMARY KEY (`website_id`),
  KEY `IDX_CATALOG_PRODUCT_INDEX_WEBSITE_WEBSITE_DATE` (`website_date`),
  CONSTRAINT `FK_CAT_PRD_IDX_WS_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Website Index Table';

INSERT INTO `catalog_product_index_website` (`website_id`, `website_date`, `rate`) VALUES
(1,	'2017-01-03',	1);

DROP TABLE IF EXISTS `catalog_product_link`;
CREATE TABLE `catalog_product_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `linked_product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Linked Product ID',
  `link_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Link Type ID',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNQ_CAT_PRD_LNK_LNK_TYPE_ID_PRD_ID_LNKED_PRD_ID` (`link_type_id`,`product_id`,`linked_product_id`),
  KEY `IDX_CATALOG_PRODUCT_LINK_PRODUCT_ID` (`product_id`),
  KEY `IDX_CATALOG_PRODUCT_LINK_LINKED_PRODUCT_ID` (`linked_product_id`),
  KEY `IDX_CATALOG_PRODUCT_LINK_LINK_TYPE_ID` (`link_type_id`),
  CONSTRAINT `FK_CAT_PRD_LNK_LNKED_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`linked_product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_LNK_LNK_TYPE_ID_CAT_PRD_LNK_TYPE_LNK_TYPE_ID` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_LNK_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Product Linkage Table';


DROP TABLE IF EXISTS `catalog_product_link_attribute`;
CREATE TABLE `catalog_product_link_attribute` (
  `product_link_attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product Link Attribute ID',
  `link_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Link Type ID',
  `product_link_attribute_code` varchar(32) DEFAULT NULL COMMENT 'Product Link Attribute Code',
  `data_type` varchar(32) DEFAULT NULL COMMENT 'Data Type',
  PRIMARY KEY (`product_link_attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_LINK_ATTRIBUTE_LINK_TYPE_ID` (`link_type_id`),
  CONSTRAINT `FK_CAT_PRD_LNK_ATTR_LNK_TYPE_ID_CAT_PRD_LNK_TYPE_LNK_TYPE_ID` FOREIGN KEY (`link_type_id`) REFERENCES `catalog_product_link_type` (`link_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Attribute Table';

INSERT INTO `catalog_product_link_attribute` (`product_link_attribute_id`, `link_type_id`, `product_link_attribute_code`, `data_type`) VALUES
(1,	1,	'position',	'int'),
(2,	3,	'position',	'int'),
(3,	3,	'qty',	'decimal'),
(4,	4,	'position',	'int'),
(5,	5,	'position',	'int');

DROP TABLE IF EXISTS `catalog_product_link_attribute_decimal`;
CREATE TABLE `catalog_product_link_attribute_decimal` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_PRD_LNK_ATTR_DEC_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `IDX_CAT_PRD_LNK_ATTR_DEC_PRD_LNK_ATTR_ID` (`product_link_attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_LINK_ATTRIBUTE_DECIMAL_LINK_ID` (`link_id`),
  CONSTRAINT `FK_AB2EFA9A14F7BCF1D5400056203D14B6` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_LNK_ATTR_DEC_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Decimal Attribute Table';


DROP TABLE IF EXISTS `catalog_product_link_attribute_int`;
CREATE TABLE `catalog_product_link_attribute_int` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_PRD_LNK_ATTR_INT_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `IDX_CATALOG_PRODUCT_LINK_ATTRIBUTE_INT_PRODUCT_LINK_ATTRIBUTE_ID` (`product_link_attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_LINK_ATTRIBUTE_INT_LINK_ID` (`link_id`),
  CONSTRAINT `FK_CAT_PRD_LNK_ATTR_INT_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_D6D878F8BA2A4282F8DDED7E6E3DE35C` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Integer Attribute Table';


DROP TABLE IF EXISTS `catalog_product_link_attribute_varchar`;
CREATE TABLE `catalog_product_link_attribute_varchar` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_link_attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Link Attribute ID',
  `link_id` int(10) unsigned NOT NULL COMMENT 'Link ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_PRD_LNK_ATTR_VCHR_PRD_LNK_ATTR_ID_LNK_ID` (`product_link_attribute_id`,`link_id`),
  KEY `IDX_CAT_PRD_LNK_ATTR_VCHR_PRD_LNK_ATTR_ID` (`product_link_attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_LINK_ATTRIBUTE_VARCHAR_LINK_ID` (`link_id`),
  CONSTRAINT `FK_CAT_PRD_LNK_ATTR_VCHR_LNK_ID_CAT_PRD_LNK_LNK_ID` FOREIGN KEY (`link_id`) REFERENCES `catalog_product_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DEE9C4DA61CFCC01DFCF50F0D79CEA51` FOREIGN KEY (`product_link_attribute_id`) REFERENCES `catalog_product_link_attribute` (`product_link_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Varchar Attribute Table';


DROP TABLE IF EXISTS `catalog_product_link_type`;
CREATE TABLE `catalog_product_link_type` (
  `link_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Type ID',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  PRIMARY KEY (`link_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Link Type Table';

INSERT INTO `catalog_product_link_type` (`link_type_id`, `code`) VALUES
(1,	'relation'),
(3,	'super'),
(4,	'up_sell'),
(5,	'cross_sell');

DROP TABLE IF EXISTS `catalog_product_option`;
CREATE TABLE `catalog_product_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `type` varchar(50) DEFAULT NULL COMMENT 'Type',
  `is_require` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Required',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `max_characters` int(10) unsigned DEFAULT NULL COMMENT 'Max Characters',
  `file_extension` varchar(50) DEFAULT NULL COMMENT 'File Extension',
  `image_size_x` smallint(5) unsigned DEFAULT NULL COMMENT 'Image Size X',
  `image_size_y` smallint(5) unsigned DEFAULT NULL COMMENT 'Image Size Y',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_id`),
  KEY `IDX_CATALOG_PRODUCT_OPTION_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_CAT_PRD_OPT_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Table';


DROP TABLE IF EXISTS `catalog_product_option_price`;
CREATE TABLE `catalog_product_option_price` (
  `option_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Price ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `price_type` varchar(7) NOT NULL DEFAULT 'fixed' COMMENT 'Price Type',
  PRIMARY KEY (`option_price_id`),
  UNIQUE KEY `UNQ_CATALOG_PRODUCT_OPTION_PRICE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_OPTION_PRICE_OPTION_ID` (`option_id`),
  KEY `IDX_CATALOG_PRODUCT_OPTION_PRICE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_PRICE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_OPT_PRICE_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Price Table';


DROP TABLE IF EXISTS `catalog_product_option_title`;
CREATE TABLE `catalog_product_option_title` (
  `option_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Title ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`option_title_id`),
  UNIQUE KEY `UNQ_CATALOG_PRODUCT_OPTION_TITLE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_OPTION_TITLE_OPTION_ID` (`option_id`),
  KEY `IDX_CATALOG_PRODUCT_OPTION_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_OPTION_TITLE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_OPT_TTL_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Title Table';


DROP TABLE IF EXISTS `catalog_product_option_type_price`;
CREATE TABLE `catalog_product_option_type_price` (
  `option_type_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type Price ID',
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Type ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `price_type` varchar(7) NOT NULL DEFAULT 'fixed' COMMENT 'Price Type',
  PRIMARY KEY (`option_type_price_id`),
  UNIQUE KEY `UNQ_CATALOG_PRODUCT_OPTION_TYPE_PRICE_OPTION_TYPE_ID_STORE_ID` (`option_type_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_OPTION_TYPE_PRICE_OPTION_TYPE_ID` (`option_type_id`),
  KEY `IDX_CATALOG_PRODUCT_OPTION_TYPE_PRICE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_B523E3378E8602F376CC415825576B7F` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_OPT_TYPE_PRICE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Price Table';


DROP TABLE IF EXISTS `catalog_product_option_type_title`;
CREATE TABLE `catalog_product_option_type_title` (
  `option_type_title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type Title ID',
  `option_type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Type ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`option_type_title_id`),
  UNIQUE KEY `UNQ_CATALOG_PRODUCT_OPTION_TYPE_TITLE_OPTION_TYPE_ID_STORE_ID` (`option_type_id`,`store_id`),
  KEY `IDX_CATALOG_PRODUCT_OPTION_TYPE_TITLE_OPTION_TYPE_ID` (`option_type_id`),
  KEY `IDX_CATALOG_PRODUCT_OPTION_TYPE_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_C085B9CF2C2A302E8043FDEA1937D6A2` FOREIGN KEY (`option_type_id`) REFERENCES `catalog_product_option_type_value` (`option_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_OPT_TYPE_TTL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Title Table';


DROP TABLE IF EXISTS `catalog_product_option_type_value`;
CREATE TABLE `catalog_product_option_type_value` (
  `option_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Type ID',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option ID',
  `sku` varchar(64) DEFAULT NULL COMMENT 'SKU',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_type_id`),
  KEY `IDX_CATALOG_PRODUCT_OPTION_TYPE_VALUE_OPTION_ID` (`option_id`),
  CONSTRAINT `FK_CAT_PRD_OPT_TYPE_VAL_OPT_ID_CAT_PRD_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Option Type Value Table';


DROP TABLE IF EXISTS `catalog_product_relation`;
CREATE TABLE `catalog_product_relation` (
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent ID',
  `child_id` int(10) unsigned NOT NULL COMMENT 'Child ID',
  PRIMARY KEY (`parent_id`,`child_id`),
  KEY `IDX_CATALOG_PRODUCT_RELATION_CHILD_ID` (`child_id`),
  CONSTRAINT `FK_CAT_PRD_RELATION_CHILD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`child_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_RELATION_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Relation Table';


DROP TABLE IF EXISTS `catalog_product_super_attribute`;
CREATE TABLE `catalog_product_super_attribute` (
  `product_super_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product Super Attribute ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Position',
  PRIMARY KEY (`product_super_attribute_id`),
  UNIQUE KEY `UNQ_CATALOG_PRODUCT_SUPER_ATTRIBUTE_PRODUCT_ID_ATTRIBUTE_ID` (`product_id`,`attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_SUPER_ATTRIBUTE_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_CAT_PRD_SPR_ATTR_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Table';


DROP TABLE IF EXISTS `catalog_product_super_attribute_label`;
CREATE TABLE `catalog_product_super_attribute_label` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_super_attribute_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Super Attribute ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `use_default` smallint(5) unsigned DEFAULT '0' COMMENT 'Use Default Value',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_PRD_SPR_ATTR_LBL_PRD_SPR_ATTR_ID_STORE_ID` (`product_super_attribute_id`,`store_id`),
  KEY `IDX_CAT_PRD_SPR_ATTR_LBL_PRD_SPR_ATTR_ID` (`product_super_attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `FK_309442281DF7784210ED82B2CC51E5D5` FOREIGN KEY (`product_super_attribute_id`) REFERENCES `catalog_product_super_attribute` (`product_super_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_SPR_ATTR_LBL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Label Table';


DROP TABLE IF EXISTS `catalog_product_super_attribute_pricing`;
CREATE TABLE `catalog_product_super_attribute_pricing` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `product_super_attribute_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Super Attribute ID',
  `value_index` varchar(255) DEFAULT NULL COMMENT 'Value Index',
  `is_percent` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Percent',
  `pricing_value` decimal(12,4) DEFAULT NULL COMMENT 'Pricing Value',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CAT_PRD_SPR_ATTR_PRICING_PRD_SPR_ATTR_ID_VAL_IDX_WS_ID` (`product_super_attribute_id`,`value_index`,`website_id`),
  KEY `IDX_CAT_PRD_SPR_ATTR_PRICING_PRD_SPR_ATTR_ID` (`product_super_attribute_id`),
  KEY `IDX_CATALOG_PRODUCT_SUPER_ATTRIBUTE_PRICING_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_CAT_PRD_SPR_ATTR_PRICING_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CDE8813117106CFAA3AD209358F66332` FOREIGN KEY (`product_super_attribute_id`) REFERENCES `catalog_product_super_attribute` (`product_super_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Attribute Pricing Table';


DROP TABLE IF EXISTS `catalog_product_super_link`;
CREATE TABLE `catalog_product_super_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent ID',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNQ_CATALOG_PRODUCT_SUPER_LINK_PRODUCT_ID_PARENT_ID` (`product_id`,`parent_id`),
  KEY `IDX_CATALOG_PRODUCT_SUPER_LINK_PARENT_ID` (`parent_id`),
  KEY `IDX_CATALOG_PRODUCT_SUPER_LINK_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_CAT_PRD_SPR_LNK_PARENT_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_SPR_LNK_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Super Link Table';


DROP TABLE IF EXISTS `catalog_product_website`;
CREATE TABLE `catalog_product_website` (
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product ID',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website ID',
  PRIMARY KEY (`product_id`,`website_id`),
  KEY `IDX_CATALOG_PRODUCT_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_CATALOG_PRODUCT_WEBSITE_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CAT_PRD_WS_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Website Linkage Table';

INSERT INTO `catalog_product_website` (`product_id`, `website_id`) VALUES
(1,	1),
(2,	1),
(3,	1),
(4,	1),
(5,	1),
(6,	1),
(7,	1);

DROP TABLE IF EXISTS `checkout_agreement`;
CREATE TABLE `checkout_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Agreement Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `content` text COMMENT 'Content',
  `content_height` varchar(25) DEFAULT NULL COMMENT 'Content Height',
  `checkbox_text` text COMMENT 'Checkbox Text',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `is_html` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Html',
  PRIMARY KEY (`agreement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkout Agreement';


DROP TABLE IF EXISTS `checkout_agreement_store`;
CREATE TABLE `checkout_agreement_store` (
  `agreement_id` int(10) unsigned NOT NULL COMMENT 'Agreement Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`agreement_id`,`store_id`),
  KEY `FK_CHECKOUT_AGREEMENT_STORE_STORE_ID_CORE_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CHECKOUT_AGREEMENT_STORE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CHKT_AGRT_STORE_AGRT_ID_CHKT_AGRT_AGRT_ID` FOREIGN KEY (`agreement_id`) REFERENCES `checkout_agreement` (`agreement_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Checkout Agreement Store';


DROP TABLE IF EXISTS `cms_block`;
CREATE TABLE `cms_block` (
  `block_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Block ID',
  `title` varchar(255) NOT NULL COMMENT 'Block Title',
  `identifier` varchar(255) NOT NULL COMMENT 'Block String Identifier',
  `content` mediumtext COMMENT 'Block Content',
  `creation_time` timestamp NULL DEFAULT NULL COMMENT 'Block Creation Time',
  `update_time` timestamp NULL DEFAULT NULL COMMENT 'Block Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Block Active',
  PRIMARY KEY (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Block Table';

INSERT INTO `cms_block` (`block_id`, `title`, `identifier`, `content`, `creation_time`, `update_time`, `is_active`) VALUES
(1,	'Footer Links',	'footer_links',	'<ul>\r\n<li><a href=\"{{store direct_url=\"about-magento-demo-store\"}}\">About Us</a></li>\r\n<li><a href=\"{{store direct_url=\"customer-service\"}}\">Customer Service</a></li>\r\n<li class=\"last privacy\"><a href=\"{{store direct_url=\"privacy-policy-cookie-restriction-mode\"}}\">Privacy Policy</a></li>\r\n</ul>',	'2016-12-14 06:01:00',	'2016-12-16 12:23:33',	0),
(2,	'Footer Links Company',	'footer_links_company',	'\n<div class=\"links\">\n    <div class=\"block-title\">\n        <strong><span>Company</span></strong>\n    </div>\n    <ul>\n        <li><a href=\"{{store url=\"\"}}about-magento-demo-store/\">About Us</a></li>\n        <li><a href=\"{{store url=\"\"}}contacts/\">Contact Us</a></li>\n        <li><a href=\"{{store url=\"\"}}customer-service/\">Customer Service</a></li>\n        <li><a href=\"{{store url=\"\"}}privacy-policy-cookie-restriction-mode/\">Privacy Policy</a></li>\n    </ul>\n</div>',	'2016-12-14 06:01:00',	'2016-12-14 06:01:00',	1),
(3,	'Cookie restriction notice',	'cookie_restriction_notice_block',	'<p>This website requires cookies to provide all of its features. For more information on what data is contained in the cookies, please see our <a href=\"{{store direct_url=\"privacy-policy-cookie-restriction-mode\"}}\">Privacy Policy page</a>. To accept cookies from this site, please click the Allow button below.</p>',	'2016-12-14 06:01:00',	'2016-12-16 12:23:40',	0),
(4,	'Astrabootstrap Footer links',	'astrabootstrap_footer_links',	'<div class=\"footer-bottom\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-5 col-xs-12 coppyright\">&copy; 2017 Aus Warehouse. All Rights Reserved.</div>\r\n<div class=\"col-sm-7 col-xs-12 company-links\">\r\n<ul class=\"links\">\r\n<li><a href=\"#\" title=\"Magento Themes\">Terms of Usage</a></li>\r\n<li><a href=\"#\" title=\"Responsive Themes\">Privacy Policy</a></li>\r\n<li class=\"last\"><a href=\"#\" title=\"Magento Extensions\">Contact Us</a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>',	'2016-12-16 12:21:29',	'2017-01-02 22:58:45',	1),
(5,	'Custom Tab1',	'astrabootstrap_custom_tab1',	'<p><strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>',	'2016-12-16 12:21:29',	'2017-01-02 22:58:54',	0),
(6,	'Custom Tab2',	'astrabootstrap_custom_tab2',	'<p><strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>',	'2016-12-16 12:21:29',	'2017-01-02 22:58:58',	0),
(7,	'Empty Category',	'astrabootstrap_empty_category',	'<p>There are no products matching the selection.<br /> This is a static CMS block displayed if category is empty. You can put your own content here.</p>',	'2016-12-16 12:21:29',	'2016-12-16 12:21:29',	1),
(8,	'Astrabootstrap Logo Brand block',	'astrabootstrap_logo_brand_block',	'<div class=\"brand-logo \">\r\n<div class=\"container\">\r\n<div class=\"slider-items-products\">\r\n<div class=\"product-flexslider hidden-buttons\" id=\"brand-logo-slider\">\r\n<div class=\"slider-items slider-width-col6\">\r\n<div class=\"item\"><a href=\"#\"><img alt=\"Image\" src=\"{{skin url=\"images/b-logo1.png\"}}\" /></a></div>\r\n<div class=\"item\"><a href=\"#\"><img alt=\"Image\" src=\"{{skin url=\"images/b-logo2.png\"}}\" /></a></div>\r\n<div class=\"item\"><a href=\"#\"><img alt=\"Image\" src=\"{{skin url=\"images/b-logo3.png\"}}\" /></a></div>\r\n<div class=\"item\"><a href=\"#\"><img alt=\"Image\" src=\"{{skin url=\"images/b-logo4.png\"}}\" /></a></div>\r\n<div class=\"item\"><a href=\"#\"><img alt=\"Image\" src=\"{{skin url=\"images/b-logo5.png\"}}\" /></a></div>\r\n<div class=\"item\"><a href=\"#\"><img alt=\"Image\" src=\"{{skin url=\"images/b-logo6.png\"}}\" /></a></div>\r\n<div class=\"item\"><a href=\"#\"><img alt=\"Image\" src=\"{{skin url=\"images/b-logo1.png\"}}\" /></a></div>\r\n<div class=\"item\"><a href=\"#\"><img alt=\"Image\" src=\"{{skin url=\"images/b-logo4.png\"}}\" /></a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>',	'2016-12-16 12:21:29',	'2016-12-16 12:23:57',	0),
(9,	'Astrabootstrap Store Logo',	'astrabootstrap_logo',	'<p><img src=\"{{media url=\"wysiwyg/siteimages/aw-logo.png\"}}\" /></p>',	'2016-12-16 12:21:29',	'2016-12-16 13:23:12',	1),
(10,	'What\'s New',	'astrabootstrap_navigation_block',	'<div class=\"grid12-5\">\r\n<div class=\"custom_img\"><img alt=\"custom img1\" src=\"{{media url=\"wysiwyg/products/oreo.jpg\"}}\" /></div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>\r\n</div>\r\n<div class=\"grid12-5\">\r\n<div class=\"custom_img\"><img alt=\"custom img2\" src=\"{{media url=\"wysiwyg/products/grocery.jpg\"}}\" /></div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>\r\n</div>\r\n<div class=\"grid12-5\">\r\n<div class=\"custom_img\"><img alt=\"custom img3\" src=\"{{media url=\"wysiwyg/products/creaspe.jpg\"}}\" /></div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>\r\n</div>\r\n<div class=\"grid12-5\">\r\n<div class=\"custom_img\"><img alt=\"custom img4\" src=\"{{media url=\"wysiwyg/products/gillette.jpg\"}}\" /></div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>\r\n</div>',	'2016-12-16 12:21:29',	'2017-01-02 22:59:21',	0),
(11,	'Astrabootstrap Home Offer Banner Block',	'astrabootstrap_home_offer_banner_block',	'<div class=\"offer-banner-section bounceInUp animated\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-3 col-sm-6 col-xs-12\">\r\n<div class=\"col\"><img alt=\"offer banner1\" src=\"{{skin url=\"images/offer-banner1.png\"}}\" /></div>\r\n</div>\r\n<div class=\"col-md-3 col-sm-6 col-xs-12\">\r\n<div class=\"col\"><img alt=\"offer banner2\" src=\"{{skin url=\"images/offer-banner2.png\"}}\" /></div>\r\n</div>\r\n<div class=\"col-md-3 col-sm-6 col-xs-12\">\r\n<div class=\"col\"><img alt=\"offer banner3\" src=\"{{skin url=\"images/offer-banner3.png\"}}\" /></div>\r\n</div>\r\n<div class=\"col-md-3 col-sm-6 col-xs-12\">\r\n<div class=\"col last\"><img alt=\"offer banner4\" src=\"{{skin url=\"images/offer-banner4.png\"}}\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>',	'2016-12-16 12:21:29',	'2016-12-16 20:13:48',	0),
(12,	'Astrabootstrap Header Block',	'astrabootstrap_header_block',	'<!--block astrabootstrap_header_block-->\r\n<div class=\"top-section\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-4 col-sm-4 col-xs-12 message\">\r\n                <img src=\"{{media url=\"wysiwyg/home/top-icon-1.png\"}}\" />\r\n                <span>Bigger sizes & savings</span>\r\n            </div>\r\n            <div class=\"col-lg-4 col-sm-4 col-xs-12 message\">\r\n                <img src=\"{{media url=\"wysiwyg/home/top-icon-2.png\"}}\" />\r\n                <span>No membership fee</span>\r\n            </div>\r\n            <div class=\"col-lg-4 col-sm-4 col-xs-12 message\">\r\n                <img src=\"{{media url=\"wysiwyg/home/top-icon-3.png\"}}\" />\r\n                <span>Fast, free shipping*</span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>',	'2016-12-16 12:21:29',	'2017-01-02 16:58:52',	1),
(13,	'Astrabootstrap Footer About Us',	'astrabootstrap_footer_about_us',	'<div class=\"col-md-3 col-sm-4 footer-column-1\">\r\n<div class=\"footer-logo\"><a href=\"#\" title=\"Logo\"><img alt=\"logo\" src=\"{{media url=\"wysiwyg/siteimages/white-aw-logo.png\"}}\" /></a></div>\r\n<div class=\"contacts-info\"><address><em class=\"add-icon\">&nbsp;</em>123 Sample Street, Sydney, <br /> &nbsp;NSW 2000 Australia</address>\r\n<div class=\"phone-footer\"><em class=\"phone-icon\">&nbsp;</em> +61 2 9999 9999</div>\r\n<div class=\"email-footer\"><em class=\"email-icon\">&nbsp;</em> <a href=\"mailto:  support@australiaswarehouse.com.au\"> support@australiaswarehouse.com.au</a></div>\r\n</div>\r\n</div>',	'2016-12-16 12:21:30',	'2016-12-16 20:52:46',	1),
(14,	'Astrabootstrap Listing Page Block',	'astrabootstrap_listing_page_block',	'<div class=\"block block-banner\"><a href=\"#\"><img alt=\"\" src=\"{{skin url=\"images/block-banner.png\"}}\" /></a></div>',	'2016-12-16 12:21:30',	'2016-12-17 07:51:36',	0),
(15,	'Astrabootstrap RHS Contact Form Block',	'astrabootstrap_rhs_contact_form_block',	'<div class=\"slider-phone active\">\r\n<h2>TALK TO US</h2>\r\n<h3>AVAILABLE 24/7</h3>\r\n<p class=\"textcenter\">Want to speak to someone? We \'re here 24/7 to answer any questions. Just call!<br /> <br /> <span class=\"phone-number\"> +1 800 123 1234</span></p>\r\n</div>',	'2016-12-16 12:21:30',	'2016-12-16 12:25:58',	0),
(16,	'Astrabootstrap Navigation Featured Product Block',	'astrabootstrap_navigation_featured_product_block',	'<p>{{block type=\"catalog/product_new\"  products_count=\"1\" name=\"home.catalog.product.new\" as=\"newproduct\" template=\"catalog/product/new.phtml\" }}</p>',	'2016-12-16 12:21:30',	'2016-12-16 12:21:30',	1),
(17,	'Astrabootstrap Home Slider Banner Block',	'astrabootstrap_home_slider_banner_block',	'<div class=\"magik-slideshow\" id=\"magik-slideshow\">\r\n<div class=\"slider slider-elastic elastic ei-slider\" id=\"slider-elastic\">\r\n<ul class=\"ei-slider-large\">\r\n<li class=\"first slide-1 slide align-\"><img alt=\"001\" class=\"attachment-full\" src=\"{{skin url=\"images/slide-bg-fix.jpg\"}}\" />\r\n<div class=\"ei-title\">\r\n<h2>Hello there!</h2>\r\n<h3>Welcome to Astra</h3>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus.</p>\r\n<a class=\"shop-btn\" href=\"#\">shop now!</a></div>\r\n</li>\r\n<li class=\"slide-4 slide align-\"><img alt=\"003\" class=\"attachment-full\" src=\"{{skin url=\"images/slide-bg-3-fix.jpg\"}}\" />\r\n<div class=\"ei-title\">\r\n<h2>Multipurpose Theme</h2>\r\n<h3>... be creative</h3>\r\n</div>\r\n</li>\r\n<li class=\"slide-2 slide align-\"><img alt=\"002\" class=\"attachment-full\" src=\"{{skin url=\"images/slide-bg-1-fix.jpg\"}}\" />\r\n<div class=\"ei-title\">\r\n<h2>Powerfull &amp; Responsive</h2>\r\n<h4>Optimized for minor resolutions &amp; mobile devices.</h4>\r\n<a class=\"shop-btn\" href=\"#\">Purchase</a></div>\r\n</li>\r\n<li class=\"slide-3 slide align-\"><img alt=\"001\" class=\"attachment-full\" src=\"{{skin url=\"images/slide-bg-2-fix.jpg\"}}\" />\r\n<div class=\"ei-title\">\r\n<h2 style=\"color: #fff;\">LOVE IT, ENJOY IT</h2>\r\n<h5 style=\"color: #999;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus.</h5>\r\n<a class=\"shop-btn center\" href=\"#\">Buy theme!</a></div>\r\n</li>\r\n<li class=\"last slide-5 slide align-\"><img alt=\"001\" class=\"attachment-full\" src=\"{{skin url=\"images/slide-bg-4-fix.jpg\"}}\" />\r\n<div class=\"ei-title\">\r\n<h1>Customizable Theme</h1>\r\n<h6>You can change colors of every element.</h6>\r\n</div>\r\n</li>\r\n</ul>\r\n<!-- ei-slider-large -->\r\n<ul class=\"ei-slider-thumbs\">\r\n<li class=\"ei-slider-element\">Current</li>\r\n<li><a href=\"#\">Hello there! </a> <img alt=\" slide img \" src=\"{{skin url=\"images/slide-bg-fix.jpg\"}}\" /></li>\r\n<li><a href=\"#\">Multipurpose Theme </a> <img alt=\" slide img \" src=\"{{skin url=\"images/slide-bg-3-fix.jpg\"}}\" /></li>\r\n<li><a href=\"#\">Powerfull &amp; Responsive </a> <img alt=\" slide img \" src=\"{{skin url=\"images/slide-bg-1-fix.jpg\"}}\" /></li>\r\n<li><a href=\"#\">LOVE IT, ENJOY IT </a> <img alt=\" slide img \" src=\"{{skin url=\"images/slide-bg-2-fix.jpg\"}}\" /></li>\r\n<li><a href=\"#\">Customizable Theme </a> <img alt=\" slide img \" src=\"{{skin url=\"images/slide-bg-4-fix.jpg\"}}\" /></li>\r\n</ul>\r\n<!-- ei-slider-thumbs -->\r\n<div class=\"shadow\">&nbsp;</div>\r\n</div>\r\n</div>',	'2016-12-16 12:21:30',	'2017-01-02 23:00:09',	0),
(18,	'Astrabootstrap Home Slider Full Width Banner Block',	'astrabootstrap_home_slider_full_width_banner_block',	'<div class=\"magik-slideshow\" id=\"magik-slideshow\">\r\n<div class=\"slider slider-elastic elastic ei-slider\" id=\"slider-elastic\">\r\n<ul class=\"ei-slider-large\">\r\n<li class=\"first slide-1 slide align-\"><img alt=\"001\" src=\"{{media url=\"wysiwyg/Slider/banner-retina-flat.jpg\"}}\" /></li>\r\n</ul>\r\n<div class=\"shadow\">&nbsp;</div>\r\n</div>\r\n</div>',	'2016-12-16 12:21:30',	'2016-12-16 13:03:33',	1),
(19,	'Astrabootstrap Footer Information Links Block',	'astrabootstrap_footer_information_links_block',	'<div class=\"col-md-2 col-sm-4\">\n<h4>Shopping Guide</h4>\n<ul class=\"links\">\n<li class=\"first\"><a title=\"How to buy\" href=\"#\">How to buy</a></li>\n<li><a title=\"FAQs\" href=\"#\">FAQs</a></li>\n<li><a title=\"Payment\" href=\"#\">Payment</a></li>\n<li><a title=\"Shipment\" href=\"#\">Shipment</a></li>\n<li><a title=\"Where is my order?\" href=\"#\">Where is my order?</a></li>\n<li class=\"last\"><a title=\"Return policy\" href=\"#\">Return policy</a></li>\n</ul>\n</div>\n<div class=\"col-md-2 col-sm-4\">\n<h4>Style Advisor</h4>\n<ul class=\"links\">\n<li class=\"first\"><a title=\"Your Account\" href=\"{{store_url=customer/account/}}\">Your Account</a></li>\n<li><a title=\"Information\" href=\"#\">Information</a></li>\n<li><a title=\"Addresses\" href=\"#\">Addresses</a></li>\n<li><a title=\"Addresses\" href=\"#\">Discount</a></li>\n<li><a title=\"Orders History\" href=\"#\">Orders History</a></li>\n<li class=\"last\"><a title=\" Additional Information\" href=\"#\"> Additional Information</a></li>\n</ul>\n</div>\n<div class=\"col-md-2 col-sm-4\">\n<h4>Information</h4>\n<ul class=\"links\">\n<li class=\"first\"><a title=\"Site Map\" href=\"{{store_url=catalog/seo_sitemap/category/}}\">Site Map</a></li>\n<li><a title=\"Search Terms\" href=\"{{store_url=catalogsearch/term/popular/}}\">Search Terms</a></li>\n<li><a title=\"Advanced Search\" href=\"{{store_url=catalogsearch/advanced/}}\">Advanced Search</a></li>\n<li><a title=\"History\" href=\"# \">History</a></li>\n<li><a title=\"Suppliers\" href=\"#\">Suppliers</a></li>\n<li class=\" last\"><a class=\"link-rss\" title=\"Our stores\" href=\"#\">Our stores</a></li>\n</ul>\n</div>',	'2016-12-16 12:21:30',	'2016-12-16 12:21:30',	1),
(20,	'Astrabootstrap Home Toplinks Block',	'astrabootstrap-home-toplinks-block',	'<div class=\"dropdown block-company-wrapper\"><a class=\"block-company dropdown-toggle hidden-xs\" href=\"#\" title=\"Company\" data-toggle=\"dropdown\" data-target=\"#\"> Company </a>\r\n<ul class=\"dropdown-menu\">\r\n<li><a href=\"{{store_url=about-us}}\" tabindex=\"-1\"> About Us </a></li>\r\n<li><a href=\"#\" tabindex=\"-1\"> Customer Service </a></li>\r\n<li><a href=\"#\" tabindex=\"-1\"> Privacy Policy </a></li>\r\n<li><a href=\"{{store_url=catalog/seo_sitemap/category/}}\" tabindex=\"-1\">Site Map </a></li>\r\n<li><a href=\"{{store_url=catalogsearch/term/popular/}}\" tabindex=\"-1\">Search Terms </a></li>\r\n<li><a href=\"{{store_url=catalogsearch/advanced/}}\" tabindex=\"-1\">Advanced Search </a></li>\r\n</ul>\r\n</div>',	'2016-12-16 12:21:30',	'2016-12-16 12:26:53',	0),
(21,	'Astrabootstrap Home Latest Blog Block',	'astrabootstrap_home_latest_blog_block',	'<div>{{block type=\"blogmate/index\" name=\"blog_home\" template=\"blogmate/right/home_right.phtml\"}}</div>',	'2016-12-16 12:21:30',	'2016-12-16 12:21:30',	1),
(22,	'Astrabootstrap Blog Banner Text Block',	'astrabootstrap_blog_banner_text_block',	'<div class=\"text-widget widget widget__sidebar\">\r\n<h3 class=\"widget-title\">Text Widget</h3>\r\n<div class=\"widget-content\">Mauris at blandit erat. Nam vel tortor non quam scelerisque cursus. Praesent nunc vitae magna pellentesque auctor. Quisque id lectus.<br /> <br /> Massa, eget eleifend tellus. Proin nec ante leo ssim nunc sit amet velit malesuada pharetra. Nulla neque sapien, sollicitudin non ornare quis, malesuada.</div>\r\n</div>',	'2016-12-16 12:21:30',	'2016-12-16 12:27:01',	0),
(23,	'Astrabootstrap Blog Banner Ad Block',	'astrabootstrap_blog_banner_ad_block',	'<div class=\"ad-spots widget widget__sidebar\">\r\n<h3 class=\"widget-title\">Ad Spots</h3>\r\n<div class=\"widget-content\"><a href=\"#\" title=\"\" target=\"_self\"><img alt=\"offer banner\" src=\"{{skin url=\"images/offer-banner1.jpg\"}}\" /></a></div>\r\n</div>',	'2016-12-16 12:21:30',	'2017-01-02 23:00:42',	0),
(24,	'Astrabootstrap Header Lettering Block',	'astrabootstrap_header_lettering_block',	'<p style=\"padding-top: 20px;\"><img src=\"{{media url=\"wysiwyg/siteimages/aw-logo.png\"}}\" /></p>',	'2016-12-16 12:21:30',	'2016-12-16 20:15:34',	0),
(25,	'Astrabootstrap Banner Block',	'astrabootstrap_banner_block',	'<!--block astrabootstrap_banner_block-->\r\n<div class=\"banner1-section\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <a class=\"top-pick\" href=\"#\" title=\"JUST FOR YOU\">\r\n                <img src=\"{{media url=\"wysiwyg/home/top-pick.jpg\"}}\" alt=\"JUST FOR YOU\">\r\n                <!--<div class=\"text\">\r\n                    <h2>TOP PICKS FOR YOU</h2>\r\n                    <p>We thought you\'d like these products</p>\r\n                    <button>SHOP NOW</button>\r\n                </div>\r\n                <div class=\"images\">\r\n                    <div class=\"left\">\r\n                        <img src=\"{{media url=\"wysiwyg/home/banner1-left.png\"}}\" alt=\"JUST FOR YOU\">\r\n                    </div>\r\n                    <div class=\"right\">\r\n                        <img src=\"{{media url=\"wysiwyg/home/banner1-right.png\"}}\" alt=\"JUST FOR YOU\">\r\n                    </div>\r\n                </div>-->\r\n            </a>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-6 col-xs-12 col-sm-6\">\r\n                <a class=\"shop-bestsellers\" href=\"#\" title=\"SHOP BESTSELLERS UNDER $5\">\r\n                    <img src=\"{{media url=\"wysiwyg/home/shop-bestsellers.jpg\"}}\" alt=\"SHOP BESTSELLERS UNDER $5\">\r\n                </a>\r\n            </div>\r\n            <div class=\"col-md-6 col-xs-12 col-sm-6 banner1-right\">\r\n                <a class=\"week-deal\" href=\"#\" title=\"DEAL OF THE WEEK\">\r\n                    <img src=\"{{media url=\"wysiwyg/home/week-deal.jpg\"}}\" alt=\"DEAL OF THE WEEK\">\r\n                </a>\r\n                <a class=\"fresh-n-clean\" href=\"#\" title=\"SO FRESH & SO CLEAN\">\r\n                    <img src=\"{{media url=\"wysiwyg/home/fresh-n-clean.jpg\"}}\" alt=\"SO FRESH & SO CLEAN\">\r\n                </a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>',	'2016-12-16 12:21:30',	'2017-01-02 16:47:46',	1),
(26,	'Astrabootstrap Banner Bottom Block',	'astrabootstrap_banner_bottom_block',	'<!--block astrabootstrap_banner_bottom_block-->\r\n<div class=\"banner2-section\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 col-xs-12 col-sm-12\">\r\n                <a href=\"#\" title=\"BOTTOMS UP\">\r\n                    <img src=\"{{media url=\"wysiwyg/home/banner2.jpg\"}}\" alt=\"BOTTOMS UP\">\r\n                </a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>',	'2016-12-16 12:21:31',	'2017-01-02 16:47:59',	1),
(27,	'Astrabootstrap Features Box Block',	'astrabootstrap_features_box_block',	'<div class=\"our-features-box\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-4 col-xs-12 col-sm-4 wow bounceInLeft animated\">\r\n<div class=\"feature-box\">\r\n<div class=\"icon-reponsive\">&nbsp;</div>\r\n<div class=\"content\">Responsive Theme <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus mi nec lectus tempor. </span></div>\r\n</div>\r\n</div>\r\n<div class=\"col-md-4 col-xs-12 col-sm-4 wow bounceInUp animated\">\r\n<div class=\"feature-box\">\r\n<div class=\"icon-admin\">&nbsp;</div>\r\n<div class=\"content\">Powerful Admin Panel <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus mi nec lectus tempor. </span></div>\r\n</div>\r\n</div>\r\n<div class=\"col-md-4 col-xs-12 col-sm-4 wow bounceInRight animated\">\r\n<div class=\"feature-box\">\r\n<div class=\"icon-support\">&nbsp;</div>\r\n<div class=\"content\">Premium Support <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus mi nec lectus tempor. </span></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>',	'2016-12-16 12:21:31',	'2016-12-16 20:47:16',	0),
(28,	'Astrabootstrap Footer Payment Method Block',	'astrabootstrap_footer_payment_method_block',	'<div class=\"payment-accept\">\n<h4>We Accept</h4>\n<div><img src=\"{{skin url=\"images/payment-1.png\"}}\" alt=\"payment1\" /> <img src=\"{{skin url=\"images/payment-2.png\"}}\" alt=\"payment2\" /> <img src=\"{{skin url=\"images/payment-3.png\"}}\" alt=\"payment3\" /> <img src=\"{{skin url=\"images/payment-4.png\"}}\" alt=\"payment4\" /></div>\n</div>',	'2016-12-16 12:21:31',	'2016-12-16 12:21:31',	1),
(29,	'Astrabootstrap Home List Block',	'astrabootstrap_home_list_block',	'<div>{{block type=\"catalog/product_list\" name=\"homelist\" as=\"homelist\" template=\"catalog/product/home-list.phtml\"}}</div>',	'2016-12-16 12:21:31',	'2016-12-16 12:21:31',	1),
(30,	'Astrabootstrap View Page Block',	'astrabootstrap_view_page_block',	'<div class=\"product-additional col-sm-3 wow bounceInLeft animated\">\r\n<div class=\"block-product-additional\"><img alt=\"custom block\" src=\"{{skin url=\"images/offer-banner3.png\"}}\" /> <img alt=\"custom block\" src=\"{{skin url=\"images/offer-banner5.png\"}}\" /></div>\r\n</div>',	'2016-12-16 12:21:31',	'2016-12-17 07:52:23',	0),
(31,	'Astrabootstrap Recommended Product Block',	'astrabootstrap_recommended_product_block',	'<div>{{block type=\"catalog/product_list\" name=\"homerecommended\" as=\"homerecommended\" num_products=\"6\" template=\"catalog/product/recommended.phtml\" }}</div>',	'2016-12-16 12:21:31',	'2016-12-16 12:21:31',	1),
(32,	'Astrabootstrap Fixed Home Tab Dropdown Block',	'astrabootstrap_fixed_home_tab_dropdown_block',	'<ul class=\"level1\" style=\"display: none;\">\r\n<li class=\"level1 first parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapflexgray\"><span>Full Width Layout</span></a></li>\r\n<li class=\"level1 parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapfixgray\"><span>Boxed Layout</span></a></li>\r\n<li class=\"level1 parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapfixblue\"><span>Blue Color</span></a></li>\r\n<li class=\"level1 parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapfixred\"><span>Red Color</span></a></li>\r\n<li class=\"level1 parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapfixgreen\"><span>Green Color</span></a></li>\r\n<li class=\"level1 parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapfixlavender\"><span>Lavender Color</span></a></li>\r\n</ul>',	'2016-12-16 12:21:31',	'2017-01-02 23:01:35',	0),
(33,	'Astrabootstrap Flex Home Tab Dropdown Block',	'astrabootstrap_flex_home_tab_dropdown_block',	'<ul class=\"level1\" style=\"display: none;\">\r\n<li class=\"level1 first parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapflexgray\"><span>Full Width Layout</span></a></li>\r\n<li class=\"level1 parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapfixgray\"><span>Boxed Layout</span></a></li>\r\n<li class=\"level1 parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapflexblue\"><span>Blue Color</span></a></li>\r\n<li class=\"level1 parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapflexred\"><span>Red Color</span></a></li>\r\n<li class=\"level1 parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapflexgreen\"><span>Green Color</span></a></li>\r\n<li class=\"level1 parent\"><a href=\"http://demo.magikthemes.com/index.php/astrabootstrapflexlavender\"><span>Lavender Color</span></a></li>\r\n</ul>',	'2016-12-16 12:21:31',	'2017-01-02 23:01:46',	0),
(34,	'Astrabootstrap Contact Us Block',	'astrabootstrap_contact_us_block',	'<div class=\"block block-company\">\r\n<div class=\"block-title\">Company</div>\r\n<div class=\"block-content\"><ol id=\"recently-viewed-items\">\r\n<li class=\"item odd\"><a href=\"{{store_url=about-us}}\">About Us</a></li>\r\n<li class=\"item even\"><a href=\"{{store_url=catalog/seo_sitemap/category/}}\">Sitemap</a></li>\r\n<li class=\"item  odd\"><a href=\"#\">Terms of Service</a></li>\r\n<li class=\"item last\"><a href=\"{{store_url=catalogsearch/term/popular/}}\">Search Terms</a></li>\r\n<li class=\"item last\"><a href=\"{{store_url=contacts/}}\"><strong>Contact Us</strong></a></li>\r\n</ol></div>\r\n</div>',	'2016-12-16 12:21:31',	'2016-12-16 12:25:29',	0),
(35,	'Header top text block',	'astrabootstrap-home-toplinks-block',	'<p><strong>&nbsp;FREE</strong><span>&nbsp;delivery on your first order &ndash; use coupon code ausfree</span></p>',	'2016-12-16 20:36:15',	'2016-12-16 20:36:15',	1),
(36,	'Astrabootstrap Banner Bottom 2 Block',	'astrabootstrap_banner_bottom2_block',	'<!--block astrabootstrap_banner_bottom2_block-->\r\n<div class=\"banner3-section\">\r\n    <div class=\"container\">\r\n        <div class=\"row top\">\r\n            <div class=\"col-md-6 col-xs-12 col-sm-6\">\r\n                <a href=\"#\" title=\"\">\r\n                    <img src=\"{{media url=\"wysiwyg/home/banner3-1.jpg\"}}\" alt=\"\">\r\n                </a>\r\n            </div>\r\n            <div class=\"col-md-6 col-xs-12 col-sm-6\">\r\n                <a href=\"#\" title=\"\">\r\n                    <img src=\"{{media url=\"wysiwyg/home/banner3-2.jpg\"}}\" alt=\"\">\r\n                </a>\r\n            </div>\r\n        </div>\r\n        <div class=\"row bottom\">\r\n            <div class=\"col-md-12 col-xs-12 col-sm-12\">\r\n                <a href=\"#\" title=\"\">\r\n                    <img src=\"{{media url=\"wysiwyg/home/banner3-3.jpg\"}}\" alt=\"\">\r\n                </a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>',	'2017-01-02 16:48:15',	'2017-01-02 16:48:15',	1);

DROP TABLE IF EXISTS `cms_block_store`;
CREATE TABLE `cms_block_store` (
  `block_id` smallint(6) NOT NULL COMMENT 'Block ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`block_id`,`store_id`),
  KEY `IDX_CMS_BLOCK_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CMS_BLOCK_STORE_BLOCK_ID_CMS_BLOCK_BLOCK_ID` FOREIGN KEY (`block_id`) REFERENCES `cms_block` (`block_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CMS_BLOCK_STORE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Block To Store Linkage Table';

INSERT INTO `cms_block_store` (`block_id`, `store_id`) VALUES
(2,	0),
(7,	0),
(16,	0),
(19,	0),
(21,	0),
(28,	0),
(29,	0),
(31,	0),
(1,	1),
(3,	1),
(4,	1),
(5,	1),
(6,	1),
(8,	1),
(9,	1),
(10,	1),
(11,	1),
(12,	1),
(13,	1),
(14,	1),
(15,	1),
(17,	1),
(18,	1),
(20,	1),
(22,	1),
(23,	1),
(24,	1),
(25,	1),
(26,	1),
(27,	1),
(30,	1),
(32,	1),
(33,	1),
(34,	1),
(35,	1),
(36,	1);

DROP TABLE IF EXISTS `cms_page`;
CREATE TABLE `cms_page` (
  `page_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Page ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Page Title',
  `root_template` varchar(255) DEFAULT NULL COMMENT 'Page Template',
  `meta_keywords` text COMMENT 'Page Meta Keywords',
  `meta_description` text COMMENT 'Page Meta Description',
  `identifier` varchar(100) DEFAULT NULL COMMENT 'Page String Identifier',
  `content_heading` varchar(255) DEFAULT NULL COMMENT 'Page Content Heading',
  `content` mediumtext COMMENT 'Page Content',
  `creation_time` timestamp NULL DEFAULT NULL COMMENT 'Page Creation Time',
  `update_time` timestamp NULL DEFAULT NULL COMMENT 'Page Modification Time',
  `is_active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is Page Active',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Page Sort Order',
  `layout_update_xml` text COMMENT 'Page Layout Update Content',
  `custom_theme` varchar(100) DEFAULT NULL COMMENT 'Page Custom Theme',
  `custom_root_template` varchar(255) DEFAULT NULL COMMENT 'Page Custom Template',
  `custom_layout_update_xml` text COMMENT 'Page Custom Layout Update Content',
  `custom_theme_from` date DEFAULT NULL COMMENT 'Page Custom Theme Active From Date',
  `custom_theme_to` date DEFAULT NULL COMMENT 'Page Custom Theme Active To Date',
  PRIMARY KEY (`page_id`),
  KEY `IDX_CMS_PAGE_IDENTIFIER` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Page Table';

INSERT INTO `cms_page` (`page_id`, `title`, `root_template`, `meta_keywords`, `meta_description`, `identifier`, `content_heading`, `content`, `creation_time`, `update_time`, `is_active`, `sort_order`, `layout_update_xml`, `custom_theme`, `custom_root_template`, `custom_layout_update_xml`, `custom_theme_from`, `custom_theme_to`) VALUES
(1,	'404 Not Found 1',	'two_columns_right',	'Page keywords',	'Page description',	'no-route',	NULL,	'\n<div class=\"page-title\"><h1>Whoops, our bad...</h1></div>\n<dl>\n    <dt>The page you requested was not found, and we have a fine guess why.</dt>\n    <dd>\n        <ul class=\"disc\">\n            <li>If you typed the URL directly, please make sure the spelling is correct.</li>\n            <li>If you clicked on a link to get here, the link is outdated.</li>\n        </ul>\n    </dd>\n</dl>\n<dl>\n    <dt>What can you do?</dt>\n    <dd>Have no fear, help is near! There are many ways you can get back on track with Magento Store.</dd>\n    <dd>\n        <ul class=\"disc\">\n            <li><a href=\"#\" onclick=\"history.go(-1); return false;\">Go back</a> to the previous page.</li>\n            <li>Use the search bar at the top of the page to search for your products.</li>\n            <li>Follow these links to get you back on track!<br /><a href=\"{{store url=\"\"}}\">Store Home</a>\n            <span class=\"separator\">|</span> <a href=\"{{store url=\"customer/account\"}}\">My Account</a></li>\n        </ul>\n    </dd>\n</dl>\n',	'2016-12-14 06:01:00',	'2016-12-14 06:01:00',	1,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(2,	'Home page',	'two_columns_right',	NULL,	NULL,	'home',	NULL,	'<div class=\"page-title\">\r\n<h2>Home Page</h2>\r\n</div>',	'2016-12-14 06:01:00',	'2017-01-02 22:52:36',	0,	0,	'<!--<reference name=\"content\">\r\n        <block type=\"catalog/product_new\" name=\"home.catalog.product.new\" alias=\"product_new\" template=\"catalog/product/new.phtml\" after=\"cms_page\">\r\n            <action method=\"addPriceBlockType\">\r\n                <type>bundle</type>\r\n                <block>bundle/catalog_product_price</block>\r\n                <template>bundle/catalog/product/price.phtml</template>\r\n            </action>\r\n        </block>\r\n        <block type=\"reports/product_viewed\" name=\"home.reports.product.viewed\" alias=\"product_viewed\" template=\"reports/home_product_viewed.phtml\" after=\"product_new\">\r\n            <action method=\"addPriceBlockType\">\r\n                <type>bundle</type>\r\n                <block>bundle/catalog_product_price</block>\r\n                <template>bundle/catalog/product/price.phtml</template>\r\n            </action>\r\n        </block>\r\n        <block type=\"reports/product_compared\" name=\"home.reports.product.compared\" template=\"reports/home_product_compared.phtml\" after=\"product_viewed\">\r\n            <action method=\"addPriceBlockType\">\r\n                <type>bundle</type>\r\n                <block>bundle/catalog_product_price</block>\r\n                <template>bundle/catalog/product/price.phtml</template>\r\n            </action>\r\n        </block>\r\n    </reference>\r\n    <reference name=\"right\">\r\n        <action method=\"unsetChild\"><alias>right.reports.product.viewed</alias></action>\r\n        <action method=\"unsetChild\"><alias>right.reports.product.compared</alias></action>\r\n    </reference>-->',	NULL,	NULL,	NULL,	NULL,	NULL),
(3,	'About Us',	'two_columns_right',	NULL,	NULL,	'about-magento-demo-store',	NULL,	'\n<div class=\"page-title\">\n    <h1>About Magento Store</h1>\n</div>\n<div class=\"col3-set\">\n<div class=\"col-1\"><p style=\"line-height:1.2em;\"><small>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\nMorbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec,\ntempus vitae, iaculis semper, pede.</small></p>\n<p style=\"color:#888; font:1.2em/1.4em georgia, serif;\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\nMorbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis,\nporta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta.</p></div>\n<div class=\"col-2\">\n<p><strong style=\"color:#de036f;\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus.\nDuis lobortis. Nulla nec velit.</strong></p>\n<p>Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper.\nPhasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada\nfames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac,\ntempus nec, tempor nec, justo. </p>\n<p>Maecenas ullamcorper, odio vel tempus egestas, dui orci faucibus orci, sit amet aliquet lectus dolor et quam.\nPellentesque consequat luctus purus. Nunc et risus. Etiam a nibh. Phasellus dignissim metus eget nisi.\nVestibulum sapien dolor, aliquet nec, porta ac, malesuada a, libero. Praesent feugiat purus eget est.\nNulla facilisi. Vestibulum tincidunt sapien eu velit. Mauris purus. Maecenas eget mauris eu orci accumsan feugiat.\nPellentesque eget velit. Nunc tincidunt.</p></div>\n<div class=\"col-3\">\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.\nMauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.\nCras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in,\nfaucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper </p>\n<p><strong style=\"color:#de036f;\">Maecenas ullamcorper, odio vel tempus egestas, dui orci faucibus orci,\nsit amet aliquet lectus dolor et quam. Pellentesque consequat luctus purus.</strong></p>\n<p>Nunc et risus. Etiam a nibh. Phasellus dignissim metus eget nisi.</p>\n<div class=\"divider\"></div>\n<p>To all of you, from all of us at Magento Store - Thank you and Happy eCommerce!</p>\n<p style=\"line-height:1.2em;\"><strong style=\"font:italic 2em Georgia, serif;\">John Doe</strong><br />\n<small>Some important guy</small></p></div>\n</div>',	'2016-12-14 06:01:00',	'2016-12-14 06:01:00',	1,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(4,	'Customer Service',	'three_columns',	NULL,	NULL,	'customer-service',	NULL,	'<div class=\"page-title\">\n<h1>Customer Service</h1>\n</div>\n<ul class=\"disc\">\n<li><a href=\"#answer1\">Shipping &amp; Delivery</a></li>\n<li><a href=\"#answer2\">Privacy &amp; Security</a></li>\n<li><a href=\"#answer3\">Returns &amp; Replacements</a></li>\n<li><a href=\"#answer4\">Ordering</a></li>\n<li><a href=\"#answer5\">Payment, Pricing &amp; Promotions</a></li>\n<li><a href=\"#answer6\">Viewing Orders</a></li>\n<li><a href=\"#answer7\">Updating Account Information</a></li>\n</ul>\n<dl>\n<dt id=\"answer1\">Shipping &amp; Delivery</dt>\n<dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.\nMauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.\nCras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in,\nfaucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa.\nPellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\nNunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec,\njusto.</dd>\n<dt id=\"answer2\">Privacy &amp; Security</dt>\n<dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.\nMauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.\nCras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in,\nfaucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa.\nPellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\nNunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec,\njusto.</dd>\n<dt id=\"answer3\">Returns &amp; Replacements</dt>\n<dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.\nMauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.\nCras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in,\nfaucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa.\nPellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\nNunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec,\njusto.</dd>\n<dt id=\"answer4\">Ordering</dt>\n<dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.\nMauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.\nCras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in,\nfaucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa.\nPellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\nNunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec,\njusto.</dd>\n<dt id=\"answer5\">Payment, Pricing &amp; Promotions</dt>\n<dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.\nMauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.\nCras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in,\nfaucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa.\nPellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\nNunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec,\njusto.</dd>\n<dt id=\"answer6\">Viewing Orders</dt>\n<dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.\nMauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.\nCras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in,\nfaucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa.\n Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\n Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec,\n justo.</dd>\n<dt id=\"answer7\">Updating Account Information</dt>\n<dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.\n Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.\n Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in,\n faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa.\n Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\n Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec,\n justo.</dd>\n</dl>',	'2016-12-14 06:01:00',	'2016-12-14 06:01:00',	1,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(5,	'Enable Cookies',	'one_column',	NULL,	NULL,	'enable-cookies',	NULL,	'<div class=\"std\">\n    <ul class=\"messages\">\n        <li class=\"notice-msg\">\n            <ul>\n                <li>Please enable cookies in your web browser to continue.</li>\n            </ul>\n        </li>\n    </ul>\n    <div class=\"page-title\">\n        <h1><a name=\"top\"></a>What are Cookies?</h1>\n    </div>\n    <p>Cookies are short pieces of data that are sent to your computer when you visit a website.\n    On later visits, this data is then returned to that website. Cookies allow us to recognize you automatically\n    whenever you visit our site so that we can personalize your experience and provide you with better service.\n    We also use cookies (and similar browser data, such as Flash cookies) for fraud prevention and other purposes.\n     If your web browser is set to refuse cookies from our website, you will not be able to complete a purchase\n     or take advantage of certain features of our website, such as storing items in your Shopping Cart or\n     receiving personalized recommendations. As a result, we strongly encourage you to configure your web\n     browser to accept cookies from our website.</p>\n    <h2 class=\"subtitle\">Enabling Cookies</h2>\n    <ul class=\"disc\">\n        <li><a href=\"#ie7\">Internet Explorer 7.x</a></li>\n        <li><a href=\"#ie6\">Internet Explorer 6.x</a></li>\n        <li><a href=\"#firefox\">Mozilla/Firefox</a></li>\n        <li><a href=\"#opera\">Opera 7.x</a></li>\n    </ul>\n    <h3><a name=\"ie7\"></a>Internet Explorer 7.x</h3>\n    <ol>\n        <li>\n            <p>Start Internet Explorer</p>\n        </li>\n        <li>\n            <p>Under the <strong>Tools</strong> menu, click <strong>Internet Options</strong></p>\n            <p><img src=\"{{skin url=\"images/cookies/ie7-1.gif\"}}\" alt=\"\" /></p>\n        </li>\n        <li>\n            <p>Click the <strong>Privacy</strong> tab</p>\n            <p><img src=\"{{skin url=\"images/cookies/ie7-2.gif\"}}\" alt=\"\" /></p>\n        </li>\n        <li>\n            <p>Click the <strong>Advanced</strong> button</p>\n            <p><img src=\"{{skin url=\"images/cookies/ie7-3.gif\"}}\" alt=\"\" /></p>\n        </li>\n        <li>\n            <p>Put a check mark in the box for <strong>Override Automatic Cookie Handling</strong>,\n            put another check mark in the <strong>Always accept session cookies </strong>box</p>\n            <p><img src=\"{{skin url=\"images/cookies/ie7-4.gif\"}}\" alt=\"\" /></p>\n        </li>\n        <li>\n            <p>Click <strong>OK</strong></p>\n            <p><img src=\"{{skin url=\"images/cookies/ie7-5.gif\"}}\" alt=\"\" /></p>\n        </li>\n        <li>\n            <p>Click <strong>OK</strong></p>\n            <p><img src=\"{{skin url=\"images/cookies/ie7-6.gif\"}}\" alt=\"\" /></p>\n        </li>\n        <li>\n            <p>Restart Internet Explore</p>\n        </li>\n    </ol>\n    <p class=\"a-top\"><a href=\"#top\">Back to Top</a></p>\n    <h3><a name=\"ie6\"></a>Internet Explorer 6.x</h3>\n    <ol>\n        <li>\n            <p>Select <strong>Internet Options</strong> from the Tools menu</p>\n            <p><img src=\"{{skin url=\"images/cookies/ie6-1.gif\"}}\" alt=\"\" /></p>\n        </li>\n        <li>\n            <p>Click on the <strong>Privacy</strong> tab</p>\n        </li>\n        <li>\n            <p>Click the <strong>Default</strong> button (or manually slide the bar down to <strong>Medium</strong>)\n            under <strong>Settings</strong>. Click <strong>OK</strong></p>\n            <p><img src=\"{{skin url=\"images/cookies/ie6-2.gif\"}}\" alt=\"\" /></p>\n        </li>\n    </ol>\n    <p class=\"a-top\"><a href=\"#top\">Back to Top</a></p>\n    <h3><a name=\"firefox\"></a>Mozilla/Firefox</h3>\n    <ol>\n        <li>\n            <p>Click on the <strong>Tools</strong>-menu in Mozilla</p>\n        </li>\n        <li>\n            <p>Click on the <strong>Options...</strong> item in the menu - a new window open</p>\n        </li>\n        <li>\n            <p>Click on the <strong>Privacy</strong> selection in the left part of the window. (See image below)</p>\n            <p><img src=\"{{skin url=\"images/cookies/firefox.png\"}}\" alt=\"\" /></p>\n        </li>\n        <li>\n            <p>Expand the <strong>Cookies</strong> section</p>\n        </li>\n        <li>\n            <p>Check the <strong>Enable cookies</strong> and <strong>Accept cookies normally</strong> checkboxes</p>\n        </li>\n        <li>\n            <p>Save changes by clicking <strong>Ok</strong>.</p>\n        </li>\n    </ol>\n    <p class=\"a-top\"><a href=\"#top\">Back to Top</a></p>\n    <h3><a name=\"opera\"></a>Opera 7.x</h3>\n    <ol>\n        <li>\n            <p>Click on the <strong>Tools</strong> menu in Opera</p>\n        </li>\n        <li>\n            <p>Click on the <strong>Preferences...</strong> item in the menu - a new window open</p>\n        </li>\n        <li>\n            <p>Click on the <strong>Privacy</strong> selection near the bottom left of the window. (See image below)</p>\n            <p><img src=\"{{skin url=\"images/cookies/opera.png\"}}\" alt=\"\" /></p>\n        </li>\n        <li>\n            <p>The <strong>Enable cookies</strong> checkbox must be checked, and <strong>Accept all cookies</strong>\n            should be selected in the &quot;<strong>Normal cookies</strong>&quot; drop-down</p>\n        </li>\n        <li>\n            <p>Save changes by clicking <strong>Ok</strong></p>\n        </li>\n    </ol>\n    <p class=\"a-top\"><a href=\"#top\">Back to Top</a></p>\n</div>\n',	'2016-12-14 06:01:00',	'2016-12-14 06:01:00',	1,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(6,	'Privacy Policy',	'one_column',	NULL,	NULL,	'privacy-policy-cookie-restriction-mode',	'Privacy Policy',	'<p style=\"color: #ff0000; font-weight: bold; font-size: 13px\">\n    Please replace this text with you Privacy Policy.\n    Please add any additional cookies your website uses below (e.g., Google Analytics)\n</p>\n<p>\n    This privacy policy sets out how {{config path=\"general/store_information/name\"}} uses and protects any information\n    that you give {{config path=\"general/store_information/name\"}} when you use this website.\n    {{config path=\"general/store_information/name\"}} is committed to ensuring that your privacy is protected.\n    Should we ask you to provide certain information by which you can be identified when using this website,\n    then you can be assured that it will only be used in accordance with this privacy statement.\n    {{config path=\"general/store_information/name\"}} may change this policy from time to time by updating this page.\n    You should check this page from time to time to ensure that you are happy with any changes.\n</p>\n<h2>What we collect</h2>\n<p>We may collect the following information:</p>\n<ul>\n    <li>name</li>\n    <li>contact information including email address</li>\n    <li>demographic information such as postcode, preferences and interests</li>\n    <li>other information relevant to customer surveys and/or offers</li>\n</ul>\n<p>\n    For the exhaustive list of cookies we collect see the <a href=\"#list\">List of cookies we collect</a> section.\n</p>\n<h2>What we do with the information we gather</h2>\n<p>\n    We require this information to understand your needs and provide you with a better service,\n    and in particular for the following reasons:\n</p>\n<ul>\n    <li>Internal record keeping.</li>\n    <li>We may use the information to improve our products and services.</li>\n    <li>\n        We may periodically send promotional emails about new products, special offers or other information which we\n        think you may find interesting using the email address which you have provided.\n    </li>\n    <li>\n        From time to time, we may also use your information to contact you for market research purposes.\n        We may contact you by email, phone, fax or mail. We may use the information to customise the website\n        according to your interests.\n    </li>\n</ul>\n<h2>Security</h2>\n<p>\n    We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure,\n    we have put in place suitable physical, electronic and managerial procedures to safeguard and secure\n    the information we collect online.\n</p>\n<h2>How we use cookies</h2>\n<p>\n    A cookie is a small file which asks permission to be placed on your computer\'s hard drive.\n    Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit\n    a particular site. Cookies allow web applications to respond to you as an individual. The web application\n    can tailor its operations to your needs, likes and dislikes by gathering and remembering information about\n    your preferences.\n</p>\n<p>\n    We use traffic log cookies to identify which pages are being used. This helps us analyse data about web page traffic\n    and improve our website in order to tailor it to customer needs. We only use this information for statistical\n    analysis purposes and then the data is removed from the system.\n</p>\n<p>\n    Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful\n    and which you do not. A cookie in no way gives us access to your computer or any information about you,\n    other than the data you choose to share with us. You can choose to accept or decline cookies.\n    Most web browsers automatically accept cookies, but you can usually modify your browser setting\n    to decline cookies if you prefer. This may prevent you from taking full advantage of the website.\n</p>\n<h2>Links to other websites</h2>\n<p>\n    Our website may contain links to other websites of interest. However, once you have used these links\n    to leave our site, you should note that we do not have any control over that other website.\n    Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst\n    visiting such sites and such sites are not governed by this privacy statement.\n    You should exercise caution and look at the privacy statement applicable to the website in question.\n</p>\n<h2>Controlling your personal information</h2>\n<p>You may choose to restrict the collection or use of your personal information in the following ways:</p>\n<ul>\n    <li>\n        whenever you are asked to fill in a form on the website, look for the box that you can click to indicate\n        that you do not want the information to be used by anybody for direct marketing purposes\n    </li>\n    <li>\n        if you have previously agreed to us using your personal information for direct marketing purposes,\n        you may change your mind at any time by writing to or emailing us at\n        {{config path=\"trans_email/ident_general/email\"}}\n    </li>\n</ul>\n<p>\n    We will not sell, distribute or lease your personal information to third parties unless we have your permission\n    or are required by law to do so. We may use your personal information to send you promotional information\n    about third parties which we think you may find interesting if you tell us that you wish this to happen.\n</p>\n<p>\n    You may request details of personal information which we hold about you under the Data Protection Act 1998.\n    A small fee will be payable. If you would like a copy of the information held on you please write to\n    {{config path=\"general/store_information/address\"}}.\n</p>\n<p>\n    If you believe that any information we are holding on you is incorrect or incomplete,\n    please write to or email us as soon as possible, at the above address.\n    We will promptly correct any information found to be incorrect.\n</p>\n<h2><a name=\"list\"></a>List of cookies we collect</h2>\n<p>The table below lists the cookies we collect and what information they store.</p>\n<table class=\"data-table\">\n    <thead>\n        <tr>\n            <th>COOKIE name</th>\n            <th>COOKIE Description</th>\n        </tr>\n    </thead>\n    <tbody>\n        <tr>\n            <th>CART</th>\n            <td>The association with your shopping cart.</td>\n        </tr>\n        <tr>\n            <th>CATEGORY_INFO</th>\n            <td>Stores the category info on the page, that allows to display pages more quickly.</td>\n        </tr>\n        <tr>\n            <th>COMPARE</th>\n            <td>The items that you have in the Compare Products list.</td>\n        </tr>\n        <tr>\n            <th>CURRENCY</th>\n            <td>Your preferred currency</td>\n        </tr>\n        <tr>\n            <th>CUSTOMER</th>\n            <td>An encrypted version of your customer id with the store.</td>\n        </tr>\n        <tr>\n            <th>CUSTOMER_AUTH</th>\n            <td>An indicator if you are currently logged into the store.</td>\n        </tr>\n        <tr>\n            <th>CUSTOMER_INFO</th>\n            <td>An encrypted version of the customer group you belong to.</td>\n        </tr>\n        <tr>\n            <th>CUSTOMER_SEGMENT_IDS</th>\n            <td>Stores the Customer Segment ID</td>\n        </tr>\n        <tr>\n            <th>EXTERNAL_NO_CACHE</th>\n            <td>A flag, which indicates whether caching is disabled or not.</td>\n        </tr>\n        <tr>\n            <th>FRONTEND</th>\n            <td>You sesssion ID on the server.</td>\n        </tr>\n        <tr>\n            <th>GUEST-VIEW</th>\n            <td>Allows guests to edit their orders.</td>\n        </tr>\n        <tr>\n            <th>LAST_CATEGORY</th>\n            <td>The last category you visited.</td>\n        </tr>\n        <tr>\n            <th>LAST_PRODUCT</th>\n            <td>The most recent product you have viewed.</td>\n        </tr>\n        <tr>\n            <th>NEWMESSAGE</th>\n            <td>Indicates whether a new message has been received.</td>\n        </tr>\n        <tr>\n            <th>NO_CACHE</th>\n            <td>Indicates whether it is allowed to use cache.</td>\n        </tr>\n        <tr>\n            <th>PERSISTENT_SHOPPING_CART</th>\n            <td>A link to information about your cart and viewing history if you have asked the site.</td>\n        </tr>\n        <tr>\n            <th>POLL</th>\n            <td>The ID of any polls you have recently voted in.</td>\n        </tr>\n        <tr>\n            <th>POLLN</th>\n            <td>Information on what polls you have voted on.</td>\n        </tr>\n        <tr>\n            <th>RECENTLYCOMPARED</th>\n            <td>The items that you have recently compared.            </td>\n        </tr>\n        <tr>\n            <th>STF</th>\n            <td>Information on products you have emailed to friends.</td>\n        </tr>\n        <tr>\n            <th>STORE</th>\n            <td>The store view or language you have selected.</td>\n        </tr>\n        <tr>\n            <th>USER_ALLOWED_SAVE_COOKIE</th>\n            <td>Indicates whether a customer allowed to use cookies.</td>\n        </tr>\n        <tr>\n            <th>VIEWED_PRODUCT_IDS</th>\n            <td>The products that you have recently viewed.</td>\n        </tr>\n        <tr>\n            <th>WISHLIST</th>\n            <td>An encrypted list of products added to your Wishlist.</td>\n        </tr>\n        <tr>\n            <th>WISHLIST_CNT</th>\n            <td>The number of items in your Wishlist.</td>\n        </tr>\n    </tbody>\n</table>',	'2016-12-14 06:01:00',	'2016-12-14 06:01:00',	1,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(7,	'Home Page',	'custom_static_page_one',	NULL,	NULL,	'home_page',	NULL,	'<div class=\"featured-pro container wow bounceInUp animated\">{{block type=\"catalog/product_list\" num_products=\"8\" name=\"featuredproduct\" as=\"featuredproduct\" template=\"catalog/product/featured.phtml\" }}</div>',	'2016-12-16 12:21:29',	'2017-01-03 01:46:12',	1,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(8,	'Astrabootstrap 404 No Route',	'one_column',	NULL,	NULL,	'astrabootstrap_no_route',	NULL,	'<div class=\"norout\">\n<div class=\"page-not-found wow bounceInRight animated\">\n<h2>404</h2>\n<h3><img src=\"{{skin url=\"images/signal.png\"}}\" alt=\"\" />Oops! The Page you requested was not found!</h3>\n<div><a class=\"btn-home\" type=\"button\" href=\"{{store direct_url=\"accent_home_one\"}}\"><span>Back To Home</span></a></div>\n</div>\n</div>\n',	'2016-12-16 12:21:29',	'2016-12-16 12:21:29',	1,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `cms_page_store`;
CREATE TABLE `cms_page_store` (
  `page_id` smallint(6) NOT NULL COMMENT 'Page ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`page_id`,`store_id`),
  KEY `IDX_CMS_PAGE_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CMS_PAGE_STORE_PAGE_ID_CMS_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CMS_PAGE_STORE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Page To Store Linkage Table';

INSERT INTO `cms_page_store` (`page_id`, `store_id`) VALUES
(1,	0),
(3,	0),
(4,	0),
(5,	0),
(6,	0),
(8,	0),
(2,	1),
(7,	1);

DROP TABLE IF EXISTS `core_cache`;
CREATE TABLE `core_cache` (
  `id` varchar(200) NOT NULL COMMENT 'Cache Id',
  `data` mediumblob COMMENT 'Cache Data',
  `create_time` int(11) DEFAULT NULL COMMENT 'Cache Creation Time',
  `update_time` int(11) DEFAULT NULL COMMENT 'Time of Cache Updating',
  `expire_time` int(11) DEFAULT NULL COMMENT 'Cache Expiration Time',
  PRIMARY KEY (`id`),
  KEY `IDX_CORE_CACHE_EXPIRE_TIME` (`expire_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Caches';


DROP TABLE IF EXISTS `core_cache_option`;
CREATE TABLE `core_cache_option` (
  `code` varchar(32) NOT NULL COMMENT 'Code',
  `value` smallint(6) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cache Options';

INSERT INTO `core_cache_option` (`code`, `value`) VALUES
('block_html',	0),
('collections',	0),
('config',	0),
('config_api',	0),
('config_api2',	0),
('eav',	0),
('layout',	0),
('translate',	0);

DROP TABLE IF EXISTS `core_cache_tag`;
CREATE TABLE `core_cache_tag` (
  `tag` varchar(100) NOT NULL COMMENT 'Tag',
  `cache_id` varchar(200) NOT NULL COMMENT 'Cache Id',
  PRIMARY KEY (`tag`,`cache_id`),
  KEY `IDX_CORE_CACHE_TAG_CACHE_ID` (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag Caches';


DROP TABLE IF EXISTS `core_config_data`;
CREATE TABLE `core_config_data` (
  `config_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Config Id',
  `scope` varchar(8) NOT NULL DEFAULT 'default' COMMENT 'Config Scope',
  `scope_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Config Scope Id',
  `path` varchar(255) NOT NULL DEFAULT 'general' COMMENT 'Config Path',
  `value` text COMMENT 'Config Value',
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `UNQ_CORE_CONFIG_DATA_SCOPE_SCOPE_ID_PATH` (`scope`,`scope_id`,`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Config Data';

INSERT INTO `core_config_data` (`config_id`, `scope`, `scope_id`, `path`, `value`) VALUES
(1,	'default',	0,	'general/region/display_all',	'0'),
(2,	'default',	0,	'general/region/state_required',	'AU'),
(3,	'default',	0,	'catalog/category/root_id',	'2'),
(4,	'default',	0,	'payment/paypal_express/skip_order_review_step',	'1'),
(5,	'default',	0,	'payment/payflow_link/mobile_optimized',	'1'),
(6,	'default',	0,	'payment/payflow_advanced/mobile_optimized',	'1'),
(7,	'default',	0,	'payment/hosted_pro/mobile_optimized',	'1'),
(8,	'default',	0,	'web/seo/use_rewrites',	'1'),
(9,	'default',	0,	'admin/dashboard/enable_charts',	'1'),
(10,	'default',	0,	'web/unsecure/base_url',	'http://auswarehouse.corre.com.au/'),
(11,	'default',	0,	'web/secure/base_url',	'http://auswarehouse.corre.com.au/'),
(12,	'default',	0,	'general/locale/code',	'en_AU'),
(13,	'default',	0,	'general/locale/timezone',	'Australia/Sydney'),
(14,	'default',	0,	'currency/options/base',	'AUD'),
(15,	'default',	0,	'currency/options/default',	'AUD'),
(16,	'default',	0,	'currency/options/allow',	'AUD'),
(17,	'default',	0,	'design/package/name',	'rwd'),
(18,	'default',	0,	'design/package/ua_regexp',	'a:0:{}'),
(19,	'default',	0,	'design/theme/locale',	NULL),
(20,	'default',	0,	'design/theme/template',	NULL),
(21,	'default',	0,	'design/theme/template_ua_regexp',	'a:0:{}'),
(22,	'default',	0,	'design/theme/skin',	NULL),
(23,	'default',	0,	'design/theme/skin_ua_regexp',	'a:0:{}'),
(24,	'default',	0,	'design/theme/layout',	NULL),
(25,	'default',	0,	'design/theme/layout_ua_regexp',	'a:0:{}'),
(26,	'default',	0,	'design/theme/default',	'astrabootstrap'),
(27,	'default',	0,	'design/theme/default_ua_regexp',	'a:0:{}'),
(28,	'default',	0,	'design/head/default_title',	'Aus Warehouse'),
(29,	'default',	0,	'design/head/title_prefix',	NULL),
(30,	'default',	0,	'design/head/title_suffix',	NULL),
(31,	'default',	0,	'design/head/default_description',	'Default Description'),
(32,	'default',	0,	'design/head/default_keywords',	'Magento, Varien, E-commerce'),
(33,	'default',	0,	'design/head/default_robots',	'INDEX,FOLLOW'),
(34,	'default',	0,	'design/head/includes',	NULL),
(35,	'default',	0,	'design/head/demonotice',	'0'),
(36,	'default',	0,	'design/header/logo_src',	'images/logo.gif'),
(37,	'default',	0,	'design/header/logo_alt',	'Magento Commerce'),
(38,	'default',	0,	'design/header/logo_src_small',	'images/logo.gif'),
(39,	'default',	0,	'design/header/welcome',	'Default welcome msg!'),
(40,	'default',	0,	'design/footer/copyright',	'&copy; 2017 Aus Warehouse Store. All Rights Reserved.'),
(41,	'default',	0,	'design/footer/absolute_footer',	NULL),
(42,	'default',	0,	'design/watermark/image_size',	NULL),
(43,	'default',	0,	'design/watermark/image_imageOpacity',	NULL),
(44,	'default',	0,	'design/watermark/image_position',	'stretch'),
(45,	'default',	0,	'design/watermark/small_image_size',	NULL),
(46,	'default',	0,	'design/watermark/small_image_imageOpacity',	NULL),
(47,	'default',	0,	'design/watermark/small_image_position',	'stretch'),
(48,	'default',	0,	'design/watermark/thumbnail_size',	NULL),
(49,	'default',	0,	'design/watermark/thumbnail_imageOpacity',	NULL),
(50,	'default',	0,	'design/watermark/thumbnail_position',	'stretch'),
(51,	'default',	0,	'design/pagination/pagination_frame',	'5'),
(52,	'default',	0,	'design/pagination/pagination_frame_skip',	NULL),
(53,	'default',	0,	'design/pagination/anchor_text_for_previous',	NULL),
(54,	'default',	0,	'design/pagination/anchor_text_for_next',	NULL),
(55,	'default',	0,	'design/email/logo_alt',	NULL),
(56,	'default',	0,	'design/email/logo_width',	NULL),
(57,	'default',	0,	'design/email/logo_height',	NULL),
(58,	'default',	0,	'design/email/header',	'design_email_header'),
(59,	'default',	0,	'design/email/footer',	'design_email_footer'),
(60,	'default',	0,	'design/email/css_non_inline',	'email-non-inline.css'),
(61,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_appearance/width',	'flexible'),
(62,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_appearance/footer',	'informative'),
(63,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_appearance/font',	'Abel'),
(64,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_appearance/color',	'#00acf3'),
(65,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_appearance/header_bg_color',	'#ffffff'),
(66,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_appearance/header_fg_color',	NULL),
(67,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_appearance/footer_bg_color',	'#00acf3'),
(68,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_appearance/footer_fg_color',	NULL),
(69,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_appearance/content_bg_color',	NULL),
(70,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_header/navigation_home',	'1'),
(71,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_menu/navigation_menu_type',	'mega-menu'),
(72,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_labels/new_label',	'1'),
(73,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_labels/new_label_text',	'New'),
(74,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_labels/new_label_position',	'top-left'),
(75,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_labels/sale_label',	'1'),
(76,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_labels/sale_label_text',	'Sale'),
(77,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_labels/sale_label_position',	'top-right'),
(78,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_footer/footer_google_analytics',	'0'),
(79,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_footer/footer_google_analytics_path',	NULL),
(80,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/social_link',	'1'),
(81,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/facebook_social_link',	'1'),
(82,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/facebook_social_link_path',	'#'),
(83,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/twitter_social_link',	'1'),
(84,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/twitter_social_link_path',	'#'),
(85,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/googleplus_social_link',	'1'),
(86,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/googleplus_social_link_path',	'#'),
(87,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/rss_social_link',	'1'),
(88,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/rss_social_link_path',	'#'),
(89,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/pinterest_social_link',	'1'),
(90,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/pinterest_social_link_path',	'#'),
(91,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/youtube_social_link',	'1'),
(92,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_social/youtube_social_link_path',	'#'),
(93,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_products/upsell_product',	'1'),
(94,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_products/related_product',	'1'),
(95,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_products/nextprevious_product',	'1'),
(96,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_newsletterpopup/newsletterpopup_enable',	'0'),
(97,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_shoppingcart/shoppingcart_top',	'1'),
(98,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_cloudzoom/cloudzoom_view',	'1'),
(99,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_quickview/enable',	'0'),
(100,	'default',	0,	'web/url/use_store',	'0'),
(101,	'default',	0,	'web/url/redirect_to_base',	'1'),
(102,	'default',	0,	'web/unsecure/base_link_url',	'{{unsecure_base_url}}'),
(103,	'default',	0,	'web/unsecure/base_skin_url',	'{{unsecure_base_url}}skin/'),
(104,	'default',	0,	'web/unsecure/base_media_url',	'{{unsecure_base_url}}media/'),
(105,	'default',	0,	'web/unsecure/base_js_url',	'{{unsecure_base_url}}js/'),
(106,	'default',	0,	'web/secure/base_link_url',	'{{secure_base_url}}'),
(107,	'default',	0,	'web/secure/base_skin_url',	'{{secure_base_url}}skin/'),
(108,	'default',	0,	'web/secure/base_media_url',	'{{secure_base_url}}media/'),
(109,	'default',	0,	'web/secure/base_js_url',	'{{secure_base_url}}js/'),
(110,	'default',	0,	'web/secure/use_in_frontend',	'0'),
(111,	'default',	0,	'web/secure/use_in_adminhtml',	'0'),
(112,	'default',	0,	'web/secure/offloader_header',	'SSL_OFFLOADED'),
(113,	'default',	0,	'web/default/front',	'cms'),
(114,	'default',	0,	'web/default/cms_home_page',	'home_page'),
(115,	'default',	0,	'web/default/no_route',	'cms/index/noRoute'),
(116,	'default',	0,	'web/default/cms_no_route',	'astrabootstrap_no_route'),
(117,	'default',	0,	'web/default/cms_no_cookies',	'enable-cookies'),
(118,	'default',	0,	'web/default/show_cms_breadcrumbs',	'1'),
(119,	'default',	0,	'web/polls/poll_check_by_ip',	'0'),
(120,	'default',	0,	'web/cookie/cookie_lifetime',	'3600'),
(121,	'default',	0,	'web/cookie/cookie_path',	NULL),
(122,	'default',	0,	'web/cookie/cookie_domain',	NULL),
(123,	'default',	0,	'web/cookie/cookie_httponly',	'1'),
(124,	'default',	0,	'web/cookie/cookie_restriction',	'0'),
(125,	'default',	0,	'web/session/use_remote_addr',	'0'),
(126,	'default',	0,	'web/session/use_http_via',	'0'),
(127,	'default',	0,	'web/session/use_http_x_forwarded_for',	'0'),
(128,	'default',	0,	'web/session/use_http_user_agent',	'0'),
(129,	'default',	0,	'web/session/use_frontend_sid',	'1'),
(130,	'default',	0,	'web/browser_capabilities/cookies',	'1'),
(131,	'default',	0,	'web/browser_capabilities/javascript',	'1'),
(132,	'websites',	1,	'dev/debug/template_hints',	'0'),
(133,	'websites',	1,	'dev/debug/template_hints_blocks',	'0'),
(134,	'default',	0,	'checkout/options/onepage_checkout_enabled',	'1'),
(135,	'default',	0,	'checkout/options/guest_checkout',	'1'),
(136,	'default',	0,	'checkout/options/enable_agreements',	'0'),
(137,	'default',	0,	'checkout/cart/delete_quote_after',	'30'),
(138,	'default',	0,	'checkout/cart/redirect_to_cart',	'0'),
(139,	'default',	0,	'checkout/cart/grouped_product_image',	'itself'),
(140,	'default',	0,	'checkout/cart/configurable_product_image',	'parent'),
(141,	'default',	0,	'checkout/cart_link/use_qty',	'1'),
(142,	'default',	0,	'checkout/sidebar/display',	'1'),
(143,	'default',	0,	'checkout/sidebar/count',	'3'),
(144,	'default',	0,	'checkout/payment_failed/reciever',	'general'),
(145,	'default',	0,	'checkout/payment_failed/identity',	'general'),
(146,	'default',	0,	'checkout/payment_failed/template',	'checkout_payment_failed_template'),
(147,	'default',	0,	'checkout/payment_failed/copy_to',	NULL),
(148,	'default',	0,	'checkout/payment_failed/copy_method',	'bcc'),
(149,	'default',	0,	'general/country/default',	'AU'),
(150,	'default',	0,	'general/country/allow',	'AU'),
(151,	'default',	0,	'general/country/optional_zip_countries',	'AU'),
(152,	'default',	0,	'general/country/eu_countries',	'AU'),
(153,	'default',	0,	'general/locale/firstday',	'0'),
(154,	'default',	0,	'general/locale/weekend',	'0,6'),
(155,	'default',	0,	'general/store_information/name',	'Aus Warehouse'),
(156,	'default',	0,	'general/store_information/phone',	'029999999'),
(157,	'default',	0,	'general/store_information/hours',	NULL),
(158,	'default',	0,	'general/store_information/merchant_country',	'AU'),
(159,	'default',	0,	'general/store_information/merchant_vat_number',	NULL),
(160,	'default',	0,	'general/store_information/address',	'Addrdess'),
(161,	'default',	0,	'catalog/frontend/list_mode',	'grid-list'),
(162,	'default',	0,	'catalog/frontend/grid_per_page_values',	'12,24,36'),
(163,	'default',	0,	'catalog/frontend/grid_per_page',	'12'),
(164,	'default',	0,	'catalog/frontend/list_per_page_values',	'5,10,15,20,25'),
(165,	'default',	0,	'catalog/frontend/list_per_page',	'10'),
(166,	'default',	0,	'catalog/frontend/list_allow_all',	'0'),
(167,	'default',	0,	'catalog/frontend/default_sort_by',	'position'),
(168,	'default',	0,	'catalog/frontend/flat_catalog_category',	'0'),
(169,	'default',	0,	'catalog/frontend/flat_catalog_product',	'0'),
(170,	'default',	0,	'catalog/frontend/parse_url_directives',	'1'),
(171,	'default',	0,	'catalog/sitemap/tree_mode',	'0'),
(172,	'default',	0,	'catalog/sitemap/lines_perpage',	'30'),
(173,	'default',	0,	'catalog/review/allow_guest',	'0'),
(174,	'default',	0,	'catalog/product_image/base_width',	'1800'),
(175,	'default',	0,	'catalog/product_image/small_width',	'210'),
(176,	'default',	0,	'catalog/product_image/max_dimension',	'5000'),
(177,	'default',	0,	'catalog/productalert/allow_price',	'0'),
(178,	'default',	0,	'catalog/productalert/email_price_template',	'catalog_productalert_email_price_template'),
(179,	'default',	0,	'catalog/productalert/allow_stock',	'0'),
(180,	'default',	0,	'catalog/productalert/email_stock_template',	'catalog_productalert_email_stock_template'),
(181,	'default',	0,	'catalog/productalert/email_identity',	'general'),
(182,	'default',	0,	'catalog/productalert_cron/frequency',	'D'),
(183,	'default',	0,	'crontab/jobs/catalog_product_alert/schedule/cron_expr',	'0 0 * * *'),
(184,	'default',	0,	'crontab/jobs/catalog_product_alert/run/model',	'productalert/observer::process'),
(185,	'default',	0,	'catalog/productalert_cron/time',	'00,00,00'),
(186,	'default',	0,	'catalog/productalert_cron/error_email',	NULL),
(187,	'default',	0,	'catalog/productalert_cron/error_email_identity',	'general'),
(188,	'default',	0,	'catalog/productalert_cron/error_email_template',	'catalog_productalert_cron_error_email_template'),
(189,	'default',	0,	'catalog/recently_products/scope',	'website'),
(190,	'default',	0,	'catalog/recently_products/viewed_count',	'5'),
(191,	'default',	0,	'catalog/recently_products/compared_count',	'5'),
(192,	'default',	0,	'catalog/price/scope',	'0'),
(193,	'default',	0,	'catalog/layered_navigation/display_product_count',	'1'),
(194,	'default',	0,	'catalog/layered_navigation/price_range_calculation',	'auto'),
(195,	'default',	0,	'catalog/navigation/max_depth',	'0'),
(196,	'default',	0,	'catalog/seo/site_map',	'1'),
(197,	'default',	0,	'catalog/seo/search_terms',	'1'),
(198,	'default',	0,	'catalog/seo/product_url_suffix',	NULL),
(199,	'default',	0,	'catalog/seo/category_url_suffix',	NULL),
(200,	'default',	0,	'catalog/seo/product_use_categories',	'1'),
(201,	'default',	0,	'catalog/seo/save_rewrites_history',	'1'),
(202,	'default',	0,	'catalog/seo/title_separator',	'-'),
(203,	'default',	0,	'catalog/seo/category_canonical_tag',	'0'),
(204,	'default',	0,	'catalog/seo/product_canonical_tag',	'0'),
(205,	'default',	0,	'catalog/search/min_query_length',	'1'),
(206,	'default',	0,	'catalog/search/max_query_length',	'128'),
(207,	'default',	0,	'catalog/search/max_query_words',	'10'),
(208,	'default',	0,	'catalog/search/search_type',	'1'),
(209,	'default',	0,	'catalog/search/use_layered_navigation_count',	'2000'),
(210,	'default',	0,	'catalog/search/show_autocomplete_results_count',	'1'),
(211,	'default',	0,	'catalog/downloadable/order_item_status',	'9'),
(212,	'default',	0,	'catalog/downloadable/downloads_number',	'0'),
(213,	'default',	0,	'catalog/downloadable/shareable',	'0'),
(214,	'default',	0,	'catalog/downloadable/samples_title',	'Samples'),
(215,	'default',	0,	'catalog/downloadable/links_title',	'Links'),
(216,	'default',	0,	'catalog/downloadable/links_target_new_window',	'1'),
(217,	'default',	0,	'catalog/downloadable/content_disposition',	'inline'),
(218,	'default',	0,	'catalog/downloadable/disable_guest_checkout',	'1'),
(219,	'default',	0,	'catalog/custom_options/use_calendar',	'0'),
(220,	'default',	0,	'catalog/custom_options/date_fields_order',	'm,d,y'),
(221,	'default',	0,	'catalog/custom_options/time_format',	'12h'),
(222,	'default',	0,	'catalog/custom_options/year_range',	','),
(223,	'default',	0,	'astrabootstrapsettings/astrabootstrapsettings_header/ecategory',	'7');

DROP TABLE IF EXISTS `core_email_queue`;
CREATE TABLE `core_email_queue` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Message Id',
  `entity_id` int(10) unsigned DEFAULT NULL COMMENT 'Entity ID',
  `entity_type` varchar(128) DEFAULT NULL COMMENT 'Entity Type',
  `event_type` varchar(128) DEFAULT NULL COMMENT 'Event Type',
  `message_body_hash` varchar(64) NOT NULL COMMENT 'Message Body Hash',
  `message_body` mediumtext NOT NULL COMMENT 'Message Body',
  `message_parameters` text NOT NULL COMMENT 'Message Parameters',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Creation Time',
  `processed_at` timestamp NULL DEFAULT NULL COMMENT 'Finish Time',
  PRIMARY KEY (`message_id`),
  KEY `0ADECE62FD629241C147389ADF20706E` (`entity_id`,`entity_type`,`event_type`,`message_body_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Email Queue';


DROP TABLE IF EXISTS `core_email_queue_recipients`;
CREATE TABLE `core_email_queue_recipients` (
  `recipient_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Recipient Id',
  `message_id` int(10) unsigned NOT NULL COMMENT 'Message ID',
  `recipient_email` varchar(128) NOT NULL COMMENT 'Recipient Email',
  `recipient_name` varchar(255) NOT NULL COMMENT 'Recipient Name',
  `email_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Email Type',
  PRIMARY KEY (`recipient_id`),
  UNIQUE KEY `19BDB9C5FE4BD685FCF992A71E976CD0` (`message_id`,`recipient_email`,`email_type`),
  KEY `IDX_CORE_EMAIL_QUEUE_RECIPIENTS_RECIPIENT_EMAIL` (`recipient_email`),
  KEY `IDX_CORE_EMAIL_QUEUE_RECIPIENTS_EMAIL_TYPE` (`email_type`),
  CONSTRAINT `FK_6F4948F3ABF97DE12127EF14B140802A` FOREIGN KEY (`message_id`) REFERENCES `core_email_queue` (`message_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Email Queue';


DROP TABLE IF EXISTS `core_email_template`;
CREATE TABLE `core_email_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template Id',
  `template_code` varchar(150) NOT NULL COMMENT 'Template Name',
  `template_text` text NOT NULL COMMENT 'Template Content',
  `template_styles` text COMMENT 'Templste Styles',
  `template_type` int(10) unsigned DEFAULT NULL COMMENT 'Template Type',
  `template_subject` varchar(200) NOT NULL COMMENT 'Template Subject',
  `template_sender_name` varchar(200) DEFAULT NULL COMMENT 'Template Sender Name',
  `template_sender_email` varchar(200) DEFAULT NULL COMMENT 'Template Sender Email',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Date of Template Creation',
  `modified_at` timestamp NULL DEFAULT NULL COMMENT 'Date of Template Modification',
  `orig_template_code` varchar(200) DEFAULT NULL COMMENT 'Original Template Code',
  `orig_template_variables` text COMMENT 'Original Template Variables',
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `UNQ_CORE_EMAIL_TEMPLATE_TEMPLATE_CODE` (`template_code`),
  KEY `IDX_CORE_EMAIL_TEMPLATE_ADDED_AT` (`added_at`),
  KEY `IDX_CORE_EMAIL_TEMPLATE_MODIFIED_AT` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Email Templates';


DROP TABLE IF EXISTS `core_flag`;
CREATE TABLE `core_flag` (
  `flag_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Flag Id',
  `flag_code` varchar(255) NOT NULL COMMENT 'Flag Code',
  `state` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag State',
  `flag_data` text COMMENT 'Flag Data',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Last Flag Update',
  PRIMARY KEY (`flag_id`),
  KEY `IDX_CORE_FLAG_LAST_UPDATE` (`last_update`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Flag';

INSERT INTO `core_flag` (`flag_id`, `flag_code`, `state`, `flag_data`, `last_update`) VALUES
(1,	'admin_notification_survey',	0,	'a:1:{s:13:\"survey_viewed\";b:1;}',	'2016-12-14 06:05:36'),
(2,	'catalog_product_flat',	0,	'a:2:{s:8:\"is_built\";b:1;s:16:\"is_store_built_1\";b:1;}',	'2016-12-16 12:05:53');

DROP TABLE IF EXISTS `core_layout_link`;
CREATE TABLE `core_layout_link` (
  `layout_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `area` varchar(64) DEFAULT NULL COMMENT 'Area',
  `package` varchar(64) DEFAULT NULL COMMENT 'Package',
  `theme` varchar(64) DEFAULT NULL COMMENT 'Theme',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Layout Update Id',
  PRIMARY KEY (`layout_link_id`),
  UNIQUE KEY `UNQ_CORE_LAYOUT_LINK_STORE_ID_PACKAGE_THEME_LAYOUT_UPDATE_ID` (`store_id`,`package`,`theme`,`layout_update_id`),
  KEY `IDX_CORE_LAYOUT_LINK_LAYOUT_UPDATE_ID` (`layout_update_id`),
  CONSTRAINT `FK_CORE_LAYOUT_LINK_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CORE_LYT_LNK_LYT_UPDATE_ID_CORE_LYT_UPDATE_LYT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `core_layout_update` (`layout_update_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout Link';


DROP TABLE IF EXISTS `core_layout_update`;
CREATE TABLE `core_layout_update` (
  `layout_update_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Layout Update Id',
  `handle` varchar(255) DEFAULT NULL COMMENT 'Handle',
  `xml` text COMMENT 'Xml',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`layout_update_id`),
  KEY `IDX_CORE_LAYOUT_UPDATE_HANDLE` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout Updates';


DROP TABLE IF EXISTS `core_resource`;
CREATE TABLE `core_resource` (
  `code` varchar(50) NOT NULL COMMENT 'Resource Code',
  `version` varchar(50) DEFAULT NULL COMMENT 'Resource Version',
  `data_version` varchar(50) DEFAULT NULL COMMENT 'Data Version',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Resources';

INSERT INTO `core_resource` (`code`, `version`, `data_version`) VALUES
('adminnotification_setup',	'1.6.0.0',	'1.6.0.0'),
('admin_setup',	'1.6.1.2',	'1.6.1.2'),
('api2_setup',	'1.0.0.0',	'1.0.0.0'),
('api_setup',	'1.6.0.1',	'1.6.0.1'),
('astrabootstrapsettings_setup',	'0.1.3',	'0.1.3'),
('backup_setup',	'1.6.0.0',	'1.6.0.0'),
('blogmate_setup',	'0.1.0',	'0.1.0'),
('bundle_setup',	'1.6.0.0.1',	'1.6.0.0.1'),
('captcha_setup',	'1.7.0.0.0',	'1.7.0.0.0'),
('catalogindex_setup',	'1.6.0.0',	'1.6.0.0'),
('cataloginventory_setup',	'1.6.0.0.2',	'1.6.0.0.2'),
('catalogrule_setup',	'1.6.0.3',	'1.6.0.3'),
('catalogsearch_setup',	'1.8.2.0',	'1.8.2.0'),
('catalog_setup',	'1.6.0.0.19.1.5',	'1.6.0.0.19.1.5'),
('checkout_setup',	'1.6.0.0',	'1.6.0.0'),
('cms_setup',	'1.6.0.0.2',	'1.6.0.0.2'),
('compiler_setup',	'1.6.0.0',	'1.6.0.0'),
('contacts_setup',	'1.6.0.0',	'1.6.0.0'),
('core_setup',	'1.6.0.6',	'1.6.0.6'),
('cron_setup',	'1.6.0.0',	'1.6.0.0'),
('customer_setup',	'1.6.2.0.5',	'1.6.2.0.5'),
('dataflow_setup',	'1.6.0.0',	'1.6.0.0'),
('directory_setup',	'1.6.0.3',	'1.6.0.3'),
('downloadable_setup',	'1.6.0.0.2',	'1.6.0.0.2'),
('eav_setup',	'1.6.0.1',	'1.6.0.1'),
('giftmessage_setup',	'1.6.0.0',	'1.6.0.0'),
('googleanalytics_setup',	'1.6.0.0',	'1.6.0.0'),
('importexport_setup',	'1.6.0.2',	'1.6.0.2'),
('index_setup',	'1.6.0.0',	'1.6.0.0'),
('log_setup',	'1.6.1.1',	'1.6.1.1'),
('longpg_custom_setup',	'0.1.0',	'0.1.0'),
('moneybookers_setup',	'1.6.0.0',	'1.6.0.0'),
('newsletter_setup',	'1.6.0.2',	'1.6.0.2'),
('oauth_setup',	'1.0.0.0',	'1.0.0.0'),
('paygate_setup',	'1.6.0.0',	'1.6.0.0'),
('payment_setup',	'1.6.0.0',	'1.6.0.0'),
('paypaluk_setup',	'1.6.0.0',	'1.6.0.0'),
('paypal_setup',	'1.6.0.6',	'1.6.0.6'),
('persistent_setup',	'1.0.0.0',	'1.0.0.0'),
('poll_setup',	'1.6.0.1',	'1.6.0.1'),
('productalert_setup',	'1.6.0.0',	'1.6.0.0'),
('rating_setup',	'1.6.0.1',	'1.6.0.1'),
('reports_setup',	'1.6.0.0.1',	'1.6.0.0.1'),
('review_setup',	'1.6.0.0',	'1.6.0.0'),
('rss_setup',	'1.6.0.0',	'1.6.0.0'),
('salesrule_setup',	'1.6.0.3',	'1.6.0.3'),
('sales_setup',	'1.6.0.10',	'1.6.0.10'),
('scrollingcart_setup',	'0.1.0',	'0.1.0'),
('sendfriend_setup',	'1.6.0.1',	'1.6.0.1'),
('shipping_setup',	'1.6.0.0',	'1.6.0.0'),
('sitemap_setup',	'1.6.0.0',	'1.6.0.0'),
('socialbar_setup',	'0.1.0',	'0.1.0'),
('tag_setup',	'1.6.0.0',	'1.6.0.0'),
('tax_setup',	'1.6.0.4',	'1.6.0.4'),
('usa_setup',	'1.6.0.3',	'1.6.0.3'),
('weee_setup',	'1.6.0.0',	'1.6.0.0'),
('widget_setup',	'1.6.0.0',	'1.6.0.0'),
('wishlist_setup',	'1.6.0.0',	'1.6.0.0');

DROP TABLE IF EXISTS `core_session`;
CREATE TABLE `core_session` (
  `session_id` varchar(255) NOT NULL COMMENT 'Session Id',
  `session_expires` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Date of Session Expiration',
  `session_data` mediumblob NOT NULL COMMENT 'Session Data',
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Database Sessions Storage';


DROP TABLE IF EXISTS `core_store`;
CREATE TABLE `core_store` (
  `store_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Store Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Name',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Sort Order',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Activity',
  PRIMARY KEY (`store_id`),
  UNIQUE KEY `UNQ_CORE_STORE_CODE` (`code`),
  KEY `IDX_CORE_STORE_WEBSITE_ID` (`website_id`),
  KEY `IDX_CORE_STORE_IS_ACTIVE_SORT_ORDER` (`is_active`,`sort_order`),
  KEY `IDX_CORE_STORE_GROUP_ID` (`group_id`),
  CONSTRAINT `FK_CORE_STORE_GROUP_ID_CORE_STORE_GROUP_GROUP_ID` FOREIGN KEY (`group_id`) REFERENCES `core_store_group` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CORE_STORE_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores';

INSERT INTO `core_store` (`store_id`, `code`, `website_id`, `group_id`, `name`, `sort_order`, `is_active`) VALUES
(0,	'admin',	0,	0,	'Admin',	0,	1),
(1,	'default',	1,	1,	'Default Store View',	0,	1);

DROP TABLE IF EXISTS `core_store_group`;
CREATE TABLE `core_store_group` (
  `group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Group Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Group Name',
  `root_category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Root Category Id',
  `default_store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Store Id',
  PRIMARY KEY (`group_id`),
  KEY `IDX_CORE_STORE_GROUP_WEBSITE_ID` (`website_id`),
  KEY `IDX_CORE_STORE_GROUP_DEFAULT_STORE_ID` (`default_store_id`),
  CONSTRAINT `FK_CORE_STORE_GROUP_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store Groups';

INSERT INTO `core_store_group` (`group_id`, `website_id`, `name`, `root_category_id`, `default_store_id`) VALUES
(0,	0,	'Default',	0,	0),
(1,	1,	'Main Website Store',	2,	1);

DROP TABLE IF EXISTS `core_translate`;
CREATE TABLE `core_translate` (
  `key_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Key Id of Translation',
  `string` varchar(255) NOT NULL DEFAULT 'Translate String' COMMENT 'Translation String',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `translate` varchar(255) DEFAULT NULL COMMENT 'Translate',
  `locale` varchar(20) NOT NULL DEFAULT 'en_US' COMMENT 'Locale',
  `crc_string` bigint(20) NOT NULL DEFAULT '1591228201' COMMENT 'Translation String CRC32 Hash',
  PRIMARY KEY (`key_id`),
  UNIQUE KEY `UNQ_CORE_TRANSLATE_STORE_ID_LOCALE_CRC_STRING_STRING` (`store_id`,`locale`,`crc_string`,`string`),
  KEY `IDX_CORE_TRANSLATE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CORE_TRANSLATE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Translations';


DROP TABLE IF EXISTS `core_url_rewrite`;
CREATE TABLE `core_url_rewrite` (
  `url_rewrite_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rewrite Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `id_path` varchar(255) DEFAULT NULL COMMENT 'Id Path',
  `request_path` varchar(255) DEFAULT NULL COMMENT 'Request Path',
  `target_path` varchar(255) DEFAULT NULL COMMENT 'Target Path',
  `is_system` smallint(5) unsigned DEFAULT '1' COMMENT 'Defines is Rewrite System',
  `options` varchar(255) DEFAULT NULL COMMENT 'Options',
  `description` varchar(255) DEFAULT NULL COMMENT 'Deascription',
  `category_id` int(10) unsigned DEFAULT NULL COMMENT 'Category Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  PRIMARY KEY (`url_rewrite_id`),
  UNIQUE KEY `UNQ_CORE_URL_REWRITE_REQUEST_PATH_STORE_ID` (`request_path`,`store_id`),
  UNIQUE KEY `UNQ_CORE_URL_REWRITE_ID_PATH_IS_SYSTEM_STORE_ID` (`id_path`,`is_system`,`store_id`),
  KEY `IDX_CORE_URL_REWRITE_TARGET_PATH_STORE_ID` (`target_path`,`store_id`),
  KEY `IDX_CORE_URL_REWRITE_ID_PATH` (`id_path`),
  KEY `IDX_CORE_URL_REWRITE_STORE_ID` (`store_id`),
  KEY `FK_CORE_URL_REWRITE_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` (`category_id`),
  KEY `FK_CORE_URL_REWRITE_PRODUCT_ID_CATALOG_CATEGORY_ENTITY_ENTITY_ID` (`product_id`),
  CONSTRAINT `FK_CORE_URL_REWRITE_CTGR_ID_CAT_CTGR_ENTT_ENTT_ID` FOREIGN KEY (`category_id`) REFERENCES `catalog_category_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CORE_URL_REWRITE_PRODUCT_ID_CATALOG_CATEGORY_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CORE_URL_REWRITE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Url Rewrites';

INSERT INTO `core_url_rewrite` (`url_rewrite_id`, `store_id`, `id_path`, `request_path`, `target_path`, `is_system`, `options`, `description`, `category_id`, `product_id`) VALUES
(1,	1,	'category/3',	'all-products',	'catalog/category/view/id/3',	1,	NULL,	NULL,	3,	NULL),
(2,	1,	'product/1/3',	'all-products/complete-whitening-scope',	'catalog/product/view/id/1/category/3',	1,	NULL,	NULL,	3,	1),
(3,	1,	'product/1',	'complete-whitening-scope',	'catalog/product/view/id/1',	1,	NULL,	NULL,	NULL,	1),
(5,	1,	'94632700_1481961488',	'hair-accessories.html',	'all-products',	0,	'RP',	NULL,	3,	NULL),
(8,	1,	'96070000_1481961488',	'hair-accessories/complete-whitening-scope.html',	'all-products/complete-whitening-scope',	0,	'RP',	NULL,	3,	1),
(9,	1,	'category/4',	'best-sellers',	'catalog/category/view/id/4',	1,	NULL,	NULL,	4,	NULL),
(10,	1,	'category/5',	'whats-new',	'catalog/category/view/id/5',	1,	NULL,	NULL,	5,	NULL),
(11,	1,	'category/6',	'on-sale',	'catalog/category/view/id/6',	1,	NULL,	NULL,	6,	NULL),
(12,	1,	'category/7',	'all-express',	'catalog/category/view/id/7',	1,	NULL,	NULL,	7,	NULL),
(13,	1,	'category/8',	'grocery',	'catalog/category/view/id/8',	1,	NULL,	NULL,	8,	NULL),
(14,	1,	'category/9',	'beverage',	'catalog/category/view/id/9',	1,	NULL,	NULL,	9,	NULL),
(15,	1,	'category/10',	'bath-body',	'catalog/category/view/id/10',	1,	NULL,	NULL,	10,	NULL),
(16,	1,	'category/11',	'health',	'catalog/category/view/id/11',	1,	NULL,	NULL,	11,	NULL),
(17,	1,	'category/12',	'cleaning-supplies',	'catalog/category/view/id/12',	1,	NULL,	NULL,	12,	NULL),
(18,	1,	'category/13',	'home-office',	'catalog/category/view/id/13',	1,	NULL,	NULL,	13,	NULL),
(19,	1,	'product/2/8',	'grocery/lorem-ipsum-dolor',	'catalog/product/view/id/2/category/8',	1,	NULL,	NULL,	8,	2),
(20,	1,	'product/2',	'lorem-ipsum-dolor',	'catalog/product/view/id/2',	1,	NULL,	NULL,	NULL,	2),
(21,	1,	'product/2/3',	'all-products/lorem-ipsum-dolor',	'catalog/product/view/id/2/category/3',	1,	NULL,	NULL,	3,	2),
(24,	1,	'product/3/3',	'all-products/lorem-ipsum-dolor-73',	'catalog/product/view/id/3/category/3',	1,	NULL,	NULL,	3,	3),
(25,	1,	'product/3/8',	'grocery/lorem-ipsum-dolor-33',	'catalog/product/view/id/3/category/8',	1,	NULL,	NULL,	8,	3),
(26,	1,	'product/3',	'lorem-ipsum-dolor-108',	'catalog/product/view/id/3',	1,	NULL,	NULL,	NULL,	3),
(27,	1,	'product/4/3',	'all-products/lorem-ipsum-dolor-74',	'catalog/product/view/id/4/category/3',	1,	NULL,	NULL,	3,	4),
(28,	1,	'product/4/8',	'grocery/lorem-ipsum-dolor-34',	'catalog/product/view/id/4/category/8',	1,	NULL,	NULL,	8,	4),
(29,	1,	'product/4',	'lorem-ipsum-dolor-109',	'catalog/product/view/id/4',	1,	NULL,	NULL,	NULL,	4),
(30,	1,	'product/5/3',	'all-products/lorem-ipsum-dolor-75',	'catalog/product/view/id/5/category/3',	1,	NULL,	NULL,	3,	5),
(31,	1,	'product/5/8',	'grocery/lorem-ipsum-dolor-35',	'catalog/product/view/id/5/category/8',	1,	NULL,	NULL,	8,	5),
(32,	1,	'product/5',	'lorem-ipsum-dolor-110',	'catalog/product/view/id/5',	1,	NULL,	NULL,	NULL,	5),
(33,	1,	'product/6/3',	'all-products/lorem-ipsum-dolor-76',	'catalog/product/view/id/6/category/3',	1,	NULL,	NULL,	3,	6),
(34,	1,	'product/6/8',	'grocery/lorem-ipsum-dolor-36',	'catalog/product/view/id/6/category/8',	1,	NULL,	NULL,	8,	6),
(35,	1,	'product/6',	'lorem-ipsum-dolor-111',	'catalog/product/view/id/6',	1,	NULL,	NULL,	NULL,	6),
(36,	1,	'product/7/3',	'all-products/lorem-ipsum-dolor-77',	'catalog/product/view/id/7/category/3',	1,	NULL,	NULL,	3,	7),
(37,	1,	'product/7/8',	'grocery/lorem-ipsum-dolor-37',	'catalog/product/view/id/7/category/8',	1,	NULL,	NULL,	8,	7),
(38,	1,	'product/7',	'lorem-ipsum-dolor-112',	'catalog/product/view/id/7',	1,	NULL,	NULL,	NULL,	7),
(40,	1,	'71491300_1483397526',	'all-products.html',	'all-products',	0,	'RP',	NULL,	3,	NULL),
(42,	1,	'71791600_1483397526',	'all-products/bath-body.html',	'bath-body',	0,	'RP',	NULL,	10,	NULL),
(44,	1,	'72070200_1483397526',	'all-products/health.html',	'health',	0,	'RP',	NULL,	11,	NULL),
(46,	1,	'72363100_1483397526',	'all-products/cleaning-supplies.html',	'cleaning-supplies',	0,	'RP',	NULL,	12,	NULL),
(48,	1,	'72652300_1483397526',	'all-products/home-office.html',	'home-office',	0,	'RP',	NULL,	13,	NULL),
(50,	1,	'72925100_1483397526',	'all-products/grocery.html',	'grocery',	0,	'RP',	NULL,	8,	NULL),
(52,	1,	'73195100_1483397526',	'all-products/beverage.html',	'beverage',	0,	'RP',	NULL,	9,	NULL),
(54,	1,	'73480500_1483397526',	'best-sellers.html',	'best-sellers',	0,	'RP',	NULL,	4,	NULL),
(56,	1,	'73757200_1483397526',	'whats-new.html',	'whats-new',	0,	'RP',	NULL,	5,	NULL),
(58,	1,	'74030900_1483397526',	'on-sale.html',	'on-sale',	0,	'RP',	NULL,	6,	NULL),
(60,	1,	'74303500_1483397526',	'all-express.html',	'all-express',	0,	'RP',	NULL,	7,	NULL),
(62,	1,	'75687500_1483397526',	'complete-whitening-scope.html',	'complete-whitening-scope',	0,	'RP',	NULL,	NULL,	1),
(64,	1,	'75976000_1483397526',	'all-products/complete-whitening-scope.html',	'all-products/complete-whitening-scope',	0,	'RP',	NULL,	3,	1),
(66,	1,	'76165700_1483397526',	'lorem-ipsum-dolor.html',	'lorem-ipsum-dolor',	0,	'RP',	NULL,	NULL,	2),
(68,	1,	'76450900_1483397526',	'all-products/lorem-ipsum-dolor.html',	'all-products/lorem-ipsum-dolor',	0,	'RP',	NULL,	3,	2),
(70,	1,	'76634200_1483397526',	'all-products/grocery/lorem-ipsum-dolor.html',	'grocery/lorem-ipsum-dolor',	0,	'RP',	NULL,	8,	2),
(72,	1,	'76840200_1483397526',	'lorem-ipsum-dolor-3.html',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(74,	1,	'77148000_1483397526',	'all-products/lorem-ipsum-dolor-3.html',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(76,	1,	'77357400_1483397526',	'all-products/grocery/lorem-ipsum-dolor-3.html',	'grocery/lorem-ipsum-dolor-33',	0,	'RP',	NULL,	8,	3),
(78,	1,	'77562500_1483397526',	'lorem-ipsum-dolor-4.html',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(80,	1,	'77875200_1483397526',	'all-products/lorem-ipsum-dolor-4.html',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(82,	1,	'78108900_1483397526',	'all-products/grocery/lorem-ipsum-dolor-4.html',	'grocery/lorem-ipsum-dolor-34',	0,	'RP',	NULL,	8,	4),
(84,	1,	'78327500_1483397526',	'lorem-ipsum-dolor-5.html',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(86,	1,	'78651400_1483397526',	'all-products/lorem-ipsum-dolor-5.html',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(88,	1,	'78864300_1483397526',	'all-products/grocery/lorem-ipsum-dolor-5.html',	'grocery/lorem-ipsum-dolor-35',	0,	'RP',	NULL,	8,	5),
(90,	1,	'79074900_1483397526',	'lorem-ipsum-dolor-6.html',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(92,	1,	'79394200_1483397526',	'all-products/lorem-ipsum-dolor-6.html',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(94,	1,	'79610500_1483397526',	'all-products/grocery/lorem-ipsum-dolor-6.html',	'grocery/lorem-ipsum-dolor-36',	0,	'RP',	NULL,	8,	6),
(96,	1,	'79818700_1483397526',	'lorem-ipsum-dolor-7.html',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(98,	1,	'80138500_1483397526',	'all-products/lorem-ipsum-dolor-7.html',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(100,	1,	'80350100_1483397526',	'all-products/grocery/lorem-ipsum-dolor-7.html',	'grocery/lorem-ipsum-dolor-37',	0,	'RP',	NULL,	8,	7),
(102,	1,	'84049000_1483401160',	'all-products/grocery',	'grocery',	0,	'RP',	NULL,	8,	NULL),
(105,	1,	'85672800_1483401160',	'all-products/grocery/lorem-ipsum-dolor',	'grocery/lorem-ipsum-dolor',	0,	'RP',	NULL,	8,	2),
(107,	1,	'86081300_1483401160',	'lorem-ipsum-dolor-3',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(109,	1,	'88912000_1483401160',	'all-products/grocery/lorem-ipsum-dolor-3',	'grocery/lorem-ipsum-dolor-33',	0,	'RP',	NULL,	8,	3),
(111,	1,	'89254500_1483401160',	'lorem-ipsum-dolor-4',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(113,	1,	'89699600_1483401160',	'all-products/grocery/lorem-ipsum-dolor-4',	'grocery/lorem-ipsum-dolor-34',	0,	'RP',	NULL,	8,	4),
(115,	1,	'90391300_1483401160',	'lorem-ipsum-dolor-5',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(117,	1,	'91243000_1483401160',	'all-products/grocery/lorem-ipsum-dolor-5',	'grocery/lorem-ipsum-dolor-35',	0,	'RP',	NULL,	8,	5),
(119,	1,	'91889700_1483401160',	'lorem-ipsum-dolor-6',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(121,	1,	'92359300_1483401160',	'all-products/grocery/lorem-ipsum-dolor-6',	'grocery/lorem-ipsum-dolor-36',	0,	'RP',	NULL,	8,	6),
(123,	1,	'92952200_1483401160',	'lorem-ipsum-dolor-7',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(125,	1,	'93321600_1483401160',	'all-products/grocery/lorem-ipsum-dolor-7',	'grocery/lorem-ipsum-dolor-37',	0,	'RP',	NULL,	8,	7),
(132,	1,	'95104000_1483401160',	'lorem-ipsum-dolor-8',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(134,	1,	'95564600_1483401160',	'all-products/lorem-ipsum-dolor-3',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(136,	1,	'95902300_1483401160',	'lorem-ipsum-dolor-9',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(138,	1,	'96361200_1483401160',	'all-products/lorem-ipsum-dolor-4',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(140,	1,	'96698200_1483401160',	'lorem-ipsum-dolor-10',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(142,	1,	'97155200_1483401160',	'all-products/lorem-ipsum-dolor-5',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(144,	1,	'97489000_1483401160',	'lorem-ipsum-dolor-11',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(146,	1,	'97953800_1483401160',	'all-products/lorem-ipsum-dolor-6',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(148,	1,	'98291100_1483401160',	'lorem-ipsum-dolor-12',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(150,	1,	'98754000_1483401160',	'all-products/lorem-ipsum-dolor-7',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(162,	1,	'01392700_1483401161',	'lorem-ipsum-dolor-13',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(164,	1,	'01866900_1483401161',	'all-products/lorem-ipsum-dolor-8',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(166,	1,	'02206400_1483401161',	'lorem-ipsum-dolor-14',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(168,	1,	'02670700_1483401161',	'all-products/lorem-ipsum-dolor-9',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(170,	1,	'03014400_1483401161',	'lorem-ipsum-dolor-15',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(172,	1,	'03481200_1483401161',	'all-products/lorem-ipsum-dolor-10',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(174,	1,	'03821400_1483401161',	'lorem-ipsum-dolor-16',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(176,	1,	'04290100_1483401161',	'all-products/lorem-ipsum-dolor-11',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(178,	1,	'04631100_1483401161',	'lorem-ipsum-dolor-17',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(180,	1,	'05101000_1483401161',	'all-products/lorem-ipsum-dolor-12',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(194,	1,	'07949000_1483401161',	'lorem-ipsum-dolor-18',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(196,	1,	'08401500_1483401161',	'grocery/lorem-ipsum-dolor-3',	'grocery/lorem-ipsum-dolor-33',	0,	'RP',	NULL,	8,	3),
(198,	1,	'08752100_1483401161',	'lorem-ipsum-dolor-19',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(200,	1,	'09204500_1483401161',	'grocery/lorem-ipsum-dolor-4',	'grocery/lorem-ipsum-dolor-34',	0,	'RP',	NULL,	8,	4),
(202,	1,	'09556000_1483401161',	'lorem-ipsum-dolor-20',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(204,	1,	'10011500_1483401161',	'grocery/lorem-ipsum-dolor-5',	'grocery/lorem-ipsum-dolor-35',	0,	'RP',	NULL,	8,	5),
(206,	1,	'10362000_1483401161',	'lorem-ipsum-dolor-21',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(208,	1,	'10822700_1483401161',	'grocery/lorem-ipsum-dolor-6',	'grocery/lorem-ipsum-dolor-36',	0,	'RP',	NULL,	8,	6),
(210,	1,	'11176000_1483401161',	'lorem-ipsum-dolor-22',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(212,	1,	'11630400_1483401161',	'grocery/lorem-ipsum-dolor-7',	'grocery/lorem-ipsum-dolor-37',	0,	'RP',	NULL,	8,	7),
(214,	1,	'69463300_1483401162',	'all-products/beverage',	'beverage',	0,	'RP',	NULL,	9,	NULL),
(221,	1,	'73952800_1483401162',	'lorem-ipsum-dolor-23',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(223,	1,	'75031300_1483401162',	'all-products/lorem-ipsum-dolor-13',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(225,	1,	'75373400_1483401162',	'lorem-ipsum-dolor-24',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(227,	1,	'76069700_1483401162',	'all-products/lorem-ipsum-dolor-14',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(229,	1,	'76785100_1483401162',	'lorem-ipsum-dolor-25',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(231,	1,	'77840200_1483401162',	'all-products/lorem-ipsum-dolor-15',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(233,	1,	'78430000_1483401162',	'lorem-ipsum-dolor-26',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(235,	1,	'79298000_1483401162',	'all-products/lorem-ipsum-dolor-16',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(237,	1,	'79927100_1483401162',	'lorem-ipsum-dolor-27',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(239,	1,	'81306500_1483401162',	'all-products/lorem-ipsum-dolor-17',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(250,	1,	'85147500_1483401162',	'lorem-ipsum-dolor-28',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(252,	1,	'85627200_1483401162',	'all-products/lorem-ipsum-dolor-18',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(254,	1,	'85972300_1483401162',	'lorem-ipsum-dolor-29',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(256,	1,	'86454500_1483401162',	'all-products/lorem-ipsum-dolor-19',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(258,	1,	'86802300_1483401162',	'lorem-ipsum-dolor-30',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(260,	1,	'87276600_1483401162',	'all-products/lorem-ipsum-dolor-20',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(262,	1,	'87624000_1483401162',	'lorem-ipsum-dolor-31',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(264,	1,	'88096900_1483401162',	'all-products/lorem-ipsum-dolor-21',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(266,	1,	'88443300_1483401162',	'lorem-ipsum-dolor-32',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(268,	1,	'88920200_1483401162',	'all-products/lorem-ipsum-dolor-22',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(281,	1,	'91164400_1483401162',	'lorem-ipsum-dolor-33',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(283,	1,	'91625700_1483401162',	'grocery/lorem-ipsum-dolor-8',	'grocery/lorem-ipsum-dolor-33',	0,	'RP',	NULL,	8,	3),
(285,	1,	'91976400_1483401162',	'lorem-ipsum-dolor-34',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(287,	1,	'92437100_1483401162',	'grocery/lorem-ipsum-dolor-9',	'grocery/lorem-ipsum-dolor-34',	0,	'RP',	NULL,	8,	4),
(289,	1,	'92798200_1483401162',	'lorem-ipsum-dolor-35',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(291,	1,	'93263800_1483401162',	'grocery/lorem-ipsum-dolor-10',	'grocery/lorem-ipsum-dolor-35',	0,	'RP',	NULL,	8,	5),
(293,	1,	'93618500_1483401162',	'lorem-ipsum-dolor-36',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(295,	1,	'94075200_1483401162',	'grocery/lorem-ipsum-dolor-11',	'grocery/lorem-ipsum-dolor-36',	0,	'RP',	NULL,	8,	6),
(297,	1,	'94447500_1483401162',	'lorem-ipsum-dolor-37',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(299,	1,	'95229000_1483401162',	'grocery/lorem-ipsum-dolor-12',	'grocery/lorem-ipsum-dolor-37',	0,	'RP',	NULL,	8,	7),
(308,	1,	'11230700_1483401164',	'lorem-ipsum-dolor-38',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(310,	1,	'11713700_1483401164',	'all-products/lorem-ipsum-dolor-23',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(312,	1,	'12059500_1483401164',	'lorem-ipsum-dolor-39',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(314,	1,	'12540200_1483401164',	'all-products/lorem-ipsum-dolor-24',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(316,	1,	'14895400_1483401164',	'lorem-ipsum-dolor-40',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(318,	1,	'15394500_1483401164',	'all-products/lorem-ipsum-dolor-25',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(320,	1,	'15752000_1483401164',	'lorem-ipsum-dolor-41',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(322,	1,	'16235600_1483401164',	'all-products/lorem-ipsum-dolor-26',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(324,	1,	'16598900_1483401164',	'lorem-ipsum-dolor-42',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(326,	1,	'17085200_1483401164',	'all-products/lorem-ipsum-dolor-27',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(337,	1,	'19542000_1483401164',	'lorem-ipsum-dolor-43',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(339,	1,	'20035900_1483401164',	'all-products/lorem-ipsum-dolor-28',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(341,	1,	'20400400_1483401164',	'lorem-ipsum-dolor-44',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(343,	1,	'20894100_1483401164',	'all-products/lorem-ipsum-dolor-29',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(345,	1,	'21305800_1483401164',	'lorem-ipsum-dolor-45',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(347,	1,	'21799800_1483401164',	'all-products/lorem-ipsum-dolor-30',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(349,	1,	'22160600_1483401164',	'lorem-ipsum-dolor-46',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(351,	1,	'22666700_1483401164',	'all-products/lorem-ipsum-dolor-31',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(353,	1,	'23034200_1483401164',	'lorem-ipsum-dolor-47',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(355,	1,	'23534000_1483401164',	'all-products/lorem-ipsum-dolor-32',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(361,	1,	'45062700_1483401165',	'all-products/health',	'health',	0,	'RP',	NULL,	11,	NULL),
(368,	1,	'47491100_1483401165',	'lorem-ipsum-dolor-48',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(370,	1,	'47936800_1483401165',	'all-products/lorem-ipsum-dolor-33',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(372,	1,	'48264200_1483401165',	'lorem-ipsum-dolor-49',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(374,	1,	'48703700_1483401165',	'all-products/lorem-ipsum-dolor-34',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(376,	1,	'49044300_1483401165',	'lorem-ipsum-dolor-50',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(378,	1,	'49474000_1483401165',	'all-products/lorem-ipsum-dolor-35',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(380,	1,	'49805700_1483401165',	'lorem-ipsum-dolor-51',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(382,	1,	'50244800_1483401165',	'all-products/lorem-ipsum-dolor-36',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(384,	1,	'50570100_1483401165',	'lorem-ipsum-dolor-52',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(386,	1,	'51020300_1483401165',	'all-products/lorem-ipsum-dolor-37',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(397,	1,	'53224500_1483401165',	'lorem-ipsum-dolor-53',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(399,	1,	'53669600_1483401165',	'all-products/lorem-ipsum-dolor-38',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(401,	1,	'54009900_1483401165',	'lorem-ipsum-dolor-54',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(403,	1,	'54462700_1483401165',	'all-products/lorem-ipsum-dolor-39',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(405,	1,	'54787400_1483401165',	'lorem-ipsum-dolor-55',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(407,	1,	'55219800_1483401165',	'all-products/lorem-ipsum-dolor-40',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(409,	1,	'55542800_1483401165',	'lorem-ipsum-dolor-56',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(411,	1,	'55984500_1483401165',	'all-products/lorem-ipsum-dolor-41',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(413,	1,	'56321700_1483401165',	'lorem-ipsum-dolor-57',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(415,	1,	'56768900_1483401165',	'all-products/lorem-ipsum-dolor-42',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(427,	1,	'58695100_1483401165',	'lorem-ipsum-dolor-58',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(429,	1,	'59116900_1483401165',	'grocery/lorem-ipsum-dolor-13',	'grocery/lorem-ipsum-dolor-33',	0,	'RP',	NULL,	8,	3),
(431,	1,	'59455700_1483401165',	'lorem-ipsum-dolor-59',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(433,	1,	'59863400_1483401165',	'grocery/lorem-ipsum-dolor-14',	'grocery/lorem-ipsum-dolor-34',	0,	'RP',	NULL,	8,	4),
(435,	1,	'60209200_1483401165',	'lorem-ipsum-dolor-60',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(437,	1,	'60646500_1483401165',	'grocery/lorem-ipsum-dolor-15',	'grocery/lorem-ipsum-dolor-35',	0,	'RP',	NULL,	8,	5),
(439,	1,	'61262000_1483401165',	'lorem-ipsum-dolor-61',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(441,	1,	'61688100_1483401165',	'grocery/lorem-ipsum-dolor-16',	'grocery/lorem-ipsum-dolor-36',	0,	'RP',	NULL,	8,	6),
(443,	1,	'62034300_1483401165',	'lorem-ipsum-dolor-62',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(445,	1,	'62457500_1483401165',	'grocery/lorem-ipsum-dolor-17',	'grocery/lorem-ipsum-dolor-37',	0,	'RP',	NULL,	8,	7),
(448,	1,	'99558100_1483401166',	'all-products/cleaning-supplies',	'cleaning-supplies',	0,	'RP',	NULL,	12,	NULL),
(455,	1,	'02048500_1483401167',	'lorem-ipsum-dolor-63',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(457,	1,	'02513200_1483401167',	'all-products/lorem-ipsum-dolor-43',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(459,	1,	'02840500_1483401167',	'lorem-ipsum-dolor-64',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(461,	1,	'03293800_1483401167',	'all-products/lorem-ipsum-dolor-44',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(463,	1,	'03632700_1483401167',	'lorem-ipsum-dolor-65',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(465,	1,	'04079700_1483401167',	'all-products/lorem-ipsum-dolor-45',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(467,	1,	'04412100_1483401167',	'lorem-ipsum-dolor-66',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(469,	1,	'04858200_1483401167',	'all-products/lorem-ipsum-dolor-46',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(471,	1,	'05189900_1483401167',	'lorem-ipsum-dolor-67',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(473,	1,	'05639000_1483401167',	'all-products/lorem-ipsum-dolor-47',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(484,	1,	'07866000_1483401167',	'lorem-ipsum-dolor-68',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(486,	1,	'08309500_1483401167',	'all-products/lorem-ipsum-dolor-48',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(488,	1,	'08649400_1483401167',	'lorem-ipsum-dolor-69',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(490,	1,	'09106400_1483401167',	'all-products/lorem-ipsum-dolor-49',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(492,	1,	'09452900_1483401167',	'lorem-ipsum-dolor-70',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(494,	1,	'09911000_1483401167',	'all-products/lorem-ipsum-dolor-50',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(496,	1,	'10251000_1483401167',	'lorem-ipsum-dolor-71',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(498,	1,	'10722800_1483401167',	'all-products/lorem-ipsum-dolor-51',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(500,	1,	'11077900_1483401167',	'lorem-ipsum-dolor-72',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(502,	1,	'11538700_1483401167',	'all-products/lorem-ipsum-dolor-52',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(513,	1,	'13288600_1483401167',	'lorem-ipsum-dolor-73',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(515,	1,	'13693600_1483401167',	'grocery/lorem-ipsum-dolor-18',	'grocery/lorem-ipsum-dolor-33',	0,	'RP',	NULL,	8,	3),
(517,	1,	'14044100_1483401167',	'lorem-ipsum-dolor-74',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(519,	1,	'14463500_1483401167',	'grocery/lorem-ipsum-dolor-19',	'grocery/lorem-ipsum-dolor-34',	0,	'RP',	NULL,	8,	4),
(521,	1,	'14814300_1483401167',	'lorem-ipsum-dolor-75',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(523,	1,	'15224900_1483401167',	'grocery/lorem-ipsum-dolor-20',	'grocery/lorem-ipsum-dolor-35',	0,	'RP',	NULL,	8,	5),
(525,	1,	'15576900_1483401167',	'lorem-ipsum-dolor-76',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(527,	1,	'15989000_1483401167',	'grocery/lorem-ipsum-dolor-21',	'grocery/lorem-ipsum-dolor-36',	0,	'RP',	NULL,	8,	6),
(529,	1,	'16363700_1483401167',	'lorem-ipsum-dolor-77',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(531,	1,	'16773400_1483401167',	'grocery/lorem-ipsum-dolor-22',	'grocery/lorem-ipsum-dolor-37',	0,	'RP',	NULL,	8,	7),
(534,	1,	'60503000_1483401168',	'all-products/home-office',	'home-office',	0,	'RP',	NULL,	13,	NULL),
(541,	1,	'62983500_1483401168',	'lorem-ipsum-dolor-78',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(543,	1,	'63456400_1483401168',	'all-products/lorem-ipsum-dolor-53',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(545,	1,	'63799200_1483401168',	'lorem-ipsum-dolor-79',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(547,	1,	'64265100_1483401168',	'all-products/lorem-ipsum-dolor-54',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(549,	1,	'64606200_1483401168',	'lorem-ipsum-dolor-80',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(551,	1,	'65086800_1483401168',	'all-products/lorem-ipsum-dolor-55',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(553,	1,	'65443100_1483401168',	'lorem-ipsum-dolor-81',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(555,	1,	'65903800_1483401168',	'all-products/lorem-ipsum-dolor-56',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(557,	1,	'66253800_1483401168',	'lorem-ipsum-dolor-82',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(559,	1,	'66718400_1483401168',	'all-products/lorem-ipsum-dolor-57',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(570,	1,	'68997700_1483401168',	'lorem-ipsum-dolor-83',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(572,	1,	'69458000_1483401168',	'all-products/lorem-ipsum-dolor-58',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(574,	1,	'69799400_1483401168',	'lorem-ipsum-dolor-84',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(576,	1,	'70279400_1483401168',	'all-products/lorem-ipsum-dolor-59',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(578,	1,	'70625600_1483401168',	'lorem-ipsum-dolor-85',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(580,	1,	'71082500_1483401168',	'all-products/lorem-ipsum-dolor-60',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(582,	1,	'71442200_1483401168',	'lorem-ipsum-dolor-86',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(584,	1,	'71936700_1483401168',	'all-products/lorem-ipsum-dolor-61',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(586,	1,	'72304000_1483401168',	'lorem-ipsum-dolor-87',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(588,	1,	'72774000_1483401168',	'all-products/lorem-ipsum-dolor-62',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(598,	1,	'74434000_1483401168',	'lorem-ipsum-dolor-88',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(600,	1,	'74839300_1483401168',	'grocery/lorem-ipsum-dolor-23',	'grocery/lorem-ipsum-dolor-33',	0,	'RP',	NULL,	8,	3),
(602,	1,	'75202100_1483401168',	'lorem-ipsum-dolor-89',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(604,	1,	'75617900_1483401168',	'grocery/lorem-ipsum-dolor-24',	'grocery/lorem-ipsum-dolor-34',	0,	'RP',	NULL,	8,	4),
(606,	1,	'76001000_1483401168',	'lorem-ipsum-dolor-90',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(608,	1,	'76414100_1483401168',	'grocery/lorem-ipsum-dolor-25',	'grocery/lorem-ipsum-dolor-35',	0,	'RP',	NULL,	8,	5),
(610,	1,	'76779300_1483401168',	'lorem-ipsum-dolor-91',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(612,	1,	'77209600_1483401168',	'grocery/lorem-ipsum-dolor-26',	'grocery/lorem-ipsum-dolor-36',	0,	'RP',	NULL,	8,	6),
(614,	1,	'77578100_1483401168',	'lorem-ipsum-dolor-92',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(616,	1,	'77992600_1483401168',	'grocery/lorem-ipsum-dolor-27',	'grocery/lorem-ipsum-dolor-37',	0,	'RP',	NULL,	8,	7),
(619,	1,	'12269300_1483401170',	'all-products/bath-body',	'bath-body',	0,	'RP',	NULL,	10,	NULL),
(626,	1,	'14571900_1483401170',	'lorem-ipsum-dolor-93',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(628,	1,	'15064700_1483401170',	'all-products/lorem-ipsum-dolor-63',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(630,	1,	'15426400_1483401170',	'lorem-ipsum-dolor-94',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(632,	1,	'15890500_1483401170',	'all-products/lorem-ipsum-dolor-64',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(634,	1,	'16277200_1483401170',	'lorem-ipsum-dolor-95',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(636,	1,	'16750300_1483401170',	'all-products/lorem-ipsum-dolor-65',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(638,	1,	'17135000_1483401170',	'lorem-ipsum-dolor-96',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(640,	1,	'17602900_1483401170',	'all-products/lorem-ipsum-dolor-66',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(642,	1,	'18005300_1483401170',	'lorem-ipsum-dolor-97',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(644,	1,	'18502500_1483401170',	'all-products/lorem-ipsum-dolor-67',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(655,	1,	'20876600_1483401170',	'lorem-ipsum-dolor-98',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(657,	1,	'21348900_1483401170',	'all-products/lorem-ipsum-dolor-68',	'all-products/lorem-ipsum-dolor-73',	0,	'RP',	NULL,	3,	3),
(659,	1,	'21755600_1483401170',	'lorem-ipsum-dolor-99',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(661,	1,	'22227000_1483401170',	'all-products/lorem-ipsum-dolor-69',	'all-products/lorem-ipsum-dolor-74',	0,	'RP',	NULL,	3,	4),
(663,	1,	'22630700_1483401170',	'lorem-ipsum-dolor-100',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(665,	1,	'23102000_1483401170',	'all-products/lorem-ipsum-dolor-70',	'all-products/lorem-ipsum-dolor-75',	0,	'RP',	NULL,	3,	5),
(667,	1,	'23474700_1483401170',	'lorem-ipsum-dolor-101',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(669,	1,	'23964300_1483401170',	'all-products/lorem-ipsum-dolor-71',	'all-products/lorem-ipsum-dolor-76',	0,	'RP',	NULL,	3,	6),
(671,	1,	'24339700_1483401170',	'lorem-ipsum-dolor-102',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(673,	1,	'24810700_1483401170',	'all-products/lorem-ipsum-dolor-72',	'all-products/lorem-ipsum-dolor-77',	0,	'RP',	NULL,	3,	7),
(682,	1,	'26325700_1483401170',	'lorem-ipsum-dolor-103',	'lorem-ipsum-dolor-108',	0,	'RP',	NULL,	NULL,	3),
(684,	1,	'26737200_1483401170',	'grocery/lorem-ipsum-dolor-28',	'grocery/lorem-ipsum-dolor-33',	0,	'RP',	NULL,	8,	3),
(686,	1,	'27119000_1483401170',	'lorem-ipsum-dolor-104',	'lorem-ipsum-dolor-109',	0,	'RP',	NULL,	NULL,	4),
(688,	1,	'27544600_1483401170',	'grocery/lorem-ipsum-dolor-29',	'grocery/lorem-ipsum-dolor-34',	0,	'RP',	NULL,	8,	4),
(690,	1,	'27932800_1483401170',	'lorem-ipsum-dolor-105',	'lorem-ipsum-dolor-110',	0,	'RP',	NULL,	NULL,	5),
(692,	1,	'28341100_1483401170',	'grocery/lorem-ipsum-dolor-30',	'grocery/lorem-ipsum-dolor-35',	0,	'RP',	NULL,	8,	5),
(694,	1,	'28744500_1483401170',	'lorem-ipsum-dolor-106',	'lorem-ipsum-dolor-111',	0,	'RP',	NULL,	NULL,	6),
(696,	1,	'29171100_1483401170',	'grocery/lorem-ipsum-dolor-31',	'grocery/lorem-ipsum-dolor-36',	0,	'RP',	NULL,	8,	6),
(698,	1,	'29558300_1483401170',	'lorem-ipsum-dolor-107',	'lorem-ipsum-dolor-112',	0,	'RP',	NULL,	NULL,	7),
(700,	1,	'29977000_1483401170',	'grocery/lorem-ipsum-dolor-32',	'grocery/lorem-ipsum-dolor-37',	0,	'RP',	NULL,	8,	7);

DROP TABLE IF EXISTS `core_variable`;
CREATE TABLE `core_variable` (
  `variable_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Variable Code',
  `name` varchar(255) DEFAULT NULL COMMENT 'Variable Name',
  PRIMARY KEY (`variable_id`),
  UNIQUE KEY `UNQ_CORE_VARIABLE_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variables';


DROP TABLE IF EXISTS `core_variable_value`;
CREATE TABLE `core_variable_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable Value Id',
  `variable_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Variable Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `plain_value` text COMMENT 'Plain Text Value',
  `html_value` text COMMENT 'Html Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CORE_VARIABLE_VALUE_VARIABLE_ID_STORE_ID` (`variable_id`,`store_id`),
  KEY `IDX_CORE_VARIABLE_VALUE_VARIABLE_ID` (`variable_id`),
  KEY `IDX_CORE_VARIABLE_VALUE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_CORE_VARIABLE_VALUE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CORE_VARIABLE_VALUE_VARIABLE_ID_CORE_VARIABLE_VARIABLE_ID` FOREIGN KEY (`variable_id`) REFERENCES `core_variable` (`variable_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variable Value';


DROP TABLE IF EXISTS `core_website`;
CREATE TABLE `core_website` (
  `website_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Website Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `name` varchar(64) DEFAULT NULL COMMENT 'Website Name',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Group Id',
  `is_default` smallint(5) unsigned DEFAULT '0' COMMENT 'Defines Is Website Default',
  PRIMARY KEY (`website_id`),
  UNIQUE KEY `UNQ_CORE_WEBSITE_CODE` (`code`),
  KEY `IDX_CORE_WEBSITE_SORT_ORDER` (`sort_order`),
  KEY `IDX_CORE_WEBSITE_DEFAULT_GROUP_ID` (`default_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Websites';

INSERT INTO `core_website` (`website_id`, `code`, `name`, `sort_order`, `default_group_id`, `is_default`) VALUES
(0,	'admin',	'Admin',	0,	0,	0),
(1,	'base',	'Main Website',	0,	1,	1);

DROP TABLE IF EXISTS `coupon_aggregated`;
CREATE TABLE `coupon_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_COUPON_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS_COUPON_CODE` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `IDX_COUPON_AGGREGATED_STORE_ID` (`store_id`),
  KEY `IDX_COUPON_AGGREGATED_RULE_NAME` (`rule_name`),
  CONSTRAINT `FK_COUPON_AGGREGATED_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated';


DROP TABLE IF EXISTS `coupon_aggregated_order`;
CREATE TABLE `coupon_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_COUPON_AGGRED_ORDER_PERIOD_STORE_ID_ORDER_STS_COUPON_CODE` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `IDX_COUPON_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  KEY `IDX_COUPON_AGGREGATED_ORDER_RULE_NAME` (`rule_name`),
  CONSTRAINT `FK_COUPON_AGGREGATED_ORDER_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated Order';


DROP TABLE IF EXISTS `coupon_aggregated_updated`;
CREATE TABLE `coupon_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_COUPON_AGGRED_UPDATED_PERIOD_STORE_ID_ORDER_STS_COUPON_CODE` (`period`,`store_id`,`order_status`,`coupon_code`),
  KEY `IDX_COUPON_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  KEY `IDX_COUPON_AGGREGATED_UPDATED_RULE_NAME` (`rule_name`),
  CONSTRAINT `FK_COUPON_AGGREGATED_UPDATED_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated Updated';


DROP TABLE IF EXISTS `cron_schedule`;
CREATE TABLE `cron_schedule` (
  `schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Schedule Id',
  `job_code` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Job Code',
  `status` varchar(7) NOT NULL DEFAULT 'pending' COMMENT 'Status',
  `messages` text COMMENT 'Messages',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `scheduled_at` timestamp NULL DEFAULT NULL COMMENT 'Scheduled At',
  `executed_at` timestamp NULL DEFAULT NULL COMMENT 'Executed At',
  `finished_at` timestamp NULL DEFAULT NULL COMMENT 'Finished At',
  PRIMARY KEY (`schedule_id`),
  KEY `IDX_CRON_SCHEDULE_JOB_CODE` (`job_code`),
  KEY `IDX_CRON_SCHEDULE_SCHEDULED_AT_STATUS` (`scheduled_at`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cron Schedule';


DROP TABLE IF EXISTS `customer_address_entity`;
CREATE TABLE `customer_address_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Active',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_CUSTOMER_ADDRESS_ENTITY_PARENT_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity';


DROP TABLE IF EXISTS `customer_address_entity_datetime`;
CREATE TABLE `customer_address_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_DATETIME_ENTITY_ID` (`entity_id`),
  KEY `IDX_CSTR_ADDR_ENTT_DTIME_ENTT_ID_ATTR_ID_VAL` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CSTR_ADDR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ADDR_ENTT_DTIME_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ADDR_ENTT_DTIME_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Datetime';


DROP TABLE IF EXISTS `customer_address_entity_decimal`;
CREATE TABLE `customer_address_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID` (`entity_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CSTR_ADDR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ADDR_ENTT_DEC_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ADDR_ENTT_DEC_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Decimal';


DROP TABLE IF EXISTS `customer_address_entity_int`;
CREATE TABLE `customer_address_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID` (`entity_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CSTR_ADDR_ENTT_INT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ADDR_ENTT_INT_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ADDR_ENTT_INT_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Int';


DROP TABLE IF EXISTS `customer_address_entity_text`;
CREATE TABLE `customer_address_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ADDRESS_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_TEXT_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_TEXT_ENTITY_ID` (`entity_id`),
  CONSTRAINT `FK_CSTR_ADDR_ENTT_TEXT_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ADDR_ENTT_TEXT_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ADDR_ENTT_TEXT_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Text';


DROP TABLE IF EXISTS `customer_address_entity_varchar`;
CREATE TABLE `customer_address_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID` (`entity_id`),
  KEY `IDX_CUSTOMER_ADDRESS_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CSTR_ADDR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ADDR_ENTT_VCHR_ENTT_ID_CSTR_ADDR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_address_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ADDR_ENTT_VCHR_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Address Entity Varchar';


DROP TABLE IF EXISTS `customer_eav_attribute`;
CREATE TABLE `customer_eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `input_filter` varchar(255) DEFAULT NULL COMMENT 'Input Filter',
  `multiline_count` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Multiline Count',
  `validate_rules` text COMMENT 'Validate Rules',
  `is_system` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is System',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `data_model` varchar(255) DEFAULT NULL COMMENT 'Data Model',
  PRIMARY KEY (`attribute_id`),
  CONSTRAINT `FK_CSTR_EAV_ATTR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Eav Attribute';

INSERT INTO `customer_eav_attribute` (`attribute_id`, `is_visible`, `input_filter`, `multiline_count`, `validate_rules`, `is_system`, `sort_order`, `data_model`) VALUES
(1,	1,	NULL,	0,	NULL,	1,	10,	NULL),
(2,	0,	NULL,	0,	NULL,	1,	0,	NULL),
(3,	1,	NULL,	0,	NULL,	1,	20,	NULL),
(4,	0,	NULL,	0,	NULL,	0,	30,	NULL),
(5,	1,	NULL,	0,	'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',	1,	40,	NULL),
(6,	1,	NULL,	0,	NULL,	0,	50,	NULL),
(7,	1,	NULL,	0,	'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',	1,	60,	NULL),
(8,	0,	NULL,	0,	NULL,	0,	70,	NULL),
(9,	1,	NULL,	0,	'a:1:{s:16:\"input_validation\";s:5:\"email\";}',	1,	80,	NULL),
(10,	1,	NULL,	0,	NULL,	1,	25,	NULL),
(11,	0,	'date',	0,	'a:1:{s:16:\"input_validation\";s:4:\"date\";}',	0,	90,	NULL),
(12,	0,	NULL,	0,	NULL,	1,	0,	NULL),
(13,	0,	NULL,	0,	NULL,	1,	0,	NULL),
(14,	0,	NULL,	0,	NULL,	1,	0,	NULL),
(15,	0,	NULL,	0,	'a:1:{s:15:\"max_text_length\";i:255;}',	0,	100,	NULL),
(16,	0,	NULL,	0,	NULL,	1,	0,	NULL),
(17,	0,	'datetime',	0,	NULL,	0,	0,	NULL),
(18,	0,	NULL,	0,	'a:0:{}',	0,	110,	NULL),
(19,	0,	NULL,	0,	NULL,	0,	10,	NULL),
(20,	1,	NULL,	0,	'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',	1,	20,	NULL),
(21,	1,	NULL,	0,	NULL,	0,	30,	NULL),
(22,	1,	NULL,	0,	'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',	1,	40,	NULL),
(23,	0,	NULL,	0,	NULL,	0,	50,	NULL),
(24,	1,	NULL,	0,	'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',	1,	60,	NULL),
(25,	1,	NULL,	2,	'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',	1,	70,	NULL),
(26,	1,	NULL,	0,	'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',	1,	80,	NULL),
(27,	1,	NULL,	0,	NULL,	1,	90,	NULL),
(28,	1,	NULL,	0,	NULL,	1,	100,	NULL),
(29,	1,	NULL,	0,	NULL,	1,	100,	NULL),
(30,	1,	NULL,	0,	'a:0:{}',	1,	110,	'customer/attribute_data_postcode'),
(31,	1,	NULL,	0,	'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',	1,	120,	NULL),
(32,	1,	NULL,	0,	'a:2:{s:15:\"max_text_length\";i:255;s:15:\"min_text_length\";i:1;}',	1,	130,	NULL),
(33,	0,	NULL,	0,	NULL,	1,	0,	NULL),
(34,	0,	NULL,	0,	'a:1:{s:16:\"input_validation\";s:4:\"date\";}',	1,	0,	NULL),
(35,	1,	NULL,	0,	NULL,	1,	28,	NULL),
(36,	1,	NULL,	0,	NULL,	1,	140,	NULL),
(37,	0,	NULL,	0,	NULL,	1,	0,	NULL),
(38,	0,	NULL,	0,	NULL,	1,	0,	NULL),
(39,	0,	NULL,	0,	NULL,	1,	0,	NULL),
(40,	0,	NULL,	0,	NULL,	1,	0,	NULL);

DROP TABLE IF EXISTS `customer_eav_attribute_website`;
CREATE TABLE `customer_eav_attribute_website` (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `is_visible` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Visible',
  `is_required` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Required',
  `default_value` text COMMENT 'Default Value',
  `multiline_count` smallint(5) unsigned DEFAULT NULL COMMENT 'Multiline Count',
  PRIMARY KEY (`attribute_id`,`website_id`),
  KEY `IDX_CUSTOMER_EAV_ATTRIBUTE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_CSTR_EAV_ATTR_WS_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_EAV_ATTR_WS_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Eav Attribute Website';


DROP TABLE IF EXISTS `customer_entity`;
CREATE TABLE `customer_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `website_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Website Id',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Active',
  `disable_auto_group_change` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Disable automatic group change based on VAT ID',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ENTITY_EMAIL_WEBSITE_ID` (`email`,`website_id`),
  KEY `IDX_CUSTOMER_ENTITY_STORE_ID` (`store_id`),
  KEY `IDX_CUSTOMER_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ENTITY_EMAIL_WEBSITE_ID` (`email`,`website_id`),
  KEY `IDX_CUSTOMER_ENTITY_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_CUSTOMER_ENTITY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ENTITY_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity';


DROP TABLE IF EXISTS `customer_entity_datetime`;
CREATE TABLE `customer_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `IDX_CUSTOMER_ENTITY_DATETIME_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CUSTOMER_ENTITY_DATETIME_ENTITY_ID` (`entity_id`),
  KEY `IDX_CUSTOMER_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CSTR_ENTT_DTIME_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ENTT_DTIME_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ENTITY_DATETIME_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Datetime';


DROP TABLE IF EXISTS `customer_entity_decimal`;
CREATE TABLE `customer_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `IDX_CUSTOMER_ENTITY_DECIMAL_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CUSTOMER_ENTITY_DECIMAL_ENTITY_ID` (`entity_id`),
  KEY `IDX_CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CSTR_ENTT_DEC_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ENTT_DEC_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ENTITY_DECIMAL_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Decimal';


DROP TABLE IF EXISTS `customer_entity_int`;
CREATE TABLE `customer_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `IDX_CUSTOMER_ENTITY_INT_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CUSTOMER_ENTITY_INT_ENTITY_ID` (`entity_id`),
  KEY `IDX_CUSTOMER_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CSTR_ENTT_INT_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ENTITY_INT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ENTITY_INT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Int';


DROP TABLE IF EXISTS `customer_entity_text`;
CREATE TABLE `customer_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `IDX_CUSTOMER_ENTITY_TEXT_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CUSTOMER_ENTITY_TEXT_ENTITY_ID` (`entity_id`),
  CONSTRAINT `FK_CSTR_ENTT_TEXT_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ENTITY_TEXT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ENTITY_TEXT_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Text';


DROP TABLE IF EXISTS `customer_entity_varchar`;
CREATE TABLE `customer_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID` (`entity_id`,`attribute_id`),
  KEY `IDX_CUSTOMER_ENTITY_VARCHAR_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_CUSTOMER_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_CUSTOMER_ENTITY_VARCHAR_ENTITY_ID` (`entity_id`),
  KEY `IDX_CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_VALUE` (`entity_id`,`attribute_id`,`value`),
  CONSTRAINT `FK_CSTR_ENTT_VCHR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CSTR_ENTT_VCHR_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_CUSTOMER_ENTITY_VARCHAR_ENTITY_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Entity Varchar';


DROP TABLE IF EXISTS `customer_flowpassword`;
CREATE TABLE `customer_flowpassword` (
  `flowpassword_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Flow password Id',
  `ip` varchar(50) NOT NULL COMMENT 'User IP',
  `email` varchar(255) NOT NULL COMMENT 'Requested email for change',
  `requested_date` varchar(255) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Requested date for change',
  PRIMARY KEY (`flowpassword_id`),
  KEY `IDX_CUSTOMER_FLOWPASSWORD_EMAIL` (`email`),
  KEY `IDX_CUSTOMER_FLOWPASSWORD_IP` (`ip`),
  KEY `IDX_CUSTOMER_FLOWPASSWORD_REQUESTED_DATE` (`requested_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer flow password';


DROP TABLE IF EXISTS `customer_form_attribute`;
CREATE TABLE `customer_form_attribute` (
  `form_code` varchar(32) NOT NULL COMMENT 'Form Code',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`form_code`,`attribute_id`),
  KEY `IDX_CUSTOMER_FORM_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `FK_CSTR_FORM_ATTR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Form Attribute';

INSERT INTO `customer_form_attribute` (`form_code`, `attribute_id`) VALUES
('adminhtml_customer',	1),
('adminhtml_customer',	3),
('adminhtml_customer',	4),
('checkout_register',	4),
('customer_account_create',	4),
('customer_account_edit',	4),
('adminhtml_customer',	5),
('checkout_register',	5),
('customer_account_create',	5),
('customer_account_edit',	5),
('adminhtml_customer',	6),
('checkout_register',	6),
('customer_account_create',	6),
('customer_account_edit',	6),
('adminhtml_customer',	7),
('checkout_register',	7),
('customer_account_create',	7),
('customer_account_edit',	7),
('adminhtml_customer',	8),
('checkout_register',	8),
('customer_account_create',	8),
('customer_account_edit',	8),
('adminhtml_checkout',	9),
('adminhtml_customer',	9),
('checkout_register',	9),
('customer_account_create',	9),
('customer_account_edit',	9),
('adminhtml_checkout',	10),
('adminhtml_customer',	10),
('adminhtml_checkout',	11),
('adminhtml_customer',	11),
('checkout_register',	11),
('customer_account_create',	11),
('customer_account_edit',	11),
('adminhtml_checkout',	15),
('adminhtml_customer',	15),
('checkout_register',	15),
('customer_account_create',	15),
('customer_account_edit',	15),
('adminhtml_customer',	17),
('checkout_register',	17),
('customer_account_create',	17),
('customer_account_edit',	17),
('adminhtml_checkout',	18),
('adminhtml_customer',	18),
('checkout_register',	18),
('customer_account_create',	18),
('customer_account_edit',	18),
('adminhtml_customer_address',	19),
('customer_address_edit',	19),
('customer_register_address',	19),
('adminhtml_customer_address',	20),
('customer_address_edit',	20),
('customer_register_address',	20),
('adminhtml_customer_address',	21),
('customer_address_edit',	21),
('customer_register_address',	21),
('adminhtml_customer_address',	22),
('customer_address_edit',	22),
('customer_register_address',	22),
('adminhtml_customer_address',	23),
('customer_address_edit',	23),
('customer_register_address',	23),
('adminhtml_customer_address',	24),
('customer_address_edit',	24),
('customer_register_address',	24),
('adminhtml_customer_address',	25),
('customer_address_edit',	25),
('customer_register_address',	25),
('adminhtml_customer_address',	26),
('customer_address_edit',	26),
('customer_register_address',	26),
('adminhtml_customer_address',	27),
('customer_address_edit',	27),
('customer_register_address',	27),
('adminhtml_customer_address',	28),
('customer_address_edit',	28),
('customer_register_address',	28),
('adminhtml_customer_address',	29),
('customer_address_edit',	29),
('customer_register_address',	29),
('adminhtml_customer_address',	30),
('customer_address_edit',	30),
('customer_register_address',	30),
('adminhtml_customer_address',	31),
('customer_address_edit',	31),
('customer_register_address',	31),
('adminhtml_customer_address',	32),
('customer_address_edit',	32),
('customer_register_address',	32),
('adminhtml_customer',	35),
('adminhtml_customer_address',	36),
('customer_address_edit',	36),
('customer_register_address',	36);

DROP TABLE IF EXISTS `customer_group`;
CREATE TABLE `customer_group` (
  `customer_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Customer Group Id',
  `customer_group_code` varchar(32) NOT NULL COMMENT 'Customer Group Code',
  `tax_class_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Tax Class Id',
  PRIMARY KEY (`customer_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Group';

INSERT INTO `customer_group` (`customer_group_id`, `customer_group_code`, `tax_class_id`) VALUES
(0,	'NOT LOGGED IN',	3),
(1,	'General',	3),
(2,	'Wholesale',	3),
(3,	'Retailer',	3);

DROP TABLE IF EXISTS `dataflow_batch`;
CREATE TABLE `dataflow_batch` (
  `batch_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Batch Id',
  `profile_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Profile ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `adapter` varchar(128) DEFAULT NULL COMMENT 'Adapter',
  `params` text COMMENT 'Parameters',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`batch_id`),
  KEY `IDX_DATAFLOW_BATCH_PROFILE_ID` (`profile_id`),
  KEY `IDX_DATAFLOW_BATCH_STORE_ID` (`store_id`),
  KEY `IDX_DATAFLOW_BATCH_CREATED_AT` (`created_at`),
  CONSTRAINT `FK_DATAFLOW_BATCH_PROFILE_ID_DATAFLOW_PROFILE_PROFILE_ID` FOREIGN KEY (`profile_id`) REFERENCES `dataflow_profile` (`profile_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_DATAFLOW_BATCH_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Dataflow Batch';


DROP TABLE IF EXISTS `dataflow_batch_export`;
CREATE TABLE `dataflow_batch_export` (
  `batch_export_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Batch Export Id',
  `batch_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Batch Id',
  `batch_data` longtext COMMENT 'Batch Data',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Status',
  PRIMARY KEY (`batch_export_id`),
  KEY `IDX_DATAFLOW_BATCH_EXPORT_BATCH_ID` (`batch_id`),
  CONSTRAINT `FK_DATAFLOW_BATCH_EXPORT_BATCH_ID_DATAFLOW_BATCH_BATCH_ID` FOREIGN KEY (`batch_id`) REFERENCES `dataflow_batch` (`batch_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Dataflow Batch Export';


DROP TABLE IF EXISTS `dataflow_batch_import`;
CREATE TABLE `dataflow_batch_import` (
  `batch_import_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Batch Import Id',
  `batch_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Batch Id',
  `batch_data` longtext COMMENT 'Batch Data',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Status',
  PRIMARY KEY (`batch_import_id`),
  KEY `IDX_DATAFLOW_BATCH_IMPORT_BATCH_ID` (`batch_id`),
  CONSTRAINT `FK_DATAFLOW_BATCH_IMPORT_BATCH_ID_DATAFLOW_BATCH_BATCH_ID` FOREIGN KEY (`batch_id`) REFERENCES `dataflow_batch` (`batch_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Dataflow Batch Import';


DROP TABLE IF EXISTS `dataflow_import_data`;
CREATE TABLE `dataflow_import_data` (
  `import_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Import Id',
  `session_id` int(11) DEFAULT NULL COMMENT 'Session Id',
  `serial_number` int(11) NOT NULL DEFAULT '0' COMMENT 'Serial Number',
  `value` text COMMENT 'Value',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Status',
  PRIMARY KEY (`import_id`),
  KEY `IDX_DATAFLOW_IMPORT_DATA_SESSION_ID` (`session_id`),
  CONSTRAINT `FK_DATAFLOW_IMPORT_DATA_SESSION_ID_DATAFLOW_SESSION_SESSION_ID` FOREIGN KEY (`session_id`) REFERENCES `dataflow_session` (`session_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Dataflow Import Data';


DROP TABLE IF EXISTS `dataflow_profile`;
CREATE TABLE `dataflow_profile` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Profile Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `actions_xml` text COMMENT 'Actions Xml',
  `gui_data` text COMMENT 'Gui Data',
  `direction` varchar(6) DEFAULT NULL COMMENT 'Direction',
  `entity_type` varchar(64) DEFAULT NULL COMMENT 'Entity Type',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `data_transfer` varchar(11) DEFAULT NULL COMMENT 'Data Transfer',
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Dataflow Profile';

INSERT INTO `dataflow_profile` (`profile_id`, `name`, `created_at`, `updated_at`, `actions_xml`, `gui_data`, `direction`, `entity_type`, `store_id`, `data_transfer`) VALUES
(1,	'Export All Products',	'2016-12-14 06:00:58',	'2016-12-14 06:00:58',	'<action type=\"catalog/convert_adapter_product\" method=\"load\">\\r\\n    <var name=\"store\"><![CDATA[0]]></var>\\r\\n</action>\\r\\n\\r\\n<action type=\"catalog/convert_parser_product\" method=\"unparse\">\\r\\n    <var name=\"store\"><![CDATA[0]]></var>\\r\\n</action>\\r\\n\\r\\n<action type=\"dataflow/convert_mapper_column\" method=\"map\">\\r\\n</action>\\r\\n\\r\\n<action type=\"dataflow/convert_parser_csv\" method=\"unparse\">\\r\\n    <var name=\"delimiter\"><![CDATA[,]]></var>\\r\\n    <var name=\"enclose\"><![CDATA[\"]]></var>\\r\\n    <var name=\"fieldnames\">true</var>\\r\\n</action>\\r\\n\\r\\n<action type=\"dataflow/convert_adapter_io\" method=\"save\">\\r\\n    <var name=\"type\">file</var>\\r\\n    <var name=\"path\">var/export</var>\\r\\n    <var name=\"filename\"><![CDATA[export_all_products.csv]]></var>\\r\\n</action>\\r\\n\\r\\n',	'a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:23:\"export_all_products.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:0:\"\";s:7:\"product\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}',	'export',	'product',	0,	'file'),
(2,	'Export Product Stocks',	'2016-12-14 06:00:58',	'2016-12-14 06:00:58',	'<action type=\"catalog/convert_adapter_product\" method=\"load\">\\r\\n    <var name=\"store\"><![CDATA[0]]></var>\\r\\n</action>\\r\\n\\r\\n<action type=\"catalog/convert_parser_product\" method=\"unparse\">\\r\\n    <var name=\"store\"><![CDATA[0]]></var>\\r\\n</action>\\r\\n\\r\\n<action type=\"dataflow/convert_mapper_column\" method=\"map\">\\r\\n</action>\\r\\n\\r\\n<action type=\"dataflow/convert_parser_csv\" method=\"unparse\">\\r\\n    <var name=\"delimiter\"><![CDATA[,]]></var>\\r\\n    <var name=\"enclose\"><![CDATA[\"]]></var>\\r\\n    <var name=\"fieldnames\">true</var>\\r\\n</action>\\r\\n\\r\\n<action type=\"dataflow/convert_adapter_io\" method=\"save\">\\r\\n    <var name=\"type\">file</var>\\r\\n    <var name=\"path\">var/export</var>\\r\\n    <var name=\"filename\"><![CDATA[export_all_products.csv]]></var>\\r\\n</action>\\r\\n\\r\\n',	'a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:25:\"export_product_stocks.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:4:\"true\";s:7:\"product\";a:2:{s:2:\"db\";a:4:{i:1;s:5:\"store\";i:2;s:3:\"sku\";i:3;s:3:\"qty\";i:4;s:11:\"is_in_stock\";}s:4:\"file\";a:4:{i:1;s:5:\"store\";i:2;s:3:\"sku\";i:3;s:3:\"qty\";i:4;s:11:\"is_in_stock\";}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}',	'export',	'product',	0,	'file'),
(3,	'Import All Products',	'2016-12-14 06:00:58',	'2016-12-14 06:00:58',	'<action type=\"dataflow/convert_parser_csv\" method=\"parse\">\\r\\n    <var name=\"delimiter\"><![CDATA[,]]></var>\\r\\n    <var name=\"enclose\"><![CDATA[\"]]></var>\\r\\n    <var name=\"fieldnames\">true</var>\\r\\n    <var name=\"store\"><![CDATA[0]]></var>\\r\\n    <var name=\"adapter\">catalog/convert_adapter_product</var>\\r\\n    <var name=\"method\">parse</var>\\r\\n</action>',	'a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:23:\"export_all_products.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:0:\"\";s:7:\"product\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}',	'import',	'product',	0,	'interactive'),
(4,	'Import Product Stocks',	'2016-12-14 06:00:58',	'2016-12-14 06:00:58',	'<action type=\"dataflow/convert_parser_csv\" method=\"parse\">\\r\\n    <var name=\"delimiter\"><![CDATA[,]]></var>\\r\\n    <var name=\"enclose\"><![CDATA[\"]]></var>\\r\\n    <var name=\"fieldnames\">true</var>\\r\\n    <var name=\"store\"><![CDATA[0]]></var>\\r\\n    <var name=\"adapter\">catalog/convert_adapter_product</var>\\r\\n    <var name=\"method\">parse</var>\\r\\n</action>',	'a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:18:\"export_product.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:0:\"\";s:7:\"product\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}',	'import',	'product',	0,	'interactive'),
(5,	'Export Customers',	'2016-12-14 06:00:58',	'2016-12-14 06:00:58',	'<action type=\"customer/convert_adapter_customer\" method=\"load\">\\r\\n    <var name=\"store\"><![CDATA[0]]></var>\\r\\n    <var name=\"filter/adressType\"><![CDATA[default_billing]]></var>\\r\\n</action>\\r\\n\\r\\n<action type=\"customer/convert_parser_customer\" method=\"unparse\">\\r\\n    <var name=\"store\"><![CDATA[0]]></var>\\r\\n</action>\\r\\n\\r\\n<action type=\"dataflow/convert_mapper_column\" method=\"map\">\\r\\n</action>\\r\\n\\r\\n<action type=\"dataflow/convert_parser_csv\" method=\"unparse\">\\r\\n    <var name=\"delimiter\"><![CDATA[,]]></var>\\r\\n    <var name=\"enclose\"><![CDATA[\"]]></var>\\r\\n    <var name=\"fieldnames\">true</var>\\r\\n</action>\\r\\n\\r\\n<action type=\"dataflow/convert_adapter_io\" method=\"save\">\\r\\n    <var name=\"type\">file</var>\\r\\n    <var name=\"path\">var/export</var>\\r\\n    <var name=\"filename\"><![CDATA[export_customers.csv]]></var>\\r\\n</action>\\r\\n\\r\\n',	'a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:20:\"export_customers.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:0:\"\";s:7:\"product\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}',	'export',	'customer',	0,	'file'),
(6,	'Import Customers',	'2016-12-14 06:00:58',	'2016-12-14 06:00:58',	'<action type=\"dataflow/convert_parser_csv\" method=\"parse\">\\r\\n    <var name=\"delimiter\"><![CDATA[,]]></var>\\r\\n    <var name=\"enclose\"><![CDATA[\"]]></var>\\r\\n    <var name=\"fieldnames\">true</var>\\r\\n    <var name=\"store\"><![CDATA[0]]></var>\\r\\n    <var name=\"adapter\">customer/convert_adapter_customer</var>\\r\\n    <var name=\"method\">parse</var>\\r\\n</action>',	'a:5:{s:4:\"file\";a:7:{s:4:\"type\";s:4:\"file\";s:8:\"filename\";s:19:\"export_customer.csv\";s:4:\"path\";s:10:\"var/export\";s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:8:\"password\";s:0:\"\";s:7:\"passive\";s:0:\"\";}s:5:\"parse\";a:5:{s:4:\"type\";s:3:\"csv\";s:12:\"single_sheet\";s:0:\"\";s:9:\"delimiter\";s:1:\",\";s:7:\"enclose\";s:1:\"\"\";s:10:\"fieldnames\";s:4:\"true\";}s:3:\"map\";a:3:{s:14:\"only_specified\";s:0:\"\";s:7:\"product\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}s:8:\"customer\";a:2:{s:2:\"db\";a:0:{}s:4:\"file\";a:0:{}}}s:7:\"product\";a:1:{s:6:\"filter\";a:8:{s:4:\"name\";s:0:\"\";s:3:\"sku\";s:0:\"\";s:4:\"type\";s:1:\"0\";s:13:\"attribute_set\";s:0:\"\";s:5:\"price\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:3:\"qty\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}s:10:\"visibility\";s:1:\"0\";s:6:\"status\";s:1:\"0\";}}s:8:\"customer\";a:1:{s:6:\"filter\";a:10:{s:9:\"firstname\";s:0:\"\";s:8:\"lastname\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"group\";s:1:\"0\";s:10:\"adressType\";s:15:\"default_billing\";s:9:\"telephone\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:7:\"country\";s:0:\"\";s:6:\"region\";s:0:\"\";s:10:\"created_at\";a:2:{s:4:\"from\";s:0:\"\";s:2:\"to\";s:0:\"\";}}}}',	'import',	'customer',	0,	'interactive');

DROP TABLE IF EXISTS `dataflow_profile_history`;
CREATE TABLE `dataflow_profile_history` (
  `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'History Id',
  `profile_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Profile Id',
  `action_code` varchar(64) DEFAULT NULL COMMENT 'Action Code',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'User Id',
  `performed_at` timestamp NULL DEFAULT NULL COMMENT 'Performed At',
  PRIMARY KEY (`history_id`),
  KEY `IDX_DATAFLOW_PROFILE_HISTORY_PROFILE_ID` (`profile_id`),
  CONSTRAINT `FK_AEA06B0C500063D3CE6EA671AE776645` FOREIGN KEY (`profile_id`) REFERENCES `dataflow_profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Dataflow Profile History';

INSERT INTO `dataflow_profile_history` (`history_id`, `profile_id`, `action_code`, `user_id`, `performed_at`) VALUES
(1,	1,	'create',	0,	'2016-12-14 06:00:58'),
(2,	2,	'create',	0,	'2016-12-14 06:00:58'),
(3,	3,	'create',	0,	'2016-12-14 06:00:58'),
(4,	4,	'create',	0,	'2016-12-14 06:00:58'),
(5,	5,	'create',	0,	'2016-12-14 06:00:58'),
(6,	6,	'create',	0,	'2016-12-14 06:00:58');

DROP TABLE IF EXISTS `dataflow_session`;
CREATE TABLE `dataflow_session` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Session Id',
  `user_id` int(11) NOT NULL COMMENT 'User Id',
  `created_date` timestamp NULL DEFAULT NULL COMMENT 'Created Date',
  `file` varchar(255) DEFAULT NULL COMMENT 'File',
  `type` varchar(32) DEFAULT NULL COMMENT 'Type',
  `direction` varchar(32) DEFAULT NULL COMMENT 'Direction',
  `comment` varchar(255) DEFAULT NULL COMMENT 'Comment',
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Dataflow Session';


DROP TABLE IF EXISTS `design_change`;
CREATE TABLE `design_change` (
  `design_change_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Design Change Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `design` varchar(255) DEFAULT NULL COMMENT 'Design',
  `date_from` date DEFAULT NULL COMMENT 'First Date of Design Activity',
  `date_to` date DEFAULT NULL COMMENT 'Last Date of Design Activity',
  PRIMARY KEY (`design_change_id`),
  KEY `IDX_DESIGN_CHANGE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_DESIGN_CHANGE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Design Changes';


DROP TABLE IF EXISTS `directory_country`;
CREATE TABLE `directory_country` (
  `country_id` varchar(2) NOT NULL DEFAULT '' COMMENT 'Country Id in ISO-2',
  `iso2_code` varchar(2) DEFAULT NULL COMMENT 'Country ISO-2 format',
  `iso3_code` varchar(3) DEFAULT NULL COMMENT 'Country ISO-3',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country';

INSERT INTO `directory_country` (`country_id`, `iso2_code`, `iso3_code`) VALUES
('AD',	'AD',	'AND'),
('AE',	'AE',	'ARE'),
('AF',	'AF',	'AFG'),
('AG',	'AG',	'ATG'),
('AI',	'AI',	'AIA'),
('AL',	'AL',	'ALB'),
('AM',	'AM',	'ARM'),
('AN',	'AN',	'ANT'),
('AO',	'AO',	'AGO'),
('AQ',	'AQ',	'ATA'),
('AR',	'AR',	'ARG'),
('AS',	'AS',	'ASM'),
('AT',	'AT',	'AUT'),
('AU',	'AU',	'AUS'),
('AW',	'AW',	'ABW'),
('AX',	'AX',	'ALA'),
('AZ',	'AZ',	'AZE'),
('BA',	'BA',	'BIH'),
('BB',	'BB',	'BRB'),
('BD',	'BD',	'BGD'),
('BE',	'BE',	'BEL'),
('BF',	'BF',	'BFA'),
('BG',	'BG',	'BGR'),
('BH',	'BH',	'BHR'),
('BI',	'BI',	'BDI'),
('BJ',	'BJ',	'BEN'),
('BL',	'BL',	'BLM'),
('BM',	'BM',	'BMU'),
('BN',	'BN',	'BRN'),
('BO',	'BO',	'BOL'),
('BR',	'BR',	'BRA'),
('BS',	'BS',	'BHS'),
('BT',	'BT',	'BTN'),
('BV',	'BV',	'BVT'),
('BW',	'BW',	'BWA'),
('BY',	'BY',	'BLR'),
('BZ',	'BZ',	'BLZ'),
('CA',	'CA',	'CAN'),
('CC',	'CC',	'CCK'),
('CD',	'CD',	'COD'),
('CF',	'CF',	'CAF'),
('CG',	'CG',	'COG'),
('CH',	'CH',	'CHE'),
('CI',	'CI',	'CIV'),
('CK',	'CK',	'COK'),
('CL',	'CL',	'CHL'),
('CM',	'CM',	'CMR'),
('CN',	'CN',	'CHN'),
('CO',	'CO',	'COL'),
('CR',	'CR',	'CRI'),
('CU',	'CU',	'CUB'),
('CV',	'CV',	'CPV'),
('CX',	'CX',	'CXR'),
('CY',	'CY',	'CYP'),
('CZ',	'CZ',	'CZE'),
('DE',	'DE',	'DEU'),
('DJ',	'DJ',	'DJI'),
('DK',	'DK',	'DNK'),
('DM',	'DM',	'DMA'),
('DO',	'DO',	'DOM'),
('DZ',	'DZ',	'DZA'),
('EC',	'EC',	'ECU'),
('EE',	'EE',	'EST'),
('EG',	'EG',	'EGY'),
('EH',	'EH',	'ESH'),
('ER',	'ER',	'ERI'),
('ES',	'ES',	'ESP'),
('ET',	'ET',	'ETH'),
('FI',	'FI',	'FIN'),
('FJ',	'FJ',	'FJI'),
('FK',	'FK',	'FLK'),
('FM',	'FM',	'FSM'),
('FO',	'FO',	'FRO'),
('FR',	'FR',	'FRA'),
('GA',	'GA',	'GAB'),
('GB',	'GB',	'GBR'),
('GD',	'GD',	'GRD'),
('GE',	'GE',	'GEO'),
('GF',	'GF',	'GUF'),
('GG',	'GG',	'GGY'),
('GH',	'GH',	'GHA'),
('GI',	'GI',	'GIB'),
('GL',	'GL',	'GRL'),
('GM',	'GM',	'GMB'),
('GN',	'GN',	'GIN'),
('GP',	'GP',	'GLP'),
('GQ',	'GQ',	'GNQ'),
('GR',	'GR',	'GRC'),
('GS',	'GS',	'SGS'),
('GT',	'GT',	'GTM'),
('GU',	'GU',	'GUM'),
('GW',	'GW',	'GNB'),
('GY',	'GY',	'GUY'),
('HK',	'HK',	'HKG'),
('HM',	'HM',	'HMD'),
('HN',	'HN',	'HND'),
('HR',	'HR',	'HRV'),
('HT',	'HT',	'HTI'),
('HU',	'HU',	'HUN'),
('ID',	'ID',	'IDN'),
('IE',	'IE',	'IRL'),
('IL',	'IL',	'ISR'),
('IM',	'IM',	'IMN'),
('IN',	'IN',	'IND'),
('IO',	'IO',	'IOT'),
('IQ',	'IQ',	'IRQ'),
('IR',	'IR',	'IRN'),
('IS',	'IS',	'ISL'),
('IT',	'IT',	'ITA'),
('JE',	'JE',	'JEY'),
('JM',	'JM',	'JAM'),
('JO',	'JO',	'JOR'),
('JP',	'JP',	'JPN'),
('KE',	'KE',	'KEN'),
('KG',	'KG',	'KGZ'),
('KH',	'KH',	'KHM'),
('KI',	'KI',	'KIR'),
('KM',	'KM',	'COM'),
('KN',	'KN',	'KNA'),
('KP',	'KP',	'PRK'),
('KR',	'KR',	'KOR'),
('KW',	'KW',	'KWT'),
('KY',	'KY',	'CYM'),
('KZ',	'KZ',	'KAZ'),
('LA',	'LA',	'LAO'),
('LB',	'LB',	'LBN'),
('LC',	'LC',	'LCA'),
('LI',	'LI',	'LIE'),
('LK',	'LK',	'LKA'),
('LR',	'LR',	'LBR'),
('LS',	'LS',	'LSO'),
('LT',	'LT',	'LTU'),
('LU',	'LU',	'LUX'),
('LV',	'LV',	'LVA'),
('LY',	'LY',	'LBY'),
('MA',	'MA',	'MAR'),
('MC',	'MC',	'MCO'),
('MD',	'MD',	'MDA'),
('ME',	'ME',	'MNE'),
('MF',	'MF',	'MAF'),
('MG',	'MG',	'MDG'),
('MH',	'MH',	'MHL'),
('MK',	'MK',	'MKD'),
('ML',	'ML',	'MLI'),
('MM',	'MM',	'MMR'),
('MN',	'MN',	'MNG'),
('MO',	'MO',	'MAC'),
('MP',	'MP',	'MNP'),
('MQ',	'MQ',	'MTQ'),
('MR',	'MR',	'MRT'),
('MS',	'MS',	'MSR'),
('MT',	'MT',	'MLT'),
('MU',	'MU',	'MUS'),
('MV',	'MV',	'MDV'),
('MW',	'MW',	'MWI'),
('MX',	'MX',	'MEX'),
('MY',	'MY',	'MYS'),
('MZ',	'MZ',	'MOZ'),
('NA',	'NA',	'NAM'),
('NC',	'NC',	'NCL'),
('NE',	'NE',	'NER'),
('NF',	'NF',	'NFK'),
('NG',	'NG',	'NGA'),
('NI',	'NI',	'NIC'),
('NL',	'NL',	'NLD'),
('NO',	'NO',	'NOR'),
('NP',	'NP',	'NPL'),
('NR',	'NR',	'NRU'),
('NU',	'NU',	'NIU'),
('NZ',	'NZ',	'NZL'),
('OM',	'OM',	'OMN'),
('PA',	'PA',	'PAN'),
('PE',	'PE',	'PER'),
('PF',	'PF',	'PYF'),
('PG',	'PG',	'PNG'),
('PH',	'PH',	'PHL'),
('PK',	'PK',	'PAK'),
('PL',	'PL',	'POL'),
('PM',	'PM',	'SPM'),
('PN',	'PN',	'PCN'),
('PR',	'PR',	'PRI'),
('PS',	'PS',	'PSE'),
('PT',	'PT',	'PRT'),
('PW',	'PW',	'PLW'),
('PY',	'PY',	'PRY'),
('QA',	'QA',	'QAT'),
('RE',	'RE',	'REU'),
('RO',	'RO',	'ROU'),
('RS',	'RS',	'SRB'),
('RU',	'RU',	'RUS'),
('RW',	'RW',	'RWA'),
('SA',	'SA',	'SAU'),
('SB',	'SB',	'SLB'),
('SC',	'SC',	'SYC'),
('SD',	'SD',	'SDN'),
('SE',	'SE',	'SWE'),
('SG',	'SG',	'SGP'),
('SH',	'SH',	'SHN'),
('SI',	'SI',	'SVN'),
('SJ',	'SJ',	'SJM'),
('SK',	'SK',	'SVK'),
('SL',	'SL',	'SLE'),
('SM',	'SM',	'SMR'),
('SN',	'SN',	'SEN'),
('SO',	'SO',	'SOM'),
('SR',	'SR',	'SUR'),
('ST',	'ST',	'STP'),
('SV',	'SV',	'SLV'),
('SY',	'SY',	'SYR'),
('SZ',	'SZ',	'SWZ'),
('TC',	'TC',	'TCA'),
('TD',	'TD',	'TCD'),
('TF',	'TF',	'ATF'),
('TG',	'TG',	'TGO'),
('TH',	'TH',	'THA'),
('TJ',	'TJ',	'TJK'),
('TK',	'TK',	'TKL'),
('TL',	'TL',	'TLS'),
('TM',	'TM',	'TKM'),
('TN',	'TN',	'TUN'),
('TO',	'TO',	'TON'),
('TR',	'TR',	'TUR'),
('TT',	'TT',	'TTO'),
('TV',	'TV',	'TUV'),
('TW',	'TW',	'TWN'),
('TZ',	'TZ',	'TZA'),
('UA',	'UA',	'UKR'),
('UG',	'UG',	'UGA'),
('UM',	'UM',	'UMI'),
('US',	'US',	'USA'),
('UY',	'UY',	'URY'),
('UZ',	'UZ',	'UZB'),
('VA',	'VA',	'VAT'),
('VC',	'VC',	'VCT'),
('VE',	'VE',	'VEN'),
('VG',	'VG',	'VGB'),
('VI',	'VI',	'VIR'),
('VN',	'VN',	'VNM'),
('VU',	'VU',	'VUT'),
('WF',	'WF',	'WLF'),
('WS',	'WS',	'WSM'),
('YE',	'YE',	'YEM'),
('YT',	'YT',	'MYT'),
('ZA',	'ZA',	'ZAF'),
('ZM',	'ZM',	'ZMB'),
('ZW',	'ZW',	'ZWE');

DROP TABLE IF EXISTS `directory_country_format`;
CREATE TABLE `directory_country_format` (
  `country_format_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Country Format Id',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id in ISO-2',
  `type` varchar(30) DEFAULT NULL COMMENT 'Country Format Type',
  `format` text NOT NULL COMMENT 'Country Format',
  PRIMARY KEY (`country_format_id`),
  UNIQUE KEY `UNQ_DIRECTORY_COUNTRY_FORMAT_COUNTRY_ID_TYPE` (`country_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Format';


DROP TABLE IF EXISTS `directory_country_region`;
CREATE TABLE `directory_country_region` (
  `region_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Region Id',
  `country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Country Id in ISO-2',
  `code` varchar(32) DEFAULT NULL COMMENT 'Region code',
  `default_name` varchar(255) DEFAULT NULL COMMENT 'Region Name',
  PRIMARY KEY (`region_id`),
  KEY `IDX_DIRECTORY_COUNTRY_REGION_COUNTRY_ID` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Region';

INSERT INTO `directory_country_region` (`region_id`, `country_id`, `code`, `default_name`) VALUES
(1,	'US',	'AL',	'Alabama'),
(2,	'US',	'AK',	'Alaska'),
(3,	'US',	'AS',	'American Samoa'),
(4,	'US',	'AZ',	'Arizona'),
(5,	'US',	'AR',	'Arkansas'),
(6,	'US',	'AF',	'Armed Forces Africa'),
(7,	'US',	'AA',	'Armed Forces Americas'),
(8,	'US',	'AC',	'Armed Forces Canada'),
(9,	'US',	'AE',	'Armed Forces Europe'),
(10,	'US',	'AM',	'Armed Forces Middle East'),
(11,	'US',	'AP',	'Armed Forces Pacific'),
(12,	'US',	'CA',	'California'),
(13,	'US',	'CO',	'Colorado'),
(14,	'US',	'CT',	'Connecticut'),
(15,	'US',	'DE',	'Delaware'),
(16,	'US',	'DC',	'District of Columbia'),
(17,	'US',	'FM',	'Federated States Of Micronesia'),
(18,	'US',	'FL',	'Florida'),
(19,	'US',	'GA',	'Georgia'),
(20,	'US',	'GU',	'Guam'),
(21,	'US',	'HI',	'Hawaii'),
(22,	'US',	'ID',	'Idaho'),
(23,	'US',	'IL',	'Illinois'),
(24,	'US',	'IN',	'Indiana'),
(25,	'US',	'IA',	'Iowa'),
(26,	'US',	'KS',	'Kansas'),
(27,	'US',	'KY',	'Kentucky'),
(28,	'US',	'LA',	'Louisiana'),
(29,	'US',	'ME',	'Maine'),
(30,	'US',	'MH',	'Marshall Islands'),
(31,	'US',	'MD',	'Maryland'),
(32,	'US',	'MA',	'Massachusetts'),
(33,	'US',	'MI',	'Michigan'),
(34,	'US',	'MN',	'Minnesota'),
(35,	'US',	'MS',	'Mississippi'),
(36,	'US',	'MO',	'Missouri'),
(37,	'US',	'MT',	'Montana'),
(38,	'US',	'NE',	'Nebraska'),
(39,	'US',	'NV',	'Nevada'),
(40,	'US',	'NH',	'New Hampshire'),
(41,	'US',	'NJ',	'New Jersey'),
(42,	'US',	'NM',	'New Mexico'),
(43,	'US',	'NY',	'New York'),
(44,	'US',	'NC',	'North Carolina'),
(45,	'US',	'ND',	'North Dakota'),
(46,	'US',	'MP',	'Northern Mariana Islands'),
(47,	'US',	'OH',	'Ohio'),
(48,	'US',	'OK',	'Oklahoma'),
(49,	'US',	'OR',	'Oregon'),
(50,	'US',	'PW',	'Palau'),
(51,	'US',	'PA',	'Pennsylvania'),
(52,	'US',	'PR',	'Puerto Rico'),
(53,	'US',	'RI',	'Rhode Island'),
(54,	'US',	'SC',	'South Carolina'),
(55,	'US',	'SD',	'South Dakota'),
(56,	'US',	'TN',	'Tennessee'),
(57,	'US',	'TX',	'Texas'),
(58,	'US',	'UT',	'Utah'),
(59,	'US',	'VT',	'Vermont'),
(60,	'US',	'VI',	'Virgin Islands'),
(61,	'US',	'VA',	'Virginia'),
(62,	'US',	'WA',	'Washington'),
(63,	'US',	'WV',	'West Virginia'),
(64,	'US',	'WI',	'Wisconsin'),
(65,	'US',	'WY',	'Wyoming'),
(66,	'CA',	'AB',	'Alberta'),
(67,	'CA',	'BC',	'British Columbia'),
(68,	'CA',	'MB',	'Manitoba'),
(69,	'CA',	'NL',	'Newfoundland and Labrador'),
(70,	'CA',	'NB',	'New Brunswick'),
(71,	'CA',	'NS',	'Nova Scotia'),
(72,	'CA',	'NT',	'Northwest Territories'),
(73,	'CA',	'NU',	'Nunavut'),
(74,	'CA',	'ON',	'Ontario'),
(75,	'CA',	'PE',	'Prince Edward Island'),
(76,	'CA',	'QC',	'Quebec'),
(77,	'CA',	'SK',	'Saskatchewan'),
(78,	'CA',	'YT',	'Yukon Territory'),
(79,	'DE',	'NDS',	'Niedersachsen'),
(80,	'DE',	'BAW',	'Baden-Württemberg'),
(81,	'DE',	'BAY',	'Bayern'),
(82,	'DE',	'BER',	'Berlin'),
(83,	'DE',	'BRG',	'Brandenburg'),
(84,	'DE',	'BRE',	'Bremen'),
(85,	'DE',	'HAM',	'Hamburg'),
(86,	'DE',	'HES',	'Hessen'),
(87,	'DE',	'MEC',	'Mecklenburg-Vorpommern'),
(88,	'DE',	'NRW',	'Nordrhein-Westfalen'),
(89,	'DE',	'RHE',	'Rheinland-Pfalz'),
(90,	'DE',	'SAR',	'Saarland'),
(91,	'DE',	'SAS',	'Sachsen'),
(92,	'DE',	'SAC',	'Sachsen-Anhalt'),
(93,	'DE',	'SCN',	'Schleswig-Holstein'),
(94,	'DE',	'THE',	'Thüringen'),
(95,	'AT',	'WI',	'Wien'),
(96,	'AT',	'NO',	'Niederösterreich'),
(97,	'AT',	'OO',	'Oberösterreich'),
(98,	'AT',	'SB',	'Salzburg'),
(99,	'AT',	'KN',	'Kärnten'),
(100,	'AT',	'ST',	'Steiermark'),
(101,	'AT',	'TI',	'Tirol'),
(102,	'AT',	'BL',	'Burgenland'),
(103,	'AT',	'VB',	'Vorarlberg'),
(104,	'CH',	'AG',	'Aargau'),
(105,	'CH',	'AI',	'Appenzell Innerrhoden'),
(106,	'CH',	'AR',	'Appenzell Ausserrhoden'),
(107,	'CH',	'BE',	'Bern'),
(108,	'CH',	'BL',	'Basel-Landschaft'),
(109,	'CH',	'BS',	'Basel-Stadt'),
(110,	'CH',	'FR',	'Freiburg'),
(111,	'CH',	'GE',	'Genf'),
(112,	'CH',	'GL',	'Glarus'),
(113,	'CH',	'GR',	'Graubünden'),
(114,	'CH',	'JU',	'Jura'),
(115,	'CH',	'LU',	'Luzern'),
(116,	'CH',	'NE',	'Neuenburg'),
(117,	'CH',	'NW',	'Nidwalden'),
(118,	'CH',	'OW',	'Obwalden'),
(119,	'CH',	'SG',	'St. Gallen'),
(120,	'CH',	'SH',	'Schaffhausen'),
(121,	'CH',	'SO',	'Solothurn'),
(122,	'CH',	'SZ',	'Schwyz'),
(123,	'CH',	'TG',	'Thurgau'),
(124,	'CH',	'TI',	'Tessin'),
(125,	'CH',	'UR',	'Uri'),
(126,	'CH',	'VD',	'Waadt'),
(127,	'CH',	'VS',	'Wallis'),
(128,	'CH',	'ZG',	'Zug'),
(129,	'CH',	'ZH',	'Zürich'),
(130,	'ES',	'A Coruсa',	'A Coruña'),
(131,	'ES',	'Alava',	'Alava'),
(132,	'ES',	'Albacete',	'Albacete'),
(133,	'ES',	'Alicante',	'Alicante'),
(134,	'ES',	'Almeria',	'Almeria'),
(135,	'ES',	'Asturias',	'Asturias'),
(136,	'ES',	'Avila',	'Avila'),
(137,	'ES',	'Badajoz',	'Badajoz'),
(138,	'ES',	'Baleares',	'Baleares'),
(139,	'ES',	'Barcelona',	'Barcelona'),
(140,	'ES',	'Burgos',	'Burgos'),
(141,	'ES',	'Caceres',	'Caceres'),
(142,	'ES',	'Cadiz',	'Cadiz'),
(143,	'ES',	'Cantabria',	'Cantabria'),
(144,	'ES',	'Castellon',	'Castellon'),
(145,	'ES',	'Ceuta',	'Ceuta'),
(146,	'ES',	'Ciudad Real',	'Ciudad Real'),
(147,	'ES',	'Cordoba',	'Cordoba'),
(148,	'ES',	'Cuenca',	'Cuenca'),
(149,	'ES',	'Girona',	'Girona'),
(150,	'ES',	'Granada',	'Granada'),
(151,	'ES',	'Guadalajara',	'Guadalajara'),
(152,	'ES',	'Guipuzcoa',	'Guipuzcoa'),
(153,	'ES',	'Huelva',	'Huelva'),
(154,	'ES',	'Huesca',	'Huesca'),
(155,	'ES',	'Jaen',	'Jaen'),
(156,	'ES',	'La Rioja',	'La Rioja'),
(157,	'ES',	'Las Palmas',	'Las Palmas'),
(158,	'ES',	'Leon',	'Leon'),
(159,	'ES',	'Lleida',	'Lleida'),
(160,	'ES',	'Lugo',	'Lugo'),
(161,	'ES',	'Madrid',	'Madrid'),
(162,	'ES',	'Malaga',	'Malaga'),
(163,	'ES',	'Melilla',	'Melilla'),
(164,	'ES',	'Murcia',	'Murcia'),
(165,	'ES',	'Navarra',	'Navarra'),
(166,	'ES',	'Ourense',	'Ourense'),
(167,	'ES',	'Palencia',	'Palencia'),
(168,	'ES',	'Pontevedra',	'Pontevedra'),
(169,	'ES',	'Salamanca',	'Salamanca'),
(170,	'ES',	'Santa Cruz de Tenerife',	'Santa Cruz de Tenerife'),
(171,	'ES',	'Segovia',	'Segovia'),
(172,	'ES',	'Sevilla',	'Sevilla'),
(173,	'ES',	'Soria',	'Soria'),
(174,	'ES',	'Tarragona',	'Tarragona'),
(175,	'ES',	'Teruel',	'Teruel'),
(176,	'ES',	'Toledo',	'Toledo'),
(177,	'ES',	'Valencia',	'Valencia'),
(178,	'ES',	'Valladolid',	'Valladolid'),
(179,	'ES',	'Vizcaya',	'Vizcaya'),
(180,	'ES',	'Zamora',	'Zamora'),
(181,	'ES',	'Zaragoza',	'Zaragoza'),
(182,	'FR',	'1',	'Ain'),
(183,	'FR',	'2',	'Aisne'),
(184,	'FR',	'3',	'Allier'),
(185,	'FR',	'4',	'Alpes-de-Haute-Provence'),
(186,	'FR',	'5',	'Hautes-Alpes'),
(187,	'FR',	'6',	'Alpes-Maritimes'),
(188,	'FR',	'7',	'Ardèche'),
(189,	'FR',	'8',	'Ardennes'),
(190,	'FR',	'9',	'Ariège'),
(191,	'FR',	'10',	'Aube'),
(192,	'FR',	'11',	'Aude'),
(193,	'FR',	'12',	'Aveyron'),
(194,	'FR',	'13',	'Bouches-du-Rhône'),
(195,	'FR',	'14',	'Calvados'),
(196,	'FR',	'15',	'Cantal'),
(197,	'FR',	'16',	'Charente'),
(198,	'FR',	'17',	'Charente-Maritime'),
(199,	'FR',	'18',	'Cher'),
(200,	'FR',	'19',	'Corrèze'),
(201,	'FR',	'2A',	'Corse-du-Sud'),
(202,	'FR',	'2B',	'Haute-Corse'),
(203,	'FR',	'21',	'Côte-d\'Or'),
(204,	'FR',	'22',	'Côtes-d\'Armor'),
(205,	'FR',	'23',	'Creuse'),
(206,	'FR',	'24',	'Dordogne'),
(207,	'FR',	'25',	'Doubs'),
(208,	'FR',	'26',	'Drôme'),
(209,	'FR',	'27',	'Eure'),
(210,	'FR',	'28',	'Eure-et-Loir'),
(211,	'FR',	'29',	'Finistère'),
(212,	'FR',	'30',	'Gard'),
(213,	'FR',	'31',	'Haute-Garonne'),
(214,	'FR',	'32',	'Gers'),
(215,	'FR',	'33',	'Gironde'),
(216,	'FR',	'34',	'Hérault'),
(217,	'FR',	'35',	'Ille-et-Vilaine'),
(218,	'FR',	'36',	'Indre'),
(219,	'FR',	'37',	'Indre-et-Loire'),
(220,	'FR',	'38',	'Isère'),
(221,	'FR',	'39',	'Jura'),
(222,	'FR',	'40',	'Landes'),
(223,	'FR',	'41',	'Loir-et-Cher'),
(224,	'FR',	'42',	'Loire'),
(225,	'FR',	'43',	'Haute-Loire'),
(226,	'FR',	'44',	'Loire-Atlantique'),
(227,	'FR',	'45',	'Loiret'),
(228,	'FR',	'46',	'Lot'),
(229,	'FR',	'47',	'Lot-et-Garonne'),
(230,	'FR',	'48',	'Lozère'),
(231,	'FR',	'49',	'Maine-et-Loire'),
(232,	'FR',	'50',	'Manche'),
(233,	'FR',	'51',	'Marne'),
(234,	'FR',	'52',	'Haute-Marne'),
(235,	'FR',	'53',	'Mayenne'),
(236,	'FR',	'54',	'Meurthe-et-Moselle'),
(237,	'FR',	'55',	'Meuse'),
(238,	'FR',	'56',	'Morbihan'),
(239,	'FR',	'57',	'Moselle'),
(240,	'FR',	'58',	'Nièvre'),
(241,	'FR',	'59',	'Nord'),
(242,	'FR',	'60',	'Oise'),
(243,	'FR',	'61',	'Orne'),
(244,	'FR',	'62',	'Pas-de-Calais'),
(245,	'FR',	'63',	'Puy-de-Dôme'),
(246,	'FR',	'64',	'Pyrénées-Atlantiques'),
(247,	'FR',	'65',	'Hautes-Pyrénées'),
(248,	'FR',	'66',	'Pyrénées-Orientales'),
(249,	'FR',	'67',	'Bas-Rhin'),
(250,	'FR',	'68',	'Haut-Rhin'),
(251,	'FR',	'69',	'Rhône'),
(252,	'FR',	'70',	'Haute-Saône'),
(253,	'FR',	'71',	'Saône-et-Loire'),
(254,	'FR',	'72',	'Sarthe'),
(255,	'FR',	'73',	'Savoie'),
(256,	'FR',	'74',	'Haute-Savoie'),
(257,	'FR',	'75',	'Paris'),
(258,	'FR',	'76',	'Seine-Maritime'),
(259,	'FR',	'77',	'Seine-et-Marne'),
(260,	'FR',	'78',	'Yvelines'),
(261,	'FR',	'79',	'Deux-Sèvres'),
(262,	'FR',	'80',	'Somme'),
(263,	'FR',	'81',	'Tarn'),
(264,	'FR',	'82',	'Tarn-et-Garonne'),
(265,	'FR',	'83',	'Var'),
(266,	'FR',	'84',	'Vaucluse'),
(267,	'FR',	'85',	'Vendée'),
(268,	'FR',	'86',	'Vienne'),
(269,	'FR',	'87',	'Haute-Vienne'),
(270,	'FR',	'88',	'Vosges'),
(271,	'FR',	'89',	'Yonne'),
(272,	'FR',	'90',	'Territoire-de-Belfort'),
(273,	'FR',	'91',	'Essonne'),
(274,	'FR',	'92',	'Hauts-de-Seine'),
(275,	'FR',	'93',	'Seine-Saint-Denis'),
(276,	'FR',	'94',	'Val-de-Marne'),
(277,	'FR',	'95',	'Val-d\'Oise'),
(278,	'RO',	'AB',	'Alba'),
(279,	'RO',	'AR',	'Arad'),
(280,	'RO',	'AG',	'Argeş'),
(281,	'RO',	'BC',	'Bacău'),
(282,	'RO',	'BH',	'Bihor'),
(283,	'RO',	'BN',	'Bistriţa-Năsăud'),
(284,	'RO',	'BT',	'Botoşani'),
(285,	'RO',	'BV',	'Braşov'),
(286,	'RO',	'BR',	'Brăila'),
(287,	'RO',	'B',	'Bucureşti'),
(288,	'RO',	'BZ',	'Buzău'),
(289,	'RO',	'CS',	'Caraş-Severin'),
(290,	'RO',	'CL',	'Călăraşi'),
(291,	'RO',	'CJ',	'Cluj'),
(292,	'RO',	'CT',	'Constanţa'),
(293,	'RO',	'CV',	'Covasna'),
(294,	'RO',	'DB',	'Dâmboviţa'),
(295,	'RO',	'DJ',	'Dolj'),
(296,	'RO',	'GL',	'Galaţi'),
(297,	'RO',	'GR',	'Giurgiu'),
(298,	'RO',	'GJ',	'Gorj'),
(299,	'RO',	'HR',	'Harghita'),
(300,	'RO',	'HD',	'Hunedoara'),
(301,	'RO',	'IL',	'Ialomiţa'),
(302,	'RO',	'IS',	'Iaşi'),
(303,	'RO',	'IF',	'Ilfov'),
(304,	'RO',	'MM',	'Maramureş'),
(305,	'RO',	'MH',	'Mehedinţi'),
(306,	'RO',	'MS',	'Mureş'),
(307,	'RO',	'NT',	'Neamţ'),
(308,	'RO',	'OT',	'Olt'),
(309,	'RO',	'PH',	'Prahova'),
(310,	'RO',	'SM',	'Satu-Mare'),
(311,	'RO',	'SJ',	'Sălaj'),
(312,	'RO',	'SB',	'Sibiu'),
(313,	'RO',	'SV',	'Suceava'),
(314,	'RO',	'TR',	'Teleorman'),
(315,	'RO',	'TM',	'Timiş'),
(316,	'RO',	'TL',	'Tulcea'),
(317,	'RO',	'VS',	'Vaslui'),
(318,	'RO',	'VL',	'Vâlcea'),
(319,	'RO',	'VN',	'Vrancea'),
(320,	'FI',	'Lappi',	'Lappi'),
(321,	'FI',	'Pohjois-Pohjanmaa',	'Pohjois-Pohjanmaa'),
(322,	'FI',	'Kainuu',	'Kainuu'),
(323,	'FI',	'Pohjois-Karjala',	'Pohjois-Karjala'),
(324,	'FI',	'Pohjois-Savo',	'Pohjois-Savo'),
(325,	'FI',	'Etelä-Savo',	'Etelä-Savo'),
(326,	'FI',	'Etelä-Pohjanmaa',	'Etelä-Pohjanmaa'),
(327,	'FI',	'Pohjanmaa',	'Pohjanmaa'),
(328,	'FI',	'Pirkanmaa',	'Pirkanmaa'),
(329,	'FI',	'Satakunta',	'Satakunta'),
(330,	'FI',	'Keski-Pohjanmaa',	'Keski-Pohjanmaa'),
(331,	'FI',	'Keski-Suomi',	'Keski-Suomi'),
(332,	'FI',	'Varsinais-Suomi',	'Varsinais-Suomi'),
(333,	'FI',	'Etelä-Karjala',	'Etelä-Karjala'),
(334,	'FI',	'Päijät-Häme',	'Päijät-Häme'),
(335,	'FI',	'Kanta-Häme',	'Kanta-Häme'),
(336,	'FI',	'Uusimaa',	'Uusimaa'),
(337,	'FI',	'Itä-Uusimaa',	'Itä-Uusimaa'),
(338,	'FI',	'Kymenlaakso',	'Kymenlaakso'),
(339,	'FI',	'Ahvenanmaa',	'Ahvenanmaa'),
(340,	'EE',	'EE-37',	'Harjumaa'),
(341,	'EE',	'EE-39',	'Hiiumaa'),
(342,	'EE',	'EE-44',	'Ida-Virumaa'),
(343,	'EE',	'EE-49',	'Jõgevamaa'),
(344,	'EE',	'EE-51',	'Järvamaa'),
(345,	'EE',	'EE-57',	'Läänemaa'),
(346,	'EE',	'EE-59',	'Lääne-Virumaa'),
(347,	'EE',	'EE-65',	'Põlvamaa'),
(348,	'EE',	'EE-67',	'Pärnumaa'),
(349,	'EE',	'EE-70',	'Raplamaa'),
(350,	'EE',	'EE-74',	'Saaremaa'),
(351,	'EE',	'EE-78',	'Tartumaa'),
(352,	'EE',	'EE-82',	'Valgamaa'),
(353,	'EE',	'EE-84',	'Viljandimaa'),
(354,	'EE',	'EE-86',	'Võrumaa'),
(355,	'LV',	'LV-DGV',	'Daugavpils'),
(356,	'LV',	'LV-JEL',	'Jelgava'),
(357,	'LV',	'Jēkabpils',	'Jēkabpils'),
(358,	'LV',	'LV-JUR',	'Jūrmala'),
(359,	'LV',	'LV-LPX',	'Liepāja'),
(360,	'LV',	'LV-LE',	'Liepājas novads'),
(361,	'LV',	'LV-REZ',	'Rēzekne'),
(362,	'LV',	'LV-RIX',	'Rīga'),
(363,	'LV',	'LV-RI',	'Rīgas novads'),
(364,	'LV',	'Valmiera',	'Valmiera'),
(365,	'LV',	'LV-VEN',	'Ventspils'),
(366,	'LV',	'Aglonas novads',	'Aglonas novads'),
(367,	'LV',	'LV-AI',	'Aizkraukles novads'),
(368,	'LV',	'Aizputes novads',	'Aizputes novads'),
(369,	'LV',	'Aknīstes novads',	'Aknīstes novads'),
(370,	'LV',	'Alojas novads',	'Alojas novads'),
(371,	'LV',	'Alsungas novads',	'Alsungas novads'),
(372,	'LV',	'LV-AL',	'Alūksnes novads'),
(373,	'LV',	'Amatas novads',	'Amatas novads'),
(374,	'LV',	'Apes novads',	'Apes novads'),
(375,	'LV',	'Auces novads',	'Auces novads'),
(376,	'LV',	'Babītes novads',	'Babītes novads'),
(377,	'LV',	'Baldones novads',	'Baldones novads'),
(378,	'LV',	'Baltinavas novads',	'Baltinavas novads'),
(379,	'LV',	'LV-BL',	'Balvu novads'),
(380,	'LV',	'LV-BU',	'Bauskas novads'),
(381,	'LV',	'Beverīnas novads',	'Beverīnas novads'),
(382,	'LV',	'Brocēnu novads',	'Brocēnu novads'),
(383,	'LV',	'Burtnieku novads',	'Burtnieku novads'),
(384,	'LV',	'Carnikavas novads',	'Carnikavas novads'),
(385,	'LV',	'Cesvaines novads',	'Cesvaines novads'),
(386,	'LV',	'Ciblas novads',	'Ciblas novads'),
(387,	'LV',	'LV-CE',	'Cēsu novads'),
(388,	'LV',	'Dagdas novads',	'Dagdas novads'),
(389,	'LV',	'LV-DA',	'Daugavpils novads'),
(390,	'LV',	'LV-DO',	'Dobeles novads'),
(391,	'LV',	'Dundagas novads',	'Dundagas novads'),
(392,	'LV',	'Durbes novads',	'Durbes novads'),
(393,	'LV',	'Engures novads',	'Engures novads'),
(394,	'LV',	'Garkalnes novads',	'Garkalnes novads'),
(395,	'LV',	'Grobiņas novads',	'Grobiņas novads'),
(396,	'LV',	'LV-GU',	'Gulbenes novads'),
(397,	'LV',	'Iecavas novads',	'Iecavas novads'),
(398,	'LV',	'Ikšķiles novads',	'Ikšķiles novads'),
(399,	'LV',	'Ilūkstes novads',	'Ilūkstes novads'),
(400,	'LV',	'Inčukalna novads',	'Inčukalna novads'),
(401,	'LV',	'Jaunjelgavas novads',	'Jaunjelgavas novads'),
(402,	'LV',	'Jaunpiebalgas novads',	'Jaunpiebalgas novads'),
(403,	'LV',	'Jaunpils novads',	'Jaunpils novads'),
(404,	'LV',	'LV-JL',	'Jelgavas novads'),
(405,	'LV',	'LV-JK',	'Jēkabpils novads'),
(406,	'LV',	'Kandavas novads',	'Kandavas novads'),
(407,	'LV',	'Kokneses novads',	'Kokneses novads'),
(408,	'LV',	'Krimuldas novads',	'Krimuldas novads'),
(409,	'LV',	'Krustpils novads',	'Krustpils novads'),
(410,	'LV',	'LV-KR',	'Krāslavas novads'),
(411,	'LV',	'LV-KU',	'Kuldīgas novads'),
(412,	'LV',	'Kārsavas novads',	'Kārsavas novads'),
(413,	'LV',	'Lielvārdes novads',	'Lielvārdes novads'),
(414,	'LV',	'LV-LM',	'Limbažu novads'),
(415,	'LV',	'Lubānas novads',	'Lubānas novads'),
(416,	'LV',	'LV-LU',	'Ludzas novads'),
(417,	'LV',	'Līgatnes novads',	'Līgatnes novads'),
(418,	'LV',	'Līvānu novads',	'Līvānu novads'),
(419,	'LV',	'LV-MA',	'Madonas novads'),
(420,	'LV',	'Mazsalacas novads',	'Mazsalacas novads'),
(421,	'LV',	'Mālpils novads',	'Mālpils novads'),
(422,	'LV',	'Mārupes novads',	'Mārupes novads'),
(423,	'LV',	'Naukšēnu novads',	'Naukšēnu novads'),
(424,	'LV',	'Neretas novads',	'Neretas novads'),
(425,	'LV',	'Nīcas novads',	'Nīcas novads'),
(426,	'LV',	'LV-OG',	'Ogres novads'),
(427,	'LV',	'Olaines novads',	'Olaines novads'),
(428,	'LV',	'Ozolnieku novads',	'Ozolnieku novads'),
(429,	'LV',	'LV-PR',	'Preiļu novads'),
(430,	'LV',	'Priekules novads',	'Priekules novads'),
(431,	'LV',	'Priekuļu novads',	'Priekuļu novads'),
(432,	'LV',	'Pārgaujas novads',	'Pārgaujas novads'),
(433,	'LV',	'Pāvilostas novads',	'Pāvilostas novads'),
(434,	'LV',	'Pļaviņu novads',	'Pļaviņu novads'),
(435,	'LV',	'Raunas novads',	'Raunas novads'),
(436,	'LV',	'Riebiņu novads',	'Riebiņu novads'),
(437,	'LV',	'Rojas novads',	'Rojas novads'),
(438,	'LV',	'Ropažu novads',	'Ropažu novads'),
(439,	'LV',	'Rucavas novads',	'Rucavas novads'),
(440,	'LV',	'Rugāju novads',	'Rugāju novads'),
(441,	'LV',	'Rundāles novads',	'Rundāles novads'),
(442,	'LV',	'LV-RE',	'Rēzeknes novads'),
(443,	'LV',	'Rūjienas novads',	'Rūjienas novads'),
(444,	'LV',	'Salacgrīvas novads',	'Salacgrīvas novads'),
(445,	'LV',	'Salas novads',	'Salas novads'),
(446,	'LV',	'Salaspils novads',	'Salaspils novads'),
(447,	'LV',	'LV-SA',	'Saldus novads'),
(448,	'LV',	'Saulkrastu novads',	'Saulkrastu novads'),
(449,	'LV',	'Siguldas novads',	'Siguldas novads'),
(450,	'LV',	'Skrundas novads',	'Skrundas novads'),
(451,	'LV',	'Skrīveru novads',	'Skrīveru novads'),
(452,	'LV',	'Smiltenes novads',	'Smiltenes novads'),
(453,	'LV',	'Stopiņu novads',	'Stopiņu novads'),
(454,	'LV',	'Strenču novads',	'Strenču novads'),
(455,	'LV',	'Sējas novads',	'Sējas novads'),
(456,	'LV',	'LV-TA',	'Talsu novads'),
(457,	'LV',	'LV-TU',	'Tukuma novads'),
(458,	'LV',	'Tērvetes novads',	'Tērvetes novads'),
(459,	'LV',	'Vaiņodes novads',	'Vaiņodes novads'),
(460,	'LV',	'LV-VK',	'Valkas novads'),
(461,	'LV',	'LV-VM',	'Valmieras novads'),
(462,	'LV',	'Varakļānu novads',	'Varakļānu novads'),
(463,	'LV',	'Vecpiebalgas novads',	'Vecpiebalgas novads'),
(464,	'LV',	'Vecumnieku novads',	'Vecumnieku novads'),
(465,	'LV',	'LV-VE',	'Ventspils novads'),
(466,	'LV',	'Viesītes novads',	'Viesītes novads'),
(467,	'LV',	'Viļakas novads',	'Viļakas novads'),
(468,	'LV',	'Viļānu novads',	'Viļānu novads'),
(469,	'LV',	'Vārkavas novads',	'Vārkavas novads'),
(470,	'LV',	'Zilupes novads',	'Zilupes novads'),
(471,	'LV',	'Ādažu novads',	'Ādažu novads'),
(472,	'LV',	'Ērgļu novads',	'Ērgļu novads'),
(473,	'LV',	'Ķeguma novads',	'Ķeguma novads'),
(474,	'LV',	'Ķekavas novads',	'Ķekavas novads'),
(475,	'LT',	'LT-AL',	'Alytaus Apskritis'),
(476,	'LT',	'LT-KU',	'Kauno Apskritis'),
(477,	'LT',	'LT-KL',	'Klaipėdos Apskritis'),
(478,	'LT',	'LT-MR',	'Marijampolės Apskritis'),
(479,	'LT',	'LT-PN',	'Panevėžio Apskritis'),
(480,	'LT',	'LT-SA',	'Šiaulių Apskritis'),
(481,	'LT',	'LT-TA',	'Tauragės Apskritis'),
(482,	'LT',	'LT-TE',	'Telšių Apskritis'),
(483,	'LT',	'LT-UT',	'Utenos Apskritis'),
(484,	'LT',	'LT-VL',	'Vilniaus Apskritis');

DROP TABLE IF EXISTS `directory_country_region_name`;
CREATE TABLE `directory_country_region_name` (
  `locale` varchar(8) NOT NULL DEFAULT '' COMMENT 'Locale',
  `region_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Region Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Region Name',
  PRIMARY KEY (`locale`,`region_id`),
  KEY `IDX_DIRECTORY_COUNTRY_REGION_NAME_REGION_ID` (`region_id`),
  CONSTRAINT `FK_D7CFDEB379F775328EB6F62695E2B3E1` FOREIGN KEY (`region_id`) REFERENCES `directory_country_region` (`region_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Country Region Name';

INSERT INTO `directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
('en_US',	1,	'Alabama'),
('en_US',	2,	'Alaska'),
('en_US',	3,	'American Samoa'),
('en_US',	4,	'Arizona'),
('en_US',	5,	'Arkansas'),
('en_US',	6,	'Armed Forces Africa'),
('en_US',	7,	'Armed Forces Americas'),
('en_US',	8,	'Armed Forces Canada'),
('en_US',	9,	'Armed Forces Europe'),
('en_US',	10,	'Armed Forces Middle East'),
('en_US',	11,	'Armed Forces Pacific'),
('en_US',	12,	'California'),
('en_US',	13,	'Colorado'),
('en_US',	14,	'Connecticut'),
('en_US',	15,	'Delaware'),
('en_US',	16,	'District of Columbia'),
('en_US',	17,	'Federated States Of Micronesia'),
('en_US',	18,	'Florida'),
('en_US',	19,	'Georgia'),
('en_US',	20,	'Guam'),
('en_US',	21,	'Hawaii'),
('en_US',	22,	'Idaho'),
('en_US',	23,	'Illinois'),
('en_US',	24,	'Indiana'),
('en_US',	25,	'Iowa'),
('en_US',	26,	'Kansas'),
('en_US',	27,	'Kentucky'),
('en_US',	28,	'Louisiana'),
('en_US',	29,	'Maine'),
('en_US',	30,	'Marshall Islands'),
('en_US',	31,	'Maryland'),
('en_US',	32,	'Massachusetts'),
('en_US',	33,	'Michigan'),
('en_US',	34,	'Minnesota'),
('en_US',	35,	'Mississippi'),
('en_US',	36,	'Missouri'),
('en_US',	37,	'Montana'),
('en_US',	38,	'Nebraska'),
('en_US',	39,	'Nevada'),
('en_US',	40,	'New Hampshire'),
('en_US',	41,	'New Jersey'),
('en_US',	42,	'New Mexico'),
('en_US',	43,	'New York'),
('en_US',	44,	'North Carolina'),
('en_US',	45,	'North Dakota'),
('en_US',	46,	'Northern Mariana Islands'),
('en_US',	47,	'Ohio'),
('en_US',	48,	'Oklahoma'),
('en_US',	49,	'Oregon'),
('en_US',	50,	'Palau'),
('en_US',	51,	'Pennsylvania'),
('en_US',	52,	'Puerto Rico'),
('en_US',	53,	'Rhode Island'),
('en_US',	54,	'South Carolina'),
('en_US',	55,	'South Dakota'),
('en_US',	56,	'Tennessee'),
('en_US',	57,	'Texas'),
('en_US',	58,	'Utah'),
('en_US',	59,	'Vermont'),
('en_US',	60,	'Virgin Islands'),
('en_US',	61,	'Virginia'),
('en_US',	62,	'Washington'),
('en_US',	63,	'West Virginia'),
('en_US',	64,	'Wisconsin'),
('en_US',	65,	'Wyoming'),
('en_US',	66,	'Alberta'),
('en_US',	67,	'British Columbia'),
('en_US',	68,	'Manitoba'),
('en_US',	69,	'Newfoundland and Labrador'),
('en_US',	70,	'New Brunswick'),
('en_US',	71,	'Nova Scotia'),
('en_US',	72,	'Northwest Territories'),
('en_US',	73,	'Nunavut'),
('en_US',	74,	'Ontario'),
('en_US',	75,	'Prince Edward Island'),
('en_US',	76,	'Quebec'),
('en_US',	77,	'Saskatchewan'),
('en_US',	78,	'Yukon Territory'),
('en_US',	79,	'Niedersachsen'),
('en_US',	80,	'Baden-Württemberg'),
('en_US',	81,	'Bayern'),
('en_US',	82,	'Berlin'),
('en_US',	83,	'Brandenburg'),
('en_US',	84,	'Bremen'),
('en_US',	85,	'Hamburg'),
('en_US',	86,	'Hessen'),
('en_US',	87,	'Mecklenburg-Vorpommern'),
('en_US',	88,	'Nordrhein-Westfalen'),
('en_US',	89,	'Rheinland-Pfalz'),
('en_US',	90,	'Saarland'),
('en_US',	91,	'Sachsen'),
('en_US',	92,	'Sachsen-Anhalt'),
('en_US',	93,	'Schleswig-Holstein'),
('en_US',	94,	'Thüringen'),
('en_US',	95,	'Wien'),
('en_US',	96,	'Niederösterreich'),
('en_US',	97,	'Oberösterreich'),
('en_US',	98,	'Salzburg'),
('en_US',	99,	'Kärnten'),
('en_US',	100,	'Steiermark'),
('en_US',	101,	'Tirol'),
('en_US',	102,	'Burgenland'),
('en_US',	103,	'Vorarlberg'),
('en_US',	104,	'Aargau'),
('en_US',	105,	'Appenzell Innerrhoden'),
('en_US',	106,	'Appenzell Ausserrhoden'),
('en_US',	107,	'Bern'),
('en_US',	108,	'Basel-Landschaft'),
('en_US',	109,	'Basel-Stadt'),
('en_US',	110,	'Freiburg'),
('en_US',	111,	'Genf'),
('en_US',	112,	'Glarus'),
('en_US',	113,	'Graubünden'),
('en_US',	114,	'Jura'),
('en_US',	115,	'Luzern'),
('en_US',	116,	'Neuenburg'),
('en_US',	117,	'Nidwalden'),
('en_US',	118,	'Obwalden'),
('en_US',	119,	'St. Gallen'),
('en_US',	120,	'Schaffhausen'),
('en_US',	121,	'Solothurn'),
('en_US',	122,	'Schwyz'),
('en_US',	123,	'Thurgau'),
('en_US',	124,	'Tessin'),
('en_US',	125,	'Uri'),
('en_US',	126,	'Waadt'),
('en_US',	127,	'Wallis'),
('en_US',	128,	'Zug'),
('en_US',	129,	'Zürich'),
('en_US',	130,	'A Coruña'),
('en_US',	131,	'Alava'),
('en_US',	132,	'Albacete'),
('en_US',	133,	'Alicante'),
('en_US',	134,	'Almeria'),
('en_US',	135,	'Asturias'),
('en_US',	136,	'Avila'),
('en_US',	137,	'Badajoz'),
('en_US',	138,	'Baleares'),
('en_US',	139,	'Barcelona'),
('en_US',	140,	'Burgos'),
('en_US',	141,	'Caceres'),
('en_US',	142,	'Cadiz'),
('en_US',	143,	'Cantabria'),
('en_US',	144,	'Castellon'),
('en_US',	145,	'Ceuta'),
('en_US',	146,	'Ciudad Real'),
('en_US',	147,	'Cordoba'),
('en_US',	148,	'Cuenca'),
('en_US',	149,	'Girona'),
('en_US',	150,	'Granada'),
('en_US',	151,	'Guadalajara'),
('en_US',	152,	'Guipuzcoa'),
('en_US',	153,	'Huelva'),
('en_US',	154,	'Huesca'),
('en_US',	155,	'Jaen'),
('en_US',	156,	'La Rioja'),
('en_US',	157,	'Las Palmas'),
('en_US',	158,	'Leon'),
('en_US',	159,	'Lleida'),
('en_US',	160,	'Lugo'),
('en_US',	161,	'Madrid'),
('en_US',	162,	'Malaga'),
('en_US',	163,	'Melilla'),
('en_US',	164,	'Murcia'),
('en_US',	165,	'Navarra'),
('en_US',	166,	'Ourense'),
('en_US',	167,	'Palencia'),
('en_US',	168,	'Pontevedra'),
('en_US',	169,	'Salamanca'),
('en_US',	170,	'Santa Cruz de Tenerife'),
('en_US',	171,	'Segovia'),
('en_US',	172,	'Sevilla'),
('en_US',	173,	'Soria'),
('en_US',	174,	'Tarragona'),
('en_US',	175,	'Teruel'),
('en_US',	176,	'Toledo'),
('en_US',	177,	'Valencia'),
('en_US',	178,	'Valladolid'),
('en_US',	179,	'Vizcaya'),
('en_US',	180,	'Zamora'),
('en_US',	181,	'Zaragoza'),
('en_US',	182,	'Ain'),
('en_US',	183,	'Aisne'),
('en_US',	184,	'Allier'),
('en_US',	185,	'Alpes-de-Haute-Provence'),
('en_US',	186,	'Hautes-Alpes'),
('en_US',	187,	'Alpes-Maritimes'),
('en_US',	188,	'Ardèche'),
('en_US',	189,	'Ardennes'),
('en_US',	190,	'Ariège'),
('en_US',	191,	'Aube'),
('en_US',	192,	'Aude'),
('en_US',	193,	'Aveyron'),
('en_US',	194,	'Bouches-du-Rhône'),
('en_US',	195,	'Calvados'),
('en_US',	196,	'Cantal'),
('en_US',	197,	'Charente'),
('en_US',	198,	'Charente-Maritime'),
('en_US',	199,	'Cher'),
('en_US',	200,	'Corrèze'),
('en_US',	201,	'Corse-du-Sud'),
('en_US',	202,	'Haute-Corse'),
('en_US',	203,	'Côte-d\'Or'),
('en_US',	204,	'Côtes-d\'Armor'),
('en_US',	205,	'Creuse'),
('en_US',	206,	'Dordogne'),
('en_US',	207,	'Doubs'),
('en_US',	208,	'Drôme'),
('en_US',	209,	'Eure'),
('en_US',	210,	'Eure-et-Loir'),
('en_US',	211,	'Finistère'),
('en_US',	212,	'Gard'),
('en_US',	213,	'Haute-Garonne'),
('en_US',	214,	'Gers'),
('en_US',	215,	'Gironde'),
('en_US',	216,	'Hérault'),
('en_US',	217,	'Ille-et-Vilaine'),
('en_US',	218,	'Indre'),
('en_US',	219,	'Indre-et-Loire'),
('en_US',	220,	'Isère'),
('en_US',	221,	'Jura'),
('en_US',	222,	'Landes'),
('en_US',	223,	'Loir-et-Cher'),
('en_US',	224,	'Loire'),
('en_US',	225,	'Haute-Loire'),
('en_US',	226,	'Loire-Atlantique'),
('en_US',	227,	'Loiret'),
('en_US',	228,	'Lot'),
('en_US',	229,	'Lot-et-Garonne'),
('en_US',	230,	'Lozère'),
('en_US',	231,	'Maine-et-Loire'),
('en_US',	232,	'Manche'),
('en_US',	233,	'Marne'),
('en_US',	234,	'Haute-Marne'),
('en_US',	235,	'Mayenne'),
('en_US',	236,	'Meurthe-et-Moselle'),
('en_US',	237,	'Meuse'),
('en_US',	238,	'Morbihan'),
('en_US',	239,	'Moselle'),
('en_US',	240,	'Nièvre'),
('en_US',	241,	'Nord'),
('en_US',	242,	'Oise'),
('en_US',	243,	'Orne'),
('en_US',	244,	'Pas-de-Calais'),
('en_US',	245,	'Puy-de-Dôme'),
('en_US',	246,	'Pyrénées-Atlantiques'),
('en_US',	247,	'Hautes-Pyrénées'),
('en_US',	248,	'Pyrénées-Orientales'),
('en_US',	249,	'Bas-Rhin'),
('en_US',	250,	'Haut-Rhin'),
('en_US',	251,	'Rhône'),
('en_US',	252,	'Haute-Saône'),
('en_US',	253,	'Saône-et-Loire'),
('en_US',	254,	'Sarthe'),
('en_US',	255,	'Savoie'),
('en_US',	256,	'Haute-Savoie'),
('en_US',	257,	'Paris'),
('en_US',	258,	'Seine-Maritime'),
('en_US',	259,	'Seine-et-Marne'),
('en_US',	260,	'Yvelines'),
('en_US',	261,	'Deux-Sèvres'),
('en_US',	262,	'Somme'),
('en_US',	263,	'Tarn'),
('en_US',	264,	'Tarn-et-Garonne'),
('en_US',	265,	'Var'),
('en_US',	266,	'Vaucluse'),
('en_US',	267,	'Vendée'),
('en_US',	268,	'Vienne'),
('en_US',	269,	'Haute-Vienne'),
('en_US',	270,	'Vosges'),
('en_US',	271,	'Yonne'),
('en_US',	272,	'Territoire-de-Belfort'),
('en_US',	273,	'Essonne'),
('en_US',	274,	'Hauts-de-Seine'),
('en_US',	275,	'Seine-Saint-Denis'),
('en_US',	276,	'Val-de-Marne'),
('en_US',	277,	'Val-d\'Oise'),
('en_US',	278,	'Alba'),
('en_US',	279,	'Arad'),
('en_US',	280,	'Argeş'),
('en_US',	281,	'Bacău'),
('en_US',	282,	'Bihor'),
('en_US',	283,	'Bistriţa-Năsăud'),
('en_US',	284,	'Botoşani'),
('en_US',	285,	'Braşov'),
('en_US',	286,	'Brăila'),
('en_US',	287,	'Bucureşti'),
('en_US',	288,	'Buzău'),
('en_US',	289,	'Caraş-Severin'),
('en_US',	290,	'Călăraşi'),
('en_US',	291,	'Cluj'),
('en_US',	292,	'Constanţa'),
('en_US',	293,	'Covasna'),
('en_US',	294,	'Dâmboviţa'),
('en_US',	295,	'Dolj'),
('en_US',	296,	'Galaţi'),
('en_US',	297,	'Giurgiu'),
('en_US',	298,	'Gorj'),
('en_US',	299,	'Harghita'),
('en_US',	300,	'Hunedoara'),
('en_US',	301,	'Ialomiţa'),
('en_US',	302,	'Iaşi'),
('en_US',	303,	'Ilfov'),
('en_US',	304,	'Maramureş'),
('en_US',	305,	'Mehedinţi'),
('en_US',	306,	'Mureş'),
('en_US',	307,	'Neamţ'),
('en_US',	308,	'Olt'),
('en_US',	309,	'Prahova'),
('en_US',	310,	'Satu-Mare'),
('en_US',	311,	'Sălaj'),
('en_US',	312,	'Sibiu'),
('en_US',	313,	'Suceava'),
('en_US',	314,	'Teleorman'),
('en_US',	315,	'Timiş'),
('en_US',	316,	'Tulcea'),
('en_US',	317,	'Vaslui'),
('en_US',	318,	'Vâlcea'),
('en_US',	319,	'Vrancea'),
('en_US',	320,	'Lappi'),
('en_US',	321,	'Pohjois-Pohjanmaa'),
('en_US',	322,	'Kainuu'),
('en_US',	323,	'Pohjois-Karjala'),
('en_US',	324,	'Pohjois-Savo'),
('en_US',	325,	'Etelä-Savo'),
('en_US',	326,	'Etelä-Pohjanmaa'),
('en_US',	327,	'Pohjanmaa'),
('en_US',	328,	'Pirkanmaa'),
('en_US',	329,	'Satakunta'),
('en_US',	330,	'Keski-Pohjanmaa'),
('en_US',	331,	'Keski-Suomi'),
('en_US',	332,	'Varsinais-Suomi'),
('en_US',	333,	'Etelä-Karjala'),
('en_US',	334,	'Päijät-Häme'),
('en_US',	335,	'Kanta-Häme'),
('en_US',	336,	'Uusimaa'),
('en_US',	337,	'Itä-Uusimaa'),
('en_US',	338,	'Kymenlaakso'),
('en_US',	339,	'Ahvenanmaa'),
('en_US',	340,	'Harjumaa'),
('en_US',	341,	'Hiiumaa'),
('en_US',	342,	'Ida-Virumaa'),
('en_US',	343,	'Jõgevamaa'),
('en_US',	344,	'Järvamaa'),
('en_US',	345,	'Läänemaa'),
('en_US',	346,	'Lääne-Virumaa'),
('en_US',	347,	'Põlvamaa'),
('en_US',	348,	'Pärnumaa'),
('en_US',	349,	'Raplamaa'),
('en_US',	350,	'Saaremaa'),
('en_US',	351,	'Tartumaa'),
('en_US',	352,	'Valgamaa'),
('en_US',	353,	'Viljandimaa'),
('en_US',	354,	'Võrumaa'),
('en_US',	355,	'Daugavpils'),
('en_US',	356,	'Jelgava'),
('en_US',	357,	'Jēkabpils'),
('en_US',	358,	'Jūrmala'),
('en_US',	359,	'Liepāja'),
('en_US',	360,	'Liepājas novads'),
('en_US',	361,	'Rēzekne'),
('en_US',	362,	'Rīga'),
('en_US',	363,	'Rīgas novads'),
('en_US',	364,	'Valmiera'),
('en_US',	365,	'Ventspils'),
('en_US',	366,	'Aglonas novads'),
('en_US',	367,	'Aizkraukles novads'),
('en_US',	368,	'Aizputes novads'),
('en_US',	369,	'Aknīstes novads'),
('en_US',	370,	'Alojas novads'),
('en_US',	371,	'Alsungas novads'),
('en_US',	372,	'Alūksnes novads'),
('en_US',	373,	'Amatas novads'),
('en_US',	374,	'Apes novads'),
('en_US',	375,	'Auces novads'),
('en_US',	376,	'Babītes novads'),
('en_US',	377,	'Baldones novads'),
('en_US',	378,	'Baltinavas novads'),
('en_US',	379,	'Balvu novads'),
('en_US',	380,	'Bauskas novads'),
('en_US',	381,	'Beverīnas novads'),
('en_US',	382,	'Brocēnu novads'),
('en_US',	383,	'Burtnieku novads'),
('en_US',	384,	'Carnikavas novads'),
('en_US',	385,	'Cesvaines novads'),
('en_US',	386,	'Ciblas novads'),
('en_US',	387,	'Cēsu novads'),
('en_US',	388,	'Dagdas novads'),
('en_US',	389,	'Daugavpils novads'),
('en_US',	390,	'Dobeles novads'),
('en_US',	391,	'Dundagas novads'),
('en_US',	392,	'Durbes novads'),
('en_US',	393,	'Engures novads'),
('en_US',	394,	'Garkalnes novads'),
('en_US',	395,	'Grobiņas novads'),
('en_US',	396,	'Gulbenes novads'),
('en_US',	397,	'Iecavas novads'),
('en_US',	398,	'Ikšķiles novads'),
('en_US',	399,	'Ilūkstes novads'),
('en_US',	400,	'Inčukalna novads'),
('en_US',	401,	'Jaunjelgavas novads'),
('en_US',	402,	'Jaunpiebalgas novads'),
('en_US',	403,	'Jaunpils novads'),
('en_US',	404,	'Jelgavas novads'),
('en_US',	405,	'Jēkabpils novads'),
('en_US',	406,	'Kandavas novads'),
('en_US',	407,	'Kokneses novads'),
('en_US',	408,	'Krimuldas novads'),
('en_US',	409,	'Krustpils novads'),
('en_US',	410,	'Krāslavas novads'),
('en_US',	411,	'Kuldīgas novads'),
('en_US',	412,	'Kārsavas novads'),
('en_US',	413,	'Lielvārdes novads'),
('en_US',	414,	'Limbažu novads'),
('en_US',	415,	'Lubānas novads'),
('en_US',	416,	'Ludzas novads'),
('en_US',	417,	'Līgatnes novads'),
('en_US',	418,	'Līvānu novads'),
('en_US',	419,	'Madonas novads'),
('en_US',	420,	'Mazsalacas novads'),
('en_US',	421,	'Mālpils novads'),
('en_US',	422,	'Mārupes novads'),
('en_US',	423,	'Naukšēnu novads'),
('en_US',	424,	'Neretas novads'),
('en_US',	425,	'Nīcas novads'),
('en_US',	426,	'Ogres novads'),
('en_US',	427,	'Olaines novads'),
('en_US',	428,	'Ozolnieku novads'),
('en_US',	429,	'Preiļu novads'),
('en_US',	430,	'Priekules novads'),
('en_US',	431,	'Priekuļu novads'),
('en_US',	432,	'Pārgaujas novads'),
('en_US',	433,	'Pāvilostas novads'),
('en_US',	434,	'Pļaviņu novads'),
('en_US',	435,	'Raunas novads'),
('en_US',	436,	'Riebiņu novads'),
('en_US',	437,	'Rojas novads'),
('en_US',	438,	'Ropažu novads'),
('en_US',	439,	'Rucavas novads'),
('en_US',	440,	'Rugāju novads'),
('en_US',	441,	'Rundāles novads'),
('en_US',	442,	'Rēzeknes novads'),
('en_US',	443,	'Rūjienas novads'),
('en_US',	444,	'Salacgrīvas novads'),
('en_US',	445,	'Salas novads'),
('en_US',	446,	'Salaspils novads'),
('en_US',	447,	'Saldus novads'),
('en_US',	448,	'Saulkrastu novads'),
('en_US',	449,	'Siguldas novads'),
('en_US',	450,	'Skrundas novads'),
('en_US',	451,	'Skrīveru novads'),
('en_US',	452,	'Smiltenes novads'),
('en_US',	453,	'Stopiņu novads'),
('en_US',	454,	'Strenču novads'),
('en_US',	455,	'Sējas novads'),
('en_US',	456,	'Talsu novads'),
('en_US',	457,	'Tukuma novads'),
('en_US',	458,	'Tērvetes novads'),
('en_US',	459,	'Vaiņodes novads'),
('en_US',	460,	'Valkas novads'),
('en_US',	461,	'Valmieras novads'),
('en_US',	462,	'Varakļānu novads'),
('en_US',	463,	'Vecpiebalgas novads'),
('en_US',	464,	'Vecumnieku novads'),
('en_US',	465,	'Ventspils novads'),
('en_US',	466,	'Viesītes novads'),
('en_US',	467,	'Viļakas novads'),
('en_US',	468,	'Viļānu novads'),
('en_US',	469,	'Vārkavas novads'),
('en_US',	470,	'Zilupes novads'),
('en_US',	471,	'Ādažu novads'),
('en_US',	472,	'Ērgļu novads'),
('en_US',	473,	'Ķeguma novads'),
('en_US',	474,	'Ķekavas novads'),
('en_US',	475,	'Alytaus Apskritis'),
('en_US',	476,	'Kauno Apskritis'),
('en_US',	477,	'Klaipėdos Apskritis'),
('en_US',	478,	'Marijampolės Apskritis'),
('en_US',	479,	'Panevėžio Apskritis'),
('en_US',	480,	'Šiaulių Apskritis'),
('en_US',	481,	'Tauragės Apskritis'),
('en_US',	482,	'Telšių Apskritis'),
('en_US',	483,	'Utenos Apskritis'),
('en_US',	484,	'Vilniaus Apskritis');

DROP TABLE IF EXISTS `directory_currency_rate`;
CREATE TABLE `directory_currency_rate` (
  `currency_from` varchar(3) NOT NULL DEFAULT '' COMMENT 'Currency Code Convert From',
  `currency_to` varchar(3) NOT NULL DEFAULT '' COMMENT 'Currency Code Convert To',
  `rate` decimal(24,12) NOT NULL DEFAULT '0.000000000000' COMMENT 'Currency Conversion Rate',
  PRIMARY KEY (`currency_from`,`currency_to`),
  KEY `IDX_DIRECTORY_CURRENCY_RATE_CURRENCY_TO` (`currency_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Directory Currency Rate';

INSERT INTO `directory_currency_rate` (`currency_from`, `currency_to`, `rate`) VALUES
('EUR',	'EUR',	1.000000000000),
('EUR',	'USD',	1.415000000000),
('USD',	'EUR',	0.706700000000),
('USD',	'USD',	1.000000000000);

DROP TABLE IF EXISTS `downloadable_link`;
CREATE TABLE `downloadable_link` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order',
  `number_of_downloads` int(11) DEFAULT NULL COMMENT 'Number of downloads',
  `is_shareable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Shareable flag',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'Link Url',
  `link_file` varchar(255) DEFAULT NULL COMMENT 'Link File',
  `link_type` varchar(20) DEFAULT NULL COMMENT 'Link Type',
  `sample_url` varchar(255) DEFAULT NULL COMMENT 'Sample Url',
  `sample_file` varchar(255) DEFAULT NULL COMMENT 'Sample File',
  `sample_type` varchar(20) DEFAULT NULL COMMENT 'Sample Type',
  PRIMARY KEY (`link_id`),
  KEY `IDX_DOWNLOADABLE_LINK_PRODUCT_ID` (`product_id`),
  KEY `IDX_DOWNLOADABLE_LINK_PRODUCT_ID_SORT_ORDER` (`product_id`,`sort_order`),
  CONSTRAINT `FK_DOWNLOADABLE_LINK_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Table';


DROP TABLE IF EXISTS `downloadable_link_price`;
CREATE TABLE `downloadable_link_price` (
  `price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Price ID',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  PRIMARY KEY (`price_id`),
  KEY `IDX_DOWNLOADABLE_LINK_PRICE_LINK_ID` (`link_id`),
  KEY `IDX_DOWNLOADABLE_LINK_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_DOWNLOADABLE_LINK_PRICE_LINK_ID_DOWNLOADABLE_LINK_LINK_ID` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DOWNLOADABLE_LINK_PRICE_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Price Table';


DROP TABLE IF EXISTS `downloadable_link_purchased`;
CREATE TABLE `downloadable_link_purchased` (
  `purchased_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Purchased ID',
  `order_id` int(10) unsigned DEFAULT '0' COMMENT 'Order ID',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment ID',
  `order_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Item ID',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of creation',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of modification',
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer ID',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product name',
  `product_sku` varchar(255) DEFAULT NULL COMMENT 'Product sku',
  `link_section_title` varchar(255) DEFAULT NULL COMMENT 'Link_section_title',
  PRIMARY KEY (`purchased_id`),
  KEY `IDX_DOWNLOADABLE_LINK_PURCHASED_ORDER_ID` (`order_id`),
  KEY `IDX_DOWNLOADABLE_LINK_PURCHASED_ORDER_ITEM_ID` (`order_item_id`),
  KEY `IDX_DOWNLOADABLE_LINK_PURCHASED_CUSTOMER_ID` (`customer_id`),
  CONSTRAINT `FK_DL_LNK_PURCHASED_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_DL_LNK_PURCHASED_ORDER_ID_SALES_FLAT_ORDER_ENTT_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Purchased Table';


DROP TABLE IF EXISTS `downloadable_link_purchased_item`;
CREATE TABLE `downloadable_link_purchased_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item ID',
  `purchased_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Purchased ID',
  `order_item_id` int(10) unsigned DEFAULT '0' COMMENT 'Order Item ID',
  `product_id` int(10) unsigned DEFAULT '0' COMMENT 'Product ID',
  `link_hash` varchar(255) DEFAULT NULL COMMENT 'Link hash',
  `number_of_downloads_bought` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of downloads bought',
  `number_of_downloads_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of downloads used',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `link_title` varchar(255) DEFAULT NULL COMMENT 'Link Title',
  `is_shareable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Shareable Flag',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'Link Url',
  `link_file` varchar(255) DEFAULT NULL COMMENT 'Link File',
  `link_type` varchar(255) DEFAULT NULL COMMENT 'Link Type',
  `status` varchar(50) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Creation Time',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Update Time',
  PRIMARY KEY (`item_id`),
  KEY `IDX_DOWNLOADABLE_LINK_PURCHASED_ITEM_LINK_HASH` (`link_hash`),
  KEY `IDX_DOWNLOADABLE_LINK_PURCHASED_ITEM_ORDER_ITEM_ID` (`order_item_id`),
  KEY `IDX_DOWNLOADABLE_LINK_PURCHASED_ITEM_PURCHASED_ID` (`purchased_id`),
  CONSTRAINT `FK_46CC8E252307CE62F00A8F1887512A39` FOREIGN KEY (`purchased_id`) REFERENCES `downloadable_link_purchased` (`purchased_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_B219BF25756700DEE44550B21220ECCE` FOREIGN KEY (`order_item_id`) REFERENCES `sales_flat_order_item` (`item_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Link Purchased Item Table';


DROP TABLE IF EXISTS `downloadable_link_title`;
CREATE TABLE `downloadable_link_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Title ID',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Link ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `UNQ_DOWNLOADABLE_LINK_TITLE_LINK_ID_STORE_ID` (`link_id`,`store_id`),
  KEY `IDX_DOWNLOADABLE_LINK_TITLE_LINK_ID` (`link_id`),
  KEY `IDX_DOWNLOADABLE_LINK_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_DOWNLOADABLE_LINK_TITLE_LINK_ID_DOWNLOADABLE_LINK_LINK_ID` FOREIGN KEY (`link_id`) REFERENCES `downloadable_link` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DOWNLOADABLE_LINK_TITLE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link Title Table';


DROP TABLE IF EXISTS `downloadable_sample`;
CREATE TABLE `downloadable_sample` (
  `sample_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Sample ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `sample_url` varchar(255) DEFAULT NULL COMMENT 'Sample URL',
  `sample_file` varchar(255) DEFAULT NULL COMMENT 'Sample file',
  `sample_type` varchar(20) DEFAULT NULL COMMENT 'Sample Type',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`sample_id`),
  KEY `IDX_DOWNLOADABLE_SAMPLE_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_DL_SAMPLE_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Sample Table';


DROP TABLE IF EXISTS `downloadable_sample_title`;
CREATE TABLE `downloadable_sample_title` (
  `title_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Title ID',
  `sample_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sample ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`title_id`),
  UNIQUE KEY `UNQ_DOWNLOADABLE_SAMPLE_TITLE_SAMPLE_ID_STORE_ID` (`sample_id`,`store_id`),
  KEY `IDX_DOWNLOADABLE_SAMPLE_TITLE_SAMPLE_ID` (`sample_id`),
  KEY `IDX_DOWNLOADABLE_SAMPLE_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_DL_SAMPLE_TTL_SAMPLE_ID_DL_SAMPLE_SAMPLE_ID` FOREIGN KEY (`sample_id`) REFERENCES `downloadable_sample` (`sample_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DOWNLOADABLE_SAMPLE_TITLE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Downloadable Sample Title Table';


DROP TABLE IF EXISTS `eav_attribute`;
CREATE TABLE `eav_attribute` (
  `attribute_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_code` varchar(255) DEFAULT NULL COMMENT 'Attribute Code',
  `attribute_model` varchar(255) DEFAULT NULL COMMENT 'Attribute Model',
  `backend_model` varchar(255) DEFAULT NULL COMMENT 'Backend Model',
  `backend_type` varchar(8) NOT NULL DEFAULT 'static' COMMENT 'Backend Type',
  `backend_table` varchar(255) DEFAULT NULL COMMENT 'Backend Table',
  `frontend_model` varchar(255) DEFAULT NULL COMMENT 'Frontend Model',
  `frontend_input` varchar(50) DEFAULT NULL COMMENT 'Frontend Input',
  `frontend_label` varchar(255) DEFAULT NULL COMMENT 'Frontend Label',
  `frontend_class` varchar(255) DEFAULT NULL COMMENT 'Frontend Class',
  `source_model` varchar(255) DEFAULT NULL COMMENT 'Source Model',
  `is_required` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is Required',
  `is_user_defined` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is User Defined',
  `default_value` text COMMENT 'Default Value',
  `is_unique` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Defines Is Unique',
  `note` varchar(255) DEFAULT NULL COMMENT 'Note',
  PRIMARY KEY (`attribute_id`),
  UNIQUE KEY `UNQ_EAV_ATTRIBUTE_ENTITY_TYPE_ID_ATTRIBUTE_CODE` (`entity_type_id`,`attribute_code`),
  KEY `IDX_EAV_ATTRIBUTE_ENTITY_TYPE_ID` (`entity_type_id`),
  CONSTRAINT `FK_EAV_ATTRIBUTE_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Attribute';

INSERT INTO `eav_attribute` (`attribute_id`, `entity_type_id`, `attribute_code`, `attribute_model`, `backend_model`, `backend_type`, `backend_table`, `frontend_model`, `frontend_input`, `frontend_label`, `frontend_class`, `source_model`, `is_required`, `is_user_defined`, `default_value`, `is_unique`, `note`) VALUES
(1,	1,	'website_id',	NULL,	'customer/customer_attribute_backend_website',	'static',	NULL,	NULL,	'select',	'Associate to Website',	NULL,	'customer/customer_attribute_source_website',	1,	0,	NULL,	0,	NULL),
(2,	1,	'store_id',	NULL,	'customer/customer_attribute_backend_store',	'static',	NULL,	NULL,	'select',	'Create In',	NULL,	'customer/customer_attribute_source_store',	1,	0,	NULL,	0,	NULL),
(3,	1,	'created_in',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Created From',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(4,	1,	'prefix',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Prefix',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(5,	1,	'firstname',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'First Name',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(6,	1,	'middlename',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Middle Name/Initial',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(7,	1,	'lastname',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Last Name',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(8,	1,	'suffix',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Suffix',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(9,	1,	'email',	NULL,	NULL,	'static',	NULL,	NULL,	'text',	'Email',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(10,	1,	'group_id',	NULL,	NULL,	'static',	NULL,	NULL,	'select',	'Group',	NULL,	'customer/customer_attribute_source_group',	1,	0,	NULL,	0,	NULL),
(11,	1,	'dob',	NULL,	'eav/entity_attribute_backend_datetime',	'datetime',	NULL,	'eav/entity_attribute_frontend_datetime',	'date',	'Date Of Birth',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(12,	1,	'password_hash',	NULL,	'customer/customer_attribute_backend_password',	'varchar',	NULL,	NULL,	'hidden',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(13,	1,	'default_billing',	NULL,	'customer/customer_attribute_backend_billing',	'int',	NULL,	NULL,	'text',	'Default Billing Address',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(14,	1,	'default_shipping',	NULL,	'customer/customer_attribute_backend_shipping',	'int',	NULL,	NULL,	'text',	'Default Shipping Address',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(15,	1,	'taxvat',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Tax/VAT Number',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(16,	1,	'confirmation',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Is Confirmed',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(17,	1,	'created_at',	NULL,	NULL,	'static',	NULL,	NULL,	'datetime',	'Created At',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(18,	1,	'gender',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Gender',	NULL,	'eav/entity_attribute_source_table',	0,	0,	NULL,	0,	NULL),
(19,	2,	'prefix',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Prefix',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(20,	2,	'firstname',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'First Name',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(21,	2,	'middlename',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Middle Name/Initial',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(22,	2,	'lastname',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Last Name',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(23,	2,	'suffix',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Suffix',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(24,	2,	'company',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Company',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(25,	2,	'street',	NULL,	'customer/entity_address_attribute_backend_street',	'text',	NULL,	NULL,	'multiline',	'Street Address',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(26,	2,	'city',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'City',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(27,	2,	'country_id',	NULL,	NULL,	'varchar',	NULL,	NULL,	'select',	'Country',	NULL,	'customer/entity_address_attribute_source_country',	1,	0,	NULL,	0,	NULL),
(28,	2,	'region',	NULL,	'customer/entity_address_attribute_backend_region',	'varchar',	NULL,	NULL,	'text',	'State/Province',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(29,	2,	'region_id',	NULL,	NULL,	'int',	NULL,	NULL,	'hidden',	'State/Province',	NULL,	'customer/entity_address_attribute_source_region',	0,	0,	NULL,	0,	NULL),
(30,	2,	'postcode',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Zip/Postal Code',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(31,	2,	'telephone',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Telephone',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(32,	2,	'fax',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Fax',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(33,	1,	'rp_token',	NULL,	NULL,	'varchar',	NULL,	NULL,	'hidden',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(34,	1,	'rp_token_created_at',	NULL,	NULL,	'datetime',	NULL,	NULL,	'date',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(35,	1,	'disable_auto_group_change',	NULL,	'customer/attribute_backend_data_boolean',	'static',	NULL,	NULL,	'boolean',	'Disable Automatic Group Change Based on VAT ID',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(36,	2,	'vat_id',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'VAT number',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(37,	2,	'vat_is_valid',	NULL,	NULL,	'int',	NULL,	NULL,	'text',	'VAT number validity',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(38,	2,	'vat_request_id',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'VAT number validation request ID',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(39,	2,	'vat_request_date',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'VAT number validation request date',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(40,	2,	'vat_request_success',	NULL,	NULL,	'int',	NULL,	NULL,	'text',	'VAT number validation request success',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(41,	3,	'name',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Name',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(42,	3,	'is_active',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Is Active',	NULL,	'eav/entity_attribute_source_boolean',	1,	0,	NULL,	0,	NULL),
(43,	3,	'url_key',	NULL,	'catalog/category_attribute_backend_urlkey',	'varchar',	NULL,	NULL,	'text',	'URL Key',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(44,	3,	'description',	NULL,	NULL,	'text',	NULL,	NULL,	'textarea',	'Description',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(45,	3,	'image',	NULL,	'catalog/category_attribute_backend_image',	'varchar',	NULL,	NULL,	'image',	'Image',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(46,	3,	'meta_title',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Page Title',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(47,	3,	'meta_keywords',	NULL,	NULL,	'text',	NULL,	NULL,	'textarea',	'Meta Keywords',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(48,	3,	'meta_description',	NULL,	NULL,	'text',	NULL,	NULL,	'textarea',	'Meta Description',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(49,	3,	'display_mode',	NULL,	NULL,	'varchar',	NULL,	NULL,	'select',	'Display Mode',	NULL,	'catalog/category_attribute_source_mode',	0,	0,	NULL,	0,	NULL),
(50,	3,	'landing_page',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'CMS Block',	NULL,	'catalog/category_attribute_source_page',	0,	0,	NULL,	0,	NULL),
(51,	3,	'is_anchor',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Is Anchor',	NULL,	'eav/entity_attribute_source_boolean',	0,	0,	NULL,	0,	NULL),
(52,	3,	'path',	NULL,	NULL,	'static',	NULL,	NULL,	'text',	'Path',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(53,	3,	'position',	NULL,	NULL,	'static',	NULL,	NULL,	'text',	'Position',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(54,	3,	'all_children',	NULL,	NULL,	'text',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(55,	3,	'path_in_store',	NULL,	NULL,	'text',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(56,	3,	'children',	NULL,	NULL,	'text',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(57,	3,	'url_path',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(58,	3,	'custom_design',	NULL,	NULL,	'varchar',	NULL,	NULL,	'select',	'Custom Design',	NULL,	'core/design_source_design',	0,	0,	NULL,	0,	NULL),
(59,	3,	'custom_design_from',	NULL,	'eav/entity_attribute_backend_datetime',	'datetime',	NULL,	NULL,	'date',	'Active From',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(60,	3,	'custom_design_to',	NULL,	'eav/entity_attribute_backend_datetime',	'datetime',	NULL,	NULL,	'date',	'Active To',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(61,	3,	'page_layout',	NULL,	NULL,	'varchar',	NULL,	NULL,	'select',	'Page Layout',	NULL,	'catalog/category_attribute_source_layout',	0,	0,	NULL,	0,	NULL),
(62,	3,	'custom_layout_update',	NULL,	'catalog/attribute_backend_customlayoutupdate',	'text',	NULL,	NULL,	'textarea',	'Custom Layout Update',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(63,	3,	'level',	NULL,	NULL,	'static',	NULL,	NULL,	'text',	'Level',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(64,	3,	'children_count',	NULL,	NULL,	'static',	NULL,	NULL,	'text',	'Children Count',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(65,	3,	'available_sort_by',	NULL,	'catalog/category_attribute_backend_sortby',	'text',	NULL,	NULL,	'multiselect',	'Available Product Listing Sort By',	NULL,	'catalog/category_attribute_source_sortby',	1,	0,	NULL,	0,	NULL),
(66,	3,	'default_sort_by',	NULL,	'catalog/category_attribute_backend_sortby',	'varchar',	NULL,	NULL,	'select',	'Default Product Listing Sort By',	NULL,	'catalog/category_attribute_source_sortby',	1,	0,	NULL,	0,	NULL),
(67,	3,	'include_in_menu',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Include in Navigation Menu',	NULL,	'eav/entity_attribute_source_boolean',	1,	0,	'1',	0,	NULL),
(68,	3,	'custom_use_parent_settings',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Use Parent Category Settings',	NULL,	'eav/entity_attribute_source_boolean',	0,	0,	NULL,	0,	NULL),
(69,	3,	'custom_apply_to_products',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Apply To Products',	NULL,	'eav/entity_attribute_source_boolean',	0,	0,	NULL,	0,	NULL),
(70,	3,	'filter_price_range',	NULL,	NULL,	'decimal',	NULL,	NULL,	'text',	'Layered Navigation Price Step',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(71,	4,	'name',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Name',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(72,	4,	'description',	NULL,	NULL,	'text',	NULL,	NULL,	'textarea',	'Description',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(73,	4,	'short_description',	NULL,	NULL,	'text',	NULL,	NULL,	'textarea',	'Short Description',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(74,	4,	'sku',	NULL,	'catalog/product_attribute_backend_sku',	'static',	NULL,	NULL,	'text',	'SKU',	NULL,	NULL,	1,	0,	NULL,	1,	NULL),
(75,	4,	'price',	NULL,	'catalog/product_attribute_backend_price',	'decimal',	NULL,	NULL,	'price',	'Price',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(76,	4,	'special_price',	NULL,	'catalog/product_attribute_backend_price',	'decimal',	NULL,	NULL,	'price',	'Special Price',	'validate-special-price',	NULL,	0,	0,	NULL,	0,	NULL),
(77,	4,	'special_from_date',	NULL,	'catalog/product_attribute_backend_startdate_specialprice',	'datetime',	NULL,	NULL,	'date',	'Special Price From Date',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(78,	4,	'special_to_date',	NULL,	'eav/entity_attribute_backend_datetime',	'datetime',	NULL,	NULL,	'date',	'Special Price To Date',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(79,	4,	'cost',	NULL,	'catalog/product_attribute_backend_price',	'decimal',	NULL,	NULL,	'price',	'Cost',	NULL,	NULL,	0,	1,	NULL,	0,	NULL),
(80,	4,	'weight',	NULL,	NULL,	'decimal',	NULL,	NULL,	'weight',	'Weight',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(81,	4,	'manufacturer',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Manufacturer',	NULL,	NULL,	0,	1,	NULL,	0,	NULL),
(82,	4,	'meta_title',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Meta Title',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(83,	4,	'meta_keyword',	NULL,	NULL,	'text',	NULL,	NULL,	'textarea',	'Meta Keywords',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(84,	4,	'meta_description',	NULL,	NULL,	'varchar',	NULL,	NULL,	'textarea',	'Meta Description',	NULL,	NULL,	0,	0,	NULL,	0,	'Maximum 255 chars'),
(85,	4,	'image',	NULL,	NULL,	'varchar',	NULL,	'catalog/product_attribute_frontend_image',	'media_image',	'Base Image',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(86,	4,	'small_image',	NULL,	NULL,	'varchar',	NULL,	'catalog/product_attribute_frontend_image',	'media_image',	'Small Image',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(87,	4,	'thumbnail',	NULL,	NULL,	'varchar',	NULL,	'catalog/product_attribute_frontend_image',	'media_image',	'Thumbnail',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(88,	4,	'media_gallery',	NULL,	'catalog/product_attribute_backend_media',	'varchar',	NULL,	NULL,	'gallery',	'Media Gallery',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(89,	4,	'old_id',	NULL,	NULL,	'int',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(90,	4,	'group_price',	NULL,	'catalog/product_attribute_backend_groupprice',	'decimal',	NULL,	NULL,	'text',	'Group Price',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(91,	4,	'tier_price',	NULL,	'catalog/product_attribute_backend_tierprice',	'decimal',	NULL,	NULL,	'text',	'Tier Price',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(92,	4,	'color',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Color',	NULL,	NULL,	0,	1,	NULL,	0,	NULL),
(93,	4,	'news_from_date',	NULL,	'catalog/product_attribute_backend_startdate',	'datetime',	NULL,	NULL,	'date',	'Set Product as New from Date',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(94,	4,	'news_to_date',	NULL,	'eav/entity_attribute_backend_datetime',	'datetime',	NULL,	NULL,	'date',	'Set Product as New to Date',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(95,	4,	'gallery',	NULL,	NULL,	'varchar',	NULL,	NULL,	'gallery',	'Image Gallery',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(96,	4,	'status',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Status',	NULL,	'catalog/product_status',	1,	0,	NULL,	0,	NULL),
(97,	4,	'url_key',	NULL,	'catalog/product_attribute_backend_urlkey',	'varchar',	NULL,	NULL,	'text',	'URL Key',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(98,	4,	'url_path',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(99,	4,	'minimal_price',	NULL,	NULL,	'decimal',	NULL,	NULL,	'price',	'Minimal Price',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(100,	4,	'is_recurring',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Enable Recurring Profile',	NULL,	'eav/entity_attribute_source_boolean',	0,	0,	NULL,	0,	'Products with recurring profile participate in catalog as nominal items.'),
(101,	4,	'recurring_profile',	NULL,	'catalog/product_attribute_backend_recurring',	'text',	NULL,	NULL,	'text',	'Recurring Payment Profile',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(102,	4,	'visibility',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Visibility',	NULL,	'catalog/product_visibility',	1,	0,	'4',	0,	NULL),
(103,	4,	'custom_design',	NULL,	NULL,	'varchar',	NULL,	NULL,	'select',	'Custom Design',	NULL,	'core/design_source_design',	0,	0,	NULL,	0,	NULL),
(104,	4,	'custom_design_from',	NULL,	'catalog/product_attribute_backend_startdate',	'datetime',	NULL,	NULL,	'date',	'Active From',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(105,	4,	'custom_design_to',	NULL,	'eav/entity_attribute_backend_datetime',	'datetime',	NULL,	NULL,	'date',	'Active To',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(106,	4,	'custom_layout_update',	NULL,	'catalog/attribute_backend_customlayoutupdate',	'text',	NULL,	NULL,	'textarea',	'Custom Layout Update',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(107,	4,	'page_layout',	NULL,	NULL,	'varchar',	NULL,	NULL,	'select',	'Page Layout',	NULL,	'catalog/product_attribute_source_layout',	0,	0,	NULL,	0,	NULL),
(108,	4,	'category_ids',	NULL,	NULL,	'static',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(109,	4,	'options_container',	NULL,	NULL,	'varchar',	NULL,	NULL,	'select',	'Display Product Options In',	NULL,	'catalog/entity_product_attribute_design_options_container',	0,	0,	'container1',	0,	NULL),
(110,	4,	'required_options',	NULL,	NULL,	'static',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(111,	4,	'has_options',	NULL,	NULL,	'static',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(112,	4,	'image_label',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Image Label',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(113,	4,	'small_image_label',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Small Image Label',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(114,	4,	'thumbnail_label',	NULL,	NULL,	'varchar',	NULL,	NULL,	'text',	'Thumbnail Label',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(115,	4,	'created_at',	NULL,	'eav/entity_attribute_backend_time_created',	'static',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(116,	4,	'updated_at',	NULL,	'eav/entity_attribute_backend_time_updated',	'static',	NULL,	NULL,	'text',	NULL,	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(117,	4,	'country_of_manufacture',	NULL,	NULL,	'varchar',	NULL,	NULL,	'select',	'Country of Manufacture',	NULL,	'catalog/product_attribute_source_countryofmanufacture',	0,	0,	NULL,	0,	NULL),
(118,	4,	'msrp_enabled',	NULL,	'catalog/product_attribute_backend_msrp',	'varchar',	NULL,	NULL,	'select',	'Apply MAP',	NULL,	'catalog/product_attribute_source_msrp_type_enabled',	0,	0,	'2',	0,	NULL),
(119,	4,	'msrp_display_actual_price_type',	NULL,	'catalog/product_attribute_backend_boolean',	'varchar',	NULL,	NULL,	'select',	'Display Actual Price',	NULL,	'catalog/product_attribute_source_msrp_type_price',	0,	0,	'4',	0,	NULL),
(120,	4,	'msrp',	NULL,	'catalog/product_attribute_backend_price',	'decimal',	NULL,	NULL,	'price',	'Manufacturer\'s Suggested Retail Price',	NULL,	NULL,	0,	0,	NULL,	0,	NULL),
(121,	4,	'tax_class_id',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Tax Class',	NULL,	'tax/class_source_product',	1,	0,	NULL,	0,	NULL),
(122,	4,	'gift_message_available',	NULL,	'catalog/product_attribute_backend_boolean',	'varchar',	NULL,	NULL,	'select',	'Allow Gift Message',	NULL,	'eav/entity_attribute_source_boolean',	0,	0,	NULL,	0,	NULL),
(123,	4,	'price_type',	NULL,	NULL,	'int',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(124,	4,	'sku_type',	NULL,	NULL,	'int',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(125,	4,	'weight_type',	NULL,	NULL,	'int',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(126,	4,	'price_view',	NULL,	NULL,	'int',	NULL,	NULL,	'select',	'Price View',	NULL,	'bundle/product_attribute_source_price_view',	1,	0,	NULL,	0,	NULL),
(127,	4,	'shipment_type',	NULL,	NULL,	'int',	NULL,	NULL,	NULL,	'Shipment',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(128,	4,	'links_purchased_separately',	NULL,	NULL,	'int',	NULL,	NULL,	NULL,	'Links can be purchased separately',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(129,	4,	'samples_title',	NULL,	NULL,	'varchar',	NULL,	NULL,	NULL,	'Samples title',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(130,	4,	'links_title',	NULL,	NULL,	'varchar',	NULL,	NULL,	NULL,	'Links title',	NULL,	NULL,	1,	0,	NULL,	0,	NULL),
(131,	4,	'links_exist',	NULL,	NULL,	'int',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	0,	'0',	0,	NULL),
(132,	4,	'magikfeatured',	NULL,	NULL,	'int',	NULL,	NULL,	'boolean',	'Featured Product On Home',	NULL,	NULL,	0,	1,	'0',	0,	NULL),
(133,	4,	'magikrecommended',	NULL,	NULL,	'int',	NULL,	NULL,	'boolean',	'Recommended Product On Home',	NULL,	NULL,	0,	1,	'0',	0,	NULL),
(134,	4,	'product_socialbar',	NULL,	NULL,	'int',	NULL,	NULL,	'boolean',	'Disable Social Bar for this product',	NULL,	NULL,	0,	0,	'0',	0,	NULL),
(135,	3,	'menu_icon',	NULL,	'catalog/category_attribute_backend_image',	'varchar',	NULL,	NULL,	'image',	'Menu Icon',	NULL,	NULL,	0,	0,	NULL,	0,	NULL);

DROP TABLE IF EXISTS `eav_attribute_group`;
CREATE TABLE `eav_attribute_group` (
  `attribute_group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Group Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `attribute_group_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Group Name',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Default Id',
  PRIMARY KEY (`attribute_group_id`),
  UNIQUE KEY `UNQ_EAV_ATTRIBUTE_GROUP_ATTRIBUTE_SET_ID_ATTRIBUTE_GROUP_NAME` (`attribute_set_id`,`attribute_group_name`),
  KEY `IDX_EAV_ATTRIBUTE_GROUP_ATTRIBUTE_SET_ID_SORT_ORDER` (`attribute_set_id`,`sort_order`),
  CONSTRAINT `FK_EAV_ATTR_GROUP_ATTR_SET_ID_EAV_ATTR_SET_ATTR_SET_ID` FOREIGN KEY (`attribute_set_id`) REFERENCES `eav_attribute_set` (`attribute_set_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Group';

INSERT INTO `eav_attribute_group` (`attribute_group_id`, `attribute_set_id`, `attribute_group_name`, `sort_order`, `default_id`) VALUES
(1,	1,	'General',	1,	1),
(2,	2,	'General',	1,	1),
(3,	3,	'General',	10,	1),
(4,	3,	'General Information',	2,	0),
(5,	3,	'Display Settings',	20,	0),
(6,	3,	'Custom Design',	30,	0),
(7,	4,	'General',	1,	1),
(8,	4,	'Prices',	2,	0),
(9,	4,	'Meta Information',	3,	0),
(10,	4,	'Images',	4,	0),
(11,	4,	'Recurring Profile',	5,	0),
(12,	4,	'Design',	6,	0),
(13,	5,	'General',	1,	1),
(14,	6,	'General',	1,	1),
(15,	7,	'General',	1,	1),
(16,	8,	'General',	1,	1),
(17,	4,	'Gift Options',	7,	0),
(18,	4,	'Magik Socialbar',	8,	0);

DROP TABLE IF EXISTS `eav_attribute_label`;
CREATE TABLE `eav_attribute_label` (
  `attribute_label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Label Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`attribute_label_id`),
  KEY `IDX_EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_EAV_ATTRIBUTE_LABEL_STORE_ID` (`store_id`),
  KEY `IDX_EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID_STORE_ID` (`attribute_id`,`store_id`),
  CONSTRAINT `FK_EAV_ATTRIBUTE_LABEL_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ATTRIBUTE_LABEL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Label';


DROP TABLE IF EXISTS `eav_attribute_option`;
CREATE TABLE `eav_attribute_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`option_id`),
  KEY `IDX_EAV_ATTRIBUTE_OPTION_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `FK_EAV_ATTRIBUTE_OPTION_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Option';

INSERT INTO `eav_attribute_option` (`option_id`, `attribute_id`, `sort_order`) VALUES
(1,	18,	0),
(2,	18,	1);

DROP TABLE IF EXISTS `eav_attribute_option_value`;
CREATE TABLE `eav_attribute_option_value` (
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  KEY `IDX_EAV_ATTRIBUTE_OPTION_VALUE_OPTION_ID` (`option_id`),
  KEY `IDX_EAV_ATTRIBUTE_OPTION_VALUE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_EAV_ATTRIBUTE_OPTION_VALUE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ATTR_OPT_VAL_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES `eav_attribute_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Option Value';

INSERT INTO `eav_attribute_option_value` (`value_id`, `option_id`, `store_id`, `value`) VALUES
(1,	1,	0,	'Male'),
(2,	2,	0,	'Female');

DROP TABLE IF EXISTS `eav_attribute_set`;
CREATE TABLE `eav_attribute_set` (
  `attribute_set_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Attribute Set Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_name` varchar(255) DEFAULT NULL COMMENT 'Attribute Set Name',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`attribute_set_id`),
  UNIQUE KEY `UNQ_EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_ATTRIBUTE_SET_NAME` (`entity_type_id`,`attribute_set_name`),
  KEY `IDX_EAV_ATTRIBUTE_SET_ENTITY_TYPE_ID_SORT_ORDER` (`entity_type_id`,`sort_order`),
  CONSTRAINT `FK_EAV_ATTR_SET_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Attribute Set';

INSERT INTO `eav_attribute_set` (`attribute_set_id`, `entity_type_id`, `attribute_set_name`, `sort_order`) VALUES
(1,	1,	'Default',	1),
(2,	2,	'Default',	1),
(3,	3,	'Default',	1),
(4,	4,	'Default',	1),
(5,	5,	'Default',	1),
(6,	6,	'Default',	1),
(7,	7,	'Default',	1),
(8,	8,	'Default',	1);

DROP TABLE IF EXISTS `eav_entity`;
CREATE TABLE `eav_entity` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Parent Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Defines Is Entity Active',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_EAV_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_EAV_ENTITY_STORE_ID` (`store_id`),
  CONSTRAINT `FK_EAV_ENTITY_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity';


DROP TABLE IF EXISTS `eav_entity_attribute`;
CREATE TABLE `eav_entity_attribute` (
  `entity_attribute_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Set Id',
  `attribute_group_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`entity_attribute_id`),
  UNIQUE KEY `UNQ_EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_SET_ID_ATTRIBUTE_ID` (`attribute_set_id`,`attribute_id`),
  UNIQUE KEY `UNQ_EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_GROUP_ID_ATTRIBUTE_ID` (`attribute_group_id`,`attribute_id`),
  KEY `IDX_EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_SET_ID_SORT_ORDER` (`attribute_set_id`,`sort_order`),
  KEY `IDX_EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `FK_EAV_ENTITY_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTT_ATTR_ATTR_GROUP_ID_EAV_ATTR_GROUP_ATTR_GROUP_ID` FOREIGN KEY (`attribute_group_id`) REFERENCES `eav_attribute_group` (`attribute_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Attributes';

INSERT INTO `eav_entity_attribute` (`entity_attribute_id`, `entity_type_id`, `attribute_set_id`, `attribute_group_id`, `attribute_id`, `sort_order`) VALUES
(1,	1,	1,	1,	1,	10),
(2,	1,	1,	1,	2,	0),
(3,	1,	1,	1,	3,	20),
(4,	1,	1,	1,	4,	30),
(5,	1,	1,	1,	5,	40),
(6,	1,	1,	1,	6,	50),
(7,	1,	1,	1,	7,	60),
(8,	1,	1,	1,	8,	70),
(9,	1,	1,	1,	9,	80),
(10,	1,	1,	1,	10,	25),
(11,	1,	1,	1,	11,	90),
(12,	1,	1,	1,	12,	0),
(13,	1,	1,	1,	13,	0),
(14,	1,	1,	1,	14,	0),
(15,	1,	1,	1,	15,	100),
(16,	1,	1,	1,	16,	0),
(17,	1,	1,	1,	17,	86),
(18,	1,	1,	1,	18,	110),
(19,	2,	2,	2,	19,	10),
(20,	2,	2,	2,	20,	20),
(21,	2,	2,	2,	21,	30),
(22,	2,	2,	2,	22,	40),
(23,	2,	2,	2,	23,	50),
(24,	2,	2,	2,	24,	60),
(25,	2,	2,	2,	25,	70),
(26,	2,	2,	2,	26,	80),
(27,	2,	2,	2,	27,	90),
(28,	2,	2,	2,	28,	100),
(29,	2,	2,	2,	29,	100),
(30,	2,	2,	2,	30,	110),
(31,	2,	2,	2,	31,	120),
(32,	2,	2,	2,	32,	130),
(33,	1,	1,	1,	33,	111),
(34,	1,	1,	1,	34,	112),
(35,	1,	1,	1,	35,	28),
(36,	2,	2,	2,	36,	140),
(37,	2,	2,	2,	37,	132),
(38,	2,	2,	2,	38,	133),
(39,	2,	2,	2,	39,	134),
(40,	2,	2,	2,	40,	135),
(41,	3,	3,	4,	41,	1),
(42,	3,	3,	4,	42,	2),
(43,	3,	3,	4,	43,	3),
(44,	3,	3,	4,	44,	4),
(45,	3,	3,	4,	45,	5),
(46,	3,	3,	4,	46,	6),
(47,	3,	3,	4,	47,	7),
(48,	3,	3,	4,	48,	8),
(49,	3,	3,	5,	49,	10),
(50,	3,	3,	5,	50,	20),
(51,	3,	3,	5,	51,	30),
(52,	3,	3,	4,	52,	12),
(53,	3,	3,	4,	53,	13),
(54,	3,	3,	4,	54,	14),
(55,	3,	3,	4,	55,	15),
(56,	3,	3,	4,	56,	16),
(57,	3,	3,	4,	57,	17),
(58,	3,	3,	6,	58,	10),
(59,	3,	3,	6,	59,	30),
(60,	3,	3,	6,	60,	40),
(61,	3,	3,	6,	61,	50),
(62,	3,	3,	6,	62,	60),
(63,	3,	3,	4,	63,	24),
(64,	3,	3,	4,	64,	25),
(65,	3,	3,	5,	65,	40),
(66,	3,	3,	5,	66,	50),
(67,	3,	3,	4,	67,	10),
(68,	3,	3,	6,	68,	5),
(69,	3,	3,	6,	69,	6),
(70,	3,	3,	5,	70,	51),
(71,	4,	4,	7,	71,	1),
(72,	4,	4,	7,	72,	2),
(73,	4,	4,	7,	73,	3),
(74,	4,	4,	7,	74,	4),
(75,	4,	4,	8,	75,	1),
(76,	4,	4,	8,	76,	3),
(77,	4,	4,	8,	77,	4),
(78,	4,	4,	8,	78,	5),
(79,	4,	4,	8,	79,	6),
(80,	4,	4,	7,	80,	5),
(81,	4,	4,	9,	82,	1),
(82,	4,	4,	9,	83,	2),
(83,	4,	4,	9,	84,	3),
(84,	4,	4,	10,	85,	1),
(85,	4,	4,	10,	86,	2),
(86,	4,	4,	10,	87,	3),
(87,	4,	4,	10,	88,	4),
(88,	4,	4,	7,	89,	6),
(89,	4,	4,	8,	90,	2),
(90,	4,	4,	8,	91,	7),
(91,	4,	4,	7,	93,	7),
(92,	4,	4,	7,	94,	8),
(93,	4,	4,	10,	95,	5),
(94,	4,	4,	7,	96,	9),
(95,	4,	4,	7,	97,	10),
(96,	4,	4,	7,	98,	11),
(97,	4,	4,	8,	99,	8),
(98,	4,	4,	11,	100,	1),
(99,	4,	4,	11,	101,	2),
(100,	4,	4,	7,	102,	12),
(101,	4,	4,	12,	103,	1),
(102,	4,	4,	12,	104,	2),
(103,	4,	4,	12,	105,	3),
(104,	4,	4,	12,	106,	4),
(105,	4,	4,	12,	107,	5),
(106,	4,	4,	7,	108,	13),
(107,	4,	4,	12,	109,	6),
(108,	4,	4,	7,	110,	14),
(109,	4,	4,	7,	111,	15),
(110,	4,	4,	7,	112,	16),
(111,	4,	4,	7,	113,	17),
(112,	4,	4,	7,	114,	18),
(113,	4,	4,	7,	115,	19),
(114,	4,	4,	7,	116,	20),
(115,	4,	4,	7,	117,	21),
(116,	4,	4,	8,	118,	9),
(117,	4,	4,	8,	119,	10),
(118,	4,	4,	8,	120,	11),
(119,	4,	4,	8,	121,	12),
(120,	4,	4,	17,	122,	1),
(121,	4,	4,	7,	123,	22),
(122,	4,	4,	7,	124,	23),
(123,	4,	4,	7,	125,	24),
(124,	4,	4,	8,	126,	13),
(125,	4,	4,	7,	127,	25),
(126,	4,	4,	7,	128,	26),
(127,	4,	4,	7,	129,	27),
(128,	4,	4,	7,	130,	28),
(129,	4,	4,	7,	131,	29),
(130,	4,	4,	7,	132,	30),
(131,	4,	4,	7,	133,	31),
(132,	4,	4,	18,	134,	1),
(133,	3,	3,	4,	135,	1000);

DROP TABLE IF EXISTS `eav_entity_datetime`;
CREATE TABLE `eav_entity_datetime` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_EAV_ENTITY_DATETIME_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_EAV_ENTITY_DATETIME_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_EAV_ENTITY_DATETIME_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_EAV_ENTITY_DATETIME_STORE_ID` (`store_id`),
  KEY `IDX_EAV_ENTITY_DATETIME_ENTITY_ID` (`entity_id`),
  KEY `IDX_EAV_ENTITY_DATETIME_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `IDX_EAV_ENTITY_DATETIME_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `FK_EAV_ENTITY_DATETIME_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_DATETIME_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTT_DTIME_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';


DROP TABLE IF EXISTS `eav_entity_decimal`;
CREATE TABLE `eav_entity_decimal` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_EAV_ENTITY_DECIMAL_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_EAV_ENTITY_DECIMAL_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_EAV_ENTITY_DECIMAL_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_EAV_ENTITY_DECIMAL_STORE_ID` (`store_id`),
  KEY `IDX_EAV_ENTITY_DECIMAL_ENTITY_ID` (`entity_id`),
  KEY `IDX_EAV_ENTITY_DECIMAL_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `IDX_EAV_ENTITY_DECIMAL_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `FK_EAV_ENTITY_DECIMAL_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_DECIMAL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTT_DEC_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';


DROP TABLE IF EXISTS `eav_entity_int`;
CREATE TABLE `eav_entity_int` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_EAV_ENTITY_INT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_EAV_ENTITY_INT_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_EAV_ENTITY_INT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_EAV_ENTITY_INT_STORE_ID` (`store_id`),
  KEY `IDX_EAV_ENTITY_INT_ENTITY_ID` (`entity_id`),
  KEY `IDX_EAV_ENTITY_INT_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `IDX_EAV_ENTITY_INT_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `FK_EAV_ENTITY_INT_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_INT_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_INT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';


DROP TABLE IF EXISTS `eav_entity_store`;
CREATE TABLE `eav_entity_store` (
  `entity_store_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Store Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `increment_prefix` varchar(20) DEFAULT NULL COMMENT 'Increment Prefix',
  `increment_last_id` varchar(50) DEFAULT NULL COMMENT 'Last Incremented Id',
  PRIMARY KEY (`entity_store_id`),
  KEY `IDX_EAV_ENTITY_STORE_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_EAV_ENTITY_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_EAV_ENTITY_STORE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTT_STORE_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Store';


DROP TABLE IF EXISTS `eav_entity_text`;
CREATE TABLE `eav_entity_text` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` text NOT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_EAV_ENTITY_TEXT_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_EAV_ENTITY_TEXT_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_EAV_ENTITY_TEXT_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_EAV_ENTITY_TEXT_STORE_ID` (`store_id`),
  KEY `IDX_EAV_ENTITY_TEXT_ENTITY_ID` (`entity_id`),
  CONSTRAINT `FK_EAV_ENTITY_TEXT_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_TEXT_ENTITY_TYPE_ID_EAV_ENTITY_TYPE_ENTITY_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_TEXT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';


DROP TABLE IF EXISTS `eav_entity_type`;
CREATE TABLE `eav_entity_type` (
  `entity_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Type Id',
  `entity_type_code` varchar(50) NOT NULL COMMENT 'Entity Type Code',
  `entity_model` varchar(255) NOT NULL COMMENT 'Entity Model',
  `attribute_model` varchar(255) DEFAULT NULL COMMENT 'Attribute Model',
  `entity_table` varchar(255) DEFAULT NULL COMMENT 'Entity Table',
  `value_table_prefix` varchar(255) DEFAULT NULL COMMENT 'Value Table Prefix',
  `entity_id_field` varchar(255) DEFAULT NULL COMMENT 'Entity Id Field',
  `is_data_sharing` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Defines Is Data Sharing',
  `data_sharing_key` varchar(100) DEFAULT 'default' COMMENT 'Data Sharing Key',
  `default_attribute_set_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Default Attribute Set Id',
  `increment_model` varchar(255) DEFAULT '' COMMENT 'Increment Model',
  `increment_per_store` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Increment Per Store',
  `increment_pad_length` smallint(5) unsigned NOT NULL DEFAULT '8' COMMENT 'Increment Pad Length',
  `increment_pad_char` varchar(1) NOT NULL DEFAULT '0' COMMENT 'Increment Pad Char',
  `additional_attribute_table` varchar(255) DEFAULT '' COMMENT 'Additional Attribute Table',
  `entity_attribute_collection` varchar(255) DEFAULT NULL COMMENT 'Entity Attribute Collection',
  PRIMARY KEY (`entity_type_id`),
  KEY `IDX_EAV_ENTITY_TYPE_ENTITY_TYPE_CODE` (`entity_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Type';

INSERT INTO `eav_entity_type` (`entity_type_id`, `entity_type_code`, `entity_model`, `attribute_model`, `entity_table`, `value_table_prefix`, `entity_id_field`, `is_data_sharing`, `data_sharing_key`, `default_attribute_set_id`, `increment_model`, `increment_per_store`, `increment_pad_length`, `increment_pad_char`, `additional_attribute_table`, `entity_attribute_collection`) VALUES
(1,	'customer',	'customer/customer',	'customer/attribute',	'customer/entity',	NULL,	NULL,	1,	'default',	1,	'eav/entity_increment_numeric',	0,	8,	'0',	'customer/eav_attribute',	'customer/attribute_collection'),
(2,	'customer_address',	'customer/address',	'customer/attribute',	'customer/address_entity',	NULL,	NULL,	1,	'default',	2,	NULL,	0,	8,	'0',	'customer/eav_attribute',	'customer/address_attribute_collection'),
(3,	'catalog_category',	'catalog/category',	'catalog/resource_eav_attribute',	'catalog/category',	NULL,	NULL,	1,	'default',	3,	NULL,	0,	8,	'0',	'catalog/eav_attribute',	'catalog/category_attribute_collection'),
(4,	'catalog_product',	'catalog/product',	'catalog/resource_eav_attribute',	'catalog/product',	NULL,	NULL,	1,	'default',	4,	NULL,	0,	8,	'0',	'catalog/eav_attribute',	'catalog/product_attribute_collection'),
(5,	'order',	'sales/order',	NULL,	'sales/order',	NULL,	NULL,	1,	'default',	0,	'eav/entity_increment_numeric',	1,	8,	'0',	NULL,	NULL),
(6,	'invoice',	'sales/order_invoice',	NULL,	'sales/invoice',	NULL,	NULL,	1,	'default',	0,	'eav/entity_increment_numeric',	1,	8,	'0',	NULL,	NULL),
(7,	'creditmemo',	'sales/order_creditmemo',	NULL,	'sales/creditmemo',	NULL,	NULL,	1,	'default',	0,	'eav/entity_increment_numeric',	1,	8,	'0',	NULL,	NULL),
(8,	'shipment',	'sales/order_shipment',	NULL,	'sales/shipment',	NULL,	NULL,	1,	'default',	0,	'eav/entity_increment_numeric',	1,	8,	'0',	NULL,	NULL);

DROP TABLE IF EXISTS `eav_entity_varchar`;
CREATE TABLE `eav_entity_varchar` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `entity_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Type Id',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `value` varchar(255) DEFAULT NULL COMMENT 'Attribute Value',
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `UNQ_EAV_ENTITY_VARCHAR_ENTITY_ID_ATTRIBUTE_ID_STORE_ID` (`entity_id`,`attribute_id`,`store_id`),
  KEY `IDX_EAV_ENTITY_VARCHAR_ENTITY_TYPE_ID` (`entity_type_id`),
  KEY `IDX_EAV_ENTITY_VARCHAR_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_EAV_ENTITY_VARCHAR_STORE_ID` (`store_id`),
  KEY `IDX_EAV_ENTITY_VARCHAR_ENTITY_ID` (`entity_id`),
  KEY `IDX_EAV_ENTITY_VARCHAR_ATTRIBUTE_ID_VALUE` (`attribute_id`,`value`),
  KEY `IDX_EAV_ENTITY_VARCHAR_ENTITY_TYPE_ID_VALUE` (`entity_type_id`,`value`),
  CONSTRAINT `FK_EAV_ENTITY_VARCHAR_ENTITY_ID_EAV_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `eav_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTITY_VARCHAR_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_ENTT_VCHR_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Entity Value Prefix';


DROP TABLE IF EXISTS `eav_form_element`;
CREATE TABLE `eav_form_element` (
  `element_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Element Id',
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `fieldset_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Fieldset Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`element_id`),
  UNIQUE KEY `UNQ_EAV_FORM_ELEMENT_TYPE_ID_ATTRIBUTE_ID` (`type_id`,`attribute_id`),
  KEY `IDX_EAV_FORM_ELEMENT_TYPE_ID` (`type_id`),
  KEY `IDX_EAV_FORM_ELEMENT_FIELDSET_ID` (`fieldset_id`),
  KEY `IDX_EAV_FORM_ELEMENT_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `FK_EAV_FORM_ELEMENT_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_FORM_ELEMENT_FIELDSET_ID_EAV_FORM_FIELDSET_FIELDSET_ID` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_FORM_ELEMENT_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Element';

INSERT INTO `eav_form_element` (`element_id`, `type_id`, `fieldset_id`, `attribute_id`, `sort_order`) VALUES
(1,	1,	NULL,	20,	0),
(2,	1,	NULL,	21,	1),
(3,	1,	NULL,	22,	2),
(4,	1,	NULL,	24,	3),
(5,	1,	NULL,	9,	4),
(6,	1,	NULL,	25,	5),
(7,	1,	NULL,	26,	6),
(8,	1,	NULL,	28,	7),
(9,	1,	NULL,	30,	8),
(10,	1,	NULL,	27,	9),
(11,	1,	NULL,	31,	10),
(12,	1,	NULL,	32,	11),
(13,	2,	NULL,	20,	0),
(14,	2,	NULL,	21,	1),
(15,	2,	NULL,	22,	2),
(16,	2,	NULL,	24,	3),
(17,	2,	NULL,	9,	4),
(18,	2,	NULL,	25,	5),
(19,	2,	NULL,	26,	6),
(20,	2,	NULL,	28,	7),
(21,	2,	NULL,	30,	8),
(22,	2,	NULL,	27,	9),
(23,	2,	NULL,	31,	10),
(24,	2,	NULL,	32,	11),
(25,	3,	NULL,	20,	0),
(26,	3,	NULL,	21,	1),
(27,	3,	NULL,	22,	2),
(28,	3,	NULL,	24,	3),
(29,	3,	NULL,	25,	4),
(30,	3,	NULL,	26,	5),
(31,	3,	NULL,	28,	6),
(32,	3,	NULL,	30,	7),
(33,	3,	NULL,	27,	8),
(34,	3,	NULL,	31,	9),
(35,	3,	NULL,	32,	10),
(36,	4,	NULL,	20,	0),
(37,	4,	NULL,	21,	1),
(38,	4,	NULL,	22,	2),
(39,	4,	NULL,	24,	3),
(40,	4,	NULL,	25,	4),
(41,	4,	NULL,	26,	5),
(42,	4,	NULL,	28,	6),
(43,	4,	NULL,	30,	7),
(44,	4,	NULL,	27,	8),
(45,	4,	NULL,	31,	9),
(46,	4,	NULL,	32,	10),
(47,	5,	1,	5,	0),
(48,	5,	1,	6,	1),
(49,	5,	1,	7,	2),
(50,	5,	1,	9,	3),
(51,	5,	2,	24,	0),
(52,	5,	2,	31,	1),
(53,	5,	2,	25,	2),
(54,	5,	2,	26,	3),
(55,	5,	2,	28,	4),
(56,	5,	2,	30,	5),
(57,	5,	2,	27,	6);

DROP TABLE IF EXISTS `eav_form_fieldset`;
CREATE TABLE `eav_form_fieldset` (
  `fieldset_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Fieldset Id',
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `code` varchar(64) NOT NULL COMMENT 'Code',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  PRIMARY KEY (`fieldset_id`),
  UNIQUE KEY `UNQ_EAV_FORM_FIELDSET_TYPE_ID_CODE` (`type_id`,`code`),
  KEY `IDX_EAV_FORM_FIELDSET_TYPE_ID` (`type_id`),
  CONSTRAINT `FK_EAV_FORM_FIELDSET_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Fieldset';

INSERT INTO `eav_form_fieldset` (`fieldset_id`, `type_id`, `code`, `sort_order`) VALUES
(1,	5,	'general',	1),
(2,	5,	'address',	2);

DROP TABLE IF EXISTS `eav_form_fieldset_label`;
CREATE TABLE `eav_form_fieldset_label` (
  `fieldset_id` smallint(5) unsigned NOT NULL COMMENT 'Fieldset Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`fieldset_id`,`store_id`),
  KEY `IDX_EAV_FORM_FIELDSET_LABEL_FIELDSET_ID` (`fieldset_id`),
  KEY `IDX_EAV_FORM_FIELDSET_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `FK_EAV_FORM_FIELDSET_LABEL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_FORM_FSET_LBL_FSET_ID_EAV_FORM_FSET_FSET_ID` FOREIGN KEY (`fieldset_id`) REFERENCES `eav_form_fieldset` (`fieldset_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Fieldset Label';

INSERT INTO `eav_form_fieldset_label` (`fieldset_id`, `store_id`, `label`) VALUES
(1,	0,	'Personal Information'),
(2,	0,	'Address Information');

DROP TABLE IF EXISTS `eav_form_type`;
CREATE TABLE `eav_form_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Type Id',
  `code` varchar(64) NOT NULL COMMENT 'Code',
  `label` varchar(255) NOT NULL COMMENT 'Label',
  `is_system` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is System',
  `theme` varchar(64) DEFAULT NULL COMMENT 'Theme',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `UNQ_EAV_FORM_TYPE_CODE_THEME_STORE_ID` (`code`,`theme`,`store_id`),
  KEY `IDX_EAV_FORM_TYPE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_EAV_FORM_TYPE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Type';

INSERT INTO `eav_form_type` (`type_id`, `code`, `label`, `is_system`, `theme`, `store_id`) VALUES
(1,	'checkout_onepage_register',	'checkout_onepage_register',	1,	'',	0),
(2,	'checkout_onepage_register_guest',	'checkout_onepage_register_guest',	1,	'',	0),
(3,	'checkout_onepage_billing_address',	'checkout_onepage_billing_address',	1,	'',	0),
(4,	'checkout_onepage_shipping_address',	'checkout_onepage_shipping_address',	1,	'',	0),
(5,	'checkout_multishipping_register',	'checkout_multishipping_register',	1,	'',	0);

DROP TABLE IF EXISTS `eav_form_type_entity`;
CREATE TABLE `eav_form_type_entity` (
  `type_id` smallint(5) unsigned NOT NULL COMMENT 'Type Id',
  `entity_type_id` smallint(5) unsigned NOT NULL COMMENT 'Entity Type Id',
  PRIMARY KEY (`type_id`,`entity_type_id`),
  KEY `IDX_EAV_FORM_TYPE_ENTITY_ENTITY_TYPE_ID` (`entity_type_id`),
  CONSTRAINT `FK_EAV_FORM_TYPE_ENTITY_TYPE_ID_EAV_FORM_TYPE_TYPE_ID` FOREIGN KEY (`type_id`) REFERENCES `eav_form_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_EAV_FORM_TYPE_ENTT_ENTT_TYPE_ID_EAV_ENTT_TYPE_ENTT_TYPE_ID` FOREIGN KEY (`entity_type_id`) REFERENCES `eav_entity_type` (`entity_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Eav Form Type Entity';

INSERT INTO `eav_form_type_entity` (`type_id`, `entity_type_id`) VALUES
(1,	1),
(2,	1),
(5,	1),
(1,	2),
(2,	2),
(3,	2),
(4,	2),
(5,	2);

DROP TABLE IF EXISTS `gift_message`;
CREATE TABLE `gift_message` (
  `gift_message_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'GiftMessage Id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `sender` varchar(255) DEFAULT NULL COMMENT 'Sender',
  `recipient` varchar(255) DEFAULT NULL COMMENT 'Recipient',
  `message` text COMMENT 'Message',
  PRIMARY KEY (`gift_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Gift Message';


DROP TABLE IF EXISTS `importexport_importdata`;
CREATE TABLE `importexport_importdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `entity` varchar(50) NOT NULL COMMENT 'Entity',
  `behavior` varchar(10) NOT NULL DEFAULT 'append' COMMENT 'Behavior',
  `data` longtext COMMENT 'Data',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Import Data Table';


DROP TABLE IF EXISTS `index_event`;
CREATE TABLE `index_event` (
  `event_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Id',
  `type` varchar(64) NOT NULL COMMENT 'Type',
  `entity` varchar(64) NOT NULL COMMENT 'Entity',
  `entity_pk` bigint(20) DEFAULT NULL COMMENT 'Entity Primary Key',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Creation Time',
  `old_data` mediumtext COMMENT 'Old Data',
  `new_data` mediumtext COMMENT 'New Data',
  PRIMARY KEY (`event_id`),
  UNIQUE KEY `UNQ_INDEX_EVENT_TYPE_ENTITY_ENTITY_PK` (`type`,`entity`,`entity_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Index Event';

INSERT INTO `index_event` (`event_id`, `type`, `entity`, `entity_pk`, `created_at`, `old_data`, `new_data`) VALUES
(1,	'save',	'catalog_category',	1,	'2016-12-14 06:01:01',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(2,	'save',	'catalog_category',	2,	'2016-12-14 06:01:01',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(3,	'save',	'catalog_category',	3,	'2016-12-16 21:01:25',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(4,	'save',	'cataloginventory_stock_item',	1,	'2016-12-17 07:47:52',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(5,	'catalog_reindex_price',	'catalog_product',	1,	'2016-12-17 07:47:53',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(6,	'save',	'catalog_product',	1,	'2016-12-17 07:47:53',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(7,	'save',	'catalog_category',	4,	'2016-12-17 07:58:26',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(8,	'save',	'catalog_category',	5,	'2016-12-17 07:58:40',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(9,	'save',	'catalog_category',	6,	'2016-12-17 07:58:51',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(10,	'save',	'catalog_category',	7,	'2016-12-17 07:59:06',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(11,	'save',	'catalog_category',	8,	'2016-12-17 07:59:44',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(12,	'save',	'catalog_category',	9,	'2016-12-17 08:00:00',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(13,	'save',	'catalog_category',	10,	'2016-12-17 08:00:24',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(14,	'save',	'catalog_category',	11,	'2016-12-17 08:00:39',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(15,	'save',	'catalog_category',	12,	'2016-12-17 08:00:51',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(16,	'save',	'catalog_category',	13,	'2016-12-17 08:01:06',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(17,	'save',	'catalog_category',	14,	'2016-12-17 08:01:23',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(18,	'save',	'cataloginventory_stock_item',	2,	'2017-01-02 22:39:53',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(19,	'catalog_reindex_price',	'catalog_product',	2,	'2017-01-02 22:39:53',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(20,	'save',	'catalog_product',	2,	'2017-01-02 22:39:53',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(21,	'save',	'cataloginventory_stock_item',	3,	'2017-01-02 22:42:26',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(22,	'catalog_reindex_price',	'catalog_product',	3,	'2017-01-02 22:42:26',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(23,	'save',	'catalog_product',	3,	'2017-01-02 22:42:26',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(24,	'save',	'cataloginventory_stock_item',	4,	'2017-01-02 22:44:34',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(25,	'catalog_reindex_price',	'catalog_product',	4,	'2017-01-02 22:44:34',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(26,	'save',	'catalog_product',	4,	'2017-01-02 22:44:34',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(27,	'save',	'cataloginventory_stock_item',	5,	'2017-01-02 22:46:12',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(28,	'catalog_reindex_price',	'catalog_product',	5,	'2017-01-02 22:46:12',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(29,	'save',	'catalog_product',	5,	'2017-01-02 22:46:12',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(30,	'save',	'cataloginventory_stock_item',	6,	'2017-01-02 22:47:01',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(31,	'catalog_reindex_price',	'catalog_product',	6,	'2017-01-02 22:47:01',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(32,	'save',	'catalog_product',	6,	'2017-01-02 22:47:01',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(33,	'save',	'cataloginventory_stock_item',	7,	'2017-01-02 22:47:39',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(34,	'catalog_reindex_price',	'catalog_product',	7,	'2017-01-02 22:47:39',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(35,	'save',	'catalog_product',	7,	'2017-01-02 22:47:39',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:1;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:1;s:35:\"catalogsearch_fulltext_match_result\";b:1;}'),
(36,	'save',	'core_config_data',	192,	'2017-01-02 22:51:58',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:1;s:24:\"catalog_url_match_result\";b:0;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(37,	'save',	'core_config_data',	198,	'2017-01-02 22:51:58',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}'),
(38,	'save',	'core_config_data',	199,	'2017-01-02 22:51:58',	NULL,	'a:5:{s:35:\"cataloginventory_stock_match_result\";b:0;s:34:\"catalog_product_price_match_result\";b:0;s:24:\"catalog_url_match_result\";b:1;s:37:\"catalog_category_product_match_result\";b:0;s:35:\"catalogsearch_fulltext_match_result\";b:0;}');

DROP TABLE IF EXISTS `index_process`;
CREATE TABLE `index_process` (
  `process_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Process Id',
  `indexer_code` varchar(32) NOT NULL COMMENT 'Indexer Code',
  `status` varchar(15) NOT NULL DEFAULT 'pending' COMMENT 'Status',
  `started_at` timestamp NULL DEFAULT NULL COMMENT 'Started At',
  `ended_at` timestamp NULL DEFAULT NULL COMMENT 'Ended At',
  `mode` varchar(9) NOT NULL DEFAULT 'real_time' COMMENT 'Mode',
  PRIMARY KEY (`process_id`),
  UNIQUE KEY `UNQ_INDEX_PROCESS_INDEXER_CODE` (`indexer_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Index Process';

INSERT INTO `index_process` (`process_id`, `indexer_code`, `status`, `started_at`, `ended_at`, `mode`) VALUES
(1,	'catalog_product_attribute',	'pending',	'2017-01-02 22:52:06',	'2017-01-02 22:52:06',	'real_time'),
(2,	'catalog_product_price',	'pending',	'2017-01-02 22:52:06',	'2017-01-02 22:52:06',	'real_time'),
(3,	'catalog_url',	'pending',	'2017-01-03 02:29:14',	'2017-01-03 02:29:14',	'real_time'),
(4,	'catalog_product_flat',	'pending',	'2016-12-16 12:05:53',	'2016-12-16 12:05:53',	'real_time'),
(5,	'catalog_category_flat',	'pending',	'2016-12-16 12:05:53',	'2016-12-16 12:05:54',	'real_time'),
(6,	'catalog_category_product',	'pending',	'2017-01-03 02:29:14',	'2017-01-03 02:29:14',	'real_time'),
(7,	'catalogsearch_fulltext',	'pending',	'2017-01-03 02:29:14',	'2017-01-03 02:29:14',	'real_time'),
(8,	'cataloginventory_stock',	'pending',	'2017-01-02 22:52:06',	'2017-01-02 22:52:06',	'real_time'),
(9,	'tag_summary',	'pending',	'2017-01-02 22:52:06',	'2017-01-02 22:52:06',	'real_time');

DROP TABLE IF EXISTS `index_process_event`;
CREATE TABLE `index_process_event` (
  `process_id` int(10) unsigned NOT NULL COMMENT 'Process Id',
  `event_id` bigint(20) unsigned NOT NULL COMMENT 'Event Id',
  `status` varchar(7) NOT NULL DEFAULT 'new' COMMENT 'Status',
  PRIMARY KEY (`process_id`,`event_id`),
  KEY `IDX_INDEX_PROCESS_EVENT_EVENT_ID` (`event_id`),
  CONSTRAINT `FK_INDEX_PROCESS_EVENT_EVENT_ID_INDEX_EVENT_EVENT_ID` FOREIGN KEY (`event_id`) REFERENCES `index_event` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_INDEX_PROCESS_EVENT_PROCESS_ID_INDEX_PROCESS_PROCESS_ID` FOREIGN KEY (`process_id`) REFERENCES `index_process` (`process_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Index Process Event';


DROP TABLE IF EXISTS `log_customer`;
CREATE TABLE `log_customer` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `visitor_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Visitor ID',
  `customer_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Customer ID',
  `login_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Login Time',
  `logout_at` timestamp NULL DEFAULT NULL COMMENT 'Logout Time',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`log_id`),
  KEY `IDX_LOG_CUSTOMER_VISITOR_ID` (`visitor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log Customers Table';


DROP TABLE IF EXISTS `log_quote`;
CREATE TABLE `log_quote` (
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote ID',
  `visitor_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Visitor ID',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Creation Time',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'Deletion Time',
  PRIMARY KEY (`quote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log Quotes Table';

INSERT INTO `log_quote` (`quote_id`, `visitor_id`, `created_at`, `deleted_at`) VALUES
(1,	93,	'2016-12-19 05:27:39',	NULL),
(2,	134,	'2016-12-24 04:10:34',	NULL),
(3,	198,	'2016-12-31 16:51:34',	NULL),
(4,	222,	'2017-01-03 00:00:17',	NULL),
(5,	237,	'2017-01-03 11:43:19',	NULL);

DROP TABLE IF EXISTS `log_summary`;
CREATE TABLE `log_summary` (
  `summary_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Summary ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  `type_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Type ID',
  `visitor_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Visitor Count',
  `customer_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Customer Count',
  `add_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date',
  PRIMARY KEY (`summary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log Summary Table';


DROP TABLE IF EXISTS `log_summary_type`;
CREATE TABLE `log_summary_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Type ID',
  `type_code` varchar(64) DEFAULT NULL COMMENT 'Type Code',
  `period` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Period',
  `period_type` varchar(6) NOT NULL DEFAULT 'MINUTE' COMMENT 'Period Type',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log Summary Types Table';

INSERT INTO `log_summary_type` (`type_id`, `type_code`, `period`, `period_type`) VALUES
(1,	'hour',	1,	'HOUR'),
(2,	'day',	1,	'DAY');

DROP TABLE IF EXISTS `log_url`;
CREATE TABLE `log_url` (
  `url_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'URL ID',
  `visitor_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Visitor ID',
  `visit_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Visit Time',
  KEY `IDX_LOG_URL_VISITOR_ID` (`visitor_id`),
  KEY `url_id` (`url_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log URL Table';


DROP TABLE IF EXISTS `log_url_info`;
CREATE TABLE `log_url_info` (
  `url_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'URL ID',
  `url` varchar(255) DEFAULT NULL COMMENT 'URL',
  `referer` varchar(255) DEFAULT NULL COMMENT 'Referrer',
  PRIMARY KEY (`url_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log URL Info Table';


DROP TABLE IF EXISTS `log_visitor`;
CREATE TABLE `log_visitor` (
  `visitor_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Visitor ID',
  `session_id` varchar(64) DEFAULT NULL COMMENT 'Session ID',
  `first_visit_at` timestamp NULL DEFAULT NULL COMMENT 'First Visit Time',
  `last_visit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Last Visit Time',
  `last_url_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Last URL ID',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store ID',
  PRIMARY KEY (`visitor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log Visitors Table';

INSERT INTO `log_visitor` (`visitor_id`, `session_id`, `first_visit_at`, `last_visit_at`, `last_url_id`, `store_id`) VALUES
(1,	'2hd6h9o2nan9v3kress2706044',	'2016-12-14 06:14:43',	'2016-12-14 06:14:43',	0,	1),
(2,	'9sjabpkpgsrgies5k9set2llk2',	'2016-12-15 03:41:26',	'2016-12-15 03:41:26',	0,	1),
(3,	'5qgphoe9om3l0rjver2aq704i0',	'2016-12-15 05:45:15',	'2016-12-15 05:45:16',	0,	1),
(4,	'r7iq5fi2pvg7g8m9ckv4djvev7',	'2016-12-15 15:05:38',	'2016-12-15 15:05:38',	0,	1),
(5,	'7a53srknpejnb88oujmc8otr26',	'2016-12-15 18:57:55',	'2016-12-15 18:57:55',	0,	1),
(6,	'9i5eic2b21llenol3alfet01d4',	'2016-12-16 09:50:09',	'2016-12-16 09:50:10',	0,	1),
(7,	'ogm9a66h9vv8dte8q0a2vdq4r4',	'2016-12-16 10:56:23',	'2016-12-16 10:56:23',	0,	1),
(8,	'hg4pdn5uellkqvie555bo8tu87',	'2016-12-16 12:04:45',	'2016-12-16 13:23:16',	0,	1),
(9,	'ptrp5cpfp3o3o5ah93fut19ki7',	'2016-12-16 12:06:31',	'2016-12-16 12:06:31',	0,	1),
(10,	'2gvq1jbpq59clp9acevcr71n10',	'2016-12-16 12:12:00',	'2016-12-16 12:12:00',	0,	1),
(11,	'mk0f32dtkifricimadqt4o1135',	'2016-12-16 12:14:05',	'2016-12-16 12:14:05',	0,	1),
(12,	'6lg72112dhrarh0fhnp8bg5aa6',	'2016-12-16 12:15:49',	'2016-12-16 12:15:49',	0,	1),
(13,	'vik8p6a4re2dadamuqlin1bem1',	'2016-12-16 12:17:15',	'2016-12-16 12:17:15',	0,	1),
(14,	's81shsnfe6flu64urr7sk52206',	'2016-12-16 12:25:01',	'2016-12-16 12:25:02',	0,	1),
(15,	'1b0uusls0madorj8paojbl3ik5',	'2016-12-16 12:25:38',	'2016-12-16 12:25:38',	0,	1),
(16,	'sjf6uo62gvi3ql0k9okdcbcqn6',	'2016-12-16 12:27:36',	'2016-12-16 12:27:37',	0,	1),
(17,	'jbgqm7mn01gtld1nhsi3gvmfl2',	'2016-12-16 12:55:45',	'2016-12-16 12:55:46',	0,	1),
(18,	'm0s6h2jorbuk67kaudbhbt8le3',	'2016-12-16 17:22:29',	'2016-12-16 17:22:31',	0,	1),
(19,	'amjmm314bmuqj4h290gmm4umr0',	'2016-12-16 17:47:03',	'2016-12-16 17:47:03',	0,	1),
(20,	'gvim5utq6g122sm8hrf4ja6ud6',	'2016-12-16 17:47:05',	'2016-12-16 17:47:05',	0,	1),
(21,	'5j2a81v6bcofs5ngvlq6dp8jk6',	'2016-12-16 17:47:06',	'2016-12-16 17:47:06',	0,	1),
(22,	'e0cb9eg1vk2igadrmko8u3cuo5',	'2016-12-16 19:30:01',	'2016-12-16 19:30:02',	0,	1),
(23,	'hgnerbsc68h1q2aqbiv9us6ti3',	'2016-12-16 20:00:32',	'2016-12-16 20:00:32',	0,	1),
(24,	'8i56ajbm35fm2g2rn0d3iqanf6',	'2016-12-16 20:04:58',	'2016-12-16 21:01:29',	0,	1),
(25,	'osls5fd9tdpn4cded6d6buquo3',	'2016-12-16 20:06:25',	'2016-12-16 20:06:25',	0,	1),
(26,	'2lehujqnb85jcih1afnk08mke7',	'2016-12-16 20:14:01',	'2016-12-16 20:14:02',	0,	1),
(27,	'rfimvlicrb9t423vjl7a0vogf2',	'2016-12-16 20:16:20',	'2016-12-16 20:16:20',	0,	1),
(28,	'ggrn6fik2rj842jfd5i2pg0a16',	'2016-12-16 20:25:18',	'2016-12-16 20:25:18',	0,	1),
(29,	'bhi3cekng6sbipnt2itpeorpe2',	'2016-12-16 21:05:03',	'2016-12-16 21:05:04',	0,	1),
(30,	'sagubd71slt6ggja831rtan767',	'2016-12-16 22:03:23',	'2016-12-16 22:03:24',	0,	1),
(31,	'12i6v3nrbc2m934f2fedjcaf27',	'2016-12-16 23:23:12',	'2016-12-17 00:07:00',	0,	1),
(32,	'c1pukp90ijj56uc8nmn3ubhca0',	'2016-12-16 23:25:20',	'2016-12-16 23:25:20',	0,	1),
(33,	'mmabftrlrpejioei1cf11fp8i5',	'2016-12-16 23:36:36',	'2016-12-16 23:36:36',	0,	1),
(34,	'ds52flbln342tbh0m88b0966g2',	'2016-12-16 23:55:23',	'2016-12-16 23:55:23',	0,	1),
(35,	'0us7so51q3tuctc5dcpmtu17f7',	'2016-12-17 02:36:06',	'2016-12-17 02:36:07',	0,	1),
(36,	'aptqr6dpo5fe7fqgvgmdiu6n03',	'2016-12-17 05:44:23',	'2016-12-17 05:44:25',	0,	1),
(37,	'1mh6vj4ucjq5u8n45p6aa3sfq0',	'2016-12-17 07:12:30',	'2016-12-17 07:12:30',	0,	1),
(38,	'sgtg8s1nu0kt1h3brc9ubsapi7',	'2016-12-17 07:32:37',	'2016-12-17 08:01:38',	0,	1),
(39,	'si36hro0v8nvfctjhl9bfmo943',	'2016-12-17 08:01:36',	'2016-12-17 08:01:37',	0,	1),
(40,	'0hvsukvj3ulpjf74pk2c4d3iv7',	'2016-12-17 08:02:14',	'2016-12-17 08:02:14',	0,	1),
(41,	'hb2anf71hk8a07qs65cm5a16f1',	'2016-12-17 08:12:43',	'2016-12-17 08:12:44',	0,	1),
(42,	'p2f6m0lc23eh8j12jkoqv8mk17',	'2016-12-17 09:23:32',	'2016-12-17 09:23:33',	0,	1),
(43,	'dm24moohu8huo6cieaokq16lb1',	'2016-12-17 12:35:08',	'2016-12-17 12:35:10',	0,	1),
(44,	'qerf7fc0t5d8casd043474kpo6',	'2016-12-17 16:49:40',	'2016-12-17 16:49:42',	0,	1),
(45,	'i66512mo7dvne702uuigv4ii85',	'2016-12-17 16:49:44',	'2016-12-17 16:50:27',	0,	1),
(46,	'ivppljkv5r5bdeokn5e2qfimq5',	'2016-12-17 21:36:06',	'2016-12-17 21:36:07',	0,	1),
(47,	'p3s5q8g0a6srf9cisittm9a3a0',	'2016-12-17 21:36:09',	'2016-12-17 21:36:09',	0,	1),
(48,	'12pb3ncdck5skocuq8dltad8t1',	'2016-12-17 21:36:16',	'2016-12-17 21:36:17',	0,	1),
(49,	'l5k00a3uv9mb8dc0f7uofa7a40',	'2016-12-17 21:36:20',	'2016-12-17 21:36:20',	0,	1),
(50,	'ode3sppvef261p9da1jjntk5k2',	'2016-12-17 21:36:22',	'2016-12-17 21:36:23',	0,	1),
(51,	'070gkn5lhamv0nsm4kaotghdh5',	'2016-12-17 21:36:25',	'2016-12-17 21:36:25',	0,	1),
(52,	'njpid3goboj7digmfg00c18n70',	'2016-12-17 21:36:27',	'2016-12-17 21:36:27',	0,	1),
(53,	'cb70rvjsh7h95pbo8j262erot4',	'2016-12-17 21:36:29',	'2016-12-17 21:36:30',	0,	1),
(54,	'ib7tv06u68905ph0mthco4tnm7',	'2016-12-17 21:36:32',	'2016-12-17 21:36:32',	0,	1),
(55,	'9ovoh1t9f6f5pbm7g3qi9fcr75',	'2016-12-17 21:36:34',	'2016-12-17 21:36:34',	0,	1),
(56,	'35d74vds1d0sordsj0siqip2m2',	'2016-12-17 21:36:36',	'2016-12-17 21:36:36',	0,	1),
(57,	'rv1ascmk0qa2cv0g6f2h04lta4',	'2016-12-17 21:36:39',	'2016-12-17 21:36:39',	0,	1),
(58,	'njqvibnqrbfrj6t7ohcp3223c0',	'2016-12-17 21:36:41',	'2016-12-17 21:36:42',	0,	1),
(59,	'o7q8lqi550ldb37u2kjjkcguu1',	'2016-12-17 21:36:44',	'2016-12-17 21:36:44',	0,	1),
(60,	'3qsnrhsbafpq0a2d0ihcgdabo6',	'2016-12-17 21:36:47',	'2016-12-17 21:36:47',	0,	1),
(61,	'6ojav5tqcr4nqdiqjnmleqsbi2',	'2016-12-17 21:36:50',	'2016-12-17 21:36:50',	0,	1),
(62,	'v0mm54q58r5s6lbt12psjsbpo3',	'2016-12-17 21:36:52',	'2016-12-17 21:36:53',	0,	1),
(63,	'vqi7463bsojb6h4pb6j5e0qel6',	'2016-12-17 21:36:55',	'2016-12-17 21:36:55',	0,	1),
(64,	'ikjrm5no37cl4ecb2g71jd3n97',	'2016-12-17 21:36:58',	'2016-12-17 21:36:58',	0,	1),
(65,	'ch5aeacimv7vkmflnmssgk7gt1',	'2016-12-17 21:37:00',	'2016-12-17 21:37:01',	0,	1),
(66,	'p21qvi7t4f8va4fk88p1jbh1v7',	'2016-12-17 21:37:03',	'2016-12-17 21:37:03',	0,	1),
(67,	'17jvmoujles8m3orrbjnvag340',	'2016-12-17 21:37:06',	'2016-12-17 21:37:06',	0,	1),
(68,	'jphrqtte8g8j4rjj3fn1oofbc0',	'2016-12-17 21:37:08',	'2016-12-17 21:37:09',	0,	1),
(69,	'jh419f7876bbendd41hpm2umg2',	'2016-12-17 21:37:11',	'2016-12-17 21:37:12',	0,	1),
(70,	'0npvkto1b3oricvbhuht43a5c7',	'2016-12-17 21:37:14',	'2016-12-17 21:37:14',	0,	1),
(71,	'k2fkuf3e418qipqhdcg35u6h95',	'2016-12-17 21:37:16',	'2016-12-17 21:37:17',	0,	1),
(72,	'26mipftpl155ajjcmi3kmfc7e0',	'2016-12-17 21:37:18',	'2016-12-17 21:37:19',	0,	1),
(73,	'pd0ct12ouagtkeguqiqffdi9a5',	'2016-12-17 21:37:21',	'2016-12-17 21:37:22',	0,	1),
(74,	'2aq88fqkvt2g683pm1q9aeard6',	'2016-12-17 21:37:24',	'2016-12-17 21:37:24',	0,	1),
(75,	'4qqu38ve328g1dh0bgekc7sfk0',	'2016-12-17 21:37:27',	'2016-12-17 21:37:27',	0,	1),
(76,	'eg5qnicqdmut1qku3fm9g9dqd4',	'2016-12-17 21:37:29',	'2016-12-17 21:37:30',	0,	1),
(77,	'fos2c8q15p5btcb0mh959db9f5',	'2016-12-17 21:37:32',	'2016-12-17 21:37:32',	0,	1),
(78,	'38udpvvqpa9dld1h6mrq4ag2d5',	'2016-12-18 08:49:10',	'2016-12-18 08:49:12',	0,	1),
(79,	'viv86jvsf4m73bl73dq5raoo11',	'2016-12-18 09:23:28',	'2016-12-18 09:23:28',	0,	1),
(80,	'5n5ql6k4rqftv5hlhsbs3d5782',	'2016-12-18 09:25:43',	'2016-12-18 09:25:58',	0,	1),
(81,	'oadre9alsmffqic73tas0n0813',	'2016-12-18 09:45:55',	'2016-12-18 09:45:56',	0,	1),
(82,	'r0getfa3d7iep3fj09f1216mr3',	'2016-12-18 12:36:53',	'2016-12-18 13:48:43',	0,	1),
(83,	'g6t7mqkpagukkohm39gbl78mk0',	'2016-12-18 20:51:19',	'2016-12-18 20:51:20',	0,	1),
(84,	'1rkabp76vq87j2fbknpv38kvg1',	'2016-12-18 23:28:08',	'2016-12-18 23:40:19',	0,	1),
(85,	'j72i8k96rf06b4ipble2di99q2',	'2016-12-19 01:10:45',	'2016-12-19 01:10:46',	0,	1),
(86,	'plkg6qr4cj7dqcgcop19ekncc5',	'2016-12-19 01:13:40',	'2016-12-19 01:13:40',	0,	1),
(87,	'f3h98lion334t769vbh1gq3f62',	'2016-12-19 01:39:21',	'2016-12-19 01:46:07',	0,	1),
(88,	'cjpess7femqm0g85grno7dehl7',	'2016-12-19 01:44:12',	'2016-12-19 01:54:29',	0,	1),
(89,	'akapduufguq5en3ri8e76vicm6',	'2016-12-19 02:18:40',	'2016-12-19 02:31:29',	0,	1),
(90,	'ulbvtke4pga2objasee349vf44',	'2016-12-19 02:23:39',	'2016-12-19 02:43:20',	0,	1),
(91,	'fps8bq4fs3btfdl6rtrfk183i7',	'2016-12-19 02:43:06',	'2016-12-19 02:43:06',	0,	1),
(92,	'rhr46cr35f25kmoei45n6e9hc1',	'2016-12-19 04:16:12',	'2016-12-19 04:16:14',	0,	1),
(93,	'pap55gsbgps40ct20aeg67pt23',	'2016-12-19 05:26:46',	'2016-12-19 05:28:38',	0,	1),
(94,	'9mdeabhnkplim9eo1dkv3jdec5',	'2016-12-19 06:54:31',	'2016-12-19 06:55:14',	0,	1),
(95,	'shc7mqbg282d23r5q6kalj70s5',	'2016-12-20 03:57:18',	'2016-12-20 03:57:19',	0,	1),
(96,	'qpnt8mdgugbg7vtcu5v4gc4pi3',	'2016-12-20 04:04:53',	'2016-12-20 04:04:53',	0,	1),
(97,	'aaoc7d49ivenc4bs0igaqs4on3',	'2016-12-20 05:42:00',	'2016-12-20 05:42:01',	0,	1),
(98,	'1o6uvrntl2j05cehvh52sslcc6',	'2016-12-20 11:13:06',	'2016-12-20 11:52:57',	0,	1),
(99,	'r80oa7vfd6066u7kbcl4p380n7',	'2016-12-20 15:32:29',	'2016-12-20 15:32:30',	0,	1),
(100,	'bdjr0k3rroq1us55ck3gs9taq6',	'2016-12-20 23:38:16',	'2016-12-21 00:33:30',	0,	1),
(101,	'q63arj0rorali3dm6om5huafg0',	'2016-12-21 05:32:34',	'2016-12-21 05:32:36',	0,	1),
(102,	'lqum6n1tkpehle6204be1aqdf6',	'2016-12-21 05:32:39',	'2016-12-21 05:32:39',	0,	1),
(103,	'jufa8e5a91ncotsqghuvcg4jm7',	'2016-12-21 05:32:43',	'2016-12-21 05:32:43',	0,	1),
(104,	'hpqhkne3eoctoduetoisduvv40',	'2016-12-21 10:03:54',	'2016-12-21 10:03:55',	0,	1),
(105,	'85gpj0kd793v29ih4eced2nb66',	'2016-12-21 10:53:40',	'2016-12-21 10:53:40',	0,	1),
(106,	'eltvukp6fh4234fdcad321vu23',	'2016-12-21 19:31:15',	'2016-12-21 19:31:16',	0,	1),
(107,	'ra2svcoubljdkb5hmf1sf54if7',	'2016-12-21 22:32:46',	'2016-12-21 22:32:47',	0,	1),
(108,	'3oqva0qds03d51cn387isl9vk0',	'2016-12-22 00:51:11',	'2016-12-22 00:51:13',	0,	1),
(109,	'gsd9jn17aekjft828ekc928c14',	'2016-12-22 01:33:47',	'2016-12-22 01:33:48',	0,	1),
(110,	'40qhitpftlffipu1lkj4qfq4k0',	'2016-12-22 03:15:14',	'2016-12-22 03:15:15',	0,	1),
(111,	'3co8t4d765qd39mrnhi66qu186',	'2016-12-22 03:48:08',	'2016-12-22 03:48:09',	0,	1),
(112,	'sl3684ads3au8tr5t6sv4gmvl5',	'2016-12-22 07:42:52',	'2016-12-22 07:43:24',	0,	1),
(113,	'o0h9caibtb8obpj6o2beehtdm7',	'2016-12-22 09:58:04',	'2016-12-22 09:58:06',	0,	1),
(114,	'tp08os067g8gplk92uslkc5c55',	'2016-12-22 09:58:07',	'2016-12-22 09:58:07',	0,	1),
(115,	'e3e91bjm682rv7ec6jbs3ufe86',	'2016-12-22 09:58:08',	'2016-12-22 09:58:08',	0,	1),
(116,	'dmjn8lp4jn1lb6rc19i9hedlm0',	'2016-12-22 11:26:12',	'2016-12-22 11:26:12',	0,	1),
(117,	'nfkjsrhe95m7ugl5r29lm6v312',	'2016-12-22 11:46:28',	'2016-12-22 11:46:28',	0,	1),
(118,	'6mnis8jb1c4ldav3og2jurdc66',	'2016-12-22 21:52:52',	'2016-12-22 21:52:53',	0,	1),
(119,	'qotf08bv8m8j67s3kr9841ph57',	'2016-12-23 01:27:16',	'2016-12-23 01:27:17',	0,	1),
(120,	'm080n9qhlms6v786ud8s6uh0g0',	'2016-12-23 03:22:59',	'2016-12-23 03:22:59',	0,	1),
(121,	'dadq6jpb9nfs6caj95hniokea3',	'2016-12-23 04:35:31',	'2016-12-23 04:35:33',	0,	1),
(122,	'd0bcj55oh52mhd6nje7dlb28h5',	'2016-12-23 04:35:31',	'2016-12-23 04:35:33',	0,	1),
(123,	'j0qmh6r80plefv38fitqlitv72',	'2016-12-23 04:35:31',	'2016-12-23 04:35:33',	0,	1),
(124,	'io26l5i4q0565m9pvev7dqf9b6',	'2016-12-23 04:35:35',	'2016-12-23 04:35:35',	0,	1),
(125,	'sibec7s3su443vkubobs1qqp66',	'2016-12-23 04:35:35',	'2016-12-23 04:35:35',	0,	1),
(126,	'4tgsgcs8v2po5qckd2ui4h6475',	'2016-12-23 04:35:36',	'2016-12-23 04:35:36',	0,	1),
(127,	'j7m2ue2fp58tl3o6fc23o65q52',	'2016-12-23 07:47:26',	'2016-12-23 07:47:36',	0,	1),
(128,	'qm0s7bde8ucl2i7n4urc53fgv0',	'2016-12-23 11:30:03',	'2016-12-23 11:30:05',	0,	1),
(129,	'vcoatcl5r9ro6oq1733b87mkc4',	'2016-12-23 11:30:14',	'2016-12-23 11:30:14',	0,	1),
(130,	's8gisthirgr5onb9nu4qdh71d4',	'2016-12-23 11:30:21',	'2016-12-23 11:30:22',	0,	1),
(131,	'av2scfg85s23skrnfl3unp8vc0',	'2016-12-23 15:24:22',	'2016-12-23 15:24:23',	0,	1),
(132,	'gf2l54751bltb5qpruqjebidt4',	'2016-12-23 20:13:57',	'2016-12-23 20:13:58',	0,	1),
(133,	'2mncas4fot02his87qb67hiin2',	'2016-12-23 20:33:34',	'2016-12-23 20:33:34',	0,	1),
(134,	'5mup0vvngil7gdob3ir6lmnoi1',	'2016-12-24 03:06:34',	'2016-12-24 04:18:44',	0,	1),
(135,	'hm87arbe3i21coil98jrf5gb73',	'2016-12-24 03:55:01',	'2016-12-24 03:55:02',	0,	1),
(136,	'djhdhujhpvivflnl5ac3dn1ud0',	'2016-12-24 06:31:57',	'2016-12-24 06:31:58',	0,	1),
(137,	'63e0vj60j4qodfphuca50gtb93',	'2016-12-24 07:24:06',	'2016-12-24 07:38:37',	0,	1),
(138,	'io6qhndn5k822qq5gkpagsr597',	'2016-12-24 08:52:52',	'2016-12-24 08:52:53',	0,	1),
(139,	'bpvsosfsmh3cm8ng7ce4r71vv7',	'2016-12-24 16:25:44',	'2016-12-24 16:25:46',	0,	1),
(140,	't8tq000m9o8b3lh8nr0981pam4',	'2016-12-24 16:26:02',	'2016-12-24 16:26:02',	0,	1),
(141,	'7ijbgh0pasaeicd07dvhjpcso5',	'2016-12-25 02:01:38',	'2016-12-25 02:01:40',	0,	1),
(142,	'ht5jmo6putagm26vuinf6l0555',	'2016-12-25 10:24:23',	'2016-12-25 10:24:24',	0,	1),
(143,	'4cn2b5qftbjkccetcq7k7i1hh6',	'2016-12-25 22:12:29',	'2016-12-25 22:12:37',	0,	1),
(144,	'se0o975a2qqe8j64g8mh68gbd6',	'2016-12-26 00:57:45',	'2016-12-26 00:57:46',	0,	1),
(145,	'nsuq008bsqh9ab4mu4b9rc5176',	'2016-12-26 02:13:13',	'2016-12-26 02:13:13',	0,	1),
(146,	'g40l8l4p5mcc95ibb1j15svbm5',	'2016-12-26 02:13:15',	'2016-12-26 02:13:16',	0,	1),
(147,	'6lam7muvh32li4s15k2rbtcdl1',	'2016-12-26 02:13:19',	'2016-12-26 02:13:19',	0,	1),
(148,	'di47qghf0gdl6po7okfs97v6e0',	'2016-12-26 02:13:22',	'2016-12-26 02:13:22',	0,	1),
(149,	'9j8vs4to3f7v7rg4qm5k8v27m7',	'2016-12-26 02:13:25',	'2016-12-26 02:13:25',	0,	1),
(150,	'hdajvi1j96n7rev9iua48tr6t7',	'2016-12-26 02:13:29',	'2016-12-26 02:13:30',	0,	1),
(151,	'5qgrhpbet21dk294bbou5e76a3',	'2016-12-26 02:13:32',	'2016-12-26 02:13:32',	0,	1),
(152,	't8v8q7u4m2e22fenanvn1qiv00',	'2016-12-26 02:13:35',	'2016-12-26 02:13:36',	0,	1),
(153,	'lhq6bvm0dta5sirsh2bel9erp3',	'2016-12-26 02:13:38',	'2016-12-26 02:13:39',	0,	1),
(154,	'pl3o5p68m20602i5it09k17cu5',	'2016-12-26 02:13:41',	'2016-12-26 02:13:41',	0,	1),
(155,	'jbdp480j785cpc5f8etst7ga35',	'2016-12-26 02:13:44',	'2016-12-26 02:13:44',	0,	1),
(156,	'np33ict95iorut60pl276uf8h1',	'2016-12-26 02:13:47',	'2016-12-26 02:13:48',	0,	1),
(157,	'kofcl01pock3rog3kr7jp01ae1',	'2016-12-26 02:13:51',	'2016-12-26 02:13:51',	0,	1),
(158,	'n3jcgi99t942vc3acc78gtuo33',	'2016-12-26 02:13:54',	'2016-12-26 02:13:54',	0,	1),
(159,	'7a6org4fcalq5re4aj6ghusmj5',	'2016-12-26 02:13:56',	'2016-12-26 02:13:57',	0,	1),
(160,	'25dj4d2l15le27lit861g7hbt7',	'2016-12-26 02:13:59',	'2016-12-26 02:14:00',	0,	1),
(161,	'k3mdurc5p9jln188sf5k56gme0',	'2016-12-26 02:14:02',	'2016-12-26 02:14:02',	0,	1),
(162,	'8vpc5okfacscsg5oq0a2b6he13',	'2016-12-26 02:14:05',	'2016-12-26 02:14:05',	0,	1),
(163,	'uu40i6dpoc8al7ajjmr9m11fv0',	'2016-12-26 02:14:08',	'2016-12-26 02:14:08',	0,	1),
(164,	'um9ene372agqtheomcfvr94k84',	'2016-12-26 02:14:11',	'2016-12-26 02:14:11',	0,	1),
(165,	'b83ipc5a3cc6060h1g5spstd53',	'2016-12-26 02:14:16',	'2016-12-26 02:14:17',	0,	1),
(166,	'5ddokj08p28v9up9ie5f4oaio3',	'2016-12-26 02:14:20',	'2016-12-26 02:14:21',	0,	1),
(167,	'mnfmbgkjkj2e7k7f3kpc6l7eo7',	'2016-12-26 02:14:25',	'2016-12-26 02:14:25',	0,	1),
(168,	'l05lvusbngriuumgep6k0r6n82',	'2016-12-26 02:14:29',	'2016-12-26 02:14:30',	0,	1),
(169,	'4c9nmfl9q7sm8l5ad30a11nhf7',	'2016-12-26 02:14:33',	'2016-12-26 02:14:34',	0,	1),
(170,	'887vbaphpt9al8fnjif39msv22',	'2016-12-26 02:14:36',	'2016-12-26 02:14:37',	0,	1),
(171,	'qe30ap17mdto2gja0jvi02bn67',	'2016-12-26 02:14:40',	'2016-12-26 02:14:40',	0,	1),
(172,	'6k8vaigs5hgleafq32683tguv6',	'2016-12-26 02:14:42',	'2016-12-26 02:14:43',	0,	1),
(173,	'0d56aa5ulgoetfro9al1d1v8d2',	'2016-12-26 02:14:45',	'2016-12-26 02:14:46',	0,	1),
(174,	'shog0e17edlo24dmekcsari712',	'2016-12-26 02:14:48',	'2016-12-26 02:14:49',	0,	1),
(175,	'h27c6cc76q28l5c2ffnprb9v81',	'2016-12-26 02:14:51',	'2016-12-26 02:14:52',	0,	1),
(176,	'2ff4qr9l152oloog3ak7f4l9d4',	'2016-12-26 02:14:55',	'2016-12-26 02:14:56',	0,	1),
(177,	'2vkvgj9q82uftnia5mdld3bkk2',	'2016-12-26 06:05:10',	'2016-12-26 06:05:56',	0,	1),
(178,	'e4uokhu2v62i5vh27st1j3e6i4',	'2016-12-26 07:28:52',	'2016-12-26 07:28:52',	0,	1),
(179,	'an5b6agguk28i7nps3i0a79vk0',	'2016-12-26 08:13:47',	'2016-12-26 08:15:58',	0,	1),
(180,	'7232js24kclm9fuh7rhk4s14k4',	'2016-12-26 08:25:38',	'2016-12-26 08:25:39',	0,	1),
(181,	'l02ehcpdeiphf5o3j8bnsvjmu7',	'2016-12-26 23:41:21',	'2016-12-26 23:41:22',	0,	1),
(182,	'00hfbu6anh3trk9lj5e1h9g185',	'2016-12-27 09:44:57',	'2016-12-27 09:44:59',	0,	1),
(183,	'4v5qcoudg0i3jqihp5tbgts7q7',	'2016-12-27 12:42:30',	'2016-12-27 12:42:31',	0,	1),
(184,	'oehia44f8qv3ujugu4376m4nh1',	'2016-12-27 16:12:05',	'2016-12-27 16:12:06',	0,	1),
(185,	'q947letd6s64m2utb5qgsl3a01',	'2016-12-27 16:12:07',	'2016-12-27 16:12:07',	0,	1),
(186,	'4bo9envpebg5kse4lrfe6rtk10',	'2016-12-27 16:12:08',	'2016-12-27 16:12:08',	0,	1),
(187,	'k5ugu3fsg9otp685l0umbuv4g2',	'2016-12-28 06:24:20',	'2016-12-28 06:24:22',	0,	1),
(188,	'p60r3o1p2lt7n30j4e2m44i0h2',	'2016-12-28 06:50:19',	'2016-12-28 06:50:19',	0,	1),
(189,	'p7fbiplfr45oaoql11h4a6nki7',	'2016-12-29 11:08:41',	'2016-12-29 11:08:42',	0,	1),
(190,	'4gftvskj8bbkseauu6erotekv6',	'2016-12-29 11:46:17',	'2016-12-29 11:46:42',	0,	1),
(191,	'u6t674gcetjet67ldrs587g311',	'2016-12-29 15:10:08',	'2016-12-29 15:10:10',	0,	1),
(192,	'09duqrp2sbnhtcsv6bgml8ael6',	'2016-12-30 15:27:19',	'2016-12-30 15:27:21',	0,	1),
(193,	'fsdqtpt0ju9svjnnksjiq681g4',	'2016-12-30 18:42:04',	'2016-12-30 18:42:05',	0,	1),
(194,	'39o6h5dpdh8ct8vuhme6h1iuj6',	'2016-12-31 02:01:30',	'2016-12-31 02:01:31',	0,	1),
(195,	'qtme5ah815oprs5373qh43mh63',	'2016-12-31 03:38:07',	'2016-12-31 03:38:07',	0,	1),
(196,	'o9bl0mi0sq647kndmvri9r90r0',	'2016-12-31 05:21:32',	'2016-12-31 05:21:34',	0,	1),
(197,	'i1n7bcgc4qqdprm4pa93bq6o22',	'2016-12-31 10:23:09',	'2016-12-31 10:23:10',	0,	1),
(198,	'jat53lg84p0c494p4h8m34ltb1',	'2016-12-31 16:42:17',	'2016-12-31 16:55:44',	0,	1),
(199,	'dom0u96vmfi271vd040i1vgun2',	'2016-12-31 16:57:50',	'2016-12-31 16:57:51',	0,	1),
(200,	'9aa1i8hn6o5er1umns7499cml6',	'2016-12-31 17:00:00',	'2016-12-31 17:00:01',	0,	1),
(201,	'6ar4r3vu7ismrd9ah2oskk6mu5',	'2016-12-31 17:37:42',	'2016-12-31 17:37:42',	0,	1),
(202,	'mqsp35tr0c08lt6bu3pc26u5n1',	'2016-12-31 23:18:03',	'2016-12-31 23:19:06',	0,	1),
(203,	'bf49avoe5h5jjlioerpucdqhd3',	'2017-01-01 05:01:57',	'2017-01-01 05:01:58',	0,	1),
(204,	'm9nh68irmhmv2rpkbesc90rig4',	'2017-01-01 05:49:01',	'2017-01-01 05:49:01',	0,	1),
(205,	'e87q986hcoaf53idoc47nt8h22',	'2017-01-01 05:49:06',	'2017-01-01 05:49:06',	0,	1),
(206,	'oiit3eplehfhdcorvjh31r2dp4',	'2017-01-01 07:34:39',	'2017-01-01 07:34:40',	0,	1),
(207,	'nm9ss46j1hitaru9g3jtvph4r4',	'2017-01-01 07:57:31',	'2017-01-01 07:58:29',	0,	1),
(208,	'bbikgn4uhmpibdthdop61kbrj3',	'2017-01-01 10:31:29',	'2017-01-01 10:31:30',	0,	1),
(209,	'u9ebvn96bb1es5kvr88d7sjij7',	'2017-01-01 12:38:52',	'2017-01-01 12:38:54',	0,	1),
(210,	't8738c3g9snnlj60af2lajlce7',	'2017-01-01 12:38:55',	'2017-01-01 12:38:55',	0,	1),
(211,	'h1aiuh07b39jep15sfdadrshp6',	'2017-01-01 12:38:56',	'2017-01-01 12:38:56',	0,	1),
(212,	'rt60cipv6hoduhsvht4fpkk6r7',	'2017-01-02 01:39:46',	'2017-01-02 01:39:47',	0,	1),
(213,	'3809uq496iad4ecmogdfd11fv5',	'2017-01-02 02:08:10',	'2017-01-02 02:25:33',	0,	1),
(214,	'ftv5m1kgm5tstrr3kggpdc7es6',	'2017-01-02 08:22:21',	'2017-01-02 08:22:21',	0,	1),
(215,	'5b8rjo4ffe6vdjec7sq923a4t0',	'2017-01-02 09:20:57',	'2017-01-02 09:20:58',	0,	1),
(216,	't81uoll1kgs9v9fafjkadiugk2',	'2017-01-02 12:02:13',	'2017-01-02 12:26:12',	0,	1),
(217,	'hen3ene67iqqkjs3qdjfb1jfa4',	'2017-01-02 12:04:05',	'2017-01-02 12:36:38',	0,	1),
(218,	'o8nmaugmcoqceevmlrt1qtjhb3',	'2017-01-02 14:30:40',	'2017-01-02 14:30:41',	0,	1),
(219,	'spbt85abbpcj16r1ln8kfkppf3',	'2017-01-02 14:30:41',	'2017-01-02 14:30:42',	0,	1),
(220,	'btrucv2dpvjan5ue4an2scqm35',	'2017-01-02 16:46:34',	'2017-01-02 16:59:07',	0,	1),
(221,	'it41jdnspa3t2hoahlkc260bg2',	'2017-01-02 20:24:01',	'2017-01-02 20:24:35',	0,	1),
(222,	'jajtpdha23l2jukob1l06qejt0',	'2017-01-02 22:24:37',	'2017-01-03 00:15:06',	0,	1),
(223,	'mjh8l77jkosbph2ham75slqnd7',	'2017-01-03 00:15:12',	'2017-01-03 00:15:13',	0,	1),
(224,	'd6ku07p9pssvvkkbu716mjthk3',	'2017-01-03 01:38:36',	'2017-01-03 02:34:05',	0,	1),
(225,	'16p9qth8pd4khj6cjhj77l9sr0',	'2017-01-03 01:46:17',	'2017-01-03 03:16:06',	0,	1),
(226,	'k3nkk1oc1g9795c7bqhgr66pi7',	'2017-01-03 01:48:03',	'2017-01-03 01:48:04',	0,	1),
(227,	'pf3v0moab0kl789co9ur0t47f7',	'2017-01-03 03:16:12',	'2017-01-03 05:41:49',	0,	1),
(228,	'77b4u63u3khslg0ko2ghp3c7r7',	'2017-01-03 03:55:47',	'2017-01-03 04:12:17',	0,	1),
(229,	'8tgdvqtrms2dfkuonhrk29j894',	'2017-01-03 04:10:29',	'2017-01-03 04:10:30',	0,	1),
(230,	'utrprblqli9sheshdlfqc529m3',	'2017-01-03 04:10:29',	'2017-01-03 04:10:30',	0,	1),
(231,	'1sa9rebc2h5j5vn1j90cgceh85',	'2017-01-03 04:10:29',	'2017-01-03 04:10:30',	0,	1),
(232,	'es29773s364g9pn577sttg28h4',	'2017-01-03 08:36:24',	'2017-01-03 08:37:01',	0,	1),
(233,	'93t34aiodlejru5bf8n11o1dp6',	'2017-01-03 08:42:48',	'2017-01-03 08:42:49',	0,	1),
(234,	'st0fp4e139lv8ul9br0cnule87',	'2017-01-03 09:44:39',	'2017-01-03 09:44:40',	0,	1),
(235,	'rd8t2m7jdru28suljvhrqru830',	'2017-01-03 09:44:40',	'2017-01-03 09:48:58',	0,	1),
(236,	'2r2haou3cbteeo0215712vtk41',	'2017-01-03 10:48:52',	'2017-01-03 10:48:55',	0,	1),
(237,	'2qsqhr257tnvtejd38d02mq8v2',	'2017-01-03 11:41:30',	'2017-01-03 12:10:27',	0,	1),
(238,	'qtkt8mut31mjif80gg5uhk4fg2',	'2017-01-03 12:56:06',	'2017-01-03 12:56:08',	0,	1),
(239,	'64t01tg37l6aca0amuam94mcc6',	'2017-01-03 13:47:17',	'2017-01-03 13:47:18',	0,	1),
(240,	'o8vc7a85dq01jrg03j14cooha3',	'2017-01-03 20:05:39',	'2017-01-03 20:06:13',	0,	1),
(241,	't8hqfgslohpi974ikfnhmln7r4',	'2017-01-03 20:32:43',	'2017-01-03 20:32:44',	0,	1),
(242,	'h6fakfckvke50gas03v5s236s3',	'2017-01-03 20:32:44',	'2017-01-03 20:33:19',	0,	1),
(243,	's2mps3vqmmfgablu7f7rpt5bn4',	'2017-01-03 20:33:56',	'2017-01-03 20:34:01',	0,	1),
(244,	'266ih3e3agh906fsidglg85132',	'2017-01-03 21:59:43',	'2017-01-03 21:59:43',	0,	1),
(245,	'gn1n49p3rd2d4di7opcmr4pcn0',	'2017-01-03 23:05:30',	'2017-01-03 23:05:31',	0,	1),
(246,	'4sad157fs216tjbqsct926g3l7',	'2017-01-04 00:39:06',	'2017-01-04 00:53:13',	0,	1),
(247,	'ds9a7504qe5gjbfpf0oq8d35h0',	'2017-01-04 01:58:54',	'2017-01-04 01:58:55',	0,	1),
(248,	'f2k8i7t9l1j4c3q47o956ap3j2',	'2017-01-04 02:23:13',	'2017-01-04 02:23:13',	0,	1),
(249,	'vjgadb8q54ojjnrm69f22u4bl7',	'2017-01-04 02:23:14',	'2017-01-04 02:23:36',	0,	1),
(250,	'n7lgp867lubr62sa1pqjp19ev3',	'2017-01-04 02:47:06',	'2017-01-04 02:54:19',	0,	1),
(251,	'm4p1hqbfrqav811ladbu1o2qs4',	'2017-01-04 03:52:28',	'2017-01-04 03:54:05',	0,	1),
(252,	'lb3uor4nit64l4k4fko78bfa51',	'2017-01-04 04:23:10',	'2017-01-04 04:53:15',	0,	1),
(253,	'6r7e1t25a24jguq9sca3lcoss4',	'2017-01-04 04:33:37',	'2017-01-04 04:33:37',	0,	1),
(254,	'omjfj0hapfks0foa5an1s8u6q2',	'2017-01-04 05:06:21',	'2017-01-04 05:07:14',	0,	1);

DROP TABLE IF EXISTS `log_visitor_info`;
CREATE TABLE `log_visitor_info` (
  `visitor_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Visitor ID',
  `http_referer` varchar(255) DEFAULT NULL COMMENT 'HTTP Referrer',
  `http_user_agent` varchar(255) DEFAULT NULL COMMENT 'HTTP User-Agent',
  `http_accept_charset` varchar(255) DEFAULT NULL COMMENT 'HTTP Accept-Charset',
  `http_accept_language` varchar(255) DEFAULT NULL COMMENT 'HTTP Accept-Language',
  `server_addr` varbinary(16) DEFAULT NULL,
  `remote_addr` varbinary(16) DEFAULT NULL,
  PRIMARY KEY (`visitor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log Visitor Info Table';

INSERT INTO `log_visitor_info` (`visitor_id`, `http_referer`, `http_user_agent`, `http_accept_charset`, `http_accept_language`, `server_addr`, `remote_addr`) VALUES
(1,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('75781285')),
(2,	NULL,	'python-requests/2.7.0 CPython/2.7.0 Windows/2003Server',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('6F492EA9')),
(3,	NULL,	'Wget(linux)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('BD049B20')),
(4,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F75862')),
(5,	NULL,	'Mozilla/5.0 (Windows NT 6.2;en-US) AppleWebKit/537.36 (KHTML, live Gecko) Chrome/40.0.1709.95 Safari/537',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('3652DA11')),
(6,	NULL,	'PycURL/7.19.5 libcurl/7.38.0 GnuTLS/3.3.8 zlib/1.2.8 libidn/1.29 libssh2/1.4.3 librtmp/2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('330F2C7A')),
(7,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('75781285')),
(8,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('8BDA80FE')),
(9,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646BCA5')),
(10,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646BCA6')),
(11,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646AD0B')),
(12,	NULL,	'Java/1.6.0_41',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646B822')),
(13,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646AD29')),
(14,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646AD0B')),
(15,	NULL,	'Java/1.6.0_41',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646B822')),
(16,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('67F1E935')),
(17,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F7485A')),
(18,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F7481A')),
(19,	NULL,	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('59F8AC10')),
(20,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('59F8AC10')),
(21,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('59F8AC10')),
(22,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('ADD0D5C6')),
(23,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F71B43')),
(24,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('8BDA80FE')),
(25,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646AD33')),
(26,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646AD0B')),
(27,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646BCA9')),
(28,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646AD0E')),
(29,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) SkypeUriPreview Preview/0.5',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17636576')),
(30,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C6CCF0DD')),
(31,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('8BDA80FE')),
(32,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646BCA7')),
(33,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646AD09')),
(34,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('1B4C077C')),
(35,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('68D67443')),
(36,	'http://52.62.250.130:80/',	'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('4010D60C')),
(37,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F71B62')),
(38,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('8BDA80FE')),
(39,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646AD0A')),
(40,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646AD0B')),
(41,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('9646AD0C')),
(42,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F7483A')),
(43,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F74814')),
(44,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) SkypeUriPreview Preview/0.5',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('0D4CF1D2')),
(45,	NULL,	'Mozilla/5.0 (Linux; Android 6.0.1; SM-G900I Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.85 Mobile Safari/537.36',	NULL,	'en-AU,en;q=0.8,en-GB;q=0.6,en-US;q=0.4,es;q=0.2',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(46,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(47,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(48,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(49,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(50,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(51,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(52,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(53,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(54,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(55,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(56,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(57,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(58,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(59,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(60,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(61,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(62,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(63,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(64,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(65,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(66,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(67,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(68,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(69,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(70,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(71,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(72,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(73,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(74,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(75,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(76,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(77,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5159C193')),
(78,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5BD6A31F')),
(79,	NULL,	'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('5E6631DA')),
(80,	'http://auswarehouse.corre.com.au/',	'Mozilla/5.0 (Linux; Android 6.0.1; SM-G900I Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.85 Mobile Safari/537.36',	NULL,	'en-AU,en;q=0.8,en-GB;q=0.6,en-US;q=0.4,es;q=0.2',	UNHEX('AC1F001B'),	UNHEX('8BDA80FE')),
(81,	'http://auswarehouse.corre.com.au/complete-whitening-scope.html',	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('8BDA80FE')),
(82,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('1B4C077C')),
(83,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('BB545FF5')),
(84,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('1B4C077C')),
(85,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(86,	NULL,	'Mozilla/5.0 (Linux; Android 4.4.2; SM-G900I Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36',	NULL,	'en-AU,en-GB;q=0.8,en-US;q=0.6,en;q=0.4',	UNHEX('AC1F001B'),	UNHEX('7811232B')),
(87,	NULL,	'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0 Mobile/14C92 Safari/602.1',	NULL,	'en-au',	UNHEX('AC1F001B'),	UNHEX('781251FC')),
(88,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(89,	'http://auswarehouse.corre.com.au/',	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('1B4C077C')),
(90,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(91,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',	NULL,	'en-AU',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(92,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('1B4C077C')),
(93,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(94,	'http://auswarehouse.corre.com.au/complete-whitening-scope.html',	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(95,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(96,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('75781287')),
(97,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('75781287')),
(98,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(99,	NULL,	'Wget(linux)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('B39B3206')),
(100,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('75781287')),
(101,	NULL,	'Mozilla/5.0',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('7AE4465B')),
(102,	NULL,	'Mozilla/5.0',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('7AE4465B')),
(103,	NULL,	'Mozilla/5.0',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('7AE4465B')),
(104,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('B15D9826')),
(105,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('BABEA7CF')),
(106,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C3EA060B')),
(107,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F7582A')),
(108,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('75781286')),
(109,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('68944B12')),
(110,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(111,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('68944B7A')),
(112,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(113,	NULL,	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('470692BA')),
(114,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('470692BA')),
(115,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('470692BA')),
(116,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('8AFF5F57')),
(117,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F7084A')),
(118,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('75781284')),
(119,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7505EACC')),
(120,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(121,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('D4813E4F')),
(122,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C39A2984')),
(123,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C39A2984')),
(124,	NULL,	'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('D4813E4F')),
(125,	NULL,	'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C39A2984')),
(126,	NULL,	'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C39A2984')),
(127,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(128,	NULL,	'Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; en) Presto/2.8.131 Version/11.11',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('6A273CB4')),
(129,	NULL,	'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('6A273CB4')),
(130,	NULL,	'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('6A273CB4')),
(131,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('68944B6B')),
(132,	NULL,	'Mozilla/5.0 (Linux; Android 6.0.1; SM-G900I Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.85 Mobile Safari/537.36',	NULL,	'en-AU,en;q=0.8,en-GB;q=0.6,en-US;q=0.4,es;q=0.2',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(133,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F71B5B')),
(134,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7505EACC')),
(135,	NULL,	'Mozilla/5.0 (Linux; Android 6.0.1; SM-G900I Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.85 Mobile Safari/537.36',	NULL,	'en-AU,en;q=0.8,en-GB;q=0.6,en-US;q=0.4,es;q=0.2',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(136,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('D457A892')),
(137,	'http://auswarehouse.corre.com.au/',	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7505EACC')),
(138,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7505EACC')),
(139,	NULL,	'python-requests/2.7.0 CPython/2.7.9 Windows/2003Server',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('73E7DE91')),
(140,	NULL,	'python-requests/2.7.0 CPython/2.7.9 Windows/2003Server',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('73E7DE91')),
(141,	NULL,	'Mozilla/5.0 (Linux; Android 6.0.1; SM-G900I Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.85 Mobile Safari/537.36',	NULL,	'en-AU,en;q=0.8,en-GB;q=0.6,en-US;q=0.4,es;q=0.2',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(142,	NULL,	'Wget(linux)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('BB26B6DA')),
(143,	NULL,	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(144,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('2BF5AE67')),
(145,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(146,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(147,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(148,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(149,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(150,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(151,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(152,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(153,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(154,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(155,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(156,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(157,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(158,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(159,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(160,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(161,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(162,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(163,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(164,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(165,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(166,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(167,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(168,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(169,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(170,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(171,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(172,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(173,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(174,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(175,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(176,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1FAC5FA1')),
(177,	NULL,	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(178,	NULL,	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(179,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('74609FC9')),
(180,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(181,	NULL,	'python-requests/2.7.0 CPython/2.7.0 Windows/2003Server',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('6F492EA9')),
(182,	'http://52.62.250.130:80/',	'Mozilla/5.0 zgrab/0.x',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('8DD47A70')),
(183,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(184,	NULL,	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C6144562')),
(185,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C6144562')),
(186,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C6144562')),
(187,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('1F38965B')),
(188,	NULL,	'Mozilla/5.0 (Windows NT 6.2;en-US) AppleWebKit/537.36 (KHTML, live Gecko) Chrome/41.0.1737.2 Safari/537',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('36595DEF')),
(189,	NULL,	'Wget(linux)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('BACD534C')),
(190,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(191,	NULL,	'Python-urllib/2.7',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('B9280426')),
(192,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('67F1E939')),
(193,	NULL,	'Mozilla/5.0 (Linux; Android 6.0.1; SM-G900I Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.85 Mobile Safari/537.36',	NULL,	'en-AU,en;q=0.8,en-GB;q=0.6,en-US;q=0.4,es;q=0.2',	UNHEX('AC1F001B'),	UNHEX('8BDA80FE')),
(194,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7500154D')),
(195,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F7083A')),
(196,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('68944B1B')),
(197,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7500154D')),
(198,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7500154D')),
(199,	'http://52.62.250.130',	'Go 1.1 package http',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('B67637DD')),
(200,	NULL,	'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('DEBA15D3')),
(201,	'http://52.62.250.130',	'Go 1.1 package http',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('B67637D1')),
(202,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(203,	NULL,	'Mozilla/5.0 (Linux; Android 6.0.1; SM-G900I Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.85 Mobile Safari/537.36',	NULL,	'en-AU,en;q=0.8,en-GB;q=0.6,en-US;q=0.4,es;q=0.2',	UNHEX('AC1F001B'),	UNHEX('31C34121')),
(204,	NULL,	'curl/7.17.1 (mips-unknown-linux-gnu) libcurl/7.17.1 OpenSSL/0.9.8i zlib/1.2.3',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('B1261422')),
(205,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('68944B5B')),
(206,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('68944B03')),
(207,	NULL,	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(208,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('68944B4A')),
(209,	NULL,	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C614454A')),
(210,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C614454A')),
(211,	NULL,	NULL,	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('C614454A')),
(212,	NULL,	'Mozilla/5.0 (Linux; Android 6.0.1; SM-G900I Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.85 Mobile Safari/537.36',	NULL,	'en-AU,en;q=0.8,en-GB;q=0.6,en-US;q=0.4,es;q=0.2',	UNHEX('AC1F001B'),	UNHEX('31C35EAC')),
(213,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7500154D')),
(214,	NULL,	'Wget(linux)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('B1C2C764')),
(215,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(216,	NULL,	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(217,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7500154D')),
(218,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7500154D')),
(219,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7500154D')),
(220,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7500154D')),
(221,	'http://auswarehouse.corre.com.au/best-sellers.html',	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(222,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(223,	'http://auswarehouse.corre.com.au/grocery/lorem-ipsum-dolor',	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(224,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7500154D')),
(225,	'http://auswarehouse.corre.com.au/grocery',	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(226,	NULL,	'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('4266065E')),
(227,	'http://auswarehouse.corre.com.au/',	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(228,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('7500154D')),
(229,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) SkypeUriPreview Preview/0.5',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('0D4CF1D2')),
(230,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) SkypeUriPreview Preview/0.5',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17636576')),
(231,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) SkypeUriPreview Preview/0.5',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('0D4CF1D2')),
(232,	NULL,	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(233,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F71B22')),
(234,	NULL,	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(235,	NULL,	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(236,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F71B3B')),
(237,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(238,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('1B4CA00C')),
(239,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F75822')),
(240,	NULL,	'Mozilla/5.0 (iPhone; CPU iPhone OS 10_1_1 like Mac OS X) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0 Mobile/14B100 Safari/602.1',	NULL,	'en-au',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(241,	NULL,	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(242,	NULL,	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(243,	'http://auswarehouse.corre.com.au/',	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('7CBEC62B')),
(244,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('75781286')),
(245,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(246,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(247,	NULL,	'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('4266065E')),
(248,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('1B4CA00C')),
(249,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('1B4CA00C')),
(250,	NULL,	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(251,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8',	UNHEX('AC1F001B'),	UNHEX('65B0E8A2')),
(252,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-GB,en-US;q=0.8,en;q=0.6',	UNHEX('AC1F001B'),	UNHEX('DCF41E9E')),
(253,	NULL,	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',	NULL,	NULL,	UNHEX('AC1F001B'),	UNHEX('17F7484A')),
(254,	NULL,	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',	NULL,	'en-US,en;q=0.8,vi;q=0.6',	UNHEX('AC1F001B'),	UNHEX('1B4CA00C'));

DROP TABLE IF EXISTS `log_visitor_online`;
CREATE TABLE `log_visitor_online` (
  `visitor_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Visitor ID',
  `visitor_type` varchar(1) NOT NULL COMMENT 'Visitor Type',
  `remote_addr` varbinary(16) DEFAULT NULL,
  `first_visit_at` timestamp NULL DEFAULT NULL COMMENT 'First Visit Time',
  `last_visit_at` timestamp NULL DEFAULT NULL COMMENT 'Last Visit Time',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer ID',
  `last_url` varchar(255) DEFAULT NULL COMMENT 'Last URL',
  PRIMARY KEY (`visitor_id`),
  KEY `IDX_LOG_VISITOR_ONLINE_VISITOR_TYPE` (`visitor_type`),
  KEY `IDX_LOG_VISITOR_ONLINE_FIRST_VISIT_AT_LAST_VISIT_AT` (`first_visit_at`,`last_visit_at`),
  KEY `IDX_LOG_VISITOR_ONLINE_CUSTOMER_ID` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log Visitor Online Table';


DROP TABLE IF EXISTS `magik_blog`;
CREATE TABLE `magik_blog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `title_slug` varchar(255) DEFAULT NULL,
  `stores_selected` varchar(100) DEFAULT NULL,
  `categories_selected` varchar(100) DEFAULT NULL,
  `short_description` mediumtext,
  `blog_content` longtext,
  `short_blog_content` text,
  `tags` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `display_order` int(11) DEFAULT '0',
  `enable_comment` smallint(6) DEFAULT '1',
  `status` smallint(6) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `magik_blog_category`;
CREATE TABLE `magik_blog_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `title_slug` varchar(255) DEFAULT NULL,
  `short_description` mediumtext,
  `meta_keywords` text,
  `meta_description` text,
  `display_order` int(11) DEFAULT '0',
  `cat_pid` int(11) DEFAULT '0',
  `subcategory` int(11) DEFAULT NULL,
  `stores_selected` varchar(255) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `magik_blog_comment`;
CREATE TABLE `magik_blog_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) unsigned DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `comment` text,
  `status` smallint(6) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `magik_socialbar`;
CREATE TABLE `magik_socialbar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `show_socialsites` varchar(255) DEFAULT NULL,
  `social_block_code` text,
  `show_pagelocation` varchar(150) DEFAULT NULL,
  `show_category` varchar(255) DEFAULT NULL,
  `store_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `magik_socialsites`;
CREATE TABLE `magik_socialsites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `favicon` varchar(255) NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `magik_socialsites` (`id`, `name`, `favicon`, `url`) VALUES
(1,	'Delicious',	'delicious.png',	'http://delicious.com/post?url=PERMALINK&amp;title=TITLE&amp;notes=EXCERPT'),
(2,	'Digg',	'digg.png',	'http://digg.com/submit?phase=2&amp;url=PERMALINK&amp;title=TITLE&amp;bodytext=EXCERPT'),
(3,	'Facebook',	'facebook.png',	'http://www.facebook.com/share.php?u=PERMALINK&amp;t=TITLE'),
(4,	'LinkedIn',	'linkedin.png',	'http://www.linkedin.com/shareArticle?mini=true&amp;url=PERMALINK&amp;title=TITLE&amp;source=BLOGNAME&amp;summary=EXCERPT'),
(5,	'Reddit',	'reddit.png',	'http://reddit.com/submit?url=PERMALINK&amp;title=TITLE'),
(6,	'StumbleUpon',	'stumbleupon.png',	'http://www.stumbleupon.com/submit?url=PERMALINK&amp;title=TITLE'),
(7,	'Tumblr',	'tumblr.png',	'http://www.tumblr.com/share?v=3&amp;u=PERMALINK&amp;t=TITLE&amp;s=EXCERPT'),
(8,	'Twitter',	'twitter.png',	'http://twitter.com/home?status=TITLE%20-%20PERMALINK'),
(9,	'Pinterest',	'PinExt.png',	'http://pinterest.com/pin/create/link/?url=PERMALINK&amp;media=Productmedia&amp;description=DESCRIPTION'),
(10,	'Google Plus',	'googleplus.png',	'https://plus.google.com/share?url=PERMALINK'),
(16,	'RSS',	'rss.png',	'FEEDLINK');

DROP TABLE IF EXISTS `newsletter_problem`;
CREATE TABLE `newsletter_problem` (
  `problem_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Problem Id',
  `subscriber_id` int(10) unsigned DEFAULT NULL COMMENT 'Subscriber Id',
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `problem_error_code` int(10) unsigned DEFAULT '0' COMMENT 'Problem Error Code',
  `problem_error_text` varchar(200) DEFAULT NULL COMMENT 'Problem Error Text',
  PRIMARY KEY (`problem_id`),
  KEY `IDX_NEWSLETTER_PROBLEM_SUBSCRIBER_ID` (`subscriber_id`),
  KEY `IDX_NEWSLETTER_PROBLEM_QUEUE_ID` (`queue_id`),
  CONSTRAINT `FK_NEWSLETTER_PROBLEM_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NLTTR_PROBLEM_SUBSCRIBER_ID_NLTTR_SUBSCRIBER_SUBSCRIBER_ID` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Problems';


DROP TABLE IF EXISTS `newsletter_queue`;
CREATE TABLE `newsletter_queue` (
  `queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Queue Id',
  `template_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Template Id',
  `newsletter_type` int(11) DEFAULT NULL COMMENT 'Newsletter Type',
  `newsletter_text` text COMMENT 'Newsletter Text',
  `newsletter_styles` text COMMENT 'Newsletter Styles',
  `newsletter_subject` varchar(200) DEFAULT NULL COMMENT 'Newsletter Subject',
  `newsletter_sender_name` varchar(200) DEFAULT NULL COMMENT 'Newsletter Sender Name',
  `newsletter_sender_email` varchar(200) DEFAULT NULL COMMENT 'Newsletter Sender Email',
  `queue_status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Status',
  `queue_start_at` timestamp NULL DEFAULT NULL COMMENT 'Queue Start At',
  `queue_finish_at` timestamp NULL DEFAULT NULL COMMENT 'Queue Finish At',
  PRIMARY KEY (`queue_id`),
  KEY `IDX_NEWSLETTER_QUEUE_TEMPLATE_ID` (`template_id`),
  CONSTRAINT `FK_NEWSLETTER_QUEUE_TEMPLATE_ID_NEWSLETTER_TEMPLATE_TEMPLATE_ID` FOREIGN KEY (`template_id`) REFERENCES `newsletter_template` (`template_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue';


DROP TABLE IF EXISTS `newsletter_queue_link`;
CREATE TABLE `newsletter_queue_link` (
  `queue_link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Queue Link Id',
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `subscriber_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subscriber Id',
  `letter_sent_at` timestamp NULL DEFAULT NULL COMMENT 'Letter Sent At',
  PRIMARY KEY (`queue_link_id`),
  KEY `IDX_NEWSLETTER_QUEUE_LINK_SUBSCRIBER_ID` (`subscriber_id`),
  KEY `IDX_NEWSLETTER_QUEUE_LINK_QUEUE_ID` (`queue_id`),
  KEY `IDX_NEWSLETTER_QUEUE_LINK_QUEUE_ID_LETTER_SENT_AT` (`queue_id`,`letter_sent_at`),
  CONSTRAINT `FK_NEWSLETTER_QUEUE_LINK_QUEUE_ID_NEWSLETTER_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NLTTR_QUEUE_LNK_SUBSCRIBER_ID_NLTTR_SUBSCRIBER_SUBSCRIBER_ID` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue Link';


DROP TABLE IF EXISTS `newsletter_queue_store_link`;
CREATE TABLE `newsletter_queue_store_link` (
  `queue_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Queue Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  PRIMARY KEY (`queue_id`,`store_id`),
  KEY `IDX_NEWSLETTER_QUEUE_STORE_LINK_STORE_ID` (`store_id`),
  CONSTRAINT `FK_NEWSLETTER_QUEUE_STORE_LINK_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NLTTR_QUEUE_STORE_LNK_QUEUE_ID_NLTTR_QUEUE_QUEUE_ID` FOREIGN KEY (`queue_id`) REFERENCES `newsletter_queue` (`queue_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Queue Store Link';


DROP TABLE IF EXISTS `newsletter_subscriber`;
CREATE TABLE `newsletter_subscriber` (
  `subscriber_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Subscriber Id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store Id',
  `change_status_at` timestamp NULL DEFAULT NULL COMMENT 'Change Status At',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `subscriber_email` varchar(150) DEFAULT NULL COMMENT 'Subscriber Email',
  `subscriber_status` int(11) NOT NULL DEFAULT '0' COMMENT 'Subscriber Status',
  `subscriber_confirm_code` varchar(32) DEFAULT 'NULL' COMMENT 'Subscriber Confirm Code',
  PRIMARY KEY (`subscriber_id`),
  KEY `IDX_NEWSLETTER_SUBSCRIBER_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_NEWSLETTER_SUBSCRIBER_STORE_ID` (`store_id`),
  CONSTRAINT `FK_NEWSLETTER_SUBSCRIBER_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Subscriber';


DROP TABLE IF EXISTS `newsletter_template`;
CREATE TABLE `newsletter_template` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Template Id',
  `template_code` varchar(150) DEFAULT NULL COMMENT 'Template Code',
  `template_text` text COMMENT 'Template Text',
  `template_text_preprocessed` text COMMENT 'Template Text Preprocessed',
  `template_styles` text COMMENT 'Template Styles',
  `template_type` int(10) unsigned DEFAULT NULL COMMENT 'Template Type',
  `template_subject` varchar(200) DEFAULT NULL COMMENT 'Template Subject',
  `template_sender_name` varchar(200) DEFAULT NULL COMMENT 'Template Sender Name',
  `template_sender_email` varchar(200) DEFAULT NULL COMMENT 'Template Sender Email',
  `template_actual` smallint(5) unsigned DEFAULT '1' COMMENT 'Template Actual',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Added At',
  `modified_at` timestamp NULL DEFAULT NULL COMMENT 'Modified At',
  PRIMARY KEY (`template_id`),
  KEY `IDX_NEWSLETTER_TEMPLATE_TEMPLATE_ACTUAL` (`template_actual`),
  KEY `IDX_NEWSLETTER_TEMPLATE_ADDED_AT` (`added_at`),
  KEY `IDX_NEWSLETTER_TEMPLATE_MODIFIED_AT` (`modified_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Newsletter Template';

INSERT INTO `newsletter_template` (`template_id`, `template_code`, `template_text`, `template_text_preprocessed`, `template_styles`, `template_type`, `template_subject`, `template_sender_name`, `template_sender_email`, `template_actual`, `added_at`, `modified_at`) VALUES
(1,	'Example Newsletter Template',	'{{template config_path=\"design/email/header\"}}\n{{inlinecss file=\"email-inline.css\"}}\n\n<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n<tr>\n    <td class=\"full\">\n        <table class=\"columns\">\n            <tr>\n                <td class=\"email-heading\">\n                    <h1>Welcome</h1>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,\n                    sed do eiusmod tempor incididunt ut labore et.</p>\n                </td>\n                <td class=\"store-info\">\n                    <h4>Contact Us</h4>\n                    <p>\n                        {{depend store_phone}}\n                        <b>Call Us:</b>\n                        <a href=\"tel:{{var phone}}\">{{var store_phone}}</a><br>\n                        {{/depend}}\n                        {{depend store_hours}}\n                        <span class=\"no-link\">{{var store_hours}}</span><br>\n                        {{/depend}}\n                        {{depend store_email}}\n                        <b>Email:</b> <a href=\"mailto:{{var store_email}}\">{{var store_email}}</a>\n                        {{/depend}}\n                    </p>\n                </td>\n            </tr>\n        </table>\n    </td>\n</tr>\n<tr>\n    <td class=\"full\">\n        <table class=\"columns\">\n            <tr>\n                <td>\n                    <img width=\"600\" src=\"http://placehold.it/600x200\" class=\"main-image\">\n                </td>\n                <td class=\"expander\"></td>\n            </tr>\n        </table>\n        <table class=\"columns\">\n            <tr>\n                <td class=\"panel\">\n                    <p>Phasellus dictum sapien a neque luctus cursus. Pellentesque sem dolor, fringilla et pharetra\n                    vitae. <a href=\"#\">Click it! &raquo;</a></p>\n                </td>\n                <td class=\"expander\"></td>\n            </tr>\n        </table>\n    </td>\n</tr>\n<tr>\n    <td>\n        <table class=\"row\">\n            <tr>\n                <td class=\"half left wrapper\">\n                    <table class=\"columns\">\n                        <tr>\n                            <td>\n                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor\n                                incididunt ut labore et. Lorem ipsum dolor sit amet, consectetur adipisicing elit,\n                                sed do eiusmod tempor incididunt ut labore et. Lorem ipsum dolor sit amet.</p>\n                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor\n                                incididunt ut labore et. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed\n                                do eiusmod tempor incididunt ut labore et. Lorem ipsum dolor sit amet.</p>\n                                <table class=\"button\">\n                                    <tr>\n                                        <td>\n                                            <a href=\"#\">Click Me!</a>\n                                        </td>\n                                    </tr>\n                                </table>\n                            </td>\n                            <td class=\"expander\"></td>\n                        </tr>\n                    </table>\n                </td>\n                <td class=\"half right wrapper last\">\n                    <table class=\"columns\">\n                        <tr>\n                            <td class=\"panel sidebar-links\">\n                                <h6>Header Thing</h6>\n                                <p>Sub-head or something</p>\n                                <table>\n                                    <tr>\n                                        <td>\n                                            <p><a href=\"#\">Just a Plain Link &raquo;</a></p>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <hr/>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <p><a href=\"#\">Just a Plain Link &raquo;</a></p>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <hr/>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <p><a href=\"#\">Just a Plain Link &raquo;</a></p>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <hr/>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <p><a href=\"#\">Just a Plain Link &raquo;</a></p>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <hr/>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <p><a href=\"#\">Just a Plain Link &raquo;</a></p>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <hr/>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <p><a href=\"#\">Just a Plain Link &raquo;</a></p>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <hr/>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>\n                                            <p><a href=\"#\">Just a Plain Link &raquo;</a></p>\n                                        </td>\n                                    </tr>\n                                    <tr><td>&nbsp;</td></tr>\n                                </table>\n                            </td>\n                            <td class=\"expander\"></td>\n                        </tr>\n                    </table>\n                    <br>\n                    <table class=\"columns\">\n                        <tr>\n                            <td class=\"panel\">\n                                <h6>Connect With Us:</h6>\n                                <table class=\"social-button facebook\">\n                                    <tr>\n                                        <td>\n                                            <a href=\"#\">Facebook</a>\n                                        </td>\n                                    </tr>\n                                </table>\n                                <hr>\n                                <table class=\"social-button twitter\">\n                                    <tr>\n                                        <td>\n                                            <a href=\"#\">Twitter</a>\n                                        </td>\n                                    </tr>\n                                </table>\n                                <hr>\n                                <table class=\"social-button google-plus\">\n                                    <tr>\n                                        <td>\n                                            <a href=\"#\">Google +</a>\n                                        </td>\n                                    </tr>\n                                </table>\n                                <br>\n                                <h6>Contact Info:</h6>\n                                {{depend store_phone}}\n                                <p>\n                                    <b>Call Us:</b>\n                                    <a href=\"tel:{{var phone}}\">{{var store_phone}}</a>\n                                </p>\n                                {{/depend}}\n                                {{depend store_hours}}\n                                <p><span class=\"no-link\">{{var store_hours}}</span><br></p>\n                                {{/depend}}\n                                {{depend store_email}}\n                                <p><b>Email:</b> <a href=\"mailto:{{var store_email}}\">{{var store_email}}</a></p>\n                                {{/depend}}\n                            </td>\n                            <td class=\"expander\"></td>\n                        </tr>\n                    </table>\n                </td>\n            </tr>\n        </table>\n        <table class=\"row\">\n            <tr>\n                <td class=\"full wrapper\">\n                    {{block type=\"catalog/product_new\" template=\"email/catalog/product/new.phtml\" products_count=\"4\"\n                    column_count=\"4\" }}\n                </td>\n            </tr>\n        </table>\n        <table class=\"row\">\n            <tr>\n                <td class=\"full wrapper last\">\n                    <table class=\"columns\">\n                        <tr>\n                            <td align=\"center\">\n                                <center>\n                                    <p><a href=\"#\">Terms</a> | <a href=\"#\">Privacy</a> | <a href=\"#\">Unsubscribe</a></p>\n                                </center>\n                            </td>\n                            <td class=\"expander\"></td>\n                        </tr>\n                    </table>\n                </td>\n            </tr>\n        </table>\n    </td>\n</tr>\n</table>\n\n{{template config_path=\"design/email/footer\"}}',	NULL,	NULL,	2,	'Example Subject',	'Owner',	'owner@example.com',	1,	'2016-12-14 06:01:01',	'2016-12-14 06:01:01');

DROP TABLE IF EXISTS `oauth_consumer`;
CREATE TABLE `oauth_consumer` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `name` varchar(255) NOT NULL COMMENT 'Name of consumer',
  `key` varchar(32) NOT NULL COMMENT 'Key code',
  `secret` varchar(32) NOT NULL COMMENT 'Secret code',
  `callback_url` varchar(255) DEFAULT NULL COMMENT 'Callback URL',
  `rejected_callback_url` varchar(255) NOT NULL COMMENT 'Rejected callback URL',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_OAUTH_CONSUMER_KEY` (`key`),
  UNIQUE KEY `UNQ_OAUTH_CONSUMER_SECRET` (`secret`),
  KEY `IDX_OAUTH_CONSUMER_CREATED_AT` (`created_at`),
  KEY `IDX_OAUTH_CONSUMER_UPDATED_AT` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Consumers';


DROP TABLE IF EXISTS `oauth_nonce`;
CREATE TABLE `oauth_nonce` (
  `nonce` varchar(32) NOT NULL COMMENT 'Nonce String',
  `timestamp` int(10) unsigned NOT NULL COMMENT 'Nonce Timestamp',
  UNIQUE KEY `UNQ_OAUTH_NONCE_NONCE` (`nonce`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='oauth_nonce';


DROP TABLE IF EXISTS `oauth_token`;
CREATE TABLE `oauth_token` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity ID',
  `consumer_id` int(10) unsigned NOT NULL COMMENT 'Consumer ID',
  `admin_id` int(10) unsigned DEFAULT NULL COMMENT 'Admin user ID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer user ID',
  `type` varchar(16) NOT NULL COMMENT 'Token Type',
  `token` varchar(32) NOT NULL COMMENT 'Token',
  `secret` varchar(32) NOT NULL COMMENT 'Token Secret',
  `verifier` varchar(32) DEFAULT NULL COMMENT 'Token Verifier',
  `callback_url` varchar(255) NOT NULL COMMENT 'Token Callback URL',
  `revoked` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Token revoked',
  `authorized` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Token authorized',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Token creation timestamp',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_OAUTH_TOKEN_TOKEN` (`token`),
  KEY `IDX_OAUTH_TOKEN_CONSUMER_ID` (`consumer_id`),
  KEY `FK_OAUTH_TOKEN_ADMIN_ID_ADMIN_USER_USER_ID` (`admin_id`),
  KEY `FK_OAUTH_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`),
  CONSTRAINT `FK_OAUTH_TOKEN_ADMIN_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`admin_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_OAUTH_TOKEN_CONSUMER_ID_OAUTH_CONSUMER_ENTITY_ID` FOREIGN KEY (`consumer_id`) REFERENCES `oauth_consumer` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_OAUTH_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='OAuth Tokens';


DROP TABLE IF EXISTS `paypal_cert`;
CREATE TABLE `paypal_cert` (
  `cert_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Cert Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `content` text COMMENT 'Content',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`cert_id`),
  KEY `IDX_PAYPAL_CERT_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_PAYPAL_CERT_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Certificate Table';


DROP TABLE IF EXISTS `paypal_payment_transaction`;
CREATE TABLE `paypal_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `UNQ_PAYPAL_PAYMENT_TRANSACTION_TXN_ID` (`txn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='PayPal Payflow Link Payment Transaction';


DROP TABLE IF EXISTS `paypal_settlement_report`;
CREATE TABLE `paypal_settlement_report` (
  `report_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Report Id',
  `report_date` timestamp NULL DEFAULT NULL COMMENT 'Report Date',
  `account_id` varchar(64) DEFAULT NULL COMMENT 'Account Id',
  `filename` varchar(24) DEFAULT NULL COMMENT 'Filename',
  `last_modified` timestamp NULL DEFAULT NULL COMMENT 'Last Modified',
  PRIMARY KEY (`report_id`),
  UNIQUE KEY `UNQ_PAYPAL_SETTLEMENT_REPORT_REPORT_DATE_ACCOUNT_ID` (`report_date`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Settlement Report Table';


DROP TABLE IF EXISTS `paypal_settlement_report_row`;
CREATE TABLE `paypal_settlement_report_row` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Row Id',
  `report_id` int(10) unsigned NOT NULL COMMENT 'Report Id',
  `transaction_id` varchar(19) DEFAULT NULL COMMENT 'Transaction Id',
  `invoice_id` varchar(127) DEFAULT NULL COMMENT 'Invoice Id',
  `paypal_reference_id` varchar(19) DEFAULT NULL COMMENT 'Paypal Reference Id',
  `paypal_reference_id_type` varchar(3) DEFAULT NULL COMMENT 'Paypal Reference Id Type',
  `transaction_event_code` varchar(5) DEFAULT NULL COMMENT 'Transaction Event Code',
  `transaction_initiation_date` timestamp NULL DEFAULT NULL COMMENT 'Transaction Initiation Date',
  `transaction_completion_date` timestamp NULL DEFAULT NULL COMMENT 'Transaction Completion Date',
  `transaction_debit_or_credit` varchar(2) NOT NULL DEFAULT 'CR' COMMENT 'Transaction Debit Or Credit',
  `gross_transaction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000' COMMENT 'Gross Transaction Amount',
  `gross_transaction_currency` varchar(3) DEFAULT '' COMMENT 'Gross Transaction Currency',
  `fee_debit_or_credit` varchar(2) DEFAULT NULL COMMENT 'Fee Debit Or Credit',
  `fee_amount` decimal(20,6) NOT NULL DEFAULT '0.000000' COMMENT 'Fee Amount',
  `fee_currency` varchar(3) DEFAULT NULL COMMENT 'Fee Currency',
  `custom_field` varchar(255) DEFAULT NULL COMMENT 'Custom Field',
  `consumer_id` varchar(127) DEFAULT NULL COMMENT 'Consumer Id',
  `payment_tracking_id` varchar(255) DEFAULT NULL COMMENT 'Payment Tracking ID',
  `store_id` varchar(50) DEFAULT NULL COMMENT 'Store ID',
  PRIMARY KEY (`row_id`),
  KEY `IDX_PAYPAL_SETTLEMENT_REPORT_ROW_REPORT_ID` (`report_id`),
  CONSTRAINT `FK_E183E488F593E0DE10C6EBFFEBAC9B55` FOREIGN KEY (`report_id`) REFERENCES `paypal_settlement_report` (`report_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Paypal Settlement Report Row Table';


DROP TABLE IF EXISTS `permission_block`;
CREATE TABLE `permission_block` (
  `block_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Block ID',
  `block_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Block Name',
  `is_allowed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Mark that block can be processed by filters',
  PRIMARY KEY (`block_id`),
  UNIQUE KEY `UNQ_PERMISSION_BLOCK_BLOCK_NAME` (`block_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='System blocks that can be processed via content filter';

INSERT INTO `permission_block` (`block_id`, `block_name`, `is_allowed`) VALUES
(1,	'core/template',	1),
(2,	'catalog/product_new',	1),
(3,	'cms/block',	1),
(4,	'catalog/product_list',	1),
(5,	'blogmate/index',	1);

DROP TABLE IF EXISTS `permission_variable`;
CREATE TABLE `permission_variable` (
  `variable_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Variable ID',
  `variable_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Config Path',
  `is_allowed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Mark that config can be processed by filters',
  PRIMARY KEY (`variable_id`,`variable_name`),
  UNIQUE KEY `UNQ_PERMISSION_VARIABLE_VARIABLE_NAME` (`variable_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='System variables that can be processed via content filter';

INSERT INTO `permission_variable` (`variable_id`, `variable_name`, `is_allowed`) VALUES
(1,	'trans_email/ident_support/name',	1),
(2,	'trans_email/ident_support/email',	1),
(3,	'web/unsecure/base_url',	1),
(4,	'web/secure/base_url',	1),
(5,	'trans_email/ident_general/name',	1),
(6,	'trans_email/ident_general/email',	1),
(7,	'trans_email/ident_sales/name',	1),
(8,	'trans_email/ident_sales/email',	1),
(9,	'trans_email/ident_custom1/name',	1),
(10,	'trans_email/ident_custom1/email',	1),
(11,	'trans_email/ident_custom2/name',	1),
(12,	'trans_email/ident_custom2/email',	1),
(13,	'general/store_information/name',	1),
(14,	'general/store_information/phone',	1),
(15,	'general/store_information/address',	1);

DROP TABLE IF EXISTS `persistent_session`;
CREATE TABLE `persistent_session` (
  `persistent_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Session id',
  `key` varchar(50) NOT NULL COMMENT 'Unique cookie key',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  `info` text COMMENT 'Session Data',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`persistent_id`),
  UNIQUE KEY `IDX_PERSISTENT_SESSION_KEY` (`key`),
  UNIQUE KEY `IDX_PERSISTENT_SESSION_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_PERSISTENT_SESSION_UPDATED_AT` (`updated_at`),
  KEY `FK_PERSISTENT_SESSION_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_PERSISTENT_SESSION_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_PERSISTENT_SESSION_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Persistent Session';


DROP TABLE IF EXISTS `poll`;
CREATE TABLE `poll` (
  `poll_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Poll Id',
  `poll_title` varchar(255) DEFAULT NULL COMMENT 'Poll title',
  `votes_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Votes Count',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  `date_posted` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date posted',
  `date_closed` timestamp NULL DEFAULT NULL COMMENT 'Date closed',
  `active` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Is active',
  `closed` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is closed',
  `answers_display` smallint(6) DEFAULT NULL COMMENT 'Answers display',
  PRIMARY KEY (`poll_id`),
  KEY `IDX_POLL_STORE_ID` (`store_id`),
  CONSTRAINT `FK_POLL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Poll';


DROP TABLE IF EXISTS `poll_answer`;
CREATE TABLE `poll_answer` (
  `answer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Answer Id',
  `poll_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Poll Id',
  `answer_title` varchar(255) DEFAULT NULL COMMENT 'Answer title',
  `votes_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Votes Count',
  `answer_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Answers display',
  PRIMARY KEY (`answer_id`),
  KEY `IDX_POLL_ANSWER_POLL_ID` (`poll_id`),
  CONSTRAINT `FK_POLL_ANSWER_POLL_ID_POLL_POLL_ID` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`poll_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Poll Answers';


DROP TABLE IF EXISTS `poll_store`;
CREATE TABLE `poll_store` (
  `poll_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Poll Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`poll_id`,`store_id`),
  KEY `IDX_POLL_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_POLL_STORE_POLL_ID_POLL_POLL_ID` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`poll_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_POLL_STORE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Poll Store';


DROP TABLE IF EXISTS `poll_vote`;
CREATE TABLE `poll_vote` (
  `vote_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Vote Id',
  `poll_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Poll Id',
  `poll_answer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Poll answer id',
  `ip_address` varbinary(16) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer id',
  `vote_time` timestamp NULL DEFAULT NULL COMMENT 'Date closed',
  PRIMARY KEY (`vote_id`),
  KEY `IDX_POLL_VOTE_POLL_ANSWER_ID` (`poll_answer_id`),
  CONSTRAINT `FK_POLL_VOTE_POLL_ANSWER_ID_POLL_ANSWER_ANSWER_ID` FOREIGN KEY (`poll_answer_id`) REFERENCES `poll_answer` (`answer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Poll Vote';


DROP TABLE IF EXISTS `product_alert_price`;
CREATE TABLE `product_alert_price` (
  `alert_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product alert price id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price amount',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website id',
  `add_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Product alert add date',
  `last_send_date` timestamp NULL DEFAULT NULL COMMENT 'Product alert last send date',
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert send count',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert status',
  PRIMARY KEY (`alert_price_id`),
  KEY `IDX_PRODUCT_ALERT_PRICE_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_PRODUCT_ALERT_PRICE_PRODUCT_ID` (`product_id`),
  KEY `IDX_PRODUCT_ALERT_PRICE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_PRD_ALERT_PRICE_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ALERT_PRICE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ALERT_PRICE_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Alert Price';


DROP TABLE IF EXISTS `product_alert_stock`;
CREATE TABLE `product_alert_stock` (
  `alert_stock_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Product alert stock id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website id',
  `add_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Product alert add date',
  `send_date` timestamp NULL DEFAULT NULL COMMENT 'Product alert send date',
  `send_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Send Count',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Product alert status',
  PRIMARY KEY (`alert_stock_id`),
  KEY `IDX_PRODUCT_ALERT_STOCK_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_PRODUCT_ALERT_STOCK_PRODUCT_ID` (`product_id`),
  KEY `IDX_PRODUCT_ALERT_STOCK_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_PRD_ALERT_STOCK_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ALERT_STOCK_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ALERT_STOCK_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Alert Stock';


DROP TABLE IF EXISTS `rating`;
CREATE TABLE `rating` (
  `rating_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rating Id',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `rating_code` varchar(64) NOT NULL COMMENT 'Rating Code',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Position On Frontend',
  PRIMARY KEY (`rating_id`),
  UNIQUE KEY `UNQ_RATING_RATING_CODE` (`rating_code`),
  KEY `IDX_RATING_ENTITY_ID` (`entity_id`),
  CONSTRAINT `FK_RATING_ENTITY_ID_RATING_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `rating_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ratings';

INSERT INTO `rating` (`rating_id`, `entity_id`, `rating_code`, `position`) VALUES
(1,	1,	'Quality',	0),
(2,	1,	'Value',	0),
(3,	1,	'Price',	0);

DROP TABLE IF EXISTS `rating_entity`;
CREATE TABLE `rating_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `entity_code` varchar(64) NOT NULL COMMENT 'Entity Code',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_RATING_ENTITY_ENTITY_CODE` (`entity_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating entities';

INSERT INTO `rating_entity` (`entity_id`, `entity_code`) VALUES
(1,	'product'),
(2,	'product_review'),
(3,	'review');

DROP TABLE IF EXISTS `rating_option`;
CREATE TABLE `rating_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rating Option Id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Id',
  `code` varchar(32) NOT NULL COMMENT 'Rating Option Code',
  `value` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Option Value',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Ration option position on frontend',
  PRIMARY KEY (`option_id`),
  KEY `IDX_RATING_OPTION_RATING_ID` (`rating_id`),
  CONSTRAINT `FK_RATING_OPTION_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating options';

INSERT INTO `rating_option` (`option_id`, `rating_id`, `code`, `value`, `position`) VALUES
(1,	1,	'1',	1,	1),
(2,	1,	'2',	2,	2),
(3,	1,	'3',	3,	3),
(4,	1,	'4',	4,	4),
(5,	1,	'5',	5,	5),
(6,	2,	'1',	1,	1),
(7,	2,	'2',	2,	2),
(8,	2,	'3',	3,	3),
(9,	2,	'4',	4,	4),
(10,	2,	'5',	5,	5),
(11,	3,	'1',	1,	1),
(12,	3,	'2',	2,	2),
(13,	3,	'3',	3,	3),
(14,	3,	'4',	4,	4),
(15,	3,	'5',	5,	5);

DROP TABLE IF EXISTS `rating_option_vote`;
CREATE TABLE `rating_option_vote` (
  `vote_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Vote id',
  `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Vote option id',
  `remote_ip` varchar(50) DEFAULT NULL,
  `remote_ip_long` varbinary(16) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Id',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `review_id` bigint(20) unsigned DEFAULT NULL COMMENT 'Review id',
  `percent` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Percent amount',
  `value` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Vote option value',
  PRIMARY KEY (`vote_id`),
  KEY `IDX_RATING_OPTION_VOTE_OPTION_ID` (`option_id`),
  KEY `FK_RATING_OPTION_VOTE_REVIEW_ID_REVIEW_REVIEW_ID` (`review_id`),
  CONSTRAINT `FK_RATING_OPTION_VOTE_OPTION_ID_RATING_OPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `rating_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RATING_OPTION_VOTE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating option values';


DROP TABLE IF EXISTS `rating_option_vote_aggregated`;
CREATE TABLE `rating_option_vote_aggregated` (
  `primary_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Vote aggregation id',
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `entity_pk_value` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `vote_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Vote dty',
  `vote_value_sum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'General vote sum',
  `percent` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Vote percent',
  `percent_approved` smallint(6) DEFAULT '0' COMMENT 'Vote percent approved by admin',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  PRIMARY KEY (`primary_id`),
  KEY `IDX_RATING_OPTION_VOTE_AGGREGATED_RATING_ID` (`rating_id`),
  KEY `IDX_RATING_OPTION_VOTE_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `FK_RATING_OPTION_VOTE_AGGREGATED_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RATING_OPTION_VOTE_AGGREGATED_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating vote aggregated';


DROP TABLE IF EXISTS `rating_store`;
CREATE TABLE `rating_store` (
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `IDX_RATING_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_RATING_STORE_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_RATING_STORE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating Store';


DROP TABLE IF EXISTS `rating_title`;
CREATE TABLE `rating_title` (
  `rating_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Rating Label',
  PRIMARY KEY (`rating_id`,`store_id`),
  KEY `IDX_RATING_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_RATING_TITLE_RATING_ID_RATING_RATING_ID` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`rating_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RATING_TITLE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Rating Title';


DROP TABLE IF EXISTS `report_compared_product_index`;
CREATE TABLE `report_compared_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Index Id',
  `visitor_id` int(10) unsigned DEFAULT NULL COMMENT 'Visitor Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `added_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Added At',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `UNQ_REPORT_COMPARED_PRODUCT_INDEX_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  UNIQUE KEY `UNQ_REPORT_COMPARED_PRODUCT_INDEX_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `IDX_REPORT_COMPARED_PRODUCT_INDEX_STORE_ID` (`store_id`),
  KEY `IDX_REPORT_COMPARED_PRODUCT_INDEX_ADDED_AT` (`added_at`),
  KEY `IDX_REPORT_COMPARED_PRODUCT_INDEX_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_REPORT_CMPD_PRD_IDX_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_CMPD_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_COMPARED_PRODUCT_INDEX_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reports Compared Product Index Table';


DROP TABLE IF EXISTS `report_event`;
CREATE TABLE `report_event` (
  `event_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Id',
  `logged_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Logged At',
  `event_type_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Event Type Id',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Object Id',
  `subject_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Subject Id',
  `subtype` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Subtype',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`event_id`),
  KEY `IDX_REPORT_EVENT_EVENT_TYPE_ID` (`event_type_id`),
  KEY `IDX_REPORT_EVENT_SUBJECT_ID` (`subject_id`),
  KEY `IDX_REPORT_EVENT_OBJECT_ID` (`object_id`),
  KEY `IDX_REPORT_EVENT_SUBTYPE` (`subtype`),
  KEY `IDX_REPORT_EVENT_STORE_ID` (`store_id`),
  CONSTRAINT `FK_REPORT_EVENT_EVENT_TYPE_ID_REPORT_EVENT_TYPES_EVENT_TYPE_ID` FOREIGN KEY (`event_type_id`) REFERENCES `report_event_types` (`event_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_EVENT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reports Event Table';

INSERT INTO `report_event` (`event_id`, `logged_at`, `event_type_id`, `object_id`, `subject_id`, `subtype`, `store_id`) VALUES
(1,	'2016-12-17 07:49:21',	1,	1,	38,	1,	1),
(2,	'2016-12-17 07:53:51',	1,	1,	38,	1,	1),
(3,	'2016-12-17 07:54:52',	1,	1,	38,	1,	1),
(4,	'2016-12-17 07:55:18',	1,	1,	38,	1,	1),
(5,	'2016-12-17 07:56:53',	1,	1,	38,	1,	1),
(6,	'2016-12-19 01:54:12',	1,	1,	88,	1,	1),
(7,	'2016-12-19 05:27:39',	4,	1,	93,	1,	1),
(8,	'2016-12-19 05:28:37',	1,	1,	93,	1,	1),
(9,	'2016-12-21 00:33:28',	1,	1,	100,	1,	1),
(10,	'2016-12-22 07:43:09',	1,	1,	112,	1,	1),
(11,	'2016-12-24 04:10:34',	4,	1,	134,	1,	1),
(12,	'2016-12-29 11:46:32',	1,	1,	190,	1,	1),
(13,	'2016-12-31 16:51:34',	4,	1,	198,	1,	1),
(14,	'2016-12-31 23:18:58',	1,	1,	202,	1,	1),
(15,	'2017-01-02 12:31:00',	1,	1,	217,	1,	1),
(16,	'2017-01-02 22:33:47',	1,	1,	222,	1,	1),
(17,	'2017-01-03 00:00:17',	4,	7,	222,	1,	1),
(18,	'2017-01-03 00:00:34',	4,	3,	222,	1,	1),
(19,	'2017-01-03 00:15:06',	1,	2,	222,	1,	1),
(20,	'2017-01-03 01:46:17',	1,	2,	225,	1,	1),
(21,	'2017-01-03 04:10:11',	1,	5,	227,	1,	1),
(22,	'2017-01-03 04:10:29',	1,	5,	229,	1,	1),
(23,	'2017-01-03 04:10:29',	1,	5,	230,	1,	1),
(24,	'2017-01-03 04:10:29',	1,	5,	231,	1,	1),
(25,	'2017-01-03 04:10:33',	1,	5,	228,	1,	1),
(26,	'2017-01-03 04:50:02',	1,	5,	227,	1,	1),
(27,	'2017-01-03 04:51:10',	1,	4,	227,	1,	1),
(28,	'2017-01-03 05:41:46',	1,	4,	227,	1,	1),
(29,	'2017-01-03 08:36:59',	1,	7,	232,	1,	1),
(30,	'2017-01-03 09:44:57',	1,	7,	235,	1,	1),
(31,	'2017-01-03 09:48:56',	1,	7,	235,	1,	1),
(32,	'2017-01-03 11:42:14',	1,	1,	237,	1,	1),
(33,	'2017-01-03 11:43:19',	4,	5,	237,	1,	1),
(34,	'2017-01-03 11:57:40',	1,	3,	237,	1,	1),
(35,	'2017-01-03 20:33:06',	1,	7,	242,	1,	1),
(36,	'2017-01-03 20:33:17',	1,	7,	242,	1,	1),
(37,	'2017-01-03 20:33:57',	1,	7,	243,	1,	1),
(38,	'2017-01-04 00:52:50',	1,	7,	246,	1,	1),
(39,	'2017-01-04 00:52:53',	1,	7,	246,	1,	1),
(40,	'2017-01-04 02:23:33',	1,	3,	249,	1,	1),
(41,	'2017-01-04 02:54:18',	1,	7,	250,	1,	1),
(42,	'2017-01-04 04:30:25',	1,	1,	252,	1,	1),
(43,	'2017-01-04 04:30:29',	1,	1,	252,	1,	1);

DROP TABLE IF EXISTS `report_event_types`;
CREATE TABLE `report_event_types` (
  `event_type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Event Type Id',
  `event_name` varchar(64) NOT NULL COMMENT 'Event Name',
  `customer_login` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Login',
  PRIMARY KEY (`event_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reports Event Type Table';

INSERT INTO `report_event_types` (`event_type_id`, `event_name`, `customer_login`) VALUES
(1,	'catalog_product_view',	0),
(2,	'sendfriend_product',	0),
(3,	'catalog_product_compare_add_product',	0),
(4,	'checkout_cart_add_product',	0),
(5,	'wishlist_add_product',	0),
(6,	'wishlist_share',	0);

DROP TABLE IF EXISTS `report_viewed_product_aggregated_daily`;
CREATE TABLE `report_viewed_product_aggregated_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_REPORT_VIEWED_PRD_AGGRED_DAILY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `IDX_REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  KEY `IDX_REPORT_VIEWED_PRODUCT_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_REPORT_VIEWED_PRD_AGGRED_DAILY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_VIEWED_PRD_AGGRED_DAILY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Daily';


DROP TABLE IF EXISTS `report_viewed_product_aggregated_monthly`;
CREATE TABLE `report_viewed_product_aggregated_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_REPORT_VIEWED_PRD_AGGRED_MONTHLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `IDX_REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  KEY `IDX_REPORT_VIEWED_PRODUCT_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_REPORT_VIEWED_PRD_AGGRED_MONTHLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_VIEWED_PRD_AGGRED_MONTHLY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Monthly';


DROP TABLE IF EXISTS `report_viewed_product_aggregated_yearly`;
CREATE TABLE `report_viewed_product_aggregated_yearly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `views_num` int(11) NOT NULL DEFAULT '0' COMMENT 'Number of Views',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_REPORT_VIEWED_PRD_AGGRED_YEARLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `IDX_REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  KEY `IDX_REPORT_VIEWED_PRODUCT_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_REPORT_VIEWED_PRD_AGGRED_YEARLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_VIEWED_PRD_AGGRED_YEARLY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Most Viewed Products Aggregated Yearly';


DROP TABLE IF EXISTS `report_viewed_product_index`;
CREATE TABLE `report_viewed_product_index` (
  `index_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Index Id',
  `visitor_id` int(10) unsigned DEFAULT NULL COMMENT 'Visitor Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `added_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Added At',
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `UNQ_REPORT_VIEWED_PRODUCT_INDEX_VISITOR_ID_PRODUCT_ID` (`visitor_id`,`product_id`),
  UNIQUE KEY `UNQ_REPORT_VIEWED_PRODUCT_INDEX_CUSTOMER_ID_PRODUCT_ID` (`customer_id`,`product_id`),
  KEY `IDX_REPORT_VIEWED_PRODUCT_INDEX_STORE_ID` (`store_id`),
  KEY `IDX_REPORT_VIEWED_PRODUCT_INDEX_ADDED_AT` (`added_at`),
  KEY `IDX_REPORT_VIEWED_PRODUCT_INDEX_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_REPORT_VIEWED_PRD_IDX_CSTR_ID_CSTR_ENTT_ENTT_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_VIEWED_PRD_IDX_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORT_VIEWED_PRODUCT_INDEX_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reports Viewed Product Index Table';

INSERT INTO `report_viewed_product_index` (`index_id`, `visitor_id`, `customer_id`, `product_id`, `store_id`, `added_at`) VALUES
(1,	38,	NULL,	1,	1,	'2016-12-17 07:56:53'),
(6,	88,	NULL,	1,	1,	'2016-12-19 01:54:12'),
(7,	93,	NULL,	1,	1,	'2016-12-19 05:28:37'),
(8,	100,	NULL,	1,	1,	'2016-12-21 00:33:28'),
(9,	112,	NULL,	1,	1,	'2016-12-22 07:43:09'),
(10,	190,	NULL,	1,	1,	'2016-12-29 11:46:32'),
(11,	202,	NULL,	1,	1,	'2016-12-31 23:18:58'),
(12,	217,	NULL,	1,	1,	'2017-01-02 12:31:00'),
(13,	222,	NULL,	1,	1,	'2017-01-02 22:33:47'),
(14,	222,	NULL,	2,	1,	'2017-01-03 00:15:06'),
(15,	225,	NULL,	2,	1,	'2017-01-03 01:46:17'),
(16,	227,	NULL,	5,	1,	'2017-01-03 04:50:02'),
(17,	229,	NULL,	5,	1,	'2017-01-03 04:10:29'),
(18,	230,	NULL,	5,	1,	'2017-01-03 04:10:29'),
(19,	231,	NULL,	5,	1,	'2017-01-03 04:10:29'),
(20,	228,	NULL,	5,	1,	'2017-01-03 04:10:33'),
(22,	227,	NULL,	4,	1,	'2017-01-03 05:41:46'),
(24,	232,	NULL,	7,	1,	'2017-01-03 08:36:59'),
(25,	235,	NULL,	7,	1,	'2017-01-03 09:48:56'),
(27,	237,	NULL,	1,	1,	'2017-01-03 11:42:14'),
(28,	237,	NULL,	3,	1,	'2017-01-03 11:57:40'),
(29,	242,	NULL,	7,	1,	'2017-01-03 20:33:17'),
(31,	243,	NULL,	7,	1,	'2017-01-03 20:33:57'),
(32,	246,	NULL,	7,	1,	'2017-01-04 00:52:53'),
(34,	249,	NULL,	3,	1,	'2017-01-04 02:23:33'),
(35,	250,	NULL,	7,	1,	'2017-01-04 02:54:18'),
(36,	252,	NULL,	1,	1,	'2017-01-04 04:30:29');

DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `review_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Review create date',
  `entity_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity id',
  `entity_pk_value` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product id',
  `status_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Status code',
  PRIMARY KEY (`review_id`),
  KEY `IDX_REVIEW_ENTITY_ID` (`entity_id`),
  KEY `IDX_REVIEW_STATUS_ID` (`status_id`),
  KEY `IDX_REVIEW_ENTITY_PK_VALUE` (`entity_pk_value`),
  CONSTRAINT `FK_REVIEW_ENTITY_ID_REVIEW_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `review_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REVIEW_STATUS_ID_REVIEW_STATUS_STATUS_ID` FOREIGN KEY (`status_id`) REFERENCES `review_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review base information';


DROP TABLE IF EXISTS `review_detail`;
CREATE TABLE `review_detail` (
  `detail_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review detail id',
  `review_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'Review id',
  `store_id` smallint(5) unsigned DEFAULT '0' COMMENT 'Store id',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `detail` text NOT NULL COMMENT 'Detail description',
  `nickname` varchar(128) NOT NULL COMMENT 'User nickname',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  PRIMARY KEY (`detail_id`),
  KEY `IDX_REVIEW_DETAIL_REVIEW_ID` (`review_id`),
  KEY `IDX_REVIEW_DETAIL_STORE_ID` (`store_id`),
  KEY `IDX_REVIEW_DETAIL_CUSTOMER_ID` (`customer_id`),
  CONSTRAINT `FK_REVIEW_DETAIL_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_REVIEW_DETAIL_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REVIEW_DETAIL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review detail information';


DROP TABLE IF EXISTS `review_entity`;
CREATE TABLE `review_entity` (
  `entity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Review entity id',
  `entity_code` varchar(32) NOT NULL COMMENT 'Review entity code',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review entities';

INSERT INTO `review_entity` (`entity_id`, `entity_code`) VALUES
(1,	'product'),
(2,	'customer'),
(3,	'category');

DROP TABLE IF EXISTS `review_entity_summary`;
CREATE TABLE `review_entity_summary` (
  `primary_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Summary review entity id',
  `entity_pk_value` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Product id',
  `entity_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Entity type id',
  `reviews_count` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Qty of reviews',
  `rating_summary` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Summarized rating',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`primary_id`),
  KEY `IDX_REVIEW_ENTITY_SUMMARY_STORE_ID` (`store_id`),
  CONSTRAINT `FK_REVIEW_ENTITY_SUMMARY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review aggregates';


DROP TABLE IF EXISTS `review_status`;
CREATE TABLE `review_status` (
  `status_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Status id',
  `status_code` varchar(32) NOT NULL COMMENT 'Status code',
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review statuses';

INSERT INTO `review_status` (`status_id`, `status_code`) VALUES
(1,	'Approved'),
(2,	'Pending'),
(3,	'Not Approved');

DROP TABLE IF EXISTS `review_store`;
CREATE TABLE `review_store` (
  `review_id` bigint(20) unsigned NOT NULL COMMENT 'Review Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  PRIMARY KEY (`review_id`,`store_id`),
  KEY `IDX_REVIEW_STORE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_REVIEW_STORE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REVIEW_STORE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review Store';


DROP TABLE IF EXISTS `salesrule`;
CREATE TABLE `salesrule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `uses_per_customer` int(11) NOT NULL DEFAULT '0' COMMENT 'Uses Per Customer',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `is_advanced` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Advanced',
  `product_ids` text COMMENT 'Product Ids',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `discount_qty` decimal(12,4) DEFAULT NULL COMMENT 'Discount Qty',
  `discount_step` int(10) unsigned NOT NULL COMMENT 'Discount Step',
  `simple_free_shipping` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Simple Free Shipping',
  `apply_to_shipping` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Apply To Shipping',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `is_rss` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Rss',
  `coupon_type` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Coupon Type',
  `use_auto_generation` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Use Auto Generation',
  `uses_per_coupon` int(11) NOT NULL DEFAULT '0' COMMENT 'Uses Per Coupon',
  PRIMARY KEY (`rule_id`),
  KEY `IDX_SALESRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule';


DROP TABLE IF EXISTS `salesrule_coupon`;
CREATE TABLE `salesrule_coupon` (
  `coupon_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Coupon Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `usage_limit` int(10) unsigned DEFAULT NULL COMMENT 'Usage Limit',
  `usage_per_customer` int(10) unsigned DEFAULT NULL COMMENT 'Usage Per Customer',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `expiration_date` timestamp NULL DEFAULT NULL COMMENT 'Expiration Date',
  `is_primary` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Primary',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Coupon Code Creation Date',
  `type` smallint(6) DEFAULT '0' COMMENT 'Coupon Code Type',
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `UNQ_SALESRULE_COUPON_CODE` (`code`),
  UNIQUE KEY `UNQ_SALESRULE_COUPON_RULE_ID_IS_PRIMARY` (`rule_id`,`is_primary`),
  KEY `IDX_SALESRULE_COUPON_RULE_ID` (`rule_id`),
  CONSTRAINT `FK_SALESRULE_COUPON_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon';


DROP TABLE IF EXISTS `salesrule_coupon_usage`;
CREATE TABLE `salesrule_coupon_usage` (
  `coupon_id` int(10) unsigned NOT NULL COMMENT 'Coupon Id',
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Id',
  `times_used` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`coupon_id`,`customer_id`),
  KEY `IDX_SALESRULE_COUPON_USAGE_COUPON_ID` (`coupon_id`),
  KEY `IDX_SALESRULE_COUPON_USAGE_CUSTOMER_ID` (`customer_id`),
  CONSTRAINT `FK_SALESRULE_COUPON_USAGE_COUPON_ID_SALESRULE_COUPON_COUPON_ID` FOREIGN KEY (`coupon_id`) REFERENCES `salesrule_coupon` (`coupon_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALESRULE_COUPON_USAGE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Usage';


DROP TABLE IF EXISTS `salesrule_customer`;
CREATE TABLE `salesrule_customer` (
  `rule_customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rule Customer Id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `times_used` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Times Used',
  PRIMARY KEY (`rule_customer_id`),
  KEY `IDX_SALESRULE_CUSTOMER_RULE_ID_CUSTOMER_ID` (`rule_id`,`customer_id`),
  KEY `IDX_SALESRULE_CUSTOMER_CUSTOMER_ID_RULE_ID` (`customer_id`,`rule_id`),
  CONSTRAINT `FK_SALESRULE_CUSTOMER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALESRULE_CUSTOMER_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Customer';


DROP TABLE IF EXISTS `salesrule_customer_group`;
CREATE TABLE `salesrule_customer_group` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  PRIMARY KEY (`rule_id`,`customer_group_id`),
  KEY `IDX_SALESRULE_CUSTOMER_GROUP_RULE_ID` (`rule_id`),
  KEY `IDX_SALESRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`),
  CONSTRAINT `FK_SALESRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALESRULE_CUSTOMER_GROUP_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Customer Groups Relations';


DROP TABLE IF EXISTS `salesrule_label`;
CREATE TABLE `salesrule_label` (
  `label_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Label Id',
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  PRIMARY KEY (`label_id`),
  UNIQUE KEY `UNQ_SALESRULE_LABEL_RULE_ID_STORE_ID` (`rule_id`,`store_id`),
  KEY `IDX_SALESRULE_LABEL_STORE_ID` (`store_id`),
  KEY `IDX_SALESRULE_LABEL_RULE_ID` (`rule_id`),
  CONSTRAINT `FK_SALESRULE_LABEL_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALESRULE_LABEL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Label';


DROP TABLE IF EXISTS `salesrule_product_attribute`;
CREATE TABLE `salesrule_product_attribute` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  PRIMARY KEY (`rule_id`,`website_id`,`customer_group_id`,`attribute_id`),
  KEY `IDX_SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID` (`website_id`),
  KEY `IDX_SALESRULE_PRODUCT_ATTRIBUTE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  KEY `IDX_SALESRULE_PRODUCT_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `FK_SALESRULE_PRD_ATTR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_SALESRULE_PRD_ATTR_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_SALESRULE_PRD_ATTR_WS_ID_CORE_WS_WS_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_SALESRULE_PRODUCT_ATTRIBUTE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Product Attribute';


DROP TABLE IF EXISTS `salesrule_website`;
CREATE TABLE `salesrule_website` (
  `rule_id` int(10) unsigned NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
  PRIMARY KEY (`rule_id`,`website_id`),
  KEY `IDX_SALESRULE_WEBSITE_RULE_ID` (`rule_id`),
  KEY `IDX_SALESRULE_WEBSITE_WEBSITE_ID` (`website_id`),
  CONSTRAINT `FK_SALESRULE_WEBSITE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALESRULE_WEBSITE_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Websites Relations';


DROP TABLE IF EXISTS `sales_bestsellers_aggregated_daily`;
CREATE TABLE `sales_bestsellers_aggregated_daily` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_type_id` varchar(32) NOT NULL DEFAULT 'simple' COMMENT 'Product Type Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_SALES_BESTSELLERS_AGGRED_DAILY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `IDX_SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  KEY `IDX_SALES_BESTSELLERS_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_SALES_BESTSELLERS_AGGRED_DAILY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_BESTSELLERS_AGGRED_DAILY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Daily';


DROP TABLE IF EXISTS `sales_bestsellers_aggregated_monthly`;
CREATE TABLE `sales_bestsellers_aggregated_monthly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_type_id` varchar(32) NOT NULL DEFAULT 'simple' COMMENT 'Product Type Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_SALES_BESTSELLERS_AGGRED_MONTHLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `IDX_SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  KEY `IDX_SALES_BESTSELLERS_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_SALES_BESTSELLERS_AGGRED_MONTHLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_BESTSELLERS_AGGRED_MONTHLY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Monthly';


DROP TABLE IF EXISTS `sales_bestsellers_aggregated_yearly`;
CREATE TABLE `sales_bestsellers_aggregated_yearly` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_type_id` varchar(32) NOT NULL DEFAULT 'simple' COMMENT 'Product Type Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Rating Pos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_SALES_BESTSELLERS_AGGRED_YEARLY_PERIOD_STORE_ID_PRD_ID` (`period`,`store_id`,`product_id`),
  KEY `IDX_SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  KEY `IDX_SALES_BESTSELLERS_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_SALES_BESTSELLERS_AGGRED_YEARLY_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_BESTSELLERS_AGGRED_YEARLY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Yearly';


DROP TABLE IF EXISTS `sales_billing_agreement`;
CREATE TABLE `sales_billing_agreement` (
  `agreement_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Agreement Id',
  `customer_id` int(10) unsigned NOT NULL COMMENT 'Customer Id',
  `method_code` varchar(32) NOT NULL COMMENT 'Method Code',
  `reference_id` varchar(32) NOT NULL COMMENT 'Reference Id',
  `status` varchar(20) NOT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `agreement_label` varchar(255) DEFAULT NULL COMMENT 'Agreement Label',
  PRIMARY KEY (`agreement_id`),
  KEY `IDX_SALES_BILLING_AGREEMENT_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_SALES_BILLING_AGREEMENT_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_BILLING_AGREEMENT_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_BILLING_AGREEMENT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Billing Agreement';


DROP TABLE IF EXISTS `sales_billing_agreement_order`;
CREATE TABLE `sales_billing_agreement_order` (
  `agreement_id` int(10) unsigned NOT NULL COMMENT 'Agreement Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  PRIMARY KEY (`agreement_id`,`order_id`),
  KEY `IDX_SALES_BILLING_AGREEMENT_ORDER_ORDER_ID` (`order_id`),
  CONSTRAINT `FK_SALES_BILLING_AGRT_ORDER_AGRT_ID_SALES_BILLING_AGRT_AGRT_ID` FOREIGN KEY (`agreement_id`) REFERENCES `sales_billing_agreement` (`agreement_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_BILLING_AGRT_ORDER_ORDER_ID_SALES_FLAT_ORDER_ENTT_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Billing Agreement Order';


DROP TABLE IF EXISTS `sales_flat_creditmemo`;
CREATE TABLE `sales_flat_creditmemo` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `creditmemo_status` int(11) DEFAULT NULL COMMENT 'Creditmemo Status',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'Invoice Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Amount',
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Amount',
  `shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Hidden Tax Amount',
  `base_shipping_hidden_tax_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Hidden Tax Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_SALES_FLAT_CREDITMEMO_INCREMENT_ID` (`increment_id`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_STORE_ID` (`store_id`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_ORDER_ID` (`order_id`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_CREDITMEMO_STATUS` (`creditmemo_status`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_STATE` (`state`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_CREATED_AT` (`created_at`),
  CONSTRAINT `FK_SALES_FLAT_CREDITMEMO_ORDER_ID_SALES_FLAT_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_CREDITMEMO_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo';


DROP TABLE IF EXISTS `sales_flat_creditmemo_comment`;
CREATE TABLE `sales_flat_creditmemo_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_COMMENT_CREATED_AT` (`created_at`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_COMMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_B0FCB0B5467075BE63D474F2CD5F7804` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_creditmemo` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Comment';


DROP TABLE IF EXISTS `sales_flat_creditmemo_grid`;
CREATE TABLE `sales_flat_creditmemo_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `creditmemo_status` int(11) DEFAULT NULL COMMENT 'Creditmemo Status',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'Invoice Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_SALES_FLAT_CREDITMEMO_GRID_INCREMENT_ID` (`increment_id`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_GRID_STORE_ID` (`store_id`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_GRID_ORDER_ID` (`order_id`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_GRID_CREDITMEMO_STATUS` (`creditmemo_status`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_GRID_STATE` (`state`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_GRID_CREATED_AT` (`created_at`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_GRID_BILLING_NAME` (`billing_name`),
  CONSTRAINT `FK_78C711B225167A11CC077B03D1C8E1CC` FOREIGN KEY (`entity_id`) REFERENCES `sales_flat_creditmemo` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_CREDITMEMO_GRID_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Grid';


DROP TABLE IF EXISTS `sales_flat_creditmemo_item`;
CREATE TABLE `sales_flat_creditmemo_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Amount',
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_CREDITMEMO_ITEM_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_306DAC836C699F0B5E13BE486557AC8A` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_creditmemo` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Item';


DROP TABLE IF EXISTS `sales_flat_invoice`;
CREATE TABLE `sales_flat_invoice` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `is_used_for_refund` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Used For Refund',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `can_void_flag` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Void Flag',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Amount',
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Amount',
  `shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Hidden Tax Amount',
  `base_shipping_hidden_tax_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Hidden Tax Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_SALES_FLAT_INVOICE_INCREMENT_ID` (`increment_id`),
  KEY `IDX_SALES_FLAT_INVOICE_STORE_ID` (`store_id`),
  KEY `IDX_SALES_FLAT_INVOICE_GRAND_TOTAL` (`grand_total`),
  KEY `IDX_SALES_FLAT_INVOICE_ORDER_ID` (`order_id`),
  KEY `IDX_SALES_FLAT_INVOICE_STATE` (`state`),
  KEY `IDX_SALES_FLAT_INVOICE_CREATED_AT` (`created_at`),
  CONSTRAINT `FK_SALES_FLAT_INVOICE_ORDER_ID_SALES_FLAT_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_INVOICE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice';


DROP TABLE IF EXISTS `sales_flat_invoice_comment`;
CREATE TABLE `sales_flat_invoice_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_INVOICE_COMMENT_CREATED_AT` (`created_at`),
  KEY `IDX_SALES_FLAT_INVOICE_COMMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_5C4B36BBE5231A76AB8018B281ED50BC` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_invoice` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Comment';


DROP TABLE IF EXISTS `sales_flat_invoice_grid`;
CREATE TABLE `sales_flat_invoice_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_SALES_FLAT_INVOICE_GRID_INCREMENT_ID` (`increment_id`),
  KEY `IDX_SALES_FLAT_INVOICE_GRID_STORE_ID` (`store_id`),
  KEY `IDX_SALES_FLAT_INVOICE_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `IDX_SALES_FLAT_INVOICE_GRID_ORDER_ID` (`order_id`),
  KEY `IDX_SALES_FLAT_INVOICE_GRID_STATE` (`state`),
  KEY `IDX_SALES_FLAT_INVOICE_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `IDX_SALES_FLAT_INVOICE_GRID_CREATED_AT` (`created_at`),
  KEY `IDX_SALES_FLAT_INVOICE_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `IDX_SALES_FLAT_INVOICE_GRID_BILLING_NAME` (`billing_name`),
  CONSTRAINT `FK_SALES_FLAT_INVOICE_GRID_ENTT_ID_SALES_FLAT_INVOICE_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `sales_flat_invoice` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_INVOICE_GRID_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Grid';


DROP TABLE IF EXISTS `sales_flat_invoice_item`;
CREATE TABLE `sales_flat_invoice_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Amount',
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Amount',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_INVOICE_ITEM_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_INVOICE_ITEM_PARENT_ID_SALES_FLAT_INVOICE_ENTT_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_invoice` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Item';


DROP TABLE IF EXISTS `sales_flat_order`;
CREATE TABLE `sales_flat_order` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `state` varchar(32) DEFAULT NULL COMMENT 'State',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `protect_code` varchar(255) DEFAULT NULL COMMENT 'Protect Code',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Canceled',
  `base_discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Invoiced',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `base_shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Canceled',
  `base_shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Invoiced',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Refunded',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `base_subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Canceled',
  `base_subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Invoiced',
  `base_subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Refunded',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Canceled',
  `base_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Invoiced',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `base_total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Canceled',
  `base_total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced',
  `base_total_invoiced_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced Cost',
  `base_total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Offline Refunded',
  `base_total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Online Refunded',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `base_total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Qty Ordered',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Canceled',
  `discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Invoiced',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Canceled',
  `shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Invoiced',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Refunded',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Canceled',
  `subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Invoiced',
  `subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Refunded',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Tax Invoiced',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Total Canceled',
  `total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Total Invoiced',
  `total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Offline Refunded',
  `total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Online Refunded',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty Ordered',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  `can_ship_partially` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially',
  `can_ship_partially_item` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Ship Partially Item',
  `customer_is_guest` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Is Guest',
  `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `edit_increment` int(11) DEFAULT NULL COMMENT 'Edit Increment',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `forced_shipment_with_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Forced Do Shipment With Invoice',
  `payment_auth_expiration` int(11) DEFAULT NULL COMMENT 'Payment Authorization Expiration',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `quote_id` int(11) DEFAULT NULL COMMENT 'Quote Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `base_total_due` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Due',
  `payment_authorization_amount` decimal(12,4) DEFAULT NULL COMMENT 'Payment Authorization Amount',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `total_due` decimal(12,4) DEFAULT NULL COMMENT 'Total Due',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `applied_rule_ids` varchar(255) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_firstname` varchar(255) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_lastname` varchar(255) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_middlename` varchar(255) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_prefix` varchar(255) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_suffix` varchar(255) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_taxvat` varchar(255) DEFAULT NULL COMMENT 'Customer Taxvat',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `ext_customer_id` varchar(255) DEFAULT NULL COMMENT 'Ext Customer Id',
  `ext_order_id` varchar(255) DEFAULT NULL COMMENT 'Ext Order Id',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `hold_before_state` varchar(255) DEFAULT NULL COMMENT 'Hold Before State',
  `hold_before_status` varchar(255) DEFAULT NULL COMMENT 'Hold Before Status',
  `order_currency_code` varchar(255) DEFAULT NULL COMMENT 'Order Currency Code',
  `original_increment_id` varchar(50) DEFAULT NULL COMMENT 'Original Increment Id',
  `relation_child_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Id',
  `relation_child_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Real Id',
  `relation_parent_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Id',
  `relation_parent_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Real Id',
  `remote_ip` varchar(255) DEFAULT NULL COMMENT 'Remote Ip',
  `shipping_method` varchar(255) DEFAULT NULL COMMENT 'Shipping Method',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `x_forwarded_for` varchar(255) DEFAULT NULL COMMENT 'X Forwarded For',
  `customer_note` text COMMENT 'Customer Note',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `total_item_count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Total Item Count',
  `customer_gender` int(11) DEFAULT NULL COMMENT 'Customer Gender',
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Amount',
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Amount',
  `shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Hidden Tax Amount',
  `base_shipping_hidden_tax_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Hidden Tax Amount',
  `hidden_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Invoiced',
  `base_hidden_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Invoiced',
  `hidden_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Refunded',
  `base_hidden_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Refunded',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `coupon_rule_name` varchar(255) DEFAULT NULL COMMENT 'Coupon Sales Rule Name',
  `paypal_ipn_customer_notified` int(11) DEFAULT '0' COMMENT 'Paypal Ipn Customer Notified',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_SALES_FLAT_ORDER_INCREMENT_ID` (`increment_id`),
  KEY `IDX_SALES_FLAT_ORDER_STATUS` (`status`),
  KEY `IDX_SALES_FLAT_ORDER_STATE` (`state`),
  KEY `IDX_SALES_FLAT_ORDER_STORE_ID` (`store_id`),
  KEY `IDX_SALES_FLAT_ORDER_CREATED_AT` (`created_at`),
  KEY `IDX_SALES_FLAT_ORDER_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_SALES_FLAT_ORDER_EXT_ORDER_ID` (`ext_order_id`),
  KEY `IDX_SALES_FLAT_ORDER_QUOTE_ID` (`quote_id`),
  KEY `IDX_SALES_FLAT_ORDER_UPDATED_AT` (`updated_at`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_ORDER_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order';


DROP TABLE IF EXISTS `sales_flat_order_address`;
CREATE TABLE `sales_flat_order_address` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `customer_address_id` int(11) DEFAULT NULL COMMENT 'Customer Address Id',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `region_id` int(11) DEFAULT NULL COMMENT 'Region Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `region` varchar(255) DEFAULT NULL COMMENT 'Region',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Postcode',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Lastname',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(255) DEFAULT NULL COMMENT 'City',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `telephone` varchar(255) DEFAULT NULL COMMENT 'Telephone',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Firstname',
  `address_type` varchar(255) DEFAULT NULL COMMENT 'Address Type',
  `prefix` varchar(255) DEFAULT NULL COMMENT 'Prefix',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middlename',
  `suffix` varchar(255) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_ORDER_ADDRESS_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_ADDRESS_PARENT_ID_SALES_FLAT_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Address';


DROP TABLE IF EXISTS `sales_flat_order_grid`;
CREATE TABLE `sales_flat_order_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `order_currency_code` varchar(255) DEFAULT NULL COMMENT 'Order Currency Code',
  `shipping_name` varchar(255) DEFAULT NULL COMMENT 'Shipping Name',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_SALES_FLAT_ORDER_GRID_INCREMENT_ID` (`increment_id`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_STATUS` (`status`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_STORE_ID` (`store_id`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_BASE_TOTAL_PAID` (`base_total_paid`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_GRAND_TOTAL` (`grand_total`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_TOTAL_PAID` (`total_paid`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_SHIPPING_NAME` (`shipping_name`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_BILLING_NAME` (`billing_name`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_CREATED_AT` (`created_at`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_SALES_FLAT_ORDER_GRID_UPDATED_AT` (`updated_at`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_GRID_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_ORDER_GRID_ENTITY_ID_SALES_FLAT_ORDER_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_ORDER_GRID_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Grid';


DROP TABLE IF EXISTS `sales_flat_order_item`;
CREATE TABLE `sales_flat_order_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Quote Item Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `product_options` text COMMENT 'Product Options',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `free_shipping` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Free Shipping',
  `is_qty_decimal` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'No Discount',
  `qty_backordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Backordered',
  `qty_canceled` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Canceled',
  `qty_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Invoiced',
  `qty_ordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `qty_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Refunded',
  `qty_shipped` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Shipped',
  `base_cost` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Cost',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `original_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `base_original_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Original Price',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Invoiced',
  `base_tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Invoiced',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Invoiced',
  `base_discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Invoiced',
  `amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Amount Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Amount Refunded',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Invoiced',
  `base_row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Invoiced',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `ext_order_item_id` varchar(255) DEFAULT NULL COMMENT 'Ext Order Item Id',
  `locked_do_invoice` smallint(5) unsigned DEFAULT NULL COMMENT 'Locked Do Invoice',
  `locked_do_ship` smallint(5) unsigned DEFAULT NULL COMMENT 'Locked Do Ship',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Amount',
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Amount',
  `hidden_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Invoiced',
  `base_hidden_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Invoiced',
  `hidden_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Refunded',
  `base_hidden_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Refunded',
  `is_nominal` int(11) NOT NULL DEFAULT '0' COMMENT 'Is Nominal',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `hidden_tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Canceled',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `gift_message_available` int(11) DEFAULT NULL COMMENT 'Gift Message Available',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  PRIMARY KEY (`item_id`),
  KEY `IDX_SALES_FLAT_ORDER_ITEM_ORDER_ID` (`order_id`),
  KEY `IDX_SALES_FLAT_ORDER_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_ITEM_ORDER_ID_SALES_FLAT_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_ORDER_ITEM_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Item';


DROP TABLE IF EXISTS `sales_flat_order_payment`;
CREATE TABLE `sales_flat_order_payment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `base_shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Captured',
  `shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Captured',
  `amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Amount Refunded',
  `base_amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid',
  `amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Amount Canceled',
  `base_amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Authorized',
  `base_amount_paid_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid Online',
  `base_amount_refunded_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded Online',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Amount Paid',
  `amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Amount Authorized',
  `base_amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Ordered',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded',
  `amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Amount Ordered',
  `base_amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Canceled',
  `quote_payment_id` int(11) DEFAULT NULL COMMENT 'Quote Payment Id',
  `additional_data` text COMMENT 'Additional Data',
  `cc_exp_month` varchar(255) DEFAULT NULL COMMENT 'Cc Exp Month',
  `cc_ss_start_year` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Start Year',
  `echeck_bank_name` varchar(255) DEFAULT NULL COMMENT 'Echeck Bank Name',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `cc_debug_request_body` varchar(255) DEFAULT NULL COMMENT 'Cc Debug Request Body',
  `cc_secure_verify` varchar(255) DEFAULT NULL COMMENT 'Cc Secure Verify',
  `protection_eligibility` varchar(255) DEFAULT NULL COMMENT 'Protection Eligibility',
  `cc_approval` varchar(255) DEFAULT NULL COMMENT 'Cc Approval',
  `cc_last4` varchar(255) DEFAULT NULL COMMENT 'Cc Last4',
  `cc_status_description` varchar(255) DEFAULT NULL COMMENT 'Cc Status Description',
  `echeck_type` varchar(255) DEFAULT NULL COMMENT 'Echeck Type',
  `cc_debug_response_serialized` varchar(255) DEFAULT NULL COMMENT 'Cc Debug Response Serialized',
  `cc_ss_start_month` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Start Month',
  `echeck_account_type` varchar(255) DEFAULT NULL COMMENT 'Echeck Account Type',
  `last_trans_id` varchar(255) DEFAULT NULL COMMENT 'Last Trans Id',
  `cc_cid_status` varchar(255) DEFAULT NULL COMMENT 'Cc Cid Status',
  `cc_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_type` varchar(255) DEFAULT NULL COMMENT 'Cc Type',
  `po_number` varchar(255) DEFAULT NULL COMMENT 'Po Number',
  `cc_exp_year` varchar(255) DEFAULT NULL COMMENT 'Cc Exp Year',
  `cc_status` varchar(255) DEFAULT NULL COMMENT 'Cc Status',
  `echeck_routing_number` varchar(255) DEFAULT NULL COMMENT 'Echeck Routing Number',
  `account_status` varchar(255) DEFAULT NULL COMMENT 'Account Status',
  `anet_trans_method` varchar(255) DEFAULT NULL COMMENT 'Anet Trans Method',
  `cc_debug_response_body` varchar(255) DEFAULT NULL COMMENT 'Cc Debug Response Body',
  `cc_ss_issue` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `echeck_account_name` varchar(255) DEFAULT NULL COMMENT 'Echeck Account Name',
  `cc_avs_status` varchar(255) DEFAULT NULL COMMENT 'Cc Avs Status',
  `cc_number_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Number Enc',
  `cc_trans_id` varchar(255) DEFAULT NULL COMMENT 'Cc Trans Id',
  `paybox_request_number` varchar(255) DEFAULT NULL COMMENT 'Paybox Request Number',
  `address_status` varchar(255) DEFAULT NULL COMMENT 'Address Status',
  `additional_information` text COMMENT 'Additional Information',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_ORDER_PAYMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_SALES_FLAT_ORDER_PAYMENT_PARENT_ID_SALES_FLAT_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Payment';


DROP TABLE IF EXISTS `sales_flat_order_status_history`;
CREATE TABLE `sales_flat_order_status_history` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `entity_name` varchar(32) DEFAULT NULL COMMENT 'Shows what entity history is bind to.',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_ORDER_STATUS_HISTORY_PARENT_ID` (`parent_id`),
  KEY `IDX_SALES_FLAT_ORDER_STATUS_HISTORY_CREATED_AT` (`created_at`),
  CONSTRAINT `FK_CE7C71E74CB74DDACED337CEE6753D5E` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Status History';


DROP TABLE IF EXISTS `sales_flat_quote`;
CREATE TABLE `sales_flat_quote` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
  `converted_at` timestamp NULL DEFAULT NULL COMMENT 'Converted At',
  `is_active` smallint(5) unsigned DEFAULT '1' COMMENT 'Is Active',
  `is_virtual` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Virtual',
  `is_multi_shipping` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Multi Shipping',
  `items_count` int(10) unsigned DEFAULT '0' COMMENT 'Items Count',
  `items_qty` decimal(12,4) DEFAULT '0.0000' COMMENT 'Items Qty',
  `orig_order_id` int(10) unsigned DEFAULT '0' COMMENT 'Orig Order Id',
  `store_to_base_rate` decimal(12,4) DEFAULT '0.0000' COMMENT 'Store To Base Rate',
  `store_to_quote_rate` decimal(12,4) DEFAULT '0.0000' COMMENT 'Store To Quote Rate',
  `base_currency_code` varchar(255) DEFAULT NULL COMMENT 'Base Currency Code',
  `store_currency_code` varchar(255) DEFAULT NULL COMMENT 'Store Currency Code',
  `quote_currency_code` varchar(255) DEFAULT NULL COMMENT 'Quote Currency Code',
  `grand_total` decimal(12,4) DEFAULT '0.0000' COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Grand Total',
  `checkout_method` varchar(255) DEFAULT NULL COMMENT 'Checkout Method',
  `customer_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Id',
  `customer_tax_class_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Tax Class Id',
  `customer_group_id` int(10) unsigned DEFAULT '0' COMMENT 'Customer Group Id',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_prefix` varchar(40) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_firstname` varchar(255) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_middlename` varchar(40) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_lastname` varchar(255) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_suffix` varchar(40) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `customer_note` varchar(255) DEFAULT NULL COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) unsigned DEFAULT '1' COMMENT 'Customer Note Notify',
  `customer_is_guest` smallint(5) unsigned DEFAULT '0' COMMENT 'Customer Is Guest',
  `remote_ip` varchar(255) DEFAULT NULL COMMENT 'Remote Ip',
  `applied_rule_ids` varchar(255) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `reserved_order_id` varchar(64) DEFAULT NULL COMMENT 'Reserved Order Id',
  `password_hash` varchar(255) DEFAULT NULL COMMENT 'Password Hash',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `global_currency_code` varchar(255) DEFAULT NULL COMMENT 'Global Currency Code',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_quote_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Quote Rate',
  `customer_taxvat` varchar(255) DEFAULT NULL COMMENT 'Customer Taxvat',
  `customer_gender` varchar(255) DEFAULT NULL COMMENT 'Customer Gender',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `subtotal_with_discount` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal With Discount',
  `base_subtotal_with_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal With Discount',
  `is_changed` int(10) unsigned DEFAULT NULL COMMENT 'Is Changed',
  `trigger_recollect` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Trigger Recollect',
  `ext_shipping_info` text COMMENT 'Ext Shipping Info',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `is_persistent` smallint(5) unsigned DEFAULT '0' COMMENT 'Is Quote Persistent',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_QUOTE_CUSTOMER_ID_STORE_ID_IS_ACTIVE` (`customer_id`,`store_id`,`is_active`),
  KEY `IDX_SALES_FLAT_QUOTE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_FLAT_QUOTE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote';

INSERT INTO `sales_flat_quote` (`entity_id`, `store_id`, `created_at`, `updated_at`, `converted_at`, `is_active`, `is_virtual`, `is_multi_shipping`, `items_count`, `items_qty`, `orig_order_id`, `store_to_base_rate`, `store_to_quote_rate`, `base_currency_code`, `store_currency_code`, `quote_currency_code`, `grand_total`, `base_grand_total`, `checkout_method`, `customer_id`, `customer_tax_class_id`, `customer_group_id`, `customer_email`, `customer_prefix`, `customer_firstname`, `customer_middlename`, `customer_lastname`, `customer_suffix`, `customer_dob`, `customer_note`, `customer_note_notify`, `customer_is_guest`, `remote_ip`, `applied_rule_ids`, `reserved_order_id`, `password_hash`, `coupon_code`, `global_currency_code`, `base_to_global_rate`, `base_to_quote_rate`, `customer_taxvat`, `customer_gender`, `subtotal`, `base_subtotal`, `subtotal_with_discount`, `base_subtotal_with_discount`, `is_changed`, `trigger_recollect`, `ext_shipping_info`, `gift_message_id`, `is_persistent`) VALUES
(1,	1,	'2016-12-19 05:27:39',	'2016-12-19 05:27:40',	NULL,	1,	0,	0,	1,	1.0000,	0,	1.0000,	1.0000,	'AUD',	'AUD',	'AUD',	1.0000,	1.0000,	NULL,	NULL,	3,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	'101.176.232.162',	NULL,	NULL,	NULL,	NULL,	'AUD',	1.0000,	1.0000,	NULL,	NULL,	1.0000,	1.0000,	1.0000,	1.0000,	1,	0,	NULL,	NULL,	0),
(2,	1,	'2016-12-24 04:10:34',	'2016-12-24 04:10:35',	NULL,	1,	0,	0,	1,	1.0000,	0,	1.0000,	1.0000,	'AUD',	'AUD',	'AUD',	1.0000,	1.0000,	NULL,	NULL,	3,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	'117.5.234.204',	NULL,	NULL,	NULL,	NULL,	'AUD',	1.0000,	1.0000,	NULL,	NULL,	1.0000,	1.0000,	1.0000,	1.0000,	1,	0,	NULL,	NULL,	0),
(3,	1,	'2016-12-31 16:51:33',	'2016-12-31 16:51:33',	NULL,	1,	0,	0,	1,	1.0000,	0,	1.0000,	1.0000,	'AUD',	'AUD',	'AUD',	1.0000,	1.0000,	NULL,	NULL,	3,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	'117.0.21.77',	NULL,	NULL,	NULL,	NULL,	'AUD',	1.0000,	1.0000,	NULL,	NULL,	1.0000,	1.0000,	1.0000,	1.0000,	1,	0,	NULL,	NULL,	0),
(4,	1,	'2017-01-03 00:00:17',	'2017-01-03 00:00:46',	NULL,	1,	0,	0,	1,	1.0000,	0,	1.0000,	1.0000,	'AUD',	'AUD',	'AUD',	123.0000,	123.0000,	NULL,	NULL,	3,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	'220.244.30.158',	NULL,	NULL,	NULL,	NULL,	'AUD',	1.0000,	1.0000,	NULL,	NULL,	123.0000,	123.0000,	123.0000,	123.0000,	1,	0,	NULL,	NULL,	0),
(5,	1,	'2017-01-03 11:43:19',	'2017-01-03 11:43:48',	NULL,	1,	0,	0,	1,	1.0000,	0,	1.0000,	1.0000,	'AUD',	'AUD',	'AUD',	123.0000,	123.0000,	NULL,	NULL,	3,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	'101.176.232.162',	NULL,	NULL,	NULL,	NULL,	'AUD',	1.0000,	1.0000,	NULL,	NULL,	123.0000,	123.0000,	123.0000,	123.0000,	1,	0,	NULL,	NULL,	0);

DROP TABLE IF EXISTS `sales_flat_quote_address`;
CREATE TABLE `sales_flat_quote_address` (
  `address_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Address Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `save_in_address_book` smallint(6) DEFAULT '0' COMMENT 'Save In Address Book',
  `customer_address_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Address Id',
  `address_type` varchar(255) DEFAULT NULL COMMENT 'Address Type',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `prefix` varchar(40) DEFAULT NULL COMMENT 'Prefix',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Firstname',
  `middlename` varchar(40) DEFAULT NULL COMMENT 'Middlename',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Lastname',
  `suffix` varchar(40) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(255) DEFAULT NULL COMMENT 'City',
  `region` varchar(255) DEFAULT NULL COMMENT 'Region',
  `region_id` int(10) unsigned DEFAULT NULL COMMENT 'Region Id',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Postcode',
  `country_id` varchar(255) DEFAULT NULL COMMENT 'Country Id',
  `telephone` varchar(255) DEFAULT NULL COMMENT 'Telephone',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `same_as_billing` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Same As Billing',
  `free_shipping` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Free Shipping',
  `collect_shipping_rates` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Collect Shipping Rates',
  `shipping_method` varchar(255) DEFAULT NULL COMMENT 'Shipping Method',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `weight` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Weight',
  `subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Subtotal',
  `subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal With Discount',
  `base_subtotal_with_discount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Subtotal With Discount',
  `tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Shipping Amount',
  `base_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Shipping Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Grand Total',
  `customer_notes` text COMMENT 'Customer Notes',
  `applied_taxes` text COMMENT 'Applied Taxes',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Total Incl Tax',
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Amount',
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Amount',
  `shipping_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Hidden Tax Amount',
  `base_shipping_hidden_tax_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Hidden Tax Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`address_id`),
  KEY `IDX_SALES_FLAT_QUOTE_ADDRESS_QUOTE_ID` (`quote_id`),
  CONSTRAINT `FK_SALES_FLAT_QUOTE_ADDRESS_QUOTE_ID_SALES_FLAT_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `sales_flat_quote` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Address';

INSERT INTO `sales_flat_quote_address` (`address_id`, `quote_id`, `created_at`, `updated_at`, `customer_id`, `save_in_address_book`, `customer_address_id`, `address_type`, `email`, `prefix`, `firstname`, `middlename`, `lastname`, `suffix`, `company`, `street`, `city`, `region`, `region_id`, `postcode`, `country_id`, `telephone`, `fax`, `same_as_billing`, `free_shipping`, `collect_shipping_rates`, `shipping_method`, `shipping_description`, `weight`, `subtotal`, `base_subtotal`, `subtotal_with_discount`, `base_subtotal_with_discount`, `tax_amount`, `base_tax_amount`, `shipping_amount`, `base_shipping_amount`, `shipping_tax_amount`, `base_shipping_tax_amount`, `discount_amount`, `base_discount_amount`, `grand_total`, `base_grand_total`, `customer_notes`, `applied_taxes`, `discount_description`, `shipping_discount_amount`, `base_shipping_discount_amount`, `subtotal_incl_tax`, `base_subtotal_total_incl_tax`, `hidden_tax_amount`, `base_hidden_tax_amount`, `shipping_hidden_tax_amount`, `base_shipping_hidden_tax_amnt`, `shipping_incl_tax`, `base_shipping_incl_tax`, `vat_id`, `vat_is_valid`, `vat_request_id`, `vat_request_date`, `vat_request_success`, `gift_message_id`) VALUES
(1,	1,	'2016-12-19 05:27:39',	'2016-12-19 05:27:40',	NULL,	0,	NULL,	'billing',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	0,	0,	NULL,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	NULL,	'a:0:{}',	NULL,	NULL,	NULL,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	0.0000,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(2,	1,	'2016-12-19 05:27:39',	'2016-12-19 05:27:40',	NULL,	0,	NULL,	'shipping',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	0,	NULL,	NULL,	1.0000,	1.0000,	1.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	1.0000,	1.0000,	NULL,	'a:0:{}',	NULL,	0.0000,	0.0000,	1.0000,	NULL,	0.0000,	0.0000,	0.0000,	NULL,	0.0000,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(3,	2,	'2016-12-24 04:10:34',	'2016-12-24 04:10:35',	NULL,	0,	NULL,	'billing',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	0,	0,	NULL,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	NULL,	'a:0:{}',	NULL,	NULL,	NULL,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	0.0000,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(4,	2,	'2016-12-24 04:10:34',	'2016-12-24 04:10:35',	NULL,	0,	NULL,	'shipping',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	0,	NULL,	NULL,	1.0000,	1.0000,	1.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	1.0000,	1.0000,	NULL,	'a:0:{}',	NULL,	0.0000,	0.0000,	1.0000,	NULL,	0.0000,	0.0000,	0.0000,	NULL,	0.0000,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(5,	3,	'2016-12-31 16:51:34',	'2016-12-31 16:51:34',	NULL,	0,	NULL,	'billing',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	0,	0,	NULL,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	NULL,	'a:0:{}',	NULL,	NULL,	NULL,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	0.0000,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(6,	3,	'2016-12-31 16:51:34',	'2016-12-31 16:51:34',	NULL,	0,	NULL,	'shipping',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	0,	NULL,	NULL,	1.0000,	1.0000,	1.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	1.0000,	1.0000,	NULL,	'a:0:{}',	NULL,	0.0000,	0.0000,	1.0000,	NULL,	0.0000,	0.0000,	0.0000,	NULL,	0.0000,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(7,	4,	'2017-01-03 00:00:17',	'2017-01-03 00:00:46',	NULL,	0,	NULL,	'billing',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	0,	0,	NULL,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	NULL,	'a:0:{}',	NULL,	NULL,	NULL,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	0.0000,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(8,	4,	'2017-01-03 00:00:17',	'2017-01-03 00:00:46',	NULL,	0,	NULL,	'shipping',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	0,	NULL,	NULL,	1.0000,	123.0000,	123.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	123.0000,	123.0000,	NULL,	'a:0:{}',	NULL,	0.0000,	0.0000,	123.0000,	NULL,	0.0000,	0.0000,	0.0000,	NULL,	0.0000,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(9,	5,	'2017-01-03 11:43:19',	'2017-01-03 11:43:48',	NULL,	0,	NULL,	'billing',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	0,	0,	NULL,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	NULL,	'a:0:{}',	NULL,	NULL,	NULL,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	0.0000,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(10,	5,	'2017-01-03 11:43:19',	'2017-01-03 11:43:48',	NULL,	0,	NULL,	'shipping',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	0,	NULL,	NULL,	1.0000,	123.0000,	123.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	123.0000,	123.0000,	NULL,	'a:0:{}',	NULL,	0.0000,	0.0000,	123.0000,	NULL,	0.0000,	0.0000,	0.0000,	NULL,	0.0000,	0.0000,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `sales_flat_quote_address_item`;
CREATE TABLE `sales_flat_quote_address_item` (
  `address_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Address Item Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Address Id',
  `quote_item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Item Id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Total With Discount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `super_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Super Product Id',
  `parent_product_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Product Id',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `image` varchar(255) DEFAULT NULL COMMENT 'Image',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `free_shipping` int(10) unsigned DEFAULT NULL COMMENT 'Free Shipping',
  `is_qty_decimal` int(10) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `discount_percent` decimal(12,4) DEFAULT NULL COMMENT 'Discount Percent',
  `no_discount` int(10) unsigned DEFAULT NULL COMMENT 'No Discount',
  `tax_percent` decimal(12,4) DEFAULT NULL COMMENT 'Tax Percent',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Amount',
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Amount',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  PRIMARY KEY (`address_item_id`),
  KEY `IDX_SALES_FLAT_QUOTE_ADDRESS_ITEM_QUOTE_ADDRESS_ID` (`quote_address_id`),
  KEY `IDX_SALES_FLAT_QUOTE_ADDRESS_ITEM_PARENT_ITEM_ID` (`parent_item_id`),
  KEY `IDX_SALES_FLAT_QUOTE_ADDRESS_ITEM_QUOTE_ITEM_ID` (`quote_item_id`),
  CONSTRAINT `FK_2EF8E28181D666D94D4E30DC2B0F80BF` FOREIGN KEY (`quote_item_id`) REFERENCES `sales_flat_quote_item` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_A345FC758F20C314169CE27DCE53477F` FOREIGN KEY (`parent_item_id`) REFERENCES `sales_flat_quote_address_item` (`address_item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_B521389746C00700D1B2B76EBBE53854` FOREIGN KEY (`quote_address_id`) REFERENCES `sales_flat_quote_address` (`address_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Address Item';


DROP TABLE IF EXISTS `sales_flat_quote_item`;
CREATE TABLE `sales_flat_quote_item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
  `product_id` int(10) unsigned DEFAULT NULL COMMENT 'Product Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `parent_item_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Item Id',
  `is_virtual` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `free_shipping` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Free Shipping',
  `is_qty_decimal` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) unsigned DEFAULT '0' COMMENT 'No Discount',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `qty` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `custom_price` decimal(12,4) DEFAULT NULL COMMENT 'Custom Price',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_total_with_discount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Total With Discount',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `original_custom_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Custom Price',
  `redirect_url` varchar(255) DEFAULT NULL COMMENT 'Redirect Url',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Hidden Tax Amount',
  `base_hidden_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Hidden Tax Amount',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  PRIMARY KEY (`item_id`),
  KEY `IDX_SALES_FLAT_QUOTE_ITEM_PARENT_ITEM_ID` (`parent_item_id`),
  KEY `IDX_SALES_FLAT_QUOTE_ITEM_PRODUCT_ID` (`product_id`),
  KEY `IDX_SALES_FLAT_QUOTE_ITEM_QUOTE_ID` (`quote_id`),
  KEY `IDX_SALES_FLAT_QUOTE_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `FK_B201DEB5DE51B791AF5C5BF87053C5A7` FOREIGN KEY (`parent_item_id`) REFERENCES `sales_flat_quote_item` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_QUOTE_ITEM_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_QUOTE_ITEM_QUOTE_ID_SALES_FLAT_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `sales_flat_quote` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_QUOTE_ITEM_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Item';

INSERT INTO `sales_flat_quote_item` (`item_id`, `quote_id`, `created_at`, `updated_at`, `product_id`, `store_id`, `parent_item_id`, `is_virtual`, `sku`, `name`, `description`, `applied_rule_ids`, `additional_data`, `free_shipping`, `is_qty_decimal`, `no_discount`, `weight`, `qty`, `price`, `base_price`, `custom_price`, `discount_percent`, `discount_amount`, `base_discount_amount`, `tax_percent`, `tax_amount`, `base_tax_amount`, `row_total`, `base_row_total`, `row_total_with_discount`, `row_weight`, `product_type`, `base_tax_before_discount`, `tax_before_discount`, `original_custom_price`, `redirect_url`, `base_cost`, `price_incl_tax`, `base_price_incl_tax`, `row_total_incl_tax`, `base_row_total_incl_tax`, `hidden_tax_amount`, `base_hidden_tax_amount`, `gift_message_id`, `weee_tax_disposition`, `weee_tax_row_disposition`, `base_weee_tax_disposition`, `base_weee_tax_row_disposition`, `weee_tax_applied`, `weee_tax_applied_amount`, `weee_tax_applied_row_amount`, `base_weee_tax_applied_amount`, `base_weee_tax_applied_row_amnt`) VALUES
(1,	1,	'2016-12-19 05:27:39',	'2016-12-19 05:27:39',	1,	1,	NULL,	0,	'00011',	'Complete Whitening + Scope',	NULL,	NULL,	NULL,	0,	0,	0,	1.0000,	1.0000,	1.0000,	1.0000,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	1.0000,	1.0000,	0.0000,	1.0000,	'simple',	NULL,	NULL,	NULL,	NULL,	NULL,	1.0000,	1.0000,	1.0000,	1.0000,	0.0000,	0.0000,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	'a:0:{}',	0.0000,	0.0000,	0.0000,	NULL),
(2,	2,	'2016-12-24 04:10:34',	'2016-12-24 04:10:34',	1,	1,	NULL,	0,	'00011',	'Complete Whitening + Scope',	NULL,	NULL,	NULL,	0,	0,	0,	1.0000,	1.0000,	1.0000,	1.0000,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	1.0000,	1.0000,	0.0000,	1.0000,	'simple',	NULL,	NULL,	NULL,	NULL,	NULL,	1.0000,	1.0000,	1.0000,	1.0000,	0.0000,	0.0000,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	'a:0:{}',	0.0000,	0.0000,	0.0000,	NULL),
(3,	3,	'2016-12-31 16:51:34',	'2016-12-31 16:51:34',	1,	1,	NULL,	0,	'00011',	'Complete Whitening + Scope',	NULL,	NULL,	NULL,	0,	0,	0,	1.0000,	1.0000,	1.0000,	1.0000,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	1.0000,	1.0000,	0.0000,	1.0000,	'simple',	NULL,	NULL,	NULL,	NULL,	NULL,	1.0000,	1.0000,	1.0000,	1.0000,	0.0000,	0.0000,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	'a:0:{}',	0.0000,	0.0000,	0.0000,	NULL),
(4,	4,	'2017-01-03 00:00:17',	'2017-01-03 00:00:17',	7,	1,	NULL,	0,	'test06',	'Lorem ipsum dolor 6',	NULL,	NULL,	NULL,	0,	0,	0,	1.0000,	1.0000,	123.0000,	123.0000,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	123.0000,	123.0000,	0.0000,	1.0000,	'simple',	NULL,	NULL,	NULL,	NULL,	NULL,	123.0000,	123.0000,	123.0000,	123.0000,	0.0000,	0.0000,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	'a:0:{}',	0.0000,	0.0000,	0.0000,	NULL),
(6,	5,	'2017-01-03 11:43:19',	'2017-01-03 11:43:19',	5,	1,	NULL,	0,	'test04',	'Lorem ipsum dolor 4',	NULL,	NULL,	NULL,	0,	0,	0,	1.0000,	1.0000,	123.0000,	123.0000,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	123.0000,	123.0000,	0.0000,	1.0000,	'simple',	NULL,	NULL,	NULL,	NULL,	NULL,	123.0000,	123.0000,	123.0000,	123.0000,	0.0000,	0.0000,	NULL,	0.0000,	0.0000,	0.0000,	0.0000,	'a:0:{}',	0.0000,	0.0000,	0.0000,	NULL);

DROP TABLE IF EXISTS `sales_flat_quote_item_option`;
CREATE TABLE `sales_flat_quote_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `item_id` int(10) unsigned NOT NULL COMMENT 'Item Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`option_id`),
  KEY `IDX_SALES_FLAT_QUOTE_ITEM_OPTION_ITEM_ID` (`item_id`),
  CONSTRAINT `FK_5F20E478CA64B6891EA8A9D6C2735739` FOREIGN KEY (`item_id`) REFERENCES `sales_flat_quote_item` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Item Option';

INSERT INTO `sales_flat_quote_item_option` (`option_id`, `item_id`, `product_id`, `code`, `value`) VALUES
(1,	1,	1,	'info_buyRequest',	'a:4:{s:4:\"uenc\";s:44:\"aHR0cDovL2F1c3dhcmVob3VzZS5jb3JyZS5jb20uYXUv\";s:7:\"product\";s:1:\"1\";s:8:\"form_key\";s:16:\"CLgA1t3cULZJbxcc\";s:3:\"qty\";d:1;}'),
(2,	2,	1,	'info_buyRequest',	'a:4:{s:4:\"uenc\";s:44:\"aHR0cDovL2F1c3dhcmVob3VzZS5jb3JyZS5jb20uYXUv\";s:7:\"product\";s:1:\"1\";s:8:\"form_key\";s:16:\"WVRTaST6TxzJh4Bw\";s:3:\"qty\";d:1;}'),
(3,	3,	1,	'info_buyRequest',	'a:4:{s:4:\"uenc\";s:44:\"aHR0cDovL2F1c3dhcmVob3VzZS5jb3JyZS5jb20uYXUv\";s:7:\"product\";s:1:\"1\";s:8:\"form_key\";s:16:\"8vOPebgXqPWNEwrP\";s:3:\"qty\";d:1;}'),
(4,	4,	7,	'info_buyRequest',	'a:4:{s:4:\"uenc\";s:44:\"aHR0cDovL2F1c3dhcmVob3VzZS5jb3JyZS5jb20uYXUv\";s:7:\"product\";s:1:\"7\";s:8:\"form_key\";s:16:\"ulvGe1P1chixsBt9\";s:3:\"qty\";d:1;}'),
(6,	6,	5,	'info_buyRequest',	'a:4:{s:4:\"uenc\";s:44:\"aHR0cDovL2F1c3dhcmVob3VzZS5jb3JyZS5jb20uYXUv\";s:7:\"product\";s:1:\"5\";s:8:\"form_key\";s:16:\"UFr3WqUfWnEBs7T8\";s:3:\"qty\";d:1;}');

DROP TABLE IF EXISTS `sales_flat_quote_payment`;
CREATE TABLE `sales_flat_quote_payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Payment Id',
  `quote_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Quote Id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `cc_type` varchar(255) DEFAULT NULL COMMENT 'Cc Type',
  `cc_number_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Number Enc',
  `cc_last4` varchar(255) DEFAULT NULL COMMENT 'Cc Last4',
  `cc_cid_enc` varchar(255) DEFAULT NULL COMMENT 'Cc Cid Enc',
  `cc_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_exp_month` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Exp Month',
  `cc_exp_year` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Exp Year',
  `cc_ss_owner` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Owner',
  `cc_ss_start_month` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Ss Start Month',
  `cc_ss_start_year` smallint(5) unsigned DEFAULT '0' COMMENT 'Cc Ss Start Year',
  `po_number` varchar(255) DEFAULT NULL COMMENT 'Po Number',
  `additional_data` text COMMENT 'Additional Data',
  `cc_ss_issue` varchar(255) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `additional_information` text COMMENT 'Additional Information',
  `paypal_payer_id` varchar(255) DEFAULT NULL COMMENT 'Paypal Payer Id',
  `paypal_payer_status` varchar(255) DEFAULT NULL COMMENT 'Paypal Payer Status',
  `paypal_correlation_id` varchar(255) DEFAULT NULL COMMENT 'Paypal Correlation Id',
  PRIMARY KEY (`payment_id`),
  KEY `IDX_SALES_FLAT_QUOTE_PAYMENT_QUOTE_ID` (`quote_id`),
  CONSTRAINT `FK_SALES_FLAT_QUOTE_PAYMENT_QUOTE_ID_SALES_FLAT_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `sales_flat_quote` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Payment';


DROP TABLE IF EXISTS `sales_flat_quote_shipping_rate`;
CREATE TABLE `sales_flat_quote_shipping_rate` (
  `rate_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Rate Id',
  `address_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Address Id',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
  `carrier` varchar(255) DEFAULT NULL COMMENT 'Carrier',
  `carrier_title` varchar(255) DEFAULT NULL COMMENT 'Carrier Title',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `method` varchar(255) DEFAULT NULL COMMENT 'Method',
  `method_description` text COMMENT 'Method Description',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `error_message` text COMMENT 'Error Message',
  `method_title` text COMMENT 'Method Title',
  PRIMARY KEY (`rate_id`),
  KEY `IDX_SALES_FLAT_QUOTE_SHIPPING_RATE_ADDRESS_ID` (`address_id`),
  CONSTRAINT `FK_B1F177EFB73D3EDF5322BA64AC48D150` FOREIGN KEY (`address_id`) REFERENCES `sales_flat_quote_address` (`address_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Quote Shipping Rate';


DROP TABLE IF EXISTS `sales_flat_shipment`;
CREATE TABLE `sales_flat_shipment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `total_weight` decimal(12,4) DEFAULT NULL COMMENT 'Total Weight',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `packages` text COMMENT 'Packed Products in Packages',
  `shipping_label` mediumblob COMMENT 'Shipping Label Content',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_SALES_FLAT_SHIPMENT_INCREMENT_ID` (`increment_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_STORE_ID` (`store_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_TOTAL_QTY` (`total_qty`),
  KEY `IDX_SALES_FLAT_SHIPMENT_ORDER_ID` (`order_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_CREATED_AT` (`created_at`),
  KEY `IDX_SALES_FLAT_SHIPMENT_UPDATED_AT` (`updated_at`),
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_ORDER_ID_SALES_FLAT_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment';


DROP TABLE IF EXISTS `sales_flat_shipment_comment`;
CREATE TABLE `sales_flat_shipment_comment` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_COMMENT_CREATED_AT` (`created_at`),
  KEY `IDX_SALES_FLAT_SHIPMENT_COMMENT_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_C2D69CC1FB03D2B2B794B0439F6650CF` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_shipment` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Comment';


DROP TABLE IF EXISTS `sales_flat_shipment_grid`;
CREATE TABLE `sales_flat_shipment_grid` (
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `shipping_name` varchar(255) DEFAULT NULL COMMENT 'Shipping Name',
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `UNQ_SALES_FLAT_SHIPMENT_GRID_INCREMENT_ID` (`increment_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_GRID_STORE_ID` (`store_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_GRID_TOTAL_QTY` (`total_qty`),
  KEY `IDX_SALES_FLAT_SHIPMENT_GRID_ORDER_ID` (`order_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_GRID_SHIPMENT_STATUS` (`shipment_status`),
  KEY `IDX_SALES_FLAT_SHIPMENT_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_GRID_CREATED_AT` (`created_at`),
  KEY `IDX_SALES_FLAT_SHIPMENT_GRID_ORDER_CREATED_AT` (`order_created_at`),
  KEY `IDX_SALES_FLAT_SHIPMENT_GRID_SHIPPING_NAME` (`shipping_name`),
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_GRID_ENTT_ID_SALES_FLAT_SHIPMENT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `sales_flat_shipment` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_FLAT_SHIPMENT_GRID_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Grid';


DROP TABLE IF EXISTS `sales_flat_shipment_item`;
CREATE TABLE `sales_flat_shipment_item` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_ITEM_PARENT_ID` (`parent_id`),
  CONSTRAINT `FK_3AECE5007D18F159231B87E8306FC02A` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_shipment` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Item';


DROP TABLE IF EXISTS `sales_flat_shipment_track`;
CREATE TABLE `sales_flat_shipment_track` (
  `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
  `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `track_number` text COMMENT 'Number',
  `description` text COMMENT 'Description',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `carrier_code` varchar(32) DEFAULT NULL COMMENT 'Carrier Code',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  PRIMARY KEY (`entity_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_TRACK_PARENT_ID` (`parent_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_TRACK_ORDER_ID` (`order_id`),
  KEY `IDX_SALES_FLAT_SHIPMENT_TRACK_CREATED_AT` (`created_at`),
  CONSTRAINT `FK_BCD2FA28717D29F37E10A153E6F2F841` FOREIGN KEY (`parent_id`) REFERENCES `sales_flat_shipment` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Track';


DROP TABLE IF EXISTS `sales_invoiced_aggregated`;
CREATE TABLE `sales_invoiced_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_SALES_INVOICED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_SALES_INVOICED_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_INVOICED_AGGREGATED_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated';


DROP TABLE IF EXISTS `sales_invoiced_aggregated_order`;
CREATE TABLE `sales_invoiced_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL DEFAULT '' COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_SALES_INVOICED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_SALES_INVOICED_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_INVOICED_AGGREGATED_ORDER_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated Order';


DROP TABLE IF EXISTS `sales_order_aggregated_created`;
CREATE TABLE `sales_order_aggregated_created` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL DEFAULT '' COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_SALES_ORDER_AGGREGATED_CREATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_SALES_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_ORDER_AGGREGATED_CREATED_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Created';


DROP TABLE IF EXISTS `sales_order_aggregated_updated`;
CREATE TABLE `sales_order_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_SALES_ORDER_AGGREGATED_UPDATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_SALES_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_ORDER_AGGREGATED_UPDATED_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Updated';


DROP TABLE IF EXISTS `sales_order_status`;
CREATE TABLE `sales_order_status` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `label` varchar(128) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';

INSERT INTO `sales_order_status` (`status`, `label`) VALUES
('canceled',	'Canceled'),
('closed',	'Closed'),
('complete',	'Complete'),
('fraud',	'Suspected Fraud'),
('holded',	'On Hold'),
('payment_review',	'Payment Review'),
('paypal_canceled_reversal',	'PayPal Canceled Reversal'),
('paypal_reversed',	'PayPal Reversed'),
('pending',	'Pending'),
('pending_payment',	'Pending Payment'),
('pending_paypal',	'Pending PayPal'),
('processing',	'Processing');

DROP TABLE IF EXISTS `sales_order_status_label`;
CREATE TABLE `sales_order_status_label` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `label` varchar(128) NOT NULL COMMENT 'Label',
  PRIMARY KEY (`status`,`store_id`),
  KEY `IDX_SALES_ORDER_STATUS_LABEL_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_ORDER_STATUS_LABEL_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_ORDER_STATUS_LABEL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Label Table';


DROP TABLE IF EXISTS `sales_order_status_state`;
CREATE TABLE `sales_order_status_state` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `state` varchar(32) NOT NULL COMMENT 'Label',
  `is_default` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Default',
  PRIMARY KEY (`status`,`state`),
  CONSTRAINT `FK_SALES_ORDER_STATUS_STATE_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';

INSERT INTO `sales_order_status_state` (`status`, `state`, `is_default`) VALUES
('canceled',	'canceled',	1),
('closed',	'closed',	1),
('complete',	'complete',	1),
('fraud',	'payment_review',	0),
('holded',	'holded',	1),
('payment_review',	'payment_review',	1),
('pending',	'new',	1),
('pending_payment',	'pending_payment',	1),
('processing',	'processing',	1);

DROP TABLE IF EXISTS `sales_order_tax`;
CREATE TABLE `sales_order_tax` (
  `tax_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tax Id',
  `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `percent` decimal(12,4) DEFAULT NULL COMMENT 'Percent',
  `amount` decimal(12,4) DEFAULT NULL COMMENT 'Amount',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `base_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount',
  `process` smallint(6) NOT NULL COMMENT 'Process',
  `base_real_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Real Amount',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Hidden',
  PRIMARY KEY (`tax_id`),
  KEY `IDX_SALES_ORDER_TAX_ORDER_ID_PRIORITY_POSITION` (`order_id`,`priority`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Table';


DROP TABLE IF EXISTS `sales_order_tax_item`;
CREATE TABLE `sales_order_tax_item` (
  `tax_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tax Item Id',
  `tax_id` int(10) unsigned NOT NULL COMMENT 'Tax Id',
  `item_id` int(10) unsigned NOT NULL COMMENT 'Item Id',
  `tax_percent` decimal(12,4) NOT NULL COMMENT 'Real Tax Percent For Item',
  PRIMARY KEY (`tax_item_id`),
  UNIQUE KEY `UNQ_SALES_ORDER_TAX_ITEM_TAX_ID_ITEM_ID` (`tax_id`,`item_id`),
  KEY `IDX_SALES_ORDER_TAX_ITEM_TAX_ID` (`tax_id`),
  KEY `IDX_SALES_ORDER_TAX_ITEM_ITEM_ID` (`item_id`),
  CONSTRAINT `FK_SALES_ORDER_TAX_ITEM_ITEM_ID_SALES_FLAT_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `sales_flat_order_item` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_ORDER_TAX_ITEM_TAX_ID_SALES_ORDER_TAX_TAX_ID` FOREIGN KEY (`tax_id`) REFERENCES `sales_order_tax` (`tax_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Item';


DROP TABLE IF EXISTS `sales_payment_transaction`;
CREATE TABLE `sales_payment_transaction` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Transaction Id',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'Parent Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `payment_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Payment Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `parent_txn_id` varchar(100) DEFAULT NULL COMMENT 'Parent Txn Id',
  `txn_type` varchar(15) DEFAULT NULL COMMENT 'Txn Type',
  `is_closed` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Closed',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`transaction_id`),
  UNIQUE KEY `UNQ_SALES_PAYMENT_TRANSACTION_ORDER_ID_PAYMENT_ID_TXN_ID` (`order_id`,`payment_id`,`txn_id`),
  KEY `IDX_SALES_PAYMENT_TRANSACTION_ORDER_ID` (`order_id`),
  KEY `IDX_SALES_PAYMENT_TRANSACTION_PARENT_ID` (`parent_id`),
  KEY `IDX_SALES_PAYMENT_TRANSACTION_PAYMENT_ID` (`payment_id`),
  CONSTRAINT `FK_B99FF1A06402D725EBDB0F3A7ECD47A2` FOREIGN KEY (`parent_id`) REFERENCES `sales_payment_transaction` (`transaction_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DA51A10B2405B64A4DAEF77A64F0DAAD` FOREIGN KEY (`payment_id`) REFERENCES `sales_flat_order_payment` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_PAYMENT_TRANSACTION_ORDER_ID_SALES_FLAT_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Payment Transaction';


DROP TABLE IF EXISTS `sales_recurring_profile`;
CREATE TABLE `sales_recurring_profile` (
  `profile_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Profile Id',
  `state` varchar(20) NOT NULL COMMENT 'State',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `method_code` varchar(32) NOT NULL COMMENT 'Method Code',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `reference_id` varchar(32) DEFAULT NULL COMMENT 'Reference Id',
  `subscriber_name` varchar(150) DEFAULT NULL COMMENT 'Subscriber Name',
  `start_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Start Datetime',
  `internal_reference_id` varchar(42) NOT NULL COMMENT 'Internal Reference Id',
  `schedule_description` varchar(255) NOT NULL COMMENT 'Schedule Description',
  `suspension_threshold` smallint(5) unsigned DEFAULT NULL COMMENT 'Suspension Threshold',
  `bill_failed_later` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Bill Failed Later',
  `period_unit` varchar(20) NOT NULL COMMENT 'Period Unit',
  `period_frequency` smallint(5) unsigned DEFAULT NULL COMMENT 'Period Frequency',
  `period_max_cycles` smallint(5) unsigned DEFAULT NULL COMMENT 'Period Max Cycles',
  `billing_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Billing Amount',
  `trial_period_unit` varchar(20) DEFAULT NULL COMMENT 'Trial Period Unit',
  `trial_period_frequency` smallint(5) unsigned DEFAULT NULL COMMENT 'Trial Period Frequency',
  `trial_period_max_cycles` smallint(5) unsigned DEFAULT NULL COMMENT 'Trial Period Max Cycles',
  `trial_billing_amount` text COMMENT 'Trial Billing Amount',
  `currency_code` varchar(3) NOT NULL COMMENT 'Currency Code',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `init_amount` decimal(12,4) DEFAULT NULL COMMENT 'Init Amount',
  `init_may_fail` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Init May Fail',
  `order_info` text NOT NULL COMMENT 'Order Info',
  `order_item_info` text NOT NULL COMMENT 'Order Item Info',
  `billing_address_info` text NOT NULL COMMENT 'Billing Address Info',
  `shipping_address_info` text COMMENT 'Shipping Address Info',
  `profile_vendor_info` text COMMENT 'Profile Vendor Info',
  `additional_info` text COMMENT 'Additional Info',
  PRIMARY KEY (`profile_id`),
  UNIQUE KEY `UNQ_SALES_RECURRING_PROFILE_INTERNAL_REFERENCE_ID` (`internal_reference_id`),
  KEY `IDX_SALES_RECURRING_PROFILE_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_SALES_RECURRING_PROFILE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_RECURRING_PROFILE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_RECURRING_PROFILE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Recurring Profile';


DROP TABLE IF EXISTS `sales_recurring_profile_order`;
CREATE TABLE `sales_recurring_profile_order` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Link Id',
  `profile_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Profile Id',
  `order_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Order Id',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNQ_SALES_RECURRING_PROFILE_ORDER_PROFILE_ID_ORDER_ID` (`profile_id`,`order_id`),
  KEY `IDX_SALES_RECURRING_PROFILE_ORDER_ORDER_ID` (`order_id`),
  CONSTRAINT `FK_7FF85741C66DCD37A4FBE3E3255A5A01` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_B8A7A5397B67455786E55461748C59F4` FOREIGN KEY (`profile_id`) REFERENCES `sales_recurring_profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Recurring Profile Order';


DROP TABLE IF EXISTS `sales_refunded_aggregated`;
CREATE TABLE `sales_refunded_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL DEFAULT '' COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_SALES_REFUNDED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_SALES_REFUNDED_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_REFUNDED_AGGREGATED_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated';


DROP TABLE IF EXISTS `sales_refunded_aggregated_order`;
CREATE TABLE `sales_refunded_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_SALES_REFUNDED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  KEY `IDX_SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated Order';


DROP TABLE IF EXISTS `sales_shipping_aggregated`;
CREATE TABLE `sales_shipping_aggregated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_SALES_SHPP_AGGRED_PERIOD_STORE_ID_ORDER_STS_SHPP_DESCRIPTION` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `IDX_SALES_SHIPPING_AGGREGATED_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_SHIPPING_AGGREGATED_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated';


DROP TABLE IF EXISTS `sales_shipping_aggregated_order`;
CREATE TABLE `sales_shipping_aggregated_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual',
  PRIMARY KEY (`id`),
  UNIQUE KEY `C05FAE47282EEA68654D0924E946761F` (`period`,`store_id`,`order_status`,`shipping_description`),
  KEY `IDX_SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated Order';


DROP TABLE IF EXISTS `sendfriend_log`;
CREATE TABLE `sendfriend_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Log ID',
  `ip` varbinary(16) DEFAULT NULL,
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Log time',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website ID',
  PRIMARY KEY (`log_id`),
  KEY `IDX_SENDFRIEND_LOG_IP` (`ip`),
  KEY `IDX_SENDFRIEND_LOG_TIME` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Send to friend function log storage table';


DROP TABLE IF EXISTS `shipping_tablerate`;
CREATE TABLE `shipping_tablerate` (
  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `website_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `dest_country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Destination coutry ISO/2 or ISO/3 code',
  `dest_region_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Destination Region Id',
  `dest_zip` varchar(10) NOT NULL DEFAULT '*' COMMENT 'Destination Post Code (Zip)',
  `condition_name` varchar(20) NOT NULL COMMENT 'Rate Condition name',
  `condition_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rate condition value',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `cost` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Cost',
  PRIMARY KEY (`pk`),
  UNIQUE KEY `D60821CDB2AFACEE1566CFC02D0D4CAA` (`website_id`,`dest_country_id`,`dest_region_id`,`dest_zip`,`condition_name`,`condition_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Shipping Tablerate';


DROP TABLE IF EXISTS `sitemap`;
CREATE TABLE `sitemap` (
  `sitemap_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Sitemap Id',
  `sitemap_type` varchar(32) DEFAULT NULL COMMENT 'Sitemap Type',
  `sitemap_filename` varchar(32) DEFAULT NULL COMMENT 'Sitemap Filename',
  `sitemap_path` varchar(255) DEFAULT NULL COMMENT 'Sitemap Path',
  `sitemap_time` timestamp NULL DEFAULT NULL COMMENT 'Sitemap Time',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store id',
  PRIMARY KEY (`sitemap_id`),
  KEY `IDX_SITEMAP_STORE_ID` (`store_id`),
  CONSTRAINT `FK_SITEMAP_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Google Sitemap';


DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tag Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `status` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Status',
  `first_customer_id` int(10) unsigned DEFAULT NULL COMMENT 'First Customer Id',
  `first_store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'First Store Id',
  PRIMARY KEY (`tag_id`),
  KEY `FK_TAG_FIRST_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`first_customer_id`),
  KEY `FK_TAG_FIRST_STORE_ID_CORE_STORE_STORE_ID` (`first_store_id`),
  CONSTRAINT `FK_TAG_FIRST_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`first_customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_TAG_FIRST_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`first_store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag';


DROP TABLE IF EXISTS `tag_properties`;
CREATE TABLE `tag_properties` (
  `tag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Tag Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `base_popularity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Base Popularity',
  PRIMARY KEY (`tag_id`,`store_id`),
  KEY `IDX_TAG_PROPERTIES_STORE_ID` (`store_id`),
  CONSTRAINT `FK_TAG_PROPERTIES_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAG_PROPERTIES_TAG_ID_TAG_TAG_ID` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag Properties';


DROP TABLE IF EXISTS `tag_relation`;
CREATE TABLE `tag_relation` (
  `tag_relation_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tag Relation Id',
  `tag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Tag Id',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'Customer Id',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Store Id',
  `active` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Active',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  PRIMARY KEY (`tag_relation_id`),
  UNIQUE KEY `UNQ_TAG_RELATION_TAG_ID_CUSTOMER_ID_PRODUCT_ID_STORE_ID` (`tag_id`,`customer_id`,`product_id`,`store_id`),
  KEY `IDX_TAG_RELATION_PRODUCT_ID` (`product_id`),
  KEY `IDX_TAG_RELATION_TAG_ID` (`tag_id`),
  KEY `IDX_TAG_RELATION_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_TAG_RELATION_STORE_ID` (`store_id`),
  CONSTRAINT `FK_TAG_RELATION_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAG_RELATION_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAG_RELATION_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAG_RELATION_TAG_ID_TAG_TAG_ID` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag Relation';


DROP TABLE IF EXISTS `tag_summary`;
CREATE TABLE `tag_summary` (
  `tag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Tag Id',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `customers` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customers',
  `products` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Products',
  `uses` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Uses',
  `historical_uses` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Historical Uses',
  `popularity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Popularity',
  `base_popularity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Base Popularity',
  PRIMARY KEY (`tag_id`,`store_id`),
  KEY `IDX_TAG_SUMMARY_STORE_ID` (`store_id`),
  KEY `IDX_TAG_SUMMARY_TAG_ID` (`tag_id`),
  CONSTRAINT `FK_TAG_SUMMARY_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAG_SUMMARY_TAG_ID_TAG_TAG_ID` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag Summary';


DROP TABLE IF EXISTS `tax_calculation`;
CREATE TABLE `tax_calculation` (
  `tax_calculation_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `tax_calculation_rule_id` int(11) NOT NULL COMMENT 'Tax Calculation Rule Id',
  `customer_tax_class_id` smallint(6) NOT NULL COMMENT 'Customer Tax Class Id',
  `product_tax_class_id` smallint(6) NOT NULL COMMENT 'Product Tax Class Id',
  PRIMARY KEY (`tax_calculation_id`),
  KEY `IDX_TAX_CALCULATION_TAX_CALCULATION_RULE_ID` (`tax_calculation_rule_id`),
  KEY `IDX_TAX_CALCULATION_TAX_CALCULATION_RATE_ID` (`tax_calculation_rate_id`),
  KEY `IDX_TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID` (`customer_tax_class_id`),
  KEY `IDX_TAX_CALCULATION_PRODUCT_TAX_CLASS_ID` (`product_tax_class_id`),
  KEY `IDX_TAX_CALC_TAX_CALC_RATE_ID_CSTR_TAX_CLASS_ID_PRD_TAX_CLASS_ID` (`tax_calculation_rate_id`,`customer_tax_class_id`,`product_tax_class_id`),
  CONSTRAINT `FK_TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`customer_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAX_CALCULATION_PRODUCT_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`product_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAX_CALC_TAX_CALC_RATE_ID_TAX_CALC_RATE_TAX_CALC_RATE_ID` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAX_CALC_TAX_CALC_RULE_ID_TAX_CALC_RULE_TAX_CALC_RULE_ID` FOREIGN KEY (`tax_calculation_rule_id`) REFERENCES `tax_calculation_rule` (`tax_calculation_rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation';

INSERT INTO `tax_calculation` (`tax_calculation_id`, `tax_calculation_rate_id`, `tax_calculation_rule_id`, `customer_tax_class_id`, `product_tax_class_id`) VALUES
(1,	1,	1,	3,	2),
(2,	2,	1,	3,	2);

DROP TABLE IF EXISTS `tax_calculation_rate`;
CREATE TABLE `tax_calculation_rate` (
  `tax_calculation_rate_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Id',
  `tax_country_id` varchar(2) NOT NULL COMMENT 'Tax Country Id',
  `tax_region_id` int(11) NOT NULL COMMENT 'Tax Region Id',
  `tax_postcode` varchar(21) DEFAULT NULL COMMENT 'Tax Postcode',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `rate` decimal(12,4) NOT NULL COMMENT 'Rate',
  `zip_is_range` smallint(6) DEFAULT NULL COMMENT 'Zip Is Range',
  `zip_from` int(10) unsigned DEFAULT NULL COMMENT 'Zip From',
  `zip_to` int(10) unsigned DEFAULT NULL COMMENT 'Zip To',
  PRIMARY KEY (`tax_calculation_rate_id`),
  KEY `IDX_TAX_CALC_RATE_TAX_COUNTRY_ID_TAX_REGION_ID_TAX_POSTCODE` (`tax_country_id`,`tax_region_id`,`tax_postcode`),
  KEY `IDX_TAX_CALCULATION_RATE_CODE` (`code`),
  KEY `CA799F1E2CB843495F601E56C84A626D` (`tax_calculation_rate_id`,`tax_country_id`,`tax_region_id`,`zip_is_range`,`tax_postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate';

INSERT INTO `tax_calculation_rate` (`tax_calculation_rate_id`, `tax_country_id`, `tax_region_id`, `tax_postcode`, `code`, `rate`, `zip_is_range`, `zip_from`, `zip_to`) VALUES
(1,	'US',	12,	'*',	'US-CA-*-Rate 1',	8.2500,	NULL,	NULL,	NULL),
(2,	'US',	43,	'*',	'US-NY-*-Rate 1',	8.3750,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `tax_calculation_rate_title`;
CREATE TABLE `tax_calculation_rate_title` (
  `tax_calculation_rate_title_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Title Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Value',
  PRIMARY KEY (`tax_calculation_rate_title_id`),
  KEY `IDX_TAX_CALCULATION_RATE_TITLE_TAX_CALCULATION_RATE_ID_STORE_ID` (`tax_calculation_rate_id`,`store_id`),
  KEY `IDX_TAX_CALCULATION_RATE_TITLE_TAX_CALCULATION_RATE_ID` (`tax_calculation_rate_id`),
  KEY `IDX_TAX_CALCULATION_RATE_TITLE_STORE_ID` (`store_id`),
  CONSTRAINT `FK_37FB965F786AD5897BB3AE90470C42AB` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_TAX_CALCULATION_RATE_TITLE_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate Title';


DROP TABLE IF EXISTS `tax_calculation_rule`;
CREATE TABLE `tax_calculation_rule` (
  `tax_calculation_rule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rule Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `calculate_subtotal` int(11) NOT NULL COMMENT 'Calculate off subtotal option',
  PRIMARY KEY (`tax_calculation_rule_id`),
  KEY `IDX_TAX_CALC_RULE_PRIORITY_POSITION_TAX_CALC_RULE_ID` (`priority`,`position`,`tax_calculation_rule_id`),
  KEY `IDX_TAX_CALCULATION_RULE_CODE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rule';

INSERT INTO `tax_calculation_rule` (`tax_calculation_rule_id`, `code`, `priority`, `position`, `calculate_subtotal`) VALUES
(1,	'Retail Customer-Taxable Goods-Rate 1',	1,	1,	0);

DROP TABLE IF EXISTS `tax_class`;
CREATE TABLE `tax_class` (
  `class_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Class Id',
  `class_name` varchar(255) NOT NULL COMMENT 'Class Name',
  `class_type` varchar(8) NOT NULL DEFAULT 'CUSTOMER' COMMENT 'Class Type',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Class';

INSERT INTO `tax_class` (`class_id`, `class_name`, `class_type`) VALUES
(2,	'Taxable Goods',	'PRODUCT'),
(3,	'Retail Customer',	'CUSTOMER'),
(4,	'Shipping',	'PRODUCT');

DROP TABLE IF EXISTS `tax_order_aggregated_created`;
CREATE TABLE `tax_order_aggregated_created` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum',
  PRIMARY KEY (`id`),
  UNIQUE KEY `FCA5E2C02689EB2641B30580D7AACF12` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `IDX_TAX_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`),
  CONSTRAINT `FK_TAX_ORDER_AGGREGATED_CREATED_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregation';


DROP TABLE IF EXISTS `tax_order_aggregated_updated`;
CREATE TABLE `tax_order_aggregated_updated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum',
  PRIMARY KEY (`id`),
  UNIQUE KEY `DB0AF14011199AA6CD31D5078B90AA8D` (`period`,`store_id`,`code`,`percent`,`order_status`),
  KEY `IDX_TAX_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  CONSTRAINT `FK_TAX_ORDER_AGGREGATED_UPDATED_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregated Updated';


DROP TABLE IF EXISTS `weee_discount`;
CREATE TABLE `weee_discount` (
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `customer_group_id` smallint(5) unsigned NOT NULL COMMENT 'Customer Group Id',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  KEY `IDX_WEEE_DISCOUNT_WEBSITE_ID` (`website_id`),
  KEY `IDX_WEEE_DISCOUNT_ENTITY_ID` (`entity_id`),
  KEY `IDX_WEEE_DISCOUNT_CUSTOMER_GROUP_ID` (`customer_group_id`),
  CONSTRAINT `FK_WEEE_DISCOUNT_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WEEE_DISCOUNT_ENTITY_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WEEE_DISCOUNT_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Weee Discount';


DROP TABLE IF EXISTS `weee_tax`;
CREATE TABLE `weee_tax` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
  `website_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `country` varchar(2) DEFAULT NULL COMMENT 'Country',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `state` varchar(255) NOT NULL DEFAULT '*' COMMENT 'State',
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute Id',
  `entity_type_id` smallint(5) unsigned NOT NULL COMMENT 'Entity Type Id',
  PRIMARY KEY (`value_id`),
  KEY `IDX_WEEE_TAX_WEBSITE_ID` (`website_id`),
  KEY `IDX_WEEE_TAX_ENTITY_ID` (`entity_id`),
  KEY `IDX_WEEE_TAX_COUNTRY` (`country`),
  KEY `IDX_WEEE_TAX_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `FK_WEEE_TAX_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WEEE_TAX_COUNTRY_DIRECTORY_COUNTRY_COUNTRY_ID` FOREIGN KEY (`country`) REFERENCES `directory_country` (`country_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WEEE_TAX_ENTITY_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WEEE_TAX_WEBSITE_ID_CORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Weee Tax';


DROP TABLE IF EXISTS `widget`;
CREATE TABLE `widget` (
  `widget_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Widget Id',
  `widget_code` varchar(255) DEFAULT NULL COMMENT 'Widget code for template directive',
  `widget_type` varchar(255) DEFAULT NULL COMMENT 'Widget Type',
  `parameters` text COMMENT 'Parameters',
  PRIMARY KEY (`widget_id`),
  KEY `IDX_WIDGET_WIDGET_CODE` (`widget_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Preconfigured Widgets';


DROP TABLE IF EXISTS `widget_instance`;
CREATE TABLE `widget_instance` (
  `instance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Instance Id',
  `instance_type` varchar(255) DEFAULT NULL COMMENT 'Instance Type',
  `package_theme` varchar(255) DEFAULT NULL COMMENT 'Package Theme',
  `title` varchar(255) DEFAULT NULL COMMENT 'Widget Title',
  `store_ids` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Store ids',
  `widget_parameters` text COMMENT 'Widget parameters',
  `sort_order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sort order',
  PRIMARY KEY (`instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Instances of Widget for Package Theme';


DROP TABLE IF EXISTS `widget_instance_page`;
CREATE TABLE `widget_instance_page` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Page Id',
  `instance_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Instance Id',
  `page_group` varchar(25) DEFAULT NULL COMMENT 'Block Group Type',
  `layout_handle` varchar(255) DEFAULT NULL COMMENT 'Layout Handle',
  `block_reference` varchar(255) DEFAULT NULL COMMENT 'Block Reference',
  `page_for` varchar(25) DEFAULT NULL COMMENT 'For instance entities',
  `entities` text COMMENT 'Catalog entities (comma separated)',
  `page_template` varchar(255) DEFAULT NULL COMMENT 'Path to widget template',
  PRIMARY KEY (`page_id`),
  KEY `IDX_WIDGET_INSTANCE_PAGE_INSTANCE_ID` (`instance_id`),
  CONSTRAINT `FK_WIDGET_INSTANCE_PAGE_INSTANCE_ID_WIDGET_INSTANCE_INSTANCE_ID` FOREIGN KEY (`instance_id`) REFERENCES `widget_instance` (`instance_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Instance of Widget on Page';


DROP TABLE IF EXISTS `widget_instance_page_layout`;
CREATE TABLE `widget_instance_page_layout` (
  `page_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Page Id',
  `layout_update_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Layout Update Id',
  UNIQUE KEY `UNQ_WIDGET_INSTANCE_PAGE_LAYOUT_LAYOUT_UPDATE_ID_PAGE_ID` (`layout_update_id`,`page_id`),
  KEY `IDX_WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID` (`page_id`),
  KEY `IDX_WIDGET_INSTANCE_PAGE_LAYOUT_LAYOUT_UPDATE_ID` (`layout_update_id`),
  CONSTRAINT `FK_0A5D06DCEC6A6845F50E5FAAC5A1C96D` FOREIGN KEY (`layout_update_id`) REFERENCES `core_layout_update` (`layout_update_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WIDGET_INSTANCE_PAGE_LYT_PAGE_ID_WIDGET_INSTANCE_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `widget_instance_page` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout updates';


DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE `wishlist` (
  `wishlist_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Wishlist ID',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Customer ID',
  `shared` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sharing flag (0 or 1)',
  `sharing_code` varchar(32) DEFAULT NULL COMMENT 'Sharing encrypted code',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Last updated date',
  PRIMARY KEY (`wishlist_id`),
  UNIQUE KEY `UNQ_WISHLIST_CUSTOMER_ID` (`customer_id`),
  KEY `IDX_WISHLIST_SHARED` (`shared`),
  CONSTRAINT `FK_WISHLIST_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist main Table';


DROP TABLE IF EXISTS `wishlist_item`;
CREATE TABLE `wishlist_item` (
  `wishlist_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Wishlist item ID',
  `wishlist_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Wishlist ID',
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store ID',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Add date and time',
  `description` text COMMENT 'Short description of wish list item',
  `qty` decimal(12,4) NOT NULL COMMENT 'Qty',
  PRIMARY KEY (`wishlist_item_id`),
  KEY `IDX_WISHLIST_ITEM_WISHLIST_ID` (`wishlist_id`),
  KEY `IDX_WISHLIST_ITEM_PRODUCT_ID` (`product_id`),
  KEY `IDX_WISHLIST_ITEM_STORE_ID` (`store_id`),
  CONSTRAINT `FK_WISHLIST_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_WISHLIST_ITEM_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_WISHLIST_ITEM_WISHLIST_ID_WISHLIST_WISHLIST_ID` FOREIGN KEY (`wishlist_id`) REFERENCES `wishlist` (`wishlist_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist items';


DROP TABLE IF EXISTS `wishlist_item_option`;
CREATE TABLE `wishlist_item_option` (
  `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
  `wishlist_item_id` int(10) unsigned NOT NULL COMMENT 'Wishlist Item Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value',
  PRIMARY KEY (`option_id`),
  KEY `FK_A014B30B04B72DD0EAB3EECD779728D6` (`wishlist_item_id`),
  CONSTRAINT `FK_A014B30B04B72DD0EAB3EECD779728D6` FOREIGN KEY (`wishlist_item_id`) REFERENCES `wishlist_item` (`wishlist_item_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist Item Option Table';


-- 2017-01-04 05:07:33
