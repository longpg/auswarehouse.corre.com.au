<?php


class Magik_Astrabootstrapsettings_Model_Config_Footer
{

    public function toOptionArray()
    {
        return array(
            array(
	            'value'=>'simple',
	            'label' => Mage::helper('astrabootstrapsettings')->__('simple')),
            array(
	            'value'=>'informative',
	            'label' => Mage::helper('astrabootstrapsettings')->__('informative')),
        );
    }

}
