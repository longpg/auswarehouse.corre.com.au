<?php


class Magik_Astrabootstrapsettings_Model_Config_Menu
{

    public function toOptionArray()
    {
        return array(
            array(
	            'value'=>'classic-menu',
	            'label' => Mage::helper('astrabootstrapsettings')->__('Classic Menu')),
            array(
	            'value'=>'mega-menu',
	            'label' => Mage::helper('astrabootstrapsettings')->__('Mega Menu')),                       

        );
    }

}
