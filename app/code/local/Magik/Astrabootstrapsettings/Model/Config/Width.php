<?php


class Magik_Astrabootstrapsettings_Model_Config_Width
{

    public function toOptionArray()
    {
        return array(
            array(
	            'value' => 'flexible',
	            'label' => Mage::helper('astrabootstrapsettings')->__('flexible')),
            array(
	            'value' => 'fixed',
	            'label' => Mage::helper('astrabootstrapsettings')->__('fixed')),
        );
    }

}
