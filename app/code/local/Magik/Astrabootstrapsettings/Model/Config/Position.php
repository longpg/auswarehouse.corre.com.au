<?php


class Magik_Astrabootstrapsettings_Model_Config_Position
{

    public function toOptionArray()
    {
        return array(
            array(
	            'value'=>'top-left',
	            'label' => Mage::helper('astrabootstrapsettings')->__('Top Left')),
            array(
	            'value'=>'top-right',
	            'label' => Mage::helper('astrabootstrapsettings')->__('Top Right')),                       

        );
    }

}
