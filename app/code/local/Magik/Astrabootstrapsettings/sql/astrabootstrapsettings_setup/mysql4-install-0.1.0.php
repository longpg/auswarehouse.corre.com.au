<?php


$installer = $this;
$installer->startSetup();

$installer->endSetup();

try {
//create pages and blocks programmatically
//home page
$cmsPage = array(
    'title' => 'Astrabootstrap Home Page',
    'identifier' => 'astrabootstrap_home',
    'content' => "<div class=\"main-container col1-layout home-content-container\">{{block type=\"catalog/product_list\" num_products=\"8\" name=\"bestsellerproduct\" as=\"bestsellerproduct\" template=\"catalog/product/bestseller.phtml\" }}</div>
<div class=\"featured-pro container wow bounceInUp animated\">{{block type=\"catalog/product_list\" num_products=\"8\" name=\"featuredproduct\" as=\"featuredproduct\" template=\"catalog/product/featured.phtml\" }}</div>",
    'is_active' => 1,
    'sort_order' => 0,
    'stores' => array(0),
    'root_template' => 'custom_static_page_one'
);
Mage::getModel('cms/page')->setData($cmsPage)->save();
//404 page
$cmsPage = array(
    'title' => 'Astrabootstrap 404 No Route',
    'identifier' => 'astrabootstrap_no_route',
    'content' => '<div class="norout">
<div class="page-not-found wow bounceInRight animated">
<h2>404</h2>
<h3><img src="{{skin url="images/signal.png"}}" alt="" />Oops! The Page you requested was not found!</h3>
<div><a class="btn-home" type="button" href="{{store direct_url="accent_home_one"}}"><span>Back To Home</span></a></div>
</div>
</div>
',
    'is_active' => 1,
    'sort_order' => 0,
    'stores' => array(0),
    'root_template' => 'one_column'
);
Mage::getModel('cms/page')->setData($cmsPage)->save();


//footer links
$staticBlock = array(
    'title' => 'Astrabootstrap Footer links',
    'identifier' => 'astrabootstrap_footer_links',
    'content' => '<div class="footer-bottom">
<div class="container">
<div class="row">
<div class="col-sm-5 col-xs-12 coppyright">&copy; 2015 Magikcommerce. All Rights Reserved.</div>
<div class="col-sm-7 col-xs-12 company-links">
<ul class="links">
<li><a title="Magento Themes" href="http://www.magikcommerce.com/magento-themes-templates">Magento Themes</a></li>
<li><a title="Responsive Themes" href="http://www.magikcommerce.com/magento-themes-templates/responsive-themes">Responsive Themes</a></li>
<li class="last"><a title="Magento Extensions" href="http://www.magikcommerce.com/magento-extensions">Magento Extensions</a></li>
</ul>
</div>
</div>
</div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();
}
catch (Exception $e) {
    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('An error occurred while installing astrabootstrap theme pages and cms blocks.'));
}