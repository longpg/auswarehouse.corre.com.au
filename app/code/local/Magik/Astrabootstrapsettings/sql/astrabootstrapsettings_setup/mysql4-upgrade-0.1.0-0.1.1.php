<?php


$installer = $this;
$installer->startSetup();
$installer->endSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->addAttribute('catalog_product', 'magikfeatured', array(
        'group'             => 'General',
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Featured Product On Home',
        'input'             => 'boolean',
        'class'             => '',
        'source'            => '',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'default'           => '0',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => 'simple,configurable,virtual,bundle,downloadable',
        'is_configurable'   => false
    ));

try {
//create pages and blocks programmatically

//Custom Tab1
$staticBlock = array(
    'title' => 'Custom Tab1',
    'identifier' => 'astrabootstrap_custom_tab1',
    'content' => "<p><strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Custom Tab2
$staticBlock = array(
    'title' => 'Custom Tab2',
    'identifier' => 'astrabootstrap_custom_tab2',
    'content' => "<p><strong>Lorem Ipsum</strong><span>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Empty Category
$staticBlock = array(
    'title' => 'Empty Category',
    'identifier' => 'astrabootstrap_empty_category',
    'content' => "<p>There are no products matching the selection.<br /> This is a static CMS block displayed if category is empty. You can put your own content here.</p>",
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Logo Brand block
 $staticBlock = array(
     'title' => 'Astrabootstrap Logo Brand block',
     'identifier' => 'astrabootstrap_logo_brand_block',
     'content' => '<div class="brand-logo ">
<div class="container">
<div class="slider-items-products">
<div id="brand-logo-slider" class="product-flexslider hidden-buttons">
<div class="slider-items slider-width-col6">
<div class="item"><a href="#"><img src="{{skin url="images/b-logo1.png"}}" alt="Image" /></a></div>
<div class="item"><a href="#"><img src="{{skin url="images/b-logo2.png"}}" alt="Image" /></a></div>
<div class="item"><a href="#"><img src="{{skin url="images/b-logo3.png"}}" alt="Image" /></a></div>
<div class="item"><a href="#"><img src="{{skin url="images/b-logo4.png"}}" alt="Image" /></a></div>
<div class="item"><a href="#"><img src="{{skin url="images/b-logo5.png"}}" alt="Image" /></a></div>
<div class="item"><a href="#"><img src="{{skin url="images/b-logo6.png"}}" alt="Image" /></a></div>
<div class="item"><a href="#"><img src="{{skin url="images/b-logo1.png"}}" alt="Image" /></a></div>
<div class="item"><a href="#"><img src="{{skin url="images/b-logo4.png"}}" alt="Image" /></a></div>
</div>
</div>
</div>
</div>
</div>',
     'is_active' => 1,
     'stores' => array(0)
 );
 Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Store Logo
$staticBlock = array(
    'title' => 'Astrabootstrap Store Logo',
    'identifier' => 'astrabootstrap_logo',
    'content' => '<p><img src="{{skin url="images/logo.png"}}" alt="Astrabootstrap Store" /></p>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();


// astrabootstrap navigation block
$staticBlock = array(
    'title' => 'Custom',
    'identifier' => 'astrabootstrap_navigation_block',
    'content' => '<div class="grid12-5">
<div class="custom_img"><img src="{{skin url="images/custom-img1.jpg"}}" alt="custom img1" /></div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
</div>
<div class="grid12-5">
<div class="custom_img"><img src="{{skin url="images/custom-img2.jpg"}}" alt="custom img2" /></div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
</div>
<div class="grid12-5">
<div class="custom_img"><img src="{{skin url="images/custom-img3.jpg"}}" alt="custom img3" /></div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
</div>
<div class="grid12-5">
<div class="custom_img"><img src="{{skin url="images/custom-img4.jpg"}}" alt="custom img4" /></div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue.</p>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();


//Astrabootstrap Home Banner Block
$staticBlock = array(
    'title' => 'Astrabootstrap Home Offer Banner Block',
    'identifier' => 'astrabootstrap_home_offer_banner_block',
    'content' => '<div class="offer-banner-section bounceInUp animated">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-6 col-xs-12">
<div class="col"><img src="{{skin url="images/offer-banner1.png"}}" alt="offer banner1" /></div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
<div class="col"><img src="{{skin url="images/offer-banner2.png"}}" alt="offer banner2" /></div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
<div class="col"><img src="{{skin url="images/offer-banner3.png"}}" alt="offer banner3" /></div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
<div class="col last"><img src="{{skin url="images/offer-banner4.png"}}" alt="offer banner4" /></div>
</div>
</div>
</div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Home Header Block
$staticBlock = array(
    'title' => 'Astrabootstrap Header Block',
    'identifier' => 'astrabootstrap_header_block',
    'content' => '<div class="service-section">
<div class="container">
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-12 message"><span><strong>Return &amp; Exchange</strong> Lorem ipsum dolor sit amet, consectetur </span></div>
<div class="col-lg-3 col-sm-6 col-xs-12 message"><span><strong>Free Shipping</strong> Lorem ipsum dolor sit amet, consectetur</span></div>
<div class="col-lg-3 col-sm-6 col-xs-12 message"><span><strong>50% Off</strong> Lorem ipsum dolor sit amet, consectetur</span></div>
<div class="col-lg-3 col-sm-6 col-xs-12 message last"><span><strong>Need help?</strong> Lorem ipsum dolor sit amet, consectetur</span></div>
</div>
</div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();



//Astrabootstrap Footer About Us Block
$staticBlock = array(
    'title' => 'Astrabootstrap Footer About Us',
    'identifier' => 'astrabootstrap_footer_about_us',
    'content' => '<div class="col-md-3 col-sm-4 footer-column-1">
<div class="footer-logo"><a title="Logo" href="#"><img src="{{skin url="images/logo.png"}}" alt="logo" /></a></div>
<div class="contacts-info"><address><em class="add-icon">&nbsp;</em>123 Main Street, Anytown, <br /> &nbsp;CA 12345 USA</address>
<div class="phone-footer"><em class="phone-icon">&nbsp;</em> +1 800 123 1234</div>
<div class="email-footer"><em class="email-icon">&nbsp;</em> <a href="#">support@magikcommerce.com</a></div>
</div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();


//Astrabootstrap Listing Page Block
$staticBlock = array(
    'title' => 'Astrabootstrap Listing Page Block',
    'identifier' => 'astrabootstrap_listing_page_block',
    'content' => '<div class="block block-banner"><a href="#"><img src="{{skin url="images/block-banner.png"}}" alt="" /></a></div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();


//Astrabootstrap RHS Contact Form Block
$staticBlock = array(
    'title' => 'Astrabootstrap RHS Contact Form Block',
    'identifier' => 'astrabootstrap_rhs_contact_form_block',
    'content' => '<div class="slider-phone active">
<h2>TALK TO US</h2>
<h3>AVAILABLE 24/7</h3>
<p class="textcenter">Want to speak to someone? We \'re here 24/7 to answer any questions. Just call!<br /> <br /> <span class="phone-number"> +1 800 123 1234</span></p>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Navigation Featured Product Block
$staticBlock = array(
    'title' => 'Astrabootstrap Navigation Featured Product Block',
    'identifier' => 'astrabootstrap_navigation_featured_product_block',
    'content' => '<p>{{block type="catalog/product_new"  products_count="1" name="home.catalog.product.new" as="newproduct" template="catalog/product/new.phtml" }}</p>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();


//Astrabootstrap Home Slider Banner Block
$staticBlock = array(
    'title' => 'Astrabootstrap Home Slider Banner Block',
    'identifier' => 'astrabootstrap_home_slider_banner_block',
    'content' => '<div id="magik-slideshow" class="magik-slideshow">
<div id="slider-elastic" class="slider slider-elastic elastic ei-slider">
<ul class="ei-slider-large">
<li class="first slide-1 slide align-"><img class="attachment-full" src="{{skin url="images/slide-bg-fix.jpg"}}" alt="001" />
<div class="ei-title">
<h2>Hello there!</h2>
<h3>Welcome to Astra</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus.</p>
<a class="shop-btn" href="#">shop now!</a></div>
</li>
<li class="slide-4 slide align-"><img class="attachment-full" src="{{skin url="images/slide-bg-3-fix.jpg"}}" alt="003" />
<div class="ei-title">
<h2>Multipurpose Theme</h2>
<h3>... be creative</h3>
</div>
</li>
<li class="slide-2 slide align-"><img class="attachment-full" src="{{skin url="images/slide-bg-1-fix.jpg"}}" alt="002" />
<div class="ei-title">
<h2>Powerfull &amp; Responsive</h2>
<h4>Optimized for minor resolutions &amp; mobile devices.</h4>
<a class="shop-btn" href="#">Purchase</a></div>
</li>
<li class="slide-3 slide align-"><img class="attachment-full" src="{{skin url="images/slide-bg-2-fix.jpg"}}" alt="001" />
<div class="ei-title">
<h2 style="color: #fff;">LOVE IT, ENJOY IT</h2>
<h5 style="color: #999;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus.</h5>
<a class="shop-btn center" href="#">Buy theme!</a></div>
</li>
<li class="last slide-5 slide align-"><img class="attachment-full" src="{{skin url="images/slide-bg-4-fix.jpg"}}" alt="001" />
<div class="ei-title">
<h1>Customizable Theme</h1>
<h6>You can change colors of every element.</h6>
</div>
</li>
</ul>
<!-- ei-slider-large -->
<ul class="ei-slider-thumbs">
<li class="ei-slider-element">Current</li>
<li><a href="#">Hello there! </a> <img src="{{skin url="images/slide-bg-fix.jpg"}}" alt=" slide img " /></li>
<li><a href="#">Multipurpose Theme </a> <img src="{{skin url="images/slide-bg-3-fix.jpg"}}" alt=" slide img " /></li>
<li><a href="#">Powerfull &amp; Responsive </a> <img src="{{skin url="images/slide-bg-1-fix.jpg"}}" alt=" slide img " /></li>
<li><a href="#">LOVE IT, ENJOY IT </a> <img src="{{skin url="images/slide-bg-2-fix.jpg"}}" alt=" slide img " /></li>
<li><a href="#">Customizable Theme </a> <img src="{{skin url="images/slide-bg-4-fix.jpg"}}" alt=" slide img " /></li>
</ul>
<!-- ei-slider-thumbs -->
<div class="shadow">&nbsp;</div>
</div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Home Slider Full Width Banner Block
$staticBlock = array(
    'title' => 'Astrabootstrap Home Slider Full Width Banner Block',
    'identifier' => 'astrabootstrap_home_slider_full_width_banner_block',
    'content' => '<div id="magik-slideshow" class="magik-slideshow">
<div id="slider-elastic" class="slider slider-elastic elastic ei-slider">
<ul class="ei-slider-large">
<li class="first slide-1 slide align-"><img class="attachment-full" src="{{skin url="images/slide-bg-full.jpg"}}" alt="001" />
<div class="ei-title">
<h2>Hello there!</h2>
<h3>Welcome to Astra</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus.</p>
<a class="shop-btn" href="#">shop now!</a></div>
</li>
<li class="slide-4 slide align-"><img class="attachment-full" src="{{skin url="images/slide-bg-3-full.jpg"}}" alt="003" />
<div class="ei-title">
<h2>Multipurpose Theme</h2>
<h3>... be creative</h3>
</div>
</li>
<li class="slide-2 slide align-"><img class="attachment-full" src="{{skin url="images/slide-bg-1-full.jpg"}}" alt="002" />
<div class="ei-title">
<h2>Powerfull &amp; Responsive</h2>
<h4>Optimized for minor resolutions &amp; mobile devices.</h4>
<a class="shop-btn" href="#">Purchase</a></div>
</li>
<li class="slide-3 slide align-"><img class="attachment-full" src="{{skin url="images/slide-bg-2-full.jpg"}}" alt="001" />
<div class="ei-title">
<h2 style="color: #fff;">LOVE IT, ENJOY IT</h2>
<h5 style="color: #999;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus.</h5>
<a class="shop-btn center" href="#">Buy theme!</a></div>
</li>
<li class="last slide-5 slide align-"><img class="attachment-full" src="{{skin url="images/slide-bg-4-full.jpg"}}" alt="001" />
<div class="ei-title">
<h1>Customizable Theme</h1>
<h6>You can change colors of every element.</h6>
</div>
</li>
</ul>
<ul class="ei-slider-thumbs">
<li class="ei-slider-element">Current</li>
<li><a href="#">Hello there! </a> <img src="{{skin url="images/slide-bg-full.jpg"}}" alt=" slide img " /></li>
<li><a href="#">Multipurpose Theme </a> <img src="{{skin url="images/slide-bg-3-full.jpg"}}" alt=" slide img " /></li>
<li><a href="#">Powerfull &amp; Responsive </a> <img src="{{skin url="images/slide-bg-1-full.jpg"}}" alt=" slide img " /></li>
<li><a href="#">LOVE IT, ENJOY IT </a> <img src="{{skin url="images/slide-bg-2-full.jpg"}}" alt=" slide img " /></li>
<li><a href="#">Customizable Theme </a> <img src="{{skin url="images/slide-bg-4-full.jpg"}}" alt=" slide img " /></li>
</ul>
<div class="shadow">&nbsp;</div>
</div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Footer Information Links Block
$staticBlock = array(
    'title' => 'Astrabootstrap Footer Information Links Block',
    'identifier' => 'astrabootstrap_footer_information_links_block',
    'content' => '<div class="col-md-2 col-sm-4">
<h4>Shopping Guide</h4>
<ul class="links">
<li class="first"><a title="How to buy" href="#">How to buy</a></li>
<li><a title="FAQs" href="#">FAQs</a></li>
<li><a title="Payment" href="#">Payment</a></li>
<li><a title="Shipment" href="#">Shipment</a></li>
<li><a title="Where is my order?" href="#">Where is my order?</a></li>
<li class="last"><a title="Return policy" href="#">Return policy</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-4">
<h4>Style Advisor</h4>
<ul class="links">
<li class="first"><a title="Your Account" href="{{store_url=customer/account/}}">Your Account</a></li>
<li><a title="Information" href="#">Information</a></li>
<li><a title="Addresses" href="#">Addresses</a></li>
<li><a title="Addresses" href="#">Discount</a></li>
<li><a title="Orders History" href="#">Orders History</a></li>
<li class="last"><a title=" Additional Information" href="#"> Additional Information</a></li>
</ul>
</div>
<div class="col-md-2 col-sm-4">
<h4>Information</h4>
<ul class="links">
<li class="first"><a title="Site Map" href="{{store_url=catalog/seo_sitemap/category/}}">Site Map</a></li>
<li><a title="Search Terms" href="{{store_url=catalogsearch/term/popular/}}">Search Terms</a></li>
<li><a title="Advanced Search" href="{{store_url=catalogsearch/advanced/}}">Advanced Search</a></li>
<li><a title="History" href="# ">History</a></li>
<li><a title="Suppliers" href="#">Suppliers</a></li>
<li class=" last"><a class="link-rss" title="Our stores" href="#">Our stores</a></li>
</ul>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Home Toplinks Block
$staticBlock = array(
    'title' => 'Astrabootstrap Home Toplinks Block',
    'identifier' => 'astrabootstrap-home-toplinks-block',
    'content' => '<div class="dropdown block-company-wrapper"><a class="block-company dropdown-toggle hidden-xs" title="Company" href="#" data-toggle="dropdown" data-target="#"> Company </a>
<ul class="dropdown-menu">
<li><a tabindex="-1" href="{{store_url=about-us}}"> About Us </a></li>
<li><a tabindex="-1" href="#"> Customer Service </a></li>
<li><a tabindex="-1" href="#"> Privacy Policy </a></li>
<li><a tabindex="-1" href="{{store_url=catalog/seo_sitemap/category/}}">Site Map </a></li>
<li><a tabindex="-1" href="{{store_url=catalogsearch/term/popular/}}">Search Terms </a></li>
<li><a tabindex="-1" href="{{store_url=catalogsearch/advanced/}}">Advanced Search </a></li>
</ul>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Home Latest Blog Block
$staticBlock = array(
    'title' => 'Astrabootstrap Home Latest Blog Block',
    'identifier' => 'astrabootstrap_home_latest_blog_block',
    'content' => '<div>{{block type="blogmate/index" name="blog_home" template="blogmate/right/home_right.phtml"}}</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Blog Banner Text Block
$staticBlock = array(
    'title' => 'Astrabootstrap Blog Banner Text Block',
    'identifier' => 'astrabootstrap_blog_banner_text_block',
    'content' => '<div class="text-widget widget widget__sidebar">
<h3 class="widget-title">Text Widget</h3>
<div class="widget-content">Mauris at blandit erat. Nam vel tortor non quam scelerisque cursus. Praesent nunc vitae magna pellentesque auctor. Quisque id lectus.<br /> <br /> Massa, eget eleifend tellus. Proin nec ante leo ssim nunc sit amet velit malesuada pharetra. Nulla neque sapien, sollicitudin non ornare quis, malesuada.</div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Blog Banner Ad Block
$staticBlock = array(
    'title' => 'Astrabootstrap Blog Banner Ad Block',
    'identifier' => 'astrabootstrap_blog_banner_ad_block',
    'content' => '<div class="ad-spots widget widget__sidebar">
<h3 class="widget-title">Ad Spots</h3>
<div class="widget-content"><a title="" href="#" target="_self"><img src="{{skin url="images/offer-banner1.jpg"}}" alt="offer banner" /></a></div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Header Lettering Block
$staticBlock = array(
    'title' => 'Astrabootstrap Header Lettering Block',
    'identifier' => 'astrabootstrap_header_lettering_block',
    'content' => '<div id="os-phrases" class="os-phrases">
<h2>Welcome to Astrabootstrap</h2>
<h2>15% Off on all Collection</h2>
<h2>Free Shipping</h2>
<h2>on order</h2>
<h2>over $99</h2>
<h2>Multipurpose design</h2>
<h2>Clean and simple design</h2>
<h2>Easy to Customizable</h2>
<h2>Responsive magento theme</h2>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Banner Block
$staticBlock = array(
    'title' => 'Astrabootstrap Banner Block',
    'identifier' => 'astrabootstrap_banner_block',
    'content' => '<div class="promo-banner-section container wow bounceInUp animated">
<div class="row">
<div class="col-sm-6 col-xs-12"><img src="{{skin url="images/promo-banner1.png"}}" alt="promo-banner1" /></div>
<div class="col-sm-6 col-xs-12"><img src="{{skin url="images/promo-banner2.png"}}" alt="promo-banner2" /></div>
</div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Banner Bottom Block
$staticBlock = array(
    'title' => 'Astrabootstrap Banner Bottom Block',
    'identifier' => 'astrabootstrap_banner_bottom_block',
    'content' => '<div class="promo-banner-section container wow bounceInDown animated">
<div class="row">
<div class="col-sm-6 col-xs-12"><img src="{{skin url="images/promo-banner3.png"}}" alt="promo-banner3" /></div>
<div class="col-sm-6 col-xs-12"><img src="{{skin url="images/promo-banner4.png"}}" alt="promo-banner4" /></div>
</div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Features Box Block
$staticBlock = array(
    'title' => 'Astrabootstrap Features Box Block',
    'identifier' => 'astrabootstrap_features_box_block',
    'content' => '<div class="our-features-box">
<div class="container">
<div class="row">
<div class="col-md-4 col-xs-12 col-sm-4 wow bounceInLeft animated">
<div class="feature-box">
<div class="icon-reponsive">&nbsp;</div>
<div class="content">Responsive Theme <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus mi nec lectus tempor. </span></div>
</div>
</div>
<div class="col-md-4 col-xs-12 col-sm-4 wow bounceInUp animated">
<div class="feature-box">
<div class="icon-admin">&nbsp;</div>
<div class="content">Powerful Admin Panel <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus mi nec lectus tempor. </span></div>
</div>
</div>
<div class="col-md-4 col-xs-12 col-sm-4 wow bounceInRight animated">
<div class="feature-box">
<div class="icon-support">&nbsp;</div>
<div class="content">Premium Support <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus mi nec lectus tempor. </span></div>
</div>
</div>
</div>
</div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Footer Payment Method Block
$staticBlock = array(
    'title' => 'Astrabootstrap Footer Payment Method Block',
    'identifier' => 'astrabootstrap_footer_payment_method_block',
    'content' => '<div class="payment-accept">
<h4>We Accept</h4>
<div><img src="{{skin url="images/payment-1.png"}}" alt="payment1" /> <img src="{{skin url="images/payment-2.png"}}" alt="payment2" /> <img src="{{skin url="images/payment-3.png"}}" alt="payment3" /> <img src="{{skin url="images/payment-4.png"}}" alt="payment4" /></div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();


//Astrabootstrap Home List Block
$staticBlock = array(
    'title' => 'Astrabootstrap Home List Block',
    'identifier' => 'astrabootstrap_home_list_block',
    'content' => '<div>{{block type="catalog/product_list" name="homelist" as="homelist" template="catalog/product/home-list.phtml"}}</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap View Page Block
$staticBlock = array(
    'title' => 'Astrabootstrap View Page Block',
    'identifier' => 'astrabootstrap_view_page_block',
    'content' => '<div class="product-additional col-sm-3 wow bounceInLeft animated">
<div class="block-product-additional"><img src="{{skin url="images/offer-banner3.png"}}" alt="custom block" /> <img src="{{skin url="images/offer-banner5.png"}}" alt="custom block" /></div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Recommended Product Block
$staticBlock = array(
    'title' => 'Astrabootstrap Recommended Product Block',
    'identifier' => 'astrabootstrap_recommended_product_block',
    'content' => '<div>{{block type="catalog/product_list" name="homerecommended" as="homerecommended" num_products="6" template="catalog/product/recommended.phtml" }}</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

//Astrabootstrap Home Tab Dropdown Block
$staticBlock = array(
    'title' => 'Astrabootstrap Fixed Home Tab Dropdown Block',
    'identifier' => 'astrabootstrap_fixed_home_tab_dropdown_block',
    'content' => '<ul class="level1" style="display: none;">
<li class="level1 first parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapflexgray"><span>Full Width Layout</span></a></li>
<li class="level1 parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapfixgray"><span>Boxed Layout</span></a></li>
<li class="level1 parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapfixblue"><span>Blue Color</span></a></li>
<li class="level1 parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapfixred"><span>Red Color</span></a></li>
<li class="level1 parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapfixgreen"><span>Green Color</span></a></li>
<li class="level1 parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapfixlavender"><span>Lavender Color</span></a></li>
</ul>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

$staticBlock = array(
    'title' => 'Astrabootstrap Flex Home Tab Dropdown Block',
    'identifier' => 'astrabootstrap_flex_home_tab_dropdown_block',
    'content' => '<ul class="level1" style="display: none;">
<li class="level1 first parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapflexgray"><span>Full Width Layout</span></a></li>
<li class="level1 parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapfixgray"><span>Boxed Layout</span></a></li>
<li class="level1 parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapflexblue"><span>Blue Color</span></a></li>
<li class="level1 parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapflexred"><span>Red Color</span></a></li>
<li class="level1 parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapflexgreen"><span>Green Color</span></a></li>
<li class="level1 parent"><a href="http://demo.magikthemes.com/index.php/astrabootstrapflexlavender"><span>Lavender Color</span></a></li>
</ul>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();


$staticBlock = array(
    'title' => 'Astrabootstrap Contact Us Block',
    'identifier' => 'astrabootstrap_contact_us_block',
    'content' => '<div class="block block-company">
<div class="block-title">Company</div>
<div class="block-content"><ol id="recently-viewed-items">
<li class="item odd"><a href="{{store_url=about-us}}">About Us</a></li>
<li class="item even"><a href="{{store_url=catalog/seo_sitemap/category/}}">Sitemap</a></li>
<li class="item  odd"><a href="#">Terms of Service</a></li>
<li class="item last"><a href="{{store_url=catalogsearch/term/popular/}}">Search Terms</a></li>
<li class="item last"><a href="{{store_url=contacts/}}"><strong>Contact Us</strong></a></li>
</ol></div>
</div>',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

}
catch (Exception $e) {
    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('An error occurred while installing Astrabootstrap theme pages and cms blocks.'));
}