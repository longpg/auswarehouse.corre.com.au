<?php


$installer = $this;
$installer->startSetup();

$staticBlock = array(
    'title' => 'allexpress popup',
    'identifier' => 'allexpress_popup',
    'content' => '
    <div class="modal-container" id="allexpress" style="display: none;">
<div class="close-modal"></div>
<div class="modal-screen"></div>
<div class="modal-content">
<div id="all-express-box">
<h2 class="headline">ZIP CODE</h2>
<p>Tell us your zip code so we can show you accurate inventory in your area! We currently cannot ship to addresses in Alaska, Hawaii or Puerto Rico - sorry!</p>
<p>Please enter a valid 5 digit zip code</p>
<div class="errors">Enter zip code</div>
<form id="form-all-express" method="post"><input class="input-text validate-zip-international required-entry" id="postcode-express" name="postcode-express" type="text" value="" /> <button type="button" onclick="allexpressForm.submit();">UPDATE</button></form>
</div>
</div>
</div>
    ',
    'is_active' => 1,
    'stores' => array(0)
);
Mage::getModel('cms/block')->setData($staticBlock)->save();

$installer->endSetup();