<?php
$installer = $this;
$installer->startSetup();
$installer->addAttribute("catalog_product", "postcode",  array(
    "type"             => "int",
    "frontend"         => "",
    "label"            => "PostCode",
    "input"            => "text",
    "class"            => "",
    "source"           => "",
    "global"           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    "visible"          => true,
    "required"         => false,
    "user_defined"     => true,
    "default"          => "",
    "searchable"       => false,
    "filterable"       => false,
    "comparable"       => false,
    "group"            => "General",
    "visible_on_front" => true,
    "unique"           => false,
    "note"             => "",
    "sort_order"       => 1000,
));
$installer->endSetup();